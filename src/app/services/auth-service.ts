import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { JwtRequest } from '../models/loginModel';
import { Observable, from, Subject } from 'rxjs';
import { JwtResponse } from '../models/jwtResponse';
import { SignupModel } from '../models/SignupModel';
import * as moment from "moment";
import {map} from 'rxjs/operators';
import { UserModel } from '../models/dataModels/userModel';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private isLoggedIn: boolean;
  isLoggedInSubject = new Subject<boolean>();

  private roles: string[] = [];
  rolesSubject = new Subject<string[]>();

  private loginUrl: string = 'https://loc-helper.herokuapp.com/login';
  private signUpUrl: string = 'https://loc-helper.herokuapp.com/signup';
  private localLoginUrl: string = 'http://localhost:8080/login';
  private localSignUpUrl: string = 'http://localhost:8080/signup';
  private localRecoverPasswordUrl: string = 'http://localhost:8080/passwordForgotten';
  private recoverPasswordUrl: string = 'https://loc-helper.herokuapp.com/passwordForgotten';
  private localRecoverPasswordPhaseTwoUrl: string = 'http://localhost:8080/recoverPassword';
  private recoverPasswordPhaseTwoUrl: string = 'https://loc-helper.herokuapp.com/recoverPassword';
  private localDeleteAccountUrl: string = 'http://localhost:8080/deleteMyAccount';
  private deleteAccountUrl: string = 'https://loc-helper.herokuapp.com/deleteMyAccount';
  private localChangePasswordUrl: string = 'http://localhost:8080/changePassword';
  private changePasswordUrl: string = 'https://loc-helper.herokuapp.com/changePassword';
  constructor( private http: HttpClient) { }

  attemptAuth(credentials: JwtRequest): Observable<JwtResponse>{
    return this.http.post<JwtResponse>(this.loginUrl, credentials, httpOptions);
  }

  attemptLocalAuth(credentials: JwtRequest): Observable<JwtResponse>{
    return this.http.post<JwtResponse>(this.localLoginUrl, credentials, httpOptions)
  }

  attemptSignUp( datas: SignupModel): Observable<JwtResponse>{
    return this.http.post<JwtResponse>(this.signUpUrl, datas, httpOptions);
  }

  attemptLocalSignup(datas: SignupModel): Observable<JwtResponse>{
    return this.http.post<JwtResponse>(this.localSignUpUrl, datas, httpOptions);
  }

  emitIsLoggedIn(){
    this.isLoggedInSubject.next(this.isLoggedIn);
  }

  emitRoles(){
    this.rolesSubject.next(this.roles);
  }
  attemptLocalPasswordRecoveryProcedure(datas: UserModel): Observable<string>{
    return this.http.post<string>(this.localRecoverPasswordUrl, datas, httpOptions);
  }
  attemptPasswordRecoveryProcedure(datas: UserModel): Observable<string>{
    return this.http.post<string>(this.recoverPasswordUrl, datas, httpOptions);
  }
  attemptLocalPasswordRecoveryProcedurePhaseTwo(userModel: UserModel): Observable<JwtResponse>{
    return this.http.post<JwtResponse>(this.localRecoverPasswordPhaseTwoUrl, userModel, httpOptions);
  }
  attemptPasswordRecoveryProcedurePhaseTwo(userModel: UserModel): Observable<JwtResponse>{
    return this.http.post<JwtResponse>(this.recoverPasswordPhaseTwoUrl, userModel, httpOptions);
  }
  attemptLocalDeleteAccount(): Observable<string>{
    return this.http.get<string>(this.localDeleteAccountUrl,  httpOptions);
  }
  attemptDeleteAccount(): Observable<string>{
    return this.http.get<string>(this.deleteAccountUrl, httpOptions);
  }
  attemptLocalChangePassword(userModel: UserModel):Observable<JwtResponse>{
    return this.http.post<JwtResponse>(this.localChangePasswordUrl, userModel, httpOptions);
  }
  attemptChangePassword(userModel: UserModel):Observable<JwtResponse>{
    return this.http.post<JwtResponse>(this.changePasswordUrl, userModel, httpOptions);
  }
  
  setRoles(roleList: string[]){
    this.roles = []
    for(let role of roleList){
      this.roles.push(role);
    }
    this.emitRoles();
  }

  removeRoles(){
    this.roles = [];
    this.emitRoles();
  }

  login(){
    this.isLoggedIn = true;
    this.emitIsLoggedIn();
  }

  logout(){
    this.isLoggedIn = false;
    this.emitIsLoggedIn();
  }

  getAuthStatus(): boolean{
    return this.isLoggedIn;
  }




  


 





}
