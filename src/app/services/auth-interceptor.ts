import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TokenStorageService } from './token-storage.service';
import * as moment from 'moment';
import { AuthService } from './auth-service';
@Injectable()
export class AuthInterceptor implements HttpInterceptor{
    constructor(private token: TokenStorageService,private authService: AuthService){}

    intercept(req: HttpRequest<any>,
        next: HttpHandler): Observable<HttpEvent<any>>{
            //const token = localStorage.getItem('AuthToken');

            if(this.token.getToken()){
                if(this.token.getExpiration()){
                    if(Number(moment(new Date()).format("X"))  > Number(this.token.getExpiration()) ){
                        console.log("La durée de validité de votre jwt a expiré, vous avez été déconnecté.")
                        this.authService.logout();
                        this.token.empty();
                        return next.handle(req);
                    }
                }
                
                const cloned = req.clone({
                    headers: req.headers.set("Authorization", "Bearer " + this.token.getToken())
                });

                return next.handle(cloned);
            }
            else{
                return next.handle(req);
            }
        }
}