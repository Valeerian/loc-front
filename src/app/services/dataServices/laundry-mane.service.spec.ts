import { TestBed } from '@angular/core/testing';

import { LaundryManeService } from './laundry-mane.service';

describe('LaundryManeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LaundryManeService = TestBed.get(LaundryManeService);
    expect(service).toBeTruthy();
  });
});
