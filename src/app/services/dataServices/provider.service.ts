import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { GenericDataProviderService } from './generic-data-provider.service';
import { QueryModel } from 'src/app/models/queryModel';
import { Observable } from 'rxjs';
import { ProviderListModel } from 'src/app/models/dataListModels/providerListModel';
import { ProviderModel } from 'src/app/models/dataModels/providerModel';

const getProviderUrl:string = "https://loc-helper.herokuapp.com/providers/get";
const localGetProviderUrl: string = "http://localhost:8080/providers/get"
const localAddProviderUrl: string= "http://localhost:8080/providers/add";
const addProviderUrl: string = "https://loc-helper.herokuapp.com/providers/add";
const localEditProviderUrl: string = "http://localhost:8080/providers/edit";
const editProviderUrl: string = "https://loc-helper.herokuapp.com/providers/edit";
const localDeleteProviderUrl: string = "http://localhost:8080/providers/delete";
const deleteProviderUrl: string = "https://loc-helper.herokuapp.com/providers/delete";
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class ProviderService extends GenericDataProviderService {

  constructor(private http: HttpClient) {
    super(new ProviderListModel());
   }

  updateList(providerList: ProviderListModel){
    super.updateList(providerList);
  }

  localGetDatas(queryModel? : QueryModel):Observable<ProviderListModel>{
    return this.http.post<ProviderListModel>(localGetProviderUrl, queryModel, httpOptions);
  }

  getDatas(queryModel? : QueryModel):Observable<ProviderListModel>{
    return this.http.post<ProviderListModel>(getProviderUrl, queryModel, httpOptions);
  }

  localAddData(provider: ProviderModel):Observable<string>{
    return this.http.post<string>(localAddProviderUrl, provider, httpOptions);
  }

  addData(provider: ProviderModel):Observable<string>{
    return this.http.post<string>(addProviderUrl, provider, httpOptions);
  }

  localEditData(provider: ProviderModel):Observable<string>{
    return this.http.post<string>(localEditProviderUrl, provider, httpOptions);
  }

  editData(provider: ProviderModel):Observable<string>{
    return this.http.post<string>(editProviderUrl, provider, httpOptions);
  }

  localDeleteData(provider: ProviderModel):Observable<string>{
    return this.http.post<string>(localDeleteProviderUrl, provider, httpOptions);
  }

  deleteData(provider: ProviderModel):Observable<string>{
    return this.http.post<string>(deleteProviderUrl, provider, httpOptions);
  }


  
}
