import { TestBed } from '@angular/core/testing';

import { OutfitNoteService } from './outfit-note.service';

describe('OutfitNoteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OutfitNoteService = TestBed.get(OutfitNoteService);
    expect(service).toBeTruthy();
  });
});
