import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { UserModel } from 'src/app/models/dataModels/userModel';
import { Subject, Observable } from 'rxjs';
import { SizeListModel } from 'src/app/models/dataListModels/sizeListModel';
import { QueryModel } from 'src/app/models/queryModel';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class ProfilService {
  private currentUser: UserModel;
  userSubject = new Subject<UserModel>();

  private localProfilUrl: string = 'http://localhost:8080/profil';
  private profilUrl: string = 'https://loc-helper.herokuapp.com/profil';
  private localEditProfileUrl: string =   'http://localhost:8080/editProfil';
  private editProfileUrl: string = 'https://loc-helper.herokuapp.com/editProfil';
  private getSizesUrl:string = 'https://loc-helper.herokuapp.com/size/get';
  private localGetSizesUrl:string = 'http://localhost:8080/size/get';

  constructor(private http: HttpClient) { }

  localGetProfile():Observable<UserModel>{
    return this.http.get<UserModel>(this.localProfilUrl, httpOptions);
  }

  getProfile():Observable<UserModel>{
    return this.http.get<UserModel>(this.profilUrl, httpOptions);
  }

  localEditProfile(userModel: UserModel):Observable<UserModel>{
    return this.http.post<UserModel>(this.localEditProfileUrl, userModel, httpOptions);
  }

  editProfile(userModel: UserModel):Observable<UserModel>{
    return this.http.post<UserModel>(this.editProfileUrl, userModel, httpOptions);
  }

  localGetSizes(queryModel?: QueryModel):Observable<SizeListModel>{
    if(queryModel){
      return this.http.post<SizeListModel>(this.localGetSizesUrl, queryModel, httpOptions);
    }
    else{
      return this.http.post<SizeListModel>(this.localGetSizesUrl, null, httpOptions)
    }
  }

  getSizes(queryModel?: QueryModel):Observable<SizeListModel>{
    if(queryModel){
      return this.http.post<SizeListModel>(this.getSizesUrl, queryModel, httpOptions);
    }
    else{
      return this.http.post<SizeListModel>(this.getSizesUrl, null, httpOptions)
    }
  }


  emitUserModel(){
    this.userSubject.next(this.currentUser);
  }

  updateUserModel(userModel: UserModel){
    this.currentUser = userModel;
    this.emitUserModel();
  }

  
}
