import { TestBed } from '@angular/core/testing';

import { SpecialOutfitService } from './special-outfit.service';

describe('SpecialOutfitService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SpecialOutfitService = TestBed.get(SpecialOutfitService);
    expect(service).toBeTruthy();
  });
});
