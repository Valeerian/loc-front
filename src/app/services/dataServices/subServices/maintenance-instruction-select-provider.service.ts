import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { GenericSelectProviderService } from './generic-select-provider-service.service';
import { MaintenanceInstructionListModel } from 'src/app/models/dataListModels/maintenanceInstructionListModel';
import { MaintenanceInstructionModel } from 'src/app/models/dataModels/maintenanceInstruction';
import { Observable } from 'rxjs';

const getMaintenanceInstructionUrl:string = "https://loc-helper.herokuapp.com/maintenanceInstruction/get";
const localGetMaintenanceInstructionUrl: string = "http://localhost:8080/maintenanceInstruction/get"
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class MaintenanceInstructionSelectProviderService extends GenericSelectProviderService{

  constructor(private http: HttpClient) { 
    super(new MaintenanceInstructionListModel());
  }

  updateList(maintenanceInstructionListModel: MaintenanceInstructionListModel):void{
    super.updateList(maintenanceInstructionListModel);
  }

  getDatas():Observable<MaintenanceInstructionListModel>{
    return this.http.post<MaintenanceInstructionListModel>(getMaintenanceInstructionUrl, this.queryModel, httpOptions);
  }

  localGetDatas():Observable<MaintenanceInstructionListModel>{
    return this.http.post<MaintenanceInstructionListModel>(localGetMaintenanceInstructionUrl, this.queryModel, httpOptions);
  }
}
