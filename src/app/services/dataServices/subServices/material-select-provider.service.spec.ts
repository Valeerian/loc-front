import { TestBed } from '@angular/core/testing';

import { MaterialSelectProviderService } from './material-select-provider.service';

describe('MaterialSelectProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MaterialSelectProviderService = TestBed.get(MaterialSelectProviderService);
    expect(service).toBeTruthy();
  });
});
