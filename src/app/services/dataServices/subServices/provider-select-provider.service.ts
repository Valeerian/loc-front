import { Injectable } from '@angular/core';
import { GenericSelectProviderService } from './generic-select-provider-service.service';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { ProviderListModel } from 'src/app/models/dataListModels/providerListModel';
import { QueryModel } from 'src/app/models/queryModel';
import { Observable } from 'rxjs';

const getProviderUrl:string = "https://loc-helper.herokuapp.com/providers/get";
const localGetProviderUrl: string = "http://localhost:8080/providers/get"
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class ProviderSelectProviderService extends GenericSelectProviderService{

  constructor(private http: HttpClient) { 
    super(new ProviderListModel());
  }

  updateList(providerListModel: ProviderListModel):void{
    super.updateList(providerListModel);
  }

  getDatas():Observable<ProviderListModel>{
    return this.http.post<ProviderListModel>(getProviderUrl, this.queryModel, httpOptions);
  }

  localGetDatas():Observable<ProviderListModel>{
    return this.http.post<ProviderListModel>(localGetProviderUrl, this.queryModel, httpOptions);
  }
}
