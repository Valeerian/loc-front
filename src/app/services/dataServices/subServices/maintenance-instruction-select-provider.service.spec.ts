import { TestBed } from '@angular/core/testing';

import { MaintenanceInstructionSelectProviderService } from './maintenance-instruction-select-provider.service';

describe('MaintenanceInstructionSelectProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MaintenanceInstructionSelectProviderService = TestBed.get(MaintenanceInstructionSelectProviderService);
    expect(service).toBeTruthy();
  });
});
