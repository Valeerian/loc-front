import { Injectable } from '@angular/core';
import { GenericListModel } from 'src/app/models/dataListModels/genericListModel';
import { Subject, Observable } from 'rxjs';
import { QueryModel } from 'src/app/models/queryModel';

@Injectable({
  providedIn: 'root'
})
/**
 * Classe +- abstraite dont le but va être de limiter le nombre de fonctionnalités que chaque classe xSelectProviderService devrai implémenter
 */
export abstract class GenericSelectProviderService {

  protected genericList: GenericListModel;
  genericListSubject = new Subject<GenericListModel>();
  protected queryModel: QueryModel;
  queryModelListSubject = new Subject<QueryModel>();

  constructor(genericList: GenericListModel) {
    this.genericList =genericList;
    this.queryModel = new QueryModel();
    this.queryModel.elementsPerPage = 1000;
    this.queryModel.pageNumber = 0;
   }
   

   /**
    * Notifie les abonnés du nouveau contenu de la liste
    */
   emitList(){
     this.genericListSubject.next(this.genericList);
   }

   /**
    * Permet de màj la liste
    * @param list : la nouvelle liste
    */
   updateList(list: GenericListModel){
     this.genericList = list;
     this.emitList();
   }

   /**
    * Indique aux abonnés les changements dans le QueryModel de GenericSelectProviderServiceService... fucc
    */
   emitQueryModel(){
     this.queryModelListSubject.next(this.queryModel);
   }

   getDataList():GenericListModel{
     return this.genericList;
   }

   /**
    * Permet de modifier le QueryModel et de notifier les autres abonnés
    * @param qm :Le QueryModel de l'implémentation
    */
  updateQueryModel(qm: QueryModel){
     this.queryModel = qm;
     this.emitQueryModel();
   }

   /**
    * Chaque Service pour Select doit avoir son implémentation car url différentes
    * @param queryModel : Dans l'éventualité où j'aurais mis une limite trop basse à la taille des listes renvoyées par défaut
    */
   abstract localGetDatas(queryModel?: QueryModel):Observable<GenericListModel>;
   abstract getDatas(queryModel?: QueryModel): Observable<GenericListModel>;

  /**
   * On aura besoin:
   * 
   * Probablement d'une fonction pour récupérer les modèles qui nous intéressent
   * 
   * D'une fonction pour envoyer le modèle au parent
   */
}
