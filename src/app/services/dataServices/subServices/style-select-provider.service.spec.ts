import { TestBed } from '@angular/core/testing';

import { StyleSelectProviderService } from './style-select-provider.service';

describe('StyleSelectProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StyleSelectProviderService = TestBed.get(StyleSelectProviderService);
    expect(service).toBeTruthy();
  });
});
