import { Injectable } from '@angular/core';
import { GenericSelectProviderService } from './generic-select-provider-service.service';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { StyleListModel } from 'src/app/models/dataListModels/styleListModel';
import { QueryModel } from 'src/app/models/queryModel';
import { Observable } from 'rxjs';

const getStyleUrl:string = "https://loc-helper.herokuapp.com/style/get";
const localGetStyleUrl: string = "http://localhost:8080/style/get"
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class StyleSelectProviderService extends GenericSelectProviderService{

  constructor(private http: HttpClient) { 
    super(new StyleListModel());
  }

  updateList(styleListModel: StyleListModel):void{
    super.updateList(styleListModel);
  }

  getDatas():Observable<StyleListModel>{
    return this.http.post<StyleListModel>(getStyleUrl, this.queryModel, httpOptions);
  }

  localGetDatas():Observable<StyleListModel>{
    return this.http.post<StyleListModel>(localGetStyleUrl, this.queryModel, httpOptions);
  }
}
