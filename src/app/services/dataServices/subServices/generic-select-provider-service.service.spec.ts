import { TestBed } from '@angular/core/testing';

import { GenericSelectProviderService } from './generic-select-provider-service.service';

describe('GenericSelectProviderServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GenericSelectProviderService = TestBed.get(GenericSelectProviderService);
    expect(service).toBeTruthy();
  });
});
