import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { GenericSelectProviderService } from './generic-select-provider-service.service';
import { ClotheTypeListModel } from 'src/app/models/dataListModels/clotheTypeListModel';
import { QueryModel } from 'src/app/models/queryModel';
import { Observable } from 'rxjs';
import { ClotheTypeModel } from 'src/app/models/dataModels/clotheTypeModel';

const getClotheTypeUrl:string = "https://loc-helper.herokuapp.com/clotheType/get";
const localGetClotheTypeUrl: string = "http://localhost:8080/clotheType/get"
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class ClotheTypeSelectProviderService extends GenericSelectProviderService{
  
  categories: string[] = [];

  constructor(private http: HttpClient) { 
    super(new ClotheTypeListModel());
    this.queryModel.addOrder("category", "ASC");
    this.queryModel.addOrder("name", "ASC");
  }

  updateList(clotheTypeListModel: ClotheTypeListModel):void{
    super.updateList(clotheTypeListModel);
    for(let ct of this.genericList.list as ClotheTypeModel[]){
      let alreadyFound:boolean = false;
      for(let cat of this.categories){
        if(ct.category === cat){
          alreadyFound = true; break;
        }
      }
      if(!alreadyFound){
        this.categories.push(ct.category);
      }
    }
  }

  getDatas():Observable<ClotheTypeListModel>{
    return this.http.post<ClotheTypeListModel>(getClotheTypeUrl, this.queryModel, httpOptions);
  }

  localGetDatas():Observable<ClotheTypeListModel>{
    return this.http.post<ClotheTypeListModel>(localGetClotheTypeUrl, this.queryModel, httpOptions);
  }
}
