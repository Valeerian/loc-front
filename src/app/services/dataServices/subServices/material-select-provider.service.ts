import { Injectable } from '@angular/core';
import { GenericSelectProviderService } from './generic-select-provider-service.service';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { MaterialListModel } from 'src/app/models/dataListModels/materialListModel';
import { QueryModel } from 'src/app/models/queryModel';
import { Observable } from 'rxjs';

const getMaterialUrl:string = "https://loc-helper.herokuapp.com/material/get";
const localGetMaterialUrl: string = "http://localhost:8080/material/get"
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class MaterialSelectProviderService extends GenericSelectProviderService {

  constructor(private http: HttpClient) { 
    super(new MaterialListModel());
  }

  updateList(materialListModel: MaterialListModel):void{
    super.updateList(materialListModel);
  }

  getDatas():Observable<MaterialListModel>{
    return this.http.post<MaterialListModel>(getMaterialUrl, this.queryModel, httpOptions);
  }

  localGetDatas():Observable<MaterialListModel>{
    return this.http.post<MaterialListModel>(localGetMaterialUrl, this.queryModel, httpOptions);
  }
}
