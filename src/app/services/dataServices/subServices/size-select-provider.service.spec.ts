import { TestBed } from '@angular/core/testing';

import { SizeSelectProviderService } from './size-select-provider.service';

describe('SizeSelectProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SizeSelectProviderService = TestBed.get(SizeSelectProviderService);
    expect(service).toBeTruthy();
  });
});
