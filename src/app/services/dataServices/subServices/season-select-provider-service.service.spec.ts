import { TestBed } from '@angular/core/testing';

import { SeasonSelectProviderService } from './season-select-provider-service.service';

describe('SeasonSelectProviderServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SeasonSelectProviderService = TestBed.get(SeasonSelectProviderService);
    expect(service).toBeTruthy();
  });
});
