import { Injectable } from '@angular/core';
import { GenericSelectProviderService } from './generic-select-provider-service.service';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { ColorListModel } from 'src/app/models/dataListModels/colorListModel';
import { QueryModel } from 'src/app/models/queryModel';
import { Observable } from 'rxjs';

const getColorUrl:string = "https://loc-helper.herokuapp.com/color/get";
const localGetColorUrl: string = "http://localhost:8080/color/get"
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class ColorSelectProviderService extends GenericSelectProviderService{

  constructor(private http: HttpClient) { 
    super(new ColorListModel());
  }

  updateList(colorListModel: ColorListModel):void{
    super.updateList(colorListModel);
  }

  getDatas():Observable<ColorListModel>{
    return this.http.post<ColorListModel>(getColorUrl, this.queryModel, httpOptions);
  }

  localGetDatas():Observable<ColorListModel>{
    return this.http.post<ColorListModel>(localGetColorUrl, this.queryModel, httpOptions);
  }
}
