import { Injectable } from '@angular/core';
import { GenericSelectProviderService } from './generic-select-provider-service.service';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { SizeListModel } from 'src/app/models/dataListModels/sizeListModel';
import { QueryModel } from 'src/app/models/queryModel';
import { Observable } from 'rxjs';
import { SizeModel } from 'src/app/models/dataModels/sizeModel';

const getSizeUrl:string = "https://loc-helper.herokuapp.com/size/get";
const localGetSizeUrl: string = "http://localhost:8080/size/get"
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class SizeSelectProviderService extends GenericSelectProviderService{

  categories: string[] = [];

  constructor(private http: HttpClient) { 
    super(new SizeListModel());
    this.queryModel.addOrder("category", "ASC");
    this.queryModel.addOrder("size", "ASC");
  }

  updateList(sizeListModel: SizeListModel):void{
    super.updateList(sizeListModel);
    for(let size  of this.genericList.list as SizeModel[]){
      let  alreadyFound: boolean = false;
      for(let cat of this.categories){
        if(size.category === cat){
          alreadyFound = true; break;
        }
      }
      if(!alreadyFound){
        this.categories.push(size.category);
      }
     }
  }

  getDatas():Observable<SizeListModel>{
    return this.http.post<SizeListModel>(getSizeUrl, this.queryModel, httpOptions);
  }

  localGetDatas():Observable<SizeListModel>{
    return this.http.post<SizeListModel>(localGetSizeUrl, this.queryModel, httpOptions);
  }
}
