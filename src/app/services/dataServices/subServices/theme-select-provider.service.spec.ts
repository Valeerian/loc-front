import { TestBed } from '@angular/core/testing';

import { ThemeSelectProviderService } from './theme-select-provider.service';

describe('ThemeSelectProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ThemeSelectProviderService = TestBed.get(ThemeSelectProviderService);
    expect(service).toBeTruthy();
  });
});
