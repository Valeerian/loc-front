import { Injectable } from '@angular/core';
import { GenericSelectProviderService } from './generic-select-provider-service.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SeasonListModel } from 'src/app/models/dataListModels/seasonListModel';
import { Observable } from 'rxjs';
import { QueryModel } from 'src/app/models/queryModel';

const getSeasonUrl:string = "https://loc-helper.herokuapp.com/season/get";
const localGetSeasonUrl: string = "http://localhost:8080/season/get"
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class SeasonSelectProviderService extends GenericSelectProviderService {

  constructor(private http: HttpClient) { 
    super(new SeasonListModel());
  }

  updateList(seasonListModel: SeasonListModel):void{
    super.updateList(seasonListModel);
  }

  getDatas():Observable<SeasonListModel>{
    return this.http.post<SeasonListModel>(getSeasonUrl, this.queryModel, httpOptions);
  }

  localGetDatas():Observable<SeasonListModel>{
    return this.http.post<SeasonListModel>(localGetSeasonUrl, this.queryModel, httpOptions);
  }
}
