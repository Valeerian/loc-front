import { Injectable } from '@angular/core';
import { GenericSelectProviderService } from './generic-select-provider-service.service';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { ThemeListModel } from 'src/app/models/dataListModels/themeListModel';
import { QueryModel } from 'src/app/models/queryModel';
import { Observable } from 'rxjs';

const getThemeUrl:string = "https://loc-helper.herokuapp.com/theme/get";
const localGetThemeUrl: string = "http://localhost:8080/theme/get"
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class ThemeSelectProviderService extends GenericSelectProviderService{

  constructor(private http: HttpClient) { 
    super(new ThemeListModel());
  }

  updateList(themeListModel: ThemeListModel):void{
    super.updateList(themeListModel);
  }

  getDatas():Observable<ThemeListModel>{
    return this.http.post<ThemeListModel>(getThemeUrl, this.queryModel, httpOptions);
  }

  localGetDatas():Observable<ThemeListModel>{
    return this.http.post<ThemeListModel>(localGetThemeUrl, this.queryModel, httpOptions);
  }
}
