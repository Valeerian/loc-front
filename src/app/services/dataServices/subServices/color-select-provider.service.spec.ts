import { TestBed } from '@angular/core/testing';

import { ColorSelectProviderService } from './color-select-provider.service';

describe('ColorSelectProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ColorSelectProviderService = TestBed.get(ColorSelectProviderService);
    expect(service).toBeTruthy();
  });
});
