import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { GenericSelectProviderService } from './generic-select-provider-service.service';
import { BrandListModel } from 'src/app/models/dataListModels/brandListModel';
import { QueryModel } from 'src/app/models/queryModel';
import { Observable } from 'rxjs';

const getBrandUrl:string = "https://loc-helper.herokuapp.com/brand/get";
const localGetBrandUrl: string = "http://localhost:8080/brand/get"
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class BrandSelectProviderService extends GenericSelectProviderService {

  constructor(private http: HttpClient) { 
    super(new BrandListModel());
  }

  updateList(brandListModel: BrandListModel):void{
    super.updateList(brandListModel);
  }

  getDatas():Observable<BrandListModel>{
    return this.http.post<BrandListModel>(getBrandUrl, this.queryModel, httpOptions);
  }

  localGetDatas():Observable<BrandListModel>{
    return this.http.post<BrandListModel>(localGetBrandUrl, this.queryModel, httpOptions);
  }
}
