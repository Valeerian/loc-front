import { TestBed } from '@angular/core/testing';

import { ClotheTypeSelectProviderService } from './clothe-type-select-provider.service';

describe('ClotheTypeSelectProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClotheTypeSelectProviderService = TestBed.get(ClotheTypeSelectProviderService);
    expect(service).toBeTruthy();
  });
});
