import { TestBed } from '@angular/core/testing';

import { ProviderSelectProviderService } from './provider-select-provider.service';

describe('ProviderSelectProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProviderSelectProviderService = TestBed.get(ProviderSelectProviderService);
    expect(service).toBeTruthy();
  });
});
