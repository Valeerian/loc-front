import { TestBed } from '@angular/core/testing';

import { BrandSelectProviderService } from './brand-select-provider.service';

describe('BrandSelectProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BrandSelectProviderService = TestBed.get(BrandSelectProviderService);
    expect(service).toBeTruthy();
  });
});
