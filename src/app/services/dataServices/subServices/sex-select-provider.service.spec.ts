import { TestBed } from '@angular/core/testing';

import { SexSelectProviderService } from './sex-select-provider.service';

describe('SexSelectProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SexSelectProviderService = TestBed.get(SexSelectProviderService);
    expect(service).toBeTruthy();
  });
});
