import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { GenericSelectProviderService } from './generic-select-provider-service.service';
import { SexListModel } from 'src/app/models/dataListModels/sexListModel';
import { QueryModel } from 'src/app/models/queryModel';
import { Observable } from 'rxjs';

const getSexnUrl:string = "https://loc-helper.herokuapp.com/sex/get";
const localGetSexUrl: string = "http://localhost:8080/sex/get"
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class SexSelectProviderService extends GenericSelectProviderService {

  constructor(private http: HttpClient) {
    super(new SexListModel());
  }

  updateList(sexListModel: SexListModel){
    super.updateList(sexListModel);
  }

  getDatas():Observable<SexListModel>{
    return this.http.post<SexListModel>(getSexnUrl, this.queryModel, httpOptions);
  }

  localGetDatas():Observable<SexListModel>{
    return this.http.post<SexListModel>(localGetSexUrl, this.queryModel, httpOptions);
  }

}
