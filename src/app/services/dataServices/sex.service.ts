import { Injectable } from '@angular/core';
import { GenericDataProviderService } from './generic-data-provider.service';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { SexListModel } from 'src/app/models/dataListModels/sexListModel';
import { QueryModel } from 'src/app/models/queryModel';
import { Observable } from 'rxjs';
import { SexModel } from 'src/app/models/dataModels/sexModel';

const getSexUrl:string = "https://loc-helper.herokuapp.com/sex/get";
const localGetSexUrl: string = "http://localhost:8080/sex/get"
const getSexForPossessionUrl:string = "https://loc-helper.herokuapp.com/sex/getForPossessions";
const localGetSexForPossessionUrl: string = "http://localhost:8080/sex/getForPossessions"
const localAddSexUrl: string= "http://localhost:8080/sex/add";
const addSexUrl: string = "https://loc-helper.herokuapp.com/sex/add";
const localEditSexUrl: string = "http://localhost:8080/sex/edit";
const editSexUrl: string = "https://loc-helper.herokuapp.com/sex/edit";
const localDeleteSexUrl: string = "http://localhost:8080/sex/delete";
const deleteSexUrl: string = "https://loc-helper.herokuapp.com/sex/delete";
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class SexService extends GenericDataProviderService{

  constructor(private http: HttpClient) {
    super(new SexListModel());
   }

  updateList(sexList: SexListModel):void{
    super.updateList(sexList);
  }

  localGetDatas(queryModel? : QueryModel):Observable<SexListModel>{
    return this.http.post<SexListModel>(localGetSexUrl, queryModel, httpOptions);
  }

  getDatas(queryModel? : QueryModel):Observable<SexListModel>{
    return this.http.post<SexListModel>(getSexUrl, queryModel, httpOptions);
  }

  localGetSexForPossession():Observable<SexListModel>{
    return this.http.get<SexListModel>(localGetSexForPossessionUrl, httpOptions);
  }

  getSexForPossession(queryModel? : QueryModel):Observable<SexListModel>{
    return this.http.get<SexListModel>(getSexForPossessionUrl,  httpOptions);
  }

  localAddData(sex: SexModel):Observable<string>{
    return this.http.post<string>(localAddSexUrl, sex, httpOptions);
  }

  addData(sex: SexModel):Observable<string>{
    return this.http.post<string>(addSexUrl, sex, httpOptions);
  }

  localEditData(sex: SexModel):Observable<string>{
    return this.http.post<string>(localEditSexUrl, sex, httpOptions);
  }

  editData(sex: SexModel):Observable<string>{
    return this.http.post<string>(editSexUrl, sex, httpOptions);
  }

  localDeleteData(sex: SexModel):Observable<string>{
    return this.http.post<string>(localDeleteSexUrl, sex, httpOptions);
  }

  deleteData(sex: SexModel):Observable<string>{
    return this.http.post<string>(deleteSexUrl, sex, httpOptions);
  }
}
