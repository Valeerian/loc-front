import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { GenericDataProviderService } from './generic-data-provider.service';
import { MaterialListModel } from 'src/app/models/dataListModels/materialListModel';
import { MaterialModel } from 'src/app/models/dataModels/materialModel';
import { QueryModel } from 'src/app/models/queryModel';
import { Observable } from 'rxjs';

const getMaterialUrl:string = "https://loc-helper.herokuapp.com/material/get";
const localGetMaterialUrl: string = "http://localhost:8080/material/get"
const localAddMaterialUrl: string= "http://localhost:8080/material/add";
const addMaterialUrl: string = "https://loc-helper.herokuapp.com/material/add";
const localEditMaterialUrl: string = "http://localhost:8080/material/edit";
const editMaterialUrl: string = "https://loc-helper.herokuapp.com/material/edit";
const localDeleteMaterialUrl: string = "http://localhost:8080/material/delete";
const deleteMaterialUrl: string = "https://loc-helper.herokuapp.com/material/delete";
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class MaterialService extends GenericDataProviderService{

  constructor(private http: HttpClient) {
    super(new MaterialListModel());
   }

   updateList(materialList: MaterialListModel):void{
     super.updateList(materialList);
   }

   localGetDatas(queryModel? : QueryModel):Observable<MaterialListModel>{
    return this.http.post<MaterialListModel>(localGetMaterialUrl, queryModel, httpOptions);
  }

  getDatas(queryModel? : QueryModel):Observable<MaterialListModel>{
    return this.http.post<MaterialListModel>(getMaterialUrl, queryModel, httpOptions);
  }

  localAddData(material: MaterialModel):Observable<string>{
    return this.http.post<string>(localAddMaterialUrl, material, httpOptions);
  }

  addData(material: MaterialModel):Observable<string>{
    return this.http.post<string>(addMaterialUrl, material, httpOptions);
  }

  localEditData(material: MaterialModel):Observable<string>{
    return this.http.post<string>(localEditMaterialUrl, material, httpOptions);
  }

  editData(material: MaterialModel):Observable<string>{
    return this.http.post<string>(editMaterialUrl, material, httpOptions);
  }

  localDeleteData(material: MaterialModel):Observable<string>{
    return this.http.post<string>(localDeleteMaterialUrl, material, httpOptions);
  }

  deleteData(material: MaterialModel):Observable<string>{
    return this.http.post<string>(deleteMaterialUrl, material, httpOptions);
  }

}
