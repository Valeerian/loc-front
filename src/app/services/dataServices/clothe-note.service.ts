import { Injectable } from '@angular/core';
import { GenericDataProviderService } from './generic-data-provider.service';
import { ClotheNoteListModel } from 'src/app/models/dataListModels/clotheNoteListModel';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { QueryModel } from 'src/app/models/queryModel';
import { Observable } from 'rxjs';
import { ClotheNoteModel } from 'src/app/models/dataModels/clotheNoteModel';

const getClotheNoteUrl:string ="https://loc-helper.herokuapp.com/clotheNotes/add";
const localGetClotheNoteUrl:string="http://localhost:8080/clotheNotes/get";
const getMyCommentUrl:string ="https://loc-helper.herokuapp.com/clotheNotes/getMine";
const localGetMyCommentUrl:string="http://localhost:8080/clotheNotes/getMine";
const localAddClotheNoteUrl: string= "http://localhost:8080/clotheNotes/createOrUpdate";
const addClotheNoteUrl: string = "https://loc-helper.herokuapp.com/clotheNotes/createOrUpdate";
const localEditClotheNoteUrl: string = "http://localhost:8080/clotheNotes/edit";
const editClotheNoteUrl: string = "https://loc-helper.herokuapp.com/clotheNotes/edit";
const localDeleteClotheNoteUrl: string = "http://localhost:8080/clotheNotes/delete";
const deleteClotheNoteUrl: string = "https://loc-helper.herokuapp.com/clotheNotes/delete";
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class ClotheNoteService extends GenericDataProviderService {
  constructor(private http: HttpClient) {
    super(new ClotheNoteListModel());
   }

   updateList(clotheNoteList: ClotheNoteListModel):void{
     super.updateList(clotheNoteList);
   }

   localGetDatas(queryModel? : QueryModel):Observable<ClotheNoteListModel>{
     
     if(queryModel.values.length < 1){
       queryModel.addCriteria("clothe",":","1");
     }
    return this.http.post<ClotheNoteListModel>(localGetClotheNoteUrl, queryModel, httpOptions);
  }

  localGetMyComment(queryModel?: QueryModel): Observable<ClotheNoteModel>{
    if(queryModel.values.length < 1){
      queryModel.addCriteria("clothe",":","1");
    }
    return this.http.post<ClotheNoteModel>(localGetMyCommentUrl,queryModel, httpOptions);
  }

  getMyComment(queryModel?: QueryModel): Observable<ClotheNoteModel>{
    if(queryModel.values.length < 1){
      queryModel.addCriteria("clothe",":","1");
    } 
    return this.http.post<ClotheNoteModel>(getMyCommentUrl,queryModel, httpOptions);
  }

  getDatas(queryModel?: QueryModel): Observable<ClotheNoteListModel>{
    if(queryModel.values.length < 1){
      queryModel.addCriteria("clothe",":","1");
    }
    return this.http.post<ClotheNoteListModel>(getClotheNoteUrl, queryModel, httpOptions);
  }

  localAddData(clotheNote: ClotheNoteModel):Observable<string>{
    return this.http.post<string>(localAddClotheNoteUrl, clotheNote, httpOptions);
  }

  addData(clotheNote: ClotheNoteModel):Observable<string>{
    return this.http.post<string>(addClotheNoteUrl, clotheNote, httpOptions);
  }

  localEditData(clotheNote: ClotheNoteModel):Observable<string>{
    return this.http.post<string>(localEditClotheNoteUrl, clotheNote, httpOptions);
  }

  editData(clotheNote: ClotheNoteModel):Observable<string>{
    return this.http.post<string>(editClotheNoteUrl, clotheNote, httpOptions);
  }

  localDeleteData(clotheNote: ClotheNoteModel):Observable<string>{
    return this.http.post<string>(localDeleteClotheNoteUrl, clotheNote, httpOptions);
  }

  deleteData(clotheNote: ClotheNoteModel):Observable<string>{
    return this.http.post<string>(deleteClotheNoteUrl, clotheNote, httpOptions);
  }
}
