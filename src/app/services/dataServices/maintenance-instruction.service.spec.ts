import { TestBed } from '@angular/core/testing';

import { MaintenanceInstructionService } from './maintenance-instruction.service';

describe('MaintenanceInstructionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MaintenanceInstructionService = TestBed.get(MaintenanceInstructionService);
    expect(service).toBeTruthy();
  });
});
