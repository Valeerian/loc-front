import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { QueryModel } from 'src/app/models/queryModel';
import { SeasonListModel } from 'src/app/models/dataListModels/seasonListModel';
import { Observable } from 'rxjs';
import { GenericDataProviderService } from './generic-data-provider.service';
import { SeasonModel } from 'src/app/models/dataModels/seasonModel';

const getSeasonUrl:string = "https://loc-helper.herokuapp.com/season/get";
const localGetSeasonUrl: string = "http://localhost:8080/season/get"
const getSeasonForPossessionUrl:string = "https://loc-helper.herokuapp.com/season/getForPossessions";
const localGetSeasonForPossessionUrl: string = "http://localhost:8080/season/getForPossessions"
const localAddSeasonUrl: string= "http://localhost:8080/season/add";
const addSeasonUrl: string = "https://loc-helper.herokuapp.com/season/add";
const localEditSeasonUrl: string = "http://localhost:8080/season/edit";
const editSeasonUrl: string = "https://loc-helper.herokuapp.com/season/edit";
const localDeleteSeasonUrl: string = "http://localhost:8080/season/delete";
const deleteSeasonUrl: string = "https://loc-helper.herokuapp.com/season/delete";
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class SeasonService extends GenericDataProviderService{
  constructor(private http: HttpClient) {
    super(new SeasonListModel());
   }

  updateList(seasonList: SeasonListModel):void{
    super.updateList(seasonList);
  }

  localGetDatas(queryModel? : QueryModel):Observable<SeasonListModel>{
    return this.http.post<SeasonListModel>(localGetSeasonUrl, queryModel, httpOptions);
  }

  getDatas(queryModel? : QueryModel):Observable<SeasonListModel>{
    return this.http.post<SeasonListModel>(getSeasonUrl, queryModel, httpOptions);
  }

  localGetSeasonForPossession():Observable<SeasonListModel>{
    return this.http.get<SeasonListModel>(localGetSeasonForPossessionUrl, httpOptions);
  }

  getSeasonForPossession():Observable<SeasonListModel>{
    return this.http.get<SeasonListModel>(getSeasonForPossessionUrl, httpOptions);
  }

  localAddData(season: SeasonModel):Observable<string>{
    return this.http.post<string>(localAddSeasonUrl, season, httpOptions);
  }

  addData(season: SeasonModel):Observable<string>{
    return this.http.post<string>(addSeasonUrl, season, httpOptions);
  }

  localEditData(season: SeasonModel):Observable<string>{
    return this.http.post<string>(localEditSeasonUrl, season, httpOptions);
  }

  editData(season: SeasonModel):Observable<string>{
    return this.http.post<string>(editSeasonUrl, season, httpOptions);
  }

  localDeleteData(season: SeasonModel):Observable<string>{
    return this.http.post<string>(localDeleteSeasonUrl, season, httpOptions);
  }

  deleteData(season: SeasonModel):Observable<string>{
    return this.http.post<string>(deleteSeasonUrl, season, httpOptions);
  }
}
