import { TestBed } from '@angular/core/testing';

import { ManeStatusService } from './mane-status.service';

describe('ManeStatusService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ManeStatusService = TestBed.get(ManeStatusService);
    expect(service).toBeTruthy();
  });
});
