import { Injectable } from '@angular/core';
import { GenericDataProviderService } from './generic-data-provider.service';
import { PossessionStatusListModel } from 'src/app/models/dataListModels/possessionStatusListModel';
import { PossessionStatusModel } from 'src/app/models/dataModels/possessionStatusModel';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { QueryModel } from 'src/app/models/queryModel';
import { Observable } from 'rxjs';

const getPossessionStatusUrl:string = "https://loc-helper.herokuapp.com/possessionStatus/get";
const localGetPossessionStatusUrl: string = "http://localhost:8080/possessionStatus/get"
const getPossessionStatusForPossessionUrl:string = "https://loc-helper.herokuapp.com/possessionStatus/getForPossessions";
const localGetPossessionStatusForPossessionUrl: string = "http://localhost:8080/possessionStatus/getForPossessions"
const localAddPossessionStatusUrl: string= "http://localhost:8080/possessionStatus/add";
const addPossessionStatusUrl: string = "https://loc-helper.herokuapp.com/possessionStatus/add";
const localEditPossessionStatusUrl: string = "http://localhost:8080/possessionStatus/edit";
const editPossessionStatusUrl: string = "https://loc-helper.herokuapp.com/possessionStatus/edit";
const localDeletePossessionStatusUrl: string = "http://localhost:8080/possessionStatus/delete";
const deletePossessionStatusUrl: string = "https://loc-helper.herokuapp.com/possessionStatus/delete";
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class PossessionStatusService extends GenericDataProviderService {

  constructor(private http: HttpClient) {
    super(new PossessionStatusListModel());
   }

  updateList(possessionStatus: PossessionStatusListModel):void{
    super.updateList(possessionStatus);
  }

  localGetDatas(queryModel? : QueryModel):Observable<PossessionStatusListModel>{
    return this.http.post<PossessionStatusListModel>(localGetPossessionStatusUrl, queryModel, httpOptions);
  }

  getDatas(queryModel? : QueryModel):Observable<PossessionStatusListModel>{
    return this.http.post<PossessionStatusListModel>(getPossessionStatusUrl, queryModel, httpOptions);
  }

  localGetPossessionStatusForPossession():Observable<PossessionStatusListModel>{
    return this.http.get<PossessionStatusListModel>(localGetPossessionStatusForPossessionUrl, httpOptions);
  }

  getDatasPossessionStatusForPossession():Observable<PossessionStatusListModel>{
    return this.http.get<PossessionStatusListModel>(getPossessionStatusForPossessionUrl, httpOptions);
  }

  localAddData(possessionStatus: PossessionStatusModel):Observable<string>{
    return this.http.post<string>(localAddPossessionStatusUrl, possessionStatus, httpOptions);
  }

  addData(possessionStatus: PossessionStatusModel):Observable<string>{
    return this.http.post<string>(addPossessionStatusUrl, possessionStatus, httpOptions);
  }

  localEditData(possessionStatus: PossessionStatusModel):Observable<string>{
    return this.http.post<string>(localEditPossessionStatusUrl, possessionStatus, httpOptions);
  }

  editData(possessionStatus: PossessionStatusModel):Observable<string>{
    return this.http.post<string>(editPossessionStatusUrl, possessionStatus, httpOptions);
  }

  localDeleteData(possessionStatus: PossessionStatusModel):Observable<string>{
    return this.http.post<string>(localDeletePossessionStatusUrl, possessionStatus, httpOptions);
  }

  deleteData(possessionStatus: PossessionStatusModel):Observable<string>{
    return this.http.post<string>(deletePossessionStatusUrl, possessionStatus, httpOptions);
  }
}
