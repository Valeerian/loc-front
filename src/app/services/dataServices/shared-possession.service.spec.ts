import { TestBed } from '@angular/core/testing';

import { SharedPossessionService } from './shared-possession.service';

describe('SharedPossessionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SharedPossessionService = TestBed.get(SharedPossessionService);
    expect(service).toBeTruthy();
  });
});
