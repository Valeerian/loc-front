import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { GenericDataProviderService } from './generic-data-provider.service';
import { PossessionListModel } from 'src/app/models/dataListModels/possessionListModel';
import { QueryModel } from 'src/app/models/queryModel';
import { Observable } from 'rxjs';
import { PossessionModel } from 'src/app/models/dataModels/possessionModel';
import { FriendshipModel } from 'src/app/models/dataModels/friendshipModel';
import { FriendshipListModel } from 'src/app/models/dataListModels/friendshipListModel';
import { LaundryGroupModel } from 'src/app/models/dataModels/laundryGroupModel';
import { UserModel } from 'src/app/models/dataModels/userModel';

const getPossessionUrl:string = "https://loc-helper.herokuapp.com/possessions/get";
const localGetPossessionUrl: string = "http://localhost:8080/possessions/get"
const getMyPossessionsUrl:string = "https://loc-helper.herokuapp.com/possessions/getMyPossessionsForSelect";
const localGetMyPossessionsUrl: string = "http://localhost:8080/possessions/getMyPossessionsForSelect"
const localAddPossessionUrl: string= "http://localhost:8080/possessions/add";
const addPossessionUrl: string = "https://loc-helper.herokuapp.com/possessions/add";
const localEditPossessionUrl: string = "http://localhost:8080/possessions/edit";
const editPossessionUrl: string = "https://loc-helper.herokuapp.com/possessions/edit";
const localDeletePossessionUrl: string = "http://localhost:8080/possessions/delete";
const deletePossessionUrl: string = "https://loc-helper.herokuapp.com/possessions/delete";
const localSharePossessionUrl: string = "http://localhost:8080/possessions/share";
const sharePossessionUrl: string = "https://loc-helper.herokuapp.com/possessions/share";
const localGiveBackPossessionUrl: string = "http://localhost:8080/possessions/giveBack";
const giveBackPossessionUrl: string = "https://loc-helper.herokuapp.com/possessions/giveBack";
const localGetPossessionsSharedWithThisFriendUrl: string = "http://localhost:8080/possessions/getSharedWithThisFriend";
const getPossessionsSharedWithThisFriendUrl: string = "https://loc-helper.herokuapp.com/possessions/getSharedWithThisFriend";
const localGetPossessionsSharedFromThisFriendUrl: string = "http://localhost:8080/possessions/getSharedFromThisFriend";
const getPossessionsSharedFromThisFriendUrl: string = "https://loc-helper.herokuapp.com/possessions/getSharedFromThisFriend";
const localGetHisPossessionsUrl: string = "http://localhost:8080/possessions/getForLaundryMane";
const getHisPossessionsUrl: string = "https://loc-helper.herokuapp.com/possessions/getForLaundryMane";
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};


@Injectable({
  providedIn: 'root'
})
export class PossessionService extends GenericDataProviderService {

  constructor(private http: HttpClient) {
    super(new PossessionListModel());
   }

  updateList(possessionList: PossessionListModel):void{
    super.updateList(possessionList);
  }

  localGetHisPossessions(userModel: UserModel):Observable<PossessionListModel>{
    return this.http.post<PossessionListModel>(localGetHisPossessionsUrl,userModel, httpOptions);
  }
  getHisPossessions(userModel: UserModel):Observable<PossessionListModel>{
    return this.http.post<PossessionListModel>(getHisPossessionsUrl, userModel, httpOptions);
  }

  localGetDatas(queryModel? : QueryModel):Observable<PossessionListModel>{
    return this.http.post<PossessionListModel>(localGetPossessionUrl, queryModel, httpOptions);
  }

  getDatas(queryModel? : QueryModel):Observable<PossessionListModel>{
    return this.http.post<PossessionListModel>(getPossessionUrl, queryModel, httpOptions);
  }

  localSharePossession(possessionModel : PossessionModel): Observable<string>{
    return this.http.post<string>(localSharePossessionUrl, possessionModel, httpOptions);
  }

  sharePossession(possessionModel : PossessionModel): Observable<string>{
    return this.http.post<string>(sharePossessionUrl, possessionModel, httpOptions);
  }

  localGiveBackPossession(possessionModel: PossessionModel):Observable<string>{
    return this.http.post<string>(localGiveBackPossessionUrl, possessionModel, httpOptions);
  }

  giveBackPossession(possessionModel: PossessionModel):Observable<string>{
    return this.http.post<string>(giveBackPossessionUrl, possessionModel, httpOptions);
  }

  localAddData(possession: PossessionModel[]):Observable<string>{
    return this.http.post<string>(localAddPossessionUrl, possession, httpOptions);
  }

  addData(possession: PossessionModel[]):Observable<string>{
    return this.http.post<string>(addPossessionUrl, possession, httpOptions);
  }

  localGetPossessionsSharedWithThisFriend(friendshipModel: FriendshipModel):Observable<PossessionListModel>{
    return this.http.post<PossessionListModel>(localGetPossessionsSharedWithThisFriendUrl,friendshipModel, httpOptions);
  }

  getPossessionsSharedWithThisFriend(friendshipModel: FriendshipListModel):Observable<PossessionListModel>{
    return this.http.post<PossessionListModel>(getPossessionsSharedWithThisFriendUrl, friendshipModel, httpOptions);
  }

  localGetPossessionsSharedFromThisFriend(friendshipModel: FriendshipModel):Observable<PossessionListModel>{
    return this.http.post<PossessionListModel>(localGetPossessionsSharedFromThisFriendUrl,friendshipModel, httpOptions);
  }

  localGetMyPossessions():Observable<PossessionListModel>{
    return this.http.get<PossessionListModel>(localGetMyPossessionsUrl, httpOptions);
  }

  getMyPossessions():Observable<PossessionListModel>{
    return this.http.get<PossessionListModel>(getMyPossessionsUrl, httpOptions);
  }

  getPossessionsSharedFromThisFriend(friendshipModel: FriendshipListModel):Observable<PossessionListModel>{
    return this.http.post<PossessionListModel>(getPossessionsSharedFromThisFriendUrl, friendshipModel, httpOptions);
  }

  localEditData(possession: PossessionModel):Observable<string>{
    return this.http.post<string>(localEditPossessionUrl, possession, httpOptions);
  }

  editData(possession: PossessionModel):Observable<string>{
    return this.http.post<string>(editPossessionUrl, possession, httpOptions);
  }

  localDeleteData(possession: PossessionModel):Observable<string>{
    return this.http.post<string>(localDeletePossessionUrl, possession, httpOptions);
  }

  deleteData(possession: PossessionModel):Observable<string>{
    return this.http.post<string>(deletePossessionUrl, possession, httpOptions);
  }
}
