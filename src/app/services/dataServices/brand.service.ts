import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { QueryModel } from 'src/app/models/queryModel';
import { Subject, Observable } from 'rxjs';
import { BrandListModel } from 'src/app/models/dataListModels/brandListModel';
import { GenericDataProviderService } from './generic-data-provider.service';
import { BrandModel } from 'src/app/models/dataModels/brandModel';

const getBrandUrl:string ="https://loc-helper.herokuapp.com/brand/get";
const localGetBrandUrl:string="http://localhost:8080/brand/get";
const getBrandForPossessionUrl:string ="https://loc-helper.herokuapp.com/brand/getForPossessions";
const localGetBrandForPossessionUrl:string="http://localhost:8080/brand/getForPossessions";
const localAddBrandUrl: string= "http://localhost:8080/brand/add";
const addBrandUrl: string = "https://loc-helper.herokuapp.com/brand/add";
const localEditBrandUrl: string = "http://localhost:8080/brand/edit";
const editBrandUrl: string = "https://loc-helper.herokuapp.com/brand/edit";
const localDeleteBrandUrl: string = "http://localhost:8080/brand/delete";
const deleteBrandUrl: string = "https://loc-helper.herokuapp.com/brand/delete";
const localGetBrandForSharedPossessionUrl: string = "http://localhost:8080/brand/getForSharedPossessions";
const getBrandForSharedPossessionUrl: string = "https://loc-helper.herokuapp.com/brand/getForSharedPossessions";
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})


export class BrandService  extends GenericDataProviderService{
  constructor(private http: HttpClient) {
    super(new BrandListModel());
   }

   updateList(brandList: BrandListModel):void{
     super.updateList(brandList);
   }

   localGetDatas(queryModel? : QueryModel):Observable<BrandListModel>{
    return this.http.post<BrandListModel>(localGetBrandUrl, queryModel, httpOptions);
  }

  getDatas(queryModel?: QueryModel): Observable<BrandListModel>{
    return this.http.post<BrandListModel>(getBrandUrl, queryModel, httpOptions);
  }

  localGetBrandForPossession():Observable<BrandListModel>{
    return this.http.get<BrandListModel>(localGetBrandForPossessionUrl, httpOptions);
  }

  getBrandForPossession(): Observable<BrandListModel>{
    return this.http.get<BrandListModel>(getBrandForPossessionUrl, httpOptions);
  }

  localGetBrandForSharedPossession():Observable<BrandListModel>{
    return this.http.get<BrandListModel>(localGetBrandForSharedPossessionUrl, httpOptions);
  }

  getBrandForSharedPossession(): Observable<BrandListModel>{
    return this.http.get<BrandListModel>(getBrandForSharedPossessionUrl, httpOptions);
  }

  localAddData(brand: BrandModel):Observable<string>{
    return this.http.post<string>(localAddBrandUrl, brand, httpOptions);
  }

  addData(brand: BrandModel):Observable<string>{
    return this.http.post<string>(addBrandUrl, brand, httpOptions);
  }

  localEditData(brand: BrandModel):Observable<string>{
    return this.http.post<string>(localEditBrandUrl, brand, httpOptions);
  }

  editData(brand: BrandModel):Observable<string>{
    return this.http.post<string>(editBrandUrl, brand, httpOptions);
  }

  localDeleteData(brand: BrandModel):Observable<string>{
    return this.http.post<string>(localDeleteBrandUrl, brand, httpOptions);
  }

  deleteData(brand: BrandModel):Observable<string>{
    return this.http.post<string>(deleteBrandUrl, brand, httpOptions);
  }

   
}
