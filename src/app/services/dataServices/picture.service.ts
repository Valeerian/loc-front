import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { GenericDataProviderService } from './generic-data-provider.service';
import { PictureListModel } from 'src/app/models/dataListModels/pictureListModel';
import { PictureModel } from 'src/app/models/dataModels/pictureModel';
import { Observable } from 'rxjs';
import { QueryModel } from 'src/app/models/queryModel';

const getPictureUrl:string = "https://loc-helper.herokuapp.com/picture/get";
const localGetPictureUrl: string = "http://localhost:8080/picture/get"
const localAddPictureUrl: string= "http://localhost:8080/picture/add";
const addPictureUrl: string = "https://loc-helper.herokuapp.com/picture/add";
const localEditPictureUrl: string = "http://localhost:8080/picture/edit";
const editPictureUrl: string = "https://loc-helper.herokuapp.com/picture/edit";
const localDeletePictureUrl: string = "http://localhost:8080/picture/delete";
const deletePictureUrl: string = "https://loc-helper.herokuapp.com/picture/delete";
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class PictureService extends GenericDataProviderService{
  constructor(private http: HttpClient) {
    super(new PictureListModel());
   }

  updateList(pictureList: PictureListModel):void{
    super.updateList(pictureList);
  }

  localGetDatas(queryModel? : QueryModel):Observable<PictureListModel>{
    return this.http.post<PictureListModel>(localGetPictureUrl, queryModel, httpOptions);
  }

  getDatas(queryModel? : QueryModel):Observable<PictureListModel>{
    return this.http.post<PictureListModel>(getPictureUrl, queryModel, httpOptions);
  }

  localAddData(picture: PictureModel):Observable<string>{
    return this.http.post<string>(localAddPictureUrl, picture, httpOptions);
  }

  addData(picture: PictureModel):Observable<string>{
    return this.http.post<string>(addPictureUrl, picture, httpOptions);
  }

  localEditData(picture: PictureModel):Observable<string>{
    return this.http.post<string>(localEditPictureUrl, picture, httpOptions);
  }

  editData(picture: PictureModel):Observable<string>{
    return this.http.post<string>(editPictureUrl, picture, httpOptions);
  }

  localDeleteData(picture: PictureModel):Observable<string>{
    return this.http.post<string>(localDeletePictureUrl, picture, httpOptions);
  }

  deleteData(picture: PictureModel):Observable<string>{
    return this.http.post<string>(deletePictureUrl, picture, httpOptions);
  }

}
