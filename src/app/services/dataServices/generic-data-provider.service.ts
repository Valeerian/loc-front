import { Injectable } from '@angular/core';
import { QueryModel } from 'src/app/models/queryModel';
import { GenericListModel } from 'src/app/models/dataListModels/genericListModel';
import { Subject, Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export abstract class GenericDataProviderService {
  private queryModel:QueryModel;
  private genericList : GenericListModel;
  queryModelSubject = new Subject<QueryModel>();
  genericListSubject = new Subject<GenericListModel>();
  constructor(genericList: GenericListModel) { 
    this.queryModel = new QueryModel;
    this.genericList = genericList;
  }

  /**
   * Permet d'émettre une liste de données à toutes les classes abonnées
   */
  emitList(){
    this.genericListSubject.next(this.genericList);
  }
  
  /**
   * Permet d'émettre le QueryModel à toutes les classes abonnées
   */
  emitQueryModel(){
    this.queryModelSubject.next(this.queryModel);
  }
  
  /**
   * Permet de mettre à jour le QueryModel utilisé en en envoyant un nouveau contenant toutes les données à jour
   * @param queryModel : Le nouveau QueryModel
   */
  updateQueryModel(queryModel: QueryModel):void{
    this.queryModel = queryModel;
    this.emitQueryModel();
  }

  /**
   * Permet de modifier le nombre d'éléments à afficher/page de données
   * @param newValue : Le nouveau nombre d'éléments/page
   */
  changeElementsPerPage(newValue: number):void{
    this.queryModel.elementsPerPage = newValue;
    this.emitQueryModel();
  }

  /**
   * Permet de passer d'une page de données à l'autre
   * Prend soin de "transformer" ce numéro de page POV client en numéro de page POV JPA
   * @param newValue : Le numéro de la page du point de vue de l'utilisateur
   */
  changePageNumber(newValue: number):void{
    this.queryModel.pageNumber = newValue -1;
    this.emitQueryModel();
  }

  /**
   * Permet de modifier l'ordre de tri des données
   * @param newOrderType : Le type de tri (Ascendant ou descendant)
   * @param newOrderBy : La colonne du tri
   */
  changeOrder(newOrderType: string, newOrderBy: string):void{
    this.queryModel.setNewOrder(newOrderBy, newOrderType);
    this.emitQueryModel();
  }

  /**
   * Permet de retirer les critères de recherche
   */
  removeCriterias():void{
    this.queryModel.operators = [];
    this.queryModel.keys = [];
    this.queryModel.values = [];
    this.emitQueryModel();
  }

  /**
   * 
   * @param key : la colonne de recherche
   * @param op : le type du critère (: => EQUAL, ~ => LIKE, < =>ST, > => GT,..)
   * @param val : la valeur de comparaison
   */
  addCriteria(key: string, op: string, val: string):void{
    this.queryModel.operators.push(op);
    this.queryModel.keys.push(key);
    this.queryModel.values.push(val);
    this.emitQueryModel();
  }

  /**
   * Permet de mettre à jour la liste de données pour toutes les classes abonnées
   * @param genericList : la nouvelle liste de données
   */
  updateList(genericList: GenericListModel):void{
    this.genericList = genericList;
    this.emitList();
  }

  abstract getDatas(queryModel? : QueryModel):Observable<GenericListModel>;
  abstract localGetDatas(queryModel? : QueryModel):Observable<GenericListModel>;
  abstract addData(model: any):Observable<string>;
  abstract localAddData(model: any): Observable<string>;
  abstract deleteData(model:any):Observable<string>;
  abstract localDeleteData(model:any):Observable<string>;
  abstract editData(model:any):Observable<string>;
  abstract localEditData(model:any):Observable<string>;


}
