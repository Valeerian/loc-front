import { TestBed } from '@angular/core/testing';

import { LaundryGroupService } from './laundry-group.service';

describe('LaundryGroupService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LaundryGroupService = TestBed.get(LaundryGroupService);
    expect(service).toBeTruthy();
  });
});
