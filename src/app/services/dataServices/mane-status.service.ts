import { Injectable } from '@angular/core';
import { GenericDataProviderService } from './generic-data-provider.service';
import { ManeStatusListModel } from 'src/app/models/dataListModels/maneStatusListModel';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ManeStatusModel } from 'src/app/models/dataModels/maneStatusModel';
import { Observable } from 'rxjs';
import { QueryModel } from 'src/app/models/queryModel';

const getManeStatusUrl:string = "https://loc-helper.herokuapp.com/maneStatus/get";
const localGetManeStatusUrl: string = "http://localhost:8080/maneStatus/get"
const localAddManeStatusUrl: string= "http://localhost:8080/maneStatus/add";
const addManeStatusUrl: string = "https://loc-helper.herokuapp.com/maneStatus/add";
const localEditManeStatusUrl: string = "http://localhost:8080/maneStatus/edit";
const editManeStatusUrl: string = "https://loc-helper.herokuapp.com/maneStatus/edit";
const localDeleteManeStatusUrl: string = "http://localhost:8080/maneStatus/delete";
const deleteManeStatusUrl: string = "https://loc-helper.herokuapp.com/maneStatus/delete";
const localGetForSelectUrl: string = "http://localhost:8080/maneStatus/getForSelect";
const getForSelectUrl: string = "https://loc-helper.herokuapp.com/maneStatus/getForSelect";
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class ManeStatusService extends GenericDataProviderService {

  constructor(private http: HttpClient) {
    super(new ManeStatusListModel());
   }

  updateList(maneStatusList: ManeStatusListModel):void{
    super.updateList(maneStatusList);
  }

  localGetDatas(queryModel? : QueryModel):Observable<ManeStatusListModel>{
    return this.http.post<ManeStatusListModel>(localGetManeStatusUrl, queryModel, httpOptions);
  }

  getDatas(queryModel? : QueryModel):Observable<ManeStatusListModel>{
    return this.http.post<ManeStatusListModel>(getManeStatusUrl, queryModel, httpOptions);
  }

  localAddData(maneStatus: ManeStatusModel):Observable<string>{
    return this.http.post<string>(localAddManeStatusUrl, maneStatus, httpOptions);
  }

  localGetForSelect():Observable<ManeStatusListModel>{
    return this.http.get<ManeStatusListModel>(localGetForSelectUrl, httpOptions);
  }

  getForSelect():Observable<ManeStatusListModel>{
    return this.http.get<ManeStatusListModel>(getForSelectUrl, httpOptions);
  }

  addData(maneStatus: ManeStatusModel):Observable<string>{
    return this.http.post<string>(addManeStatusUrl, maneStatus, httpOptions);
  }

  localEditData(maneStatus: ManeStatusModel):Observable<string>{
    return this.http.post<string>(localEditManeStatusUrl, maneStatus, httpOptions);
  }

  editData(maneStatus: ManeStatusModel):Observable<string>{
    return this.http.post<string>(editManeStatusUrl, maneStatus, httpOptions);
  }

  localDeleteData(maneStatus: ManeStatusModel):Observable<string>{
    return this.http.post<string>(localDeleteManeStatusUrl, maneStatus, httpOptions);
  }

  deleteData(maneStatus: ManeStatusModel):Observable<string>{
    return this.http.post<string>(deleteManeStatusUrl, maneStatus, httpOptions);
  }
}
