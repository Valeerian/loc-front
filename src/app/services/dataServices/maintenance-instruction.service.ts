import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { MaintenanceInstructionListModel } from 'src/app/models/dataListModels/maintenanceInstructionListModel';
import { GenericDataProviderService } from './generic-data-provider.service';
import { MaintenanceInstructionModel } from 'src/app/models/dataModels/maintenanceInstruction';
import { Observable } from 'rxjs';
import { QueryModel } from 'src/app/models/queryModel';

const getMaintenanceInstructionUrl:string = "https://loc-helper.herokuapp.com/color/get";
const localGetMaintenanceInstructionUrl: string = "http://localhost:8080/maintenanceInstruction/get"
const localAddMaintenanceInstructionUrl: string= "http://localhost:8080/maintenanceInstruction/add";
const addMaintenanceInstructionUrl: string = "https://loc-helper.herokuapp.com/maintenanceInstruction/add";
const localEditMaintenanceInstructionUrl: string = "http://localhost:8080/maintenanceInstruction/edit";
const editMaintenanceInstructionUrl: string = "https://loc-helper.herokuapp.com/maintenanceInstruction/edit";
const localDeleteMaintenanceInstructionUrl: string = "http://localhost:8080/maintenanceInstruction/delete";
const deleteMaintenanceInstructionUrl: string = "https://loc-helper.herokuapp.com/maintenanceInstruction/delete";
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class MaintenanceInstructionService extends GenericDataProviderService {

  constructor(private http: HttpClient) {
    super(new MaintenanceInstructionListModel());
   }

  updateList(maintenanceInstructionList: MaintenanceInstructionListModel):void{
    super.updateList(maintenanceInstructionList);
  }

  localGetDatas(queryModel? : QueryModel):Observable<MaintenanceInstructionListModel>{
    return this.http.post<MaintenanceInstructionListModel>(localGetMaintenanceInstructionUrl, queryModel, httpOptions);
  }

  getDatas(queryModel? : QueryModel):Observable<MaintenanceInstructionListModel>{
    return this.http.post<MaintenanceInstructionListModel>(getMaintenanceInstructionUrl, queryModel, httpOptions);
  }

  localAddData(maintenanceInstruction: MaintenanceInstructionModel):Observable<string>{
    return this.http.post<string>(localAddMaintenanceInstructionUrl, maintenanceInstruction, httpOptions);
  }

  addData(maintenanceInstruction: MaintenanceInstructionModel):Observable<string>{
    return this.http.post<string>(addMaintenanceInstructionUrl, maintenanceInstruction, httpOptions);
  }

  localEditData(maintenanceInstruction: MaintenanceInstructionModel):Observable<string>{
    return this.http.post<string>(localEditMaintenanceInstructionUrl, maintenanceInstruction, httpOptions);
  }

  editData(maintenanceInstruction: MaintenanceInstructionModel):Observable<string>{
    return this.http.post<string>(editMaintenanceInstructionUrl, maintenanceInstruction, httpOptions);
  }

  localDeleteData(maintenanceInstruction: MaintenanceInstructionModel):Observable<string>{
    return this.http.post<string>(localDeleteMaintenanceInstructionUrl, maintenanceInstruction, httpOptions);
  }

  deleteData(maintenanceInstruction: MaintenanceInstructionModel):Observable<string>{
    return this.http.post<string>(deleteMaintenanceInstructionUrl, maintenanceInstruction, httpOptions);
  }
}
