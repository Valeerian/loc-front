import { Injectable } from '@angular/core';
import { GenericDataProviderService } from './generic-data-provider.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { PossessionListModel } from 'src/app/models/dataListModels/possessionListModel';
import { QueryModel } from 'src/app/models/queryModel';
import { Observable } from 'rxjs';
import { PossessionModel } from 'src/app/models/dataModels/possessionModel';

const getPossessionUrl:string = "https://loc-helper.herokuapp.com/possessions/getShared";
const localGetPossessionUrl: string = "http://localhost:8080/possessions/getShared"
const localAddPossessionUrl: string= "http://localhost:8080/possessions/add";
const addPossessionUrl: string = "https://loc-helper.herokuapp.com/possessions/add";
const localEditPossessionUrl: string = "http://localhost:8080/possessions/edit";
const editPossessionUrl: string = "https://loc-helper.herokuapp.com/possessions/edit";
const localDeletePossessionUrl: string = "http://localhost:8080/possessions/delete";
const deletePossessionUrl: string = "https://loc-helper.herokuapp.com/possessions/delete";
const localSharePossessionUrl: string = "http://localhost:8080/possessions/share";
const sharePossessionUrl: string = "https://loc-helper.herokuapp.com/possessions/share";
const localGiveBackPossessionUrl: string = "http://localhost:8080/possessions/giveBack";
const giveBackPossessionUrl: string = "https://loc-helper.herokuapp.com/possessions/giveBack";
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};


@Injectable({
  providedIn: 'root'
})
export class SharedPossessionService extends GenericDataProviderService{
  constructor(private http: HttpClient){
    super(new PossessionListModel());
  }

  updateList(possessionList: PossessionListModel):void{
    super.updateList(possessionList);
  }

  localGetDatas(queryModel? : QueryModel):Observable<PossessionListModel>{
    return this.http.post<PossessionListModel>(localGetPossessionUrl, queryModel, httpOptions);
  }

  getDatas(queryModel? : QueryModel):Observable<PossessionListModel>{
    return this.http.post<PossessionListModel>(getPossessionUrl, queryModel, httpOptions);
  }

  /**
   * Ne pas utiliser
   * @param possession 
   */
  localAddData(possession: PossessionModel[]):Observable<string>{
    return this.http.post<string>(localAddPossessionUrl, possession, httpOptions);
  }

  /**
   * Ne pas utiliser
   * @param possession 
   */
  addData(possession: PossessionModel[]):Observable<string>{
    return this.http.post<string>(addPossessionUrl, possession, httpOptions);
  }

  /**
   * Ne pas utiliser
   * @param possession 
   */
  localEditData(possession: PossessionModel):Observable<string>{
    return this.http.post<string>(localEditPossessionUrl, possession, httpOptions);
  }

  /**
   * Ne pas utiliser
   * @param possession 
   */
  editData(possession: PossessionModel):Observable<string>{
    return this.http.post<string>(editPossessionUrl, possession, httpOptions);
  }

  /**
   * Ne pas utiliser
   * @param possession 
   */
  localDeleteData(possession: PossessionModel):Observable<string>{
    return this.http.post<string>(localDeletePossessionUrl, possession, httpOptions);
  }

  /**
   * Ne pas utiliser
   * @param possession 
   */
  deleteData(possession: PossessionModel):Observable<string>{
    return this.http.post<string>(deletePossessionUrl, possession, httpOptions);
  }

}
