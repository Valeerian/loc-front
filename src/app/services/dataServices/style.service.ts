import { Injectable } from '@angular/core';
import { GenericDataProviderService } from './generic-data-provider.service';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { StyleListModel } from 'src/app/models/dataListModels/styleListModel';
import { QueryModel } from 'src/app/models/queryModel';
import { Observable } from 'rxjs';
import { StyleModel } from 'src/app/models/dataModels/styleModel';

const getStyleUrl:string = "https://loc-helper.herokuapp.com/style/get";
const localGetStyleUrl: string = "http://localhost:8080/style/get"
const getStyleForPossessionUrl:string = "https://loc-helper.herokuapp.com/style/getForPossessions";
const localGetStyleForPossessionUrl: string = "http://localhost:8080/style/getForPossessions"
const localAddStyleUrl: string= "http://localhost:8080/style/add";
const addStyleUrl: string = "https://loc-helper.herokuapp.com/style/add";
const localEditStyleUrl: string = "http://localhost:8080/style/edit";
const editStyleUrl: string = "https://loc-helper.herokuapp.com/style/edit";
const localDeleteStyleUrl: string = "http://localhost:8080/style/delete";
const deleteStyleUrl: string = "https://loc-helper.herokuapp.com/style/delete";
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class StyleService extends GenericDataProviderService{

  constructor(private http: HttpClient) {
    super(new StyleListModel());
   }

  updateList(colorList: StyleListModel):void{
    super.updateList(colorList);
  }

  localGetDatas(queryModel? : QueryModel):Observable<StyleListModel>{
    return this.http.post<StyleListModel>(localGetStyleUrl, queryModel, httpOptions);
  }

  getDatas(queryModel? : QueryModel):Observable<StyleListModel>{
    return this.http.post<StyleListModel>(getStyleUrl, queryModel, httpOptions);
  }

  localGetStyleForPossession():Observable<StyleListModel>{
    return this.http.get<StyleListModel>(localGetStyleForPossessionUrl, httpOptions);
  }

  getStyleForPossession():Observable<StyleListModel>{
    return this.http.get<StyleListModel>(getStyleForPossessionUrl, httpOptions);
  }

  localAddData(style: StyleModel):Observable<string>{
    return this.http.post<string>(localAddStyleUrl, style, httpOptions);
  }

  addData(style: StyleModel):Observable<string>{
    return this.http.post<string>(addStyleUrl, style, httpOptions);
  }

  localEditData(style: StyleModel):Observable<string>{
    return this.http.post<string>(localEditStyleUrl, style, httpOptions);
  }

  editData(style: StyleModel):Observable<string>{
    return this.http.post<string>(editStyleUrl, style, httpOptions);
  }

  localDeleteData(style: StyleModel):Observable<string>{
    return this.http.post<string>(localDeleteStyleUrl, style, httpOptions);
  }

  deleteData(style: StyleModel):Observable<string>{
    return this.http.post<string>(deleteStyleUrl, style, httpOptions);
  }
}
