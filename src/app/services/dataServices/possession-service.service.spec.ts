import { TestBed } from '@angular/core/testing';

import { PossessionService } from './possession-service.service';

describe('PossessionServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PossessionService = TestBed.get(PossessionService);
    expect(service).toBeTruthy();
  });
});
