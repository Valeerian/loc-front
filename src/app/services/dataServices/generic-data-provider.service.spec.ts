import { TestBed } from '@angular/core/testing';

import { GenericDataProviderService } from './generic-data-provider.service';

describe('GenericDataProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GenericDataProviderService = TestBed.get(GenericDataProviderService);
    expect(service).toBeTruthy();
  });
});
