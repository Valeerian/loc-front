import { Injectable } from '@angular/core';
import { GenericDataProviderService } from './generic-data-provider.service';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { ClotheTypeListModel } from 'src/app/models/dataListModels/clotheTypeListModel';
import { QueryModel } from 'src/app/models/queryModel';
import { Observable } from 'rxjs';
import { ClotheTypeModel } from 'src/app/models/dataModels/clotheTypeModel';

const getClotheTypeUrl:string = "https://loc-helper.herokuapp.com/clotheType/get";
const localGetClotheTypeUrl: string = "http://localhost:8080/clotheType/get"
const getClotheTypeForPossessionUrl:string = "https://loc-helper.herokuapp.com/clotheType/getForPossessions";
const localGetClotheTypeForPossessionUrl: string = "http://localhost:8080/clotheType/getForPossessions"
const localAddClotheTypeUrl: string= "http://localhost:8080/clotheType/add";
const addClotheTypeUrl: string = "https://loc-helper.herokuapp.com/clotheType/add";
const localEditClotheTypeUrl: string = "http://localhost:8080/clotheType/edit";
const editClotheTypeUrl: string = "https://loc-helper.herokuapp.com/clotheType/edit";
const localDeleteClotheTypeUrl: string = "http://localhost:8080/clotheType/delete";
const deleteClotheTypeUrl: string = "https://loc-helper.herokuapp.com/clotheType/delete";
const localGetClotheTypeForSharedPossessionUrl: string = "http://localhost:8080/clotheType/getForSharedPossessions";
const getClotheTypeForSharedPossessionUrl: string = "https://loc-helper.herokuapp.com/clotheType/getForSharedPossessions";

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class ClotheTypeService extends GenericDataProviderService {

  constructor(private http: HttpClient){ 
    super(new ClotheTypeListModel());
  }

  updateList(clotheType : ClotheTypeListModel):void{
    super.updateList(clotheType);
  }

  localGetDatas(queryModel? : QueryModel):Observable<ClotheTypeListModel>{
    return this.http.post<ClotheTypeListModel>(localGetClotheTypeUrl, queryModel, httpOptions);
  }

  getDatas(queryModel? : QueryModel):Observable<ClotheTypeListModel>{
    return this.http.post<ClotheTypeListModel>(getClotheTypeUrl, queryModel, httpOptions);
  }

  localGetClotheTypeForPossession():Observable<ClotheTypeListModel>{
    return this.http.get<ClotheTypeListModel>(localGetClotheTypeForPossessionUrl, httpOptions);
  }

  getClotheTypeForPossession():Observable<ClotheTypeListModel>{
    return this.http.get<ClotheTypeListModel>(getClotheTypeForPossessionUrl, httpOptions);
  }

  localGetClotheTypeForSharedPossession():Observable<ClotheTypeListModel>{
    return this.http.get<ClotheTypeListModel>(localGetClotheTypeForSharedPossessionUrl, httpOptions);
  }

  getClotheTypeForSharedPossession():Observable<ClotheTypeListModel>{
    return this.http.get<ClotheTypeListModel>(getClotheTypeForSharedPossessionUrl, httpOptions);
  }

  localAddData(clotheType : ClotheTypeModel):Observable<string>{
    return this.http.post<string>(localAddClotheTypeUrl, clotheType, httpOptions);
  }

  addData(clotheType: ClotheTypeModel):Observable<string>{
    return this.http.post<string>(addClotheTypeUrl, clotheType, httpOptions);
  }

  localEditData(clotheType: ClotheTypeModel):Observable<string>{
    return this.http.post<string>(localEditClotheTypeUrl, clotheType, httpOptions);
  }

  editData(clotheType: ClotheTypeModel):Observable<string>{
    return this.http.post<string>(editClotheTypeUrl, clotheType, httpOptions);
  }

  localDeleteData(clotheType: ClotheTypeModel):Observable<string>{
    return this.http.post<string>(localDeleteClotheTypeUrl, clotheType, httpOptions);
  }

  deleteData(clotheType: ClotheTypeModel):Observable<string>{
    return this.http.post<string>(deleteClotheTypeUrl, clotheType, httpOptions);
  }

}
