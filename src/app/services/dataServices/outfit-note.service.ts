import { Injectable } from '@angular/core';
import { GenericDataProviderService } from './generic-data-provider.service';
import { OutfitNoteListModel } from 'src/app/models/dataListModels/outfitNoteListModel';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { QueryModel } from 'src/app/models/queryModel';
import { Observable } from 'rxjs';
import { OutfitNoteModel } from 'src/app/models/dataModels/outfitNoteModel';

const getOutfitNoteUrl:string ="https://loc-helper.herokuapp.com/outfitNotes/add";
const localGetOutfitNoteUrl:string="http://localhost:8080/outfitNotes/get";
const getMyCommentUrl:string ="https://loc-helper.herokuapp.com/outfitNotes/getMine";
const localGetMyCommentUrl:string="http://localhost:8080/outfitNotes/getMine";
const localAddOutfitNoteUrl: string= "http://localhost:8080/outfitNotes/createOrUpdate";
const addOutfitNoteUrl: string = "https://loc-helper.herokuapp.com/outfitNotes/createOrUpdate";
const localEditOutfitNoteUrl: string = "http://localhost:8080/outfitNotes/edit";
const editOutfitNoteUrl: string = "https://loc-helper.herokuapp.com/outfitNotes/edit";
const localDeleteOutfitNoteUrl: string = "http://localhost:8080/outfitNotes/delete";
const deleteOutfitNoteUrl: string = "https://loc-helper.herokuapp.com/outfitNotes/delete";
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};
@Injectable({
  providedIn: 'root'
})
export class OutfitNoteService extends GenericDataProviderService {
  constructor(private http: HttpClient) {
    super(new OutfitNoteListModel());
   }

   updateList(outfitNoteList: OutfitNoteListModel):void{
     super.updateList(outfitNoteList);
   }

   localGetDatas(queryModel? : QueryModel):Observable<OutfitNoteListModel>{
     
     if(queryModel.values.length < 1){
       queryModel.addCriteria("outfitId",":","1");
     }
    return this.http.post<OutfitNoteListModel>(localGetOutfitNoteUrl, queryModel, httpOptions);
  }

  localGetMyComment(queryModel?: QueryModel): Observable<OutfitNoteModel>{
    if(queryModel.values.length < 1){
      queryModel.addCriteria("outfitId",":","1");
    }
    return this.http.post<OutfitNoteModel>(localGetMyCommentUrl,queryModel, httpOptions);
  }

  getMyComment(queryModel?: QueryModel): Observable<OutfitNoteModel>{
    if(queryModel.values.length < 1){
      queryModel.addCriteria("outfitId",":","1");
    } 
    return this.http.post<OutfitNoteModel>(getMyCommentUrl,queryModel, httpOptions);
  }

  getDatas(queryModel?: QueryModel): Observable<OutfitNoteListModel>{
    if(queryModel.values.length < 1){
      queryModel.addCriteria("outfitId",":","1");
    }
    return this.http.post<OutfitNoteListModel>(getOutfitNoteUrl, queryModel, httpOptions);
  }

  localAddData(outfitNote: OutfitNoteModel):Observable<string>{
    return this.http.post<string>(localAddOutfitNoteUrl, outfitNote, httpOptions);
  }

  addData(outfitNote: OutfitNoteModel):Observable<string>{
    return this.http.post<string>(addOutfitNoteUrl, outfitNote, httpOptions);
  }

  localEditData(outfitNote: OutfitNoteModel):Observable<string>{
    return this.http.post<string>(localEditOutfitNoteUrl, outfitNote, httpOptions);
  }

  editData(outfitNote: OutfitNoteModel):Observable<string>{
    return this.http.post<string>(editOutfitNoteUrl, outfitNote, httpOptions);
  }

  localDeleteData(outfitNote: OutfitNoteModel):Observable<string>{
    return this.http.post<string>(localDeleteOutfitNoteUrl, outfitNote, httpOptions);
  }

  deleteData(outfitNote: OutfitNoteModel):Observable<string>{
    return this.http.post<string>(deleteOutfitNoteUrl, outfitNote, httpOptions);
  }
}
