import { Injectable } from '@angular/core';
import { ColorListModel } from 'src/app/models/dataListModels/colorListModel';
import { Subject, Observable } from 'rxjs';
import { ColorModel } from 'src/app/models/dataModels/colorModel';
import { QueryModel } from 'src/app/models/queryModel';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GenericDataProviderService } from './generic-data-provider.service';

const getColorUrl:string = "https://loc-helper.herokuapp.com/color/get";
const localGetColorUrl: string = "http://localhost:8080/color/get"
const getColorForPossessionsUrl:string = "https://loc-helper.herokuapp.com/color/getForPossessions";
const localGetColorForPossessionsUrl: string = "http://localhost:8080/color/getForPossessions"
const localAddColorUrl: string= "http://localhost:8080/color/add";
const addColorUrl: string = "https://loc-helper.herokuapp.com/color/add";
const localEditColorUrl: string = "http://localhost:8080/color/edit";
const editColorUrl: string = "https://loc-helper.herokuapp.com/color/edit";
const localDeleteColorUrl: string = "http://localhost:8080/color/delete";
const deleteColorUrl: string = "https://loc-helper.herokuapp.com/color/delete";
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class ColorService  extends GenericDataProviderService{
  

  constructor(private http: HttpClient) {
    super(new ColorListModel());
   }

  updateList(colorList: ColorListModel):void{
    super.updateList(colorList);
  }

  localGetDatas(queryModel? : QueryModel):Observable<ColorListModel>{
    return this.http.post<ColorListModel>(localGetColorUrl, queryModel, httpOptions);
  }

  getDatas(queryModel? : QueryModel):Observable<ColorListModel>{
    return this.http.post<ColorListModel>(getColorUrl, queryModel, httpOptions);
  }
  
  localGetColorForPossession():Observable<ColorListModel>{
    return this.http.get<ColorListModel>(localGetColorForPossessionsUrl, httpOptions);
  }

  getColorForPossession():Observable<ColorListModel>{
    return this.http.get<ColorListModel>(getColorForPossessionsUrl,  httpOptions);
  }

  localAddData(color: ColorModel):Observable<string>{
    return this.http.post<string>(localAddColorUrl, color, httpOptions);
  }

  addData(color: ColorModel):Observable<string>{
    return this.http.post<string>(addColorUrl, color, httpOptions);
  }

  localEditData(color: ColorModel):Observable<string>{
    return this.http.post<string>(localEditColorUrl, color, httpOptions);
  }

  editData(color: ColorModel):Observable<string>{
    return this.http.post<string>(editColorUrl, color, httpOptions);
  }

  localDeleteData(color: ColorModel):Observable<string>{
    return this.http.post<string>(localDeleteColorUrl, color, httpOptions);
  }

  deleteData(color: ColorModel):Observable<string>{
    return this.http.post<string>(deleteColorUrl, color, httpOptions);
  }

}
