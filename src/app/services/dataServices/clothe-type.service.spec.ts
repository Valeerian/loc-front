import { TestBed } from '@angular/core/testing';

import { ClotheTypeService } from './clothe-type.service';

describe('ClotheTypeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClotheTypeService = TestBed.get(ClotheTypeService);
    expect(service).toBeTruthy();
  });
});
