import { Injectable } from '@angular/core';
import { GenericDataProviderService } from './generic-data-provider.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { OutfitListModel } from 'src/app/models/dataListModels/outfitListModel';
import { QueryModel } from 'src/app/models/queryModel';
import { Observable } from 'rxjs';
import { OutfitModel } from 'src/app/models/dataModels/outfitModel';

const getOutfitUrl:string = "https://loc-helper.herokuapp.com/outfits/get";
const localGetOutfitUrl: string = "http://localhost:8080/outfits/get"
const localAddOutfitUrl: string= "http://localhost:8080/outfits/add";
const addOutfitUrl: string = "https://loc-helper.herokuapp.com/outfits/add";
const localEditOutfitUrl: string = "http://localhost:8080/outfits/edit";
const editOutfitUrl: string = "https://loc-helper.herokuapp.com/outfits/edit";
const localDeleteOutfitUrl: string = "http://localhost:8080/outfits/delete";
const deleteOutfitUrl: string = "https://loc-helper.herokuapp.com/outfits/delete";
const getOutfitsForSelectUrl:string = "https://loc-helper.herokuapp.com/outfits/getForSelect";
const localGetOutfitsForSelectUrl: string = "http://localhost:8080/outfits/getForSelect"
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class OutfitService extends GenericDataProviderService {
  constructor(private http: HttpClient) {
    super(new OutfitListModel());
   }

   updateList(outfitList: OutfitListModel):void{
    super.updateList(outfitList);
  }

  localGetDatas(queryModel? : QueryModel):Observable<OutfitListModel>{
   return this.http.post<OutfitListModel>(localGetOutfitUrl, queryModel, httpOptions);
 }

 getDatas(queryModel? : QueryModel):Observable<OutfitListModel>{
   return this.http.post<OutfitListModel>(getOutfitUrl, queryModel, httpOptions);
 }

 localAddData(outfit: OutfitModel):Observable<string>{
   return this.http.post<string>(localAddOutfitUrl, outfit, httpOptions);
 }

 addData(outfit: OutfitModel):Observable<string>{
   return this.http.post<string>(addOutfitUrl, outfit, httpOptions);
 }

 localGetForSelect():Observable<OutfitListModel>{
   return this.http.get<OutfitListModel>(localGetOutfitsForSelectUrl, httpOptions);
 }

 getForSelect():Observable<OutfitListModel>{
  return this.http.get<OutfitListModel>(getOutfitsForSelectUrl, httpOptions);
}

 localEditData(outfit: OutfitModel):Observable<string>{
   return this.http.post<string>(localEditOutfitUrl, outfit, httpOptions);
 }

 editData(outfit: OutfitModel):Observable<string>{
   return this.http.post<string>(editOutfitUrl, outfit, httpOptions);
 }

 localDeleteData(outfit: OutfitModel):Observable<string>{
   return this.http.post<string>(localDeleteOutfitUrl, outfit, httpOptions);
 }

 deleteData(outfit: OutfitModel):Observable<string>{
   return this.http.post<string>(deleteOutfitUrl, outfit, httpOptions);
 }

}
