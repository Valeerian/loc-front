import { TestBed } from '@angular/core/testing';

import { ClotheNoteService } from './clothe-note.service';

describe('ClotheNoteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClotheNoteService = TestBed.get(ClotheNoteService);
    expect(service).toBeTruthy();
  });
});
