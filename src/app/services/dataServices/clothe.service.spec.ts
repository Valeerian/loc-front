import { TestBed } from '@angular/core/testing';

import { ClotheService } from './clothe.service';

describe('ClotheService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClotheService = TestBed.get(ClotheService);
    expect(service).toBeTruthy();
  });
});
