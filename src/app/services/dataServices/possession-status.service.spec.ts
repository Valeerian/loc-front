import { TestBed } from '@angular/core/testing';

import { PossessionStatusService } from './possession-status.service';

describe('PossessionStatusService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PossessionStatusService = TestBed.get(PossessionStatusService);
    expect(service).toBeTruthy();
  });
});
