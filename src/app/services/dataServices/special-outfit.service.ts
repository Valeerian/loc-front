import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { OutfitListModel } from 'src/app/models/dataListModels/outfitListModel';
import { QueryModel } from 'src/app/models/queryModel';
import { Observable } from 'rxjs';
import { OutfitModel } from 'src/app/models/dataModels/outfitModel';
import { GenericDataProviderService } from './generic-data-provider.service';

const getOutfitUrl:string = "https://loc-helper.herokuapp.com/outfits/getForPublicOrFriendsList";
const localGetOutfitUrl: string = "http://localhost:8080/outfits/getForPublicOrFriendsList"
const localAddOutfitUrl: string= "http://localhost:8080/outfits/add";
const addOutfitUrl: string = "https://loc-helper.herokuapp.com/outfits/add";
const localEditOutfitUrl: string = "http://localhost:8080/outfits/edit";
const editOutfitUrl: string = "https://loc-helper.herokuapp.com/outfits/edit";
const localDeleteOutfitUrl: string = "http://localhost:8080/outfits/delete";
const deleteOutfitUrl: string = "https://loc-helper.herokuapp.com/outfits/delete";
const getOutfitsForSelectUrl:string = "https://loc-helper.herokuapp.com/outfits/getForSelect";
const localGetOutfitsForSelectUrl: string = "http://localhost:8080/outfits/getForSelect"
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class SpecialOutfitService extends GenericDataProviderService{
  constructor(private http: HttpClient) {
    super(new OutfitListModel());
   }

   updateList(outfitList: OutfitListModel):void{
    super.updateList(outfitList);
  }

  localGetDatas(queryModel? : QueryModel):Observable<OutfitListModel>{
   return this.http.post<OutfitListModel>(localGetOutfitUrl, queryModel, httpOptions);
 }

 getDatas(queryModel? : QueryModel):Observable<OutfitListModel>{
   return this.http.post<OutfitListModel>(getOutfitUrl, queryModel, httpOptions);
 }

 /**
  * 
  * @param outfit Ne pas utiliser
  */
 localAddData(outfit: OutfitModel):Observable<string>{
   return null;
 }

 addData(outfit: OutfitModel):Observable<string>{
  return null;
 }

 localGetForSelect():Observable<OutfitListModel>{
  return null;
 }

 getForSelect():Observable<OutfitListModel>{
  return null;
}

 localEditData(outfit: OutfitModel):Observable<string>{
  return null;
 }

 editData(outfit: OutfitModel):Observable<string>{
  return null;
 }

 localDeleteData(outfit: OutfitModel):Observable<string>{
  return null;
 }

 deleteData(outfit: OutfitModel):Observable<string>{
  return null;
 }
}
