import { Injectable } from '@angular/core';
import { GenericDataProviderService } from './generic-data-provider.service';
import { FriendshipListModel } from 'src/app/models/dataListModels/friendshipListModel';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { QueryModel } from 'src/app/models/queryModel';
import { Observable } from 'rxjs';
import { FriendshipModel } from 'src/app/models/dataModels/friendshipModel';
import { UserListModel } from 'src/app/models/dataListModels/userListModel';


const getFriendshipUrl:string = "https://loc-helper.herokuapp.com/friendship/get";
const localGetFriendshipUrl: string = "http://localhost:8080/friendship/get"
const localAddFriendshipUrl: string= "http://localhost:8080/friendship/add";
const addFriendshipUrl: string = "https://loc-helper.herokuapp.com/friendship/add";
const localEditFriendshipUrl: string = "http://localhost:8080/friendship/edit";
const editFriendshipUrl: string = "https://loc-helper.herokuapp.com/friendship/edit";
const localDeleteFriendshipUrl: string = "http://localhost:8080/friendship/delete";
const deleteFriendshipUrl: string = "https://loc-helper.herokuapp.com/friendship/delete";
const getMyFriendsUrl: string = "https://loc-helper.herokuapp.com/friendship/getMyFriends";
const localGetMyFriendsUrl: string = "http://localhost:8080/friendship/getMyFriends";
const localGetOwnersUrl: string = "http://localhost:8080/friendship/getForPossessions";
const getOwnersUrl:string = "https://loc-helper.herokuapp.com/friendship/getForPossessions";
const localGetWearersUrl: string = "http://localhost:8080/friendship/getForSharedPossessions";
const getWearersUrl:string = "https://loc-helper.herokuapp.com/friendship/getForSharedPossessions";
const localGetOnHoldFriendshipsUrl: string ="http://localhost:8080/friendship/getOnHold";
const getOnHoldFriendshipsUrl: string = "https://loc-helper.herokuapp.com/friendship/getOnHold";
const localGetFriendshipsToConfirmUrl: string="http://localhost:8080/friendship/getToConfirm";
const getFriendshipsToConfirmUrl: string = "https://loc-helper.herokuapp.com/friendship/getToConfirm";
const localSendFriendshipRequestUrl: string  = "http://localhost:8080/friendship/addFriend";
const sendFriendshipRequestUrl: string = "https://loc-helper.herokuapp.com/friendship/addFriend";
const localAcceptFriendshipRequestUrl: string  = "http://localhost:8080/friendship/acceptFriendshipRequest";
const acceptFriendshipRequestUrl: string = "https://loc-helper.herokuapp.com/friendship/acceptFriendshipRequest";
const localRejectFriendshipRequestUrl: string  = "http://localhost:8080/friendship/rejectFriendshipRequest";
const rejectFriendshipRequestUrl: string = "https://loc-helper.herokuapp.com/friendship/rejectFriendshipRequest";
const localCancelFriendshipRequestUrl: string  = "http://localhost:8080/friendship/cancelFriendshipRequest";
const cancelFriendshipRequestUrl: string = "https://loc-helper.herokuapp.com/friendship/cancelFriendshipRequest";

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class FriendshipService extends GenericDataProviderService{
  constructor(private http: HttpClient) {
    super(new FriendshipListModel());
   }

  updateList(friendshipList: FriendshipListModel):void{
    super.updateList(friendshipList);
  }

  localGetOwners():Observable<UserListModel>{
    return this.http.get<UserListModel>(localGetOwnersUrl, httpOptions);
  }

  localGetFriendshipsOnHold():Observable<FriendshipListModel>{
    return this.http.get<FriendshipListModel>(localGetOnHoldFriendshipsUrl);
  }

  getFriendshipsOnHold():Observable<FriendshipListModel>{
    return this.http.get<FriendshipListModel>(getOnHoldFriendshipsUrl);
  }

  localGetFriendshipsToConfirm():Observable<FriendshipListModel>{
    return this.http.get<FriendshipListModel>(localGetFriendshipsToConfirmUrl);
  }

  getFriendshipsToConfirm():Observable<FriendshipListModel>{
    return this.http.get<FriendshipListModel>(getFriendshipsToConfirmUrl);
  }

  getOwners():Observable<UserListModel>{
    return this.http.get<UserListModel>(getOwnersUrl, httpOptions);
  }

  localGetWearers():Observable<UserListModel>{
    return this.http.get<UserListModel>(localGetWearersUrl, httpOptions);
  }

  getWearers():Observable<UserListModel>{
    return this.http.get<UserListModel>(getWearersUrl, httpOptions);
  }

  localGetDatas(queryModel? : QueryModel):Observable<FriendshipListModel>{
    return this.http.post<FriendshipListModel>(localGetFriendshipUrl, queryModel, httpOptions);
  }

  localGetMyFriends(): Observable<UserListModel>{
    return this.http.get<UserListModel>(localGetMyFriendsUrl, httpOptions);
  }

  localSendRequest(userName: string):Observable<String>{
    return this.http.post<String>(localSendFriendshipRequestUrl, userName, httpOptions);
  }

  sendRequest(userName: string):Observable<String>{
    return this.http.post<String>(sendFriendshipRequestUrl, userName, httpOptions);
  }

  localAcceptFriendshipRequest(friendship: FriendshipModel):Observable<String>{
    return this.http.post<String>(localAcceptFriendshipRequestUrl, friendship, httpOptions);
  }

  acceptFriendshipRequest(friendship: FriendshipModel):Observable<String>{
    return this.http.post<String>(acceptFriendshipRequestUrl, friendship, httpOptions);
  }

  localCancelFriendshipRequest(friendship: FriendshipModel):Observable<String>{
    return this.http.post<String>(localCancelFriendshipRequestUrl, friendship, httpOptions);
  }

  cancelFriendshipRequest(friendship: FriendshipModel):Observable<String>{
    return this.http.post<String>(cancelFriendshipRequestUrl, friendship, httpOptions);
  }

  localRejectFriendshipRequest(friendship: FriendshipModel):Observable<String>{
    return this.http.post<String>(localRejectFriendshipRequestUrl, friendship, httpOptions);
  }

  rejecttFriendshipRequest(friendship: FriendshipModel):Observable<String>{
    return this.http.post<String>(rejectFriendshipRequestUrl, friendship, httpOptions);
  }

  getMyFriends(): Observable<UserListModel>{
    return this.http.get<UserListModel>(getMyFriendsUrl, httpOptions);
  }

  getDatas(queryModel? : QueryModel):Observable<FriendshipListModel>{
    return this.http.post<FriendshipListModel>(getFriendshipUrl, queryModel, httpOptions);
  }

  localAddData(friendship: FriendshipModel):Observable<string>{
    return this.http.post<string>(localAddFriendshipUrl, friendship, httpOptions);
  }

  addData(friendship: FriendshipModel):Observable<string>{
    return this.http.post<string>(addFriendshipUrl, friendship, httpOptions);
  }

  localEditData(friendship: FriendshipModel):Observable<string>{
    return this.http.post<string>(localEditFriendshipUrl, friendship, httpOptions);
  }

  editData(friendship: FriendshipModel):Observable<string>{
    return this.http.post<string>(editFriendshipUrl, friendship, httpOptions);
  }

  localDeleteData(friendship: FriendshipModel):Observable<string>{
    return this.http.post<string>(localDeleteFriendshipUrl, friendship, httpOptions);
  }

  deleteData(friendship: FriendshipModel):Observable<string>{
    return this.http.post<string>(deleteFriendshipUrl, friendship, httpOptions);
  }
}
