import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { GenericDataProviderService } from './generic-data-provider.service';
import { ThemeListModel } from 'src/app/models/dataListModels/themeListModel';
import { QueryModel } from 'src/app/models/queryModel';
import { Observable } from 'rxjs';
import { ThemeModel } from 'src/app/models/dataModels/themeModel';

const getThemeUrl:string = "https://loc-helper.herokuapp.com/theme/get";
const localGetThemeUrl: string = "http://localhost:8080/theme/get"
const getThemeForPossessionUrl:string = "https://loc-helper.herokuapp.com/theme/getForPossessions";
const localGetThemeForPossessionUrl: string = "http://localhost:8080/theme/getForPossessions"
const localAddThemeUrl: string= "http://localhost:8080/theme/add";
const addThemeUrl: string = "https://loc-helper.herokuapp.com/theme/add";
const localEditThemeUrl: string = "http://localhost:8080/theme/edit";
const editThemeUrl: string = "https://loc-helper.herokuapp.com/theme/edit";
const localDeleteThemeUrl: string = "http://localhost:8080/theme/delete";
const deleteThemeUrl: string = "https://loc-helper.herokuapp.com/theme/delete";
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class ThemeService extends GenericDataProviderService{

  constructor(private http: HttpClient) {
    super(new ThemeListModel());
   }

  updateList(colorList: ThemeListModel):void{
    super.updateList(colorList);
  }

  localGetDatas(queryModel? : QueryModel):Observable<ThemeListModel>{
    return this.http.post<ThemeListModel>(localGetThemeUrl, queryModel, httpOptions);
  }

  getDatas(queryModel? : QueryModel):Observable<ThemeListModel>{
    return this.http.post<ThemeListModel>(getThemeUrl, queryModel, httpOptions);
  }

  localGetThemeForPossession():Observable<ThemeListModel>{
    return this.http.get<ThemeListModel>(localGetThemeForPossessionUrl, httpOptions);
  }

  getThemeForPossession():Observable<ThemeListModel>{
    return this.http.get<ThemeListModel>(getThemeForPossessionUrl,  httpOptions);
  }

  localAddData(theme: ThemeModel):Observable<string>{
    return this.http.post<string>(localAddThemeUrl, theme, httpOptions);
  }

  addData(theme: ThemeModel):Observable<string>{
    return this.http.post<string>(addThemeUrl, theme, httpOptions);
  }

  localEditData(theme: ThemeModel):Observable<string>{
    return this.http.post<string>(localEditThemeUrl, theme, httpOptions);
  }

  editData(theme: ThemeModel):Observable<string>{
    return this.http.post<string>(editThemeUrl, theme, httpOptions);
  }

  localDeleteData(theme: ThemeModel):Observable<string>{
    return this.http.post<string>(localDeleteThemeUrl, theme, httpOptions);
  }

  deleteData(theme: ThemeModel):Observable<string>{
    return this.http.post<string>(deleteThemeUrl, theme, httpOptions);
  }
}
