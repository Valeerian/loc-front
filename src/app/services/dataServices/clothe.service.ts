import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { GenericDataProviderService } from './generic-data-provider.service';
import { ClotheListModel } from 'src/app/models/dataListModels/clotheListModel';
import { QueryModel } from 'src/app/models/queryModel';
import { Observable } from 'rxjs';
import { ClotheModel } from 'src/app/models/dataModels/clotheModel';

const getClotheUrl:string = "https://loc-helper.herokuapp.com/clothes/get";
const localGetClotheUrl: string = "http://localhost:8080/clothes/get"
const localAddClotheUrl: string= "http://localhost:8080/clothes/add";
const addClotheUrl: string = "https://loc-helper.herokuapp.com/clothes/add";
const localEditClotheUrl: string = "http://localhost:8080/clothes/edit";
const editClotheUrl: string = "https://loc-helper.herokuapp.com/clothes/edit";
const localDeleteClotheUrl: string = "http://localhost:8080/clothes/delete";
const deleteClotheUrl: string = "https://loc-helper.herokuapp.com/clothes/delete";
const localGetClotheForPossessionUrl: string = "http://localhost:8080/clothes/getForPossessions";
const getClotheForPossessionUrl: string = "https://loc-helper.herokuapp.com/clothes/getForPossessions";
const localGetClotheForSharedPossessionUrl: string = "http://localhost:8080/clothes/getForSharedPossessions";
const getClotheForSharedPossessionUrl: string = "https://loc-helper.herokuapp.com/clothes/getForSharedPossessions";
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class ClotheService extends GenericDataProviderService {

  constructor(private http: HttpClient) {
    super(new ClotheListModel());
   }


   updateList(colorList: ClotheListModel):void{
    super.updateList(colorList);
  }

  localGetDatas(queryModel? : QueryModel):Observable<ClotheListModel>{
    return this.http.post<ClotheListModel>(localGetClotheUrl, queryModel, httpOptions);
  }

  getDatas(queryModel? : QueryModel):Observable<ClotheListModel>{
    return this.http.post<ClotheListModel>(getClotheUrl, queryModel, httpOptions);
  }

  localGetClotheForPossession():Observable<ClotheListModel>{
    return this.http.get<ClotheListModel>(localGetClotheForPossessionUrl, httpOptions);
  }

  getClotheForPossession():Observable<ClotheListModel>{
    return this.http.get<ClotheListModel>(getClotheForPossessionUrl, httpOptions);
  }

  localGetClotheForSharedPossession():Observable<ClotheListModel>{
    return this.http.get<ClotheListModel>(localGetClotheForSharedPossessionUrl, httpOptions);
  }

  getClotheForSharedPossession():Observable<ClotheListModel>{
    return this.http.get<ClotheListModel>(getClotheForSharedPossessionUrl, httpOptions);
  }

  localAddData(color: ClotheModel):Observable<string>{
    return this.http.post<string>(localAddClotheUrl, color, httpOptions);
  }

  addData(color: ClotheModel):Observable<string>{
    return this.http.post<string>(addClotheUrl, color, httpOptions);
  }

  localEditData(color: ClotheModel):Observable<string>{
    return this.http.post<string>(localEditClotheUrl, color, httpOptions);
  }

  editData(color: ClotheModel):Observable<string>{
    return this.http.post<string>(editClotheUrl, color, httpOptions);
  }

  localDeleteData(color: ClotheModel):Observable<string>{
    return this.http.post<string>(localDeleteClotheUrl, color, httpOptions);
  }

  deleteData(color: ClotheModel):Observable<string>{
    return this.http.post<string>(deleteClotheUrl, color, httpOptions);
  }



}
