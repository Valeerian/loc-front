import { Injectable } from '@angular/core';
import { ColorListModel } from 'src/app/models/dataListModels/colorListModel';
import { Subject, Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { QueryModel } from 'src/app/models/queryModel';
import { SizeModel } from 'src/app/models/dataModels/sizeModel';
import { SizeListModel } from 'src/app/models/dataListModels/sizeListModel';
import { GenericDataProviderService } from './generic-data-provider.service';

const getSizesUrl: string = 'https://loc-helper.herokuapp.com/size/get';
const localGetSizesUrl:string = 'http://localhost:8080/size/get';
const getSizesForPossessionUrl: string = 'https://loc-helper.herokuapp.com/size/getForPossessions';
const localGetSizesForPossessionUrl:string = 'http://localhost:8080/size/getForPossessions';
const deleteSizeUrl: string='https://loc-helper.herokuapp.com/size/delete';
const localDeleteSizeUrl: string='http://localhost:8080/size/delete';
const editSizeUrl:string = 'https://loc-helper.herokuapp.com/size/edit';
const localEditSizeUrl:string = 'http://localhost:8080/size/edit';
const addSizeUrl: string= 'https://loc-helper.herokuapp.com/size/add';
const localAddSizeUrl:string = 'http://localhost:8080/size/add';
 
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class SizesService extends GenericDataProviderService{  
  constructor(private http: HttpClient) {
    super(new SizeListModel());
  }

  updateList(sizeListModel: SizeListModel):void{
    super.updateList(sizeListModel);
  }

  

  localGetDatas(queryModel? : QueryModel):Observable<SizeListModel>{
      return this.http.post<SizeListModel>(localGetSizesUrl, queryModel, httpOptions);
  }

  getDatas(queryModel? : QueryModel):Observable<SizeListModel>{
    return this.http.post<SizeListModel>(getSizesUrl, queryModel, httpOptions);
  }

  localGetSizeForPossession():Observable<SizeListModel>{
    return this.http.get<SizeListModel>(localGetSizesForPossessionUrl, httpOptions);
  }

  getSizeForPossession():Observable<SizeListModel>{
    return this.http.get<SizeListModel>(getSizesForPossessionUrl, httpOptions);
  }

  localDeleteData(sizeModel: SizeModel):Observable<string>{
    return this.http.post<string>(localDeleteSizeUrl, sizeModel, httpOptions);
  }

  deleteData(sizeModel: SizeModel):Observable<string>{
    return this.http.post<string>(deleteSizeUrl, sizeModel, httpOptions);
  }

  localEditData(sizeModel: SizeModel):Observable<string>{
    return this.http.post<string>(localEditSizeUrl, sizeModel, httpOptions);
  }

  editData(sizeModel: SizeModel):Observable<string>{
    return this.http.post<string>(editSizeUrl, sizeModel, httpOptions);
  }

  localAddData(sizeModel: SizeModel):Observable<string>{
    return this.http.post<string>(localAddSizeUrl, sizeModel, httpOptions);
  }

  addData(sizeModel: SizeModel):Observable<string>{
    return this.http.post<string>(addSizeUrl, sizeModel, httpOptions);
  }
}
