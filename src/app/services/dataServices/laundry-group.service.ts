import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GenericDataProviderService } from './generic-data-provider.service';
import { QueryModel } from 'src/app/models/queryModel';
import { Observable } from 'rxjs';
import { LaundryGroupModel } from 'src/app/models/dataModels/laundryGroupModel';
import { LaundryGroupListModel } from 'src/app/models/dataListModels/LaundryGroupListModel';
import { UserModel } from 'src/app/models/dataModels/userModel';
import { UserListModel } from 'src/app/models/dataListModels/userListModel';

const getLaundryGroupModelUrl:string = "https://loc-helper.herokuapp.com/laundryGroups/get";
const localGetLaundryGroupModelUrl: string = "http://localhost:8080/laundryGroups/get"
const localAddLaundryGroupModelUrl: string= "http://localhost:8080/laundryGroups/add";
const addLaundryGroupModelUrl: string = "https://loc-helper.herokuapp.com/laundryGroups/add";
const localEditLaundryGroupModelUrl: string = "http://localhost:8080/laundryGroups/edit";
const editLaundryGroupModelUrl: string = "https://loc-helper.herokuapp.com/laundryGroups/edit";
const localDeleteLaundryGroupModelUrl: string = "http://localhost:8080/laundryGroups/delete";
const deleteLaundryGroupModelUrl: string = "https://loc-helper.herokuapp.com/laundryGroups/delete";
const localGetCommonLaundryGroupsUrl: string = "http://localhost:8080/laundryGroups/getCommonLaundryGroups";
const getCommonLaundryGroupsUrl: string = "https://loc-helper.herokuapp.com/laundryGroups/getCommonLaundryGroups";
const localGetPotentialGroupMembersUrl: string = "http://localhost:8080/laundryGroups/getPotentialGroupMembers"
const getPotentialGroupMembersUrl: string = "https://loc-helper.herokuapp.com/laundryGroups/getPotentialGroupMembers"
const localQuitUrl: string = "http://localhost:8080/laundryGroups/quit";
const quitUrl: string = "https://loc-helper.herokuapp.com/laundryGroups/quit";
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class LaundryGroupService extends GenericDataProviderService{

  constructor(private http:HttpClient) {
    super(new LaundryGroupListModel());
   }

   updateList(possessionList: LaundryGroupListModel):void{
    super.updateList(possessionList);
  }

  localGetDatas(queryModel? : QueryModel): Observable<LaundryGroupListModel>{
    return this.http.post<LaundryGroupListModel>(localGetLaundryGroupModelUrl, queryModel, httpOptions);
  }

  localGetCommonLaundryGroups(userModel: UserModel): Observable<LaundryGroupListModel>{
    return this.http.post<LaundryGroupListModel>(localGetCommonLaundryGroupsUrl, userModel, httpOptions);
  }

  getCommonLaundryGroups(userModel: UserModel): Observable<LaundryGroupListModel>{
    return this.http.post<LaundryGroupListModel>(getCommonLaundryGroupsUrl, userModel, httpOptions);
  }

  localQuit(laundryGroupModel: LaundryGroupModel): Observable<String>{
    return this.http.post<String>(localQuitUrl, laundryGroupModel, httpOptions)
  }

  quit(laundryGroupModel: LaundryGroupModel): Observable<String>{
    return this.http.post<String>(quitUrl, laundryGroupModel, httpOptions)
  }

  localGetPotentialGroupMembers(): Observable<UserListModel>{
    return this.http.get<UserListModel>(localGetPotentialGroupMembersUrl, httpOptions);
  }

  getPotentialGroupMembers(): Observable<UserListModel>{
    return this.http.get<UserListModel>(getPotentialGroupMembersUrl, httpOptions);
  }

  getDatas(queryModel? : QueryModel):Observable<LaundryGroupListModel>{
    return this.http.post<LaundryGroupListModel>(getLaundryGroupModelUrl, queryModel, httpOptions);
  }

  localAddData(laundryGroupModel: LaundryGroupModel):Observable<string>{
    return this.http.post<string>(localAddLaundryGroupModelUrl, laundryGroupModel, httpOptions);
  }

  addData(laundryGroupModel: LaundryGroupModel):Observable<string>{
    return this.http.post<string>(addLaundryGroupModelUrl, laundryGroupModel, httpOptions);
  }

  localEditData(laundryGroupModel: LaundryGroupModel):Observable<string>{
    return this.http.post<string>(localEditLaundryGroupModelUrl, laundryGroupModel, httpOptions);
  }

  editData(laundryGroupModel: LaundryGroupModel):Observable<string>{
    return this.http.post<string>(editLaundryGroupModelUrl, laundryGroupModel, httpOptions);
  }

  localDeleteData(possession: LaundryGroupModel):Observable<string>{
    return this.http.post<string>(localDeleteLaundryGroupModelUrl, possession, httpOptions);
  }

  deleteData(laundryGroupModel: LaundryGroupModel):Observable<string>{
    return this.http.post<string>(deleteLaundryGroupModelUrl, laundryGroupModel, httpOptions);
  }
}
