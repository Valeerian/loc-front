import { Injectable } from '@angular/core';
import * as moment from 'moment';

const TOKEN_KEY : string = 'AuthToken';
const USER_NAME_KEY : string ='AuthUsername';
const USER_ID_KEY : string = 'AuthId';
const FIRST_NAME_KEY : string = "AuthFirstName";
const LAST_NAME_KEY : string = "AuthLastName";
const MAIL_ADDRESS_KEY : string = "AuthMailAddress";
const SEX_KEY : string = "AuthSex";
const ROLES_KEY: string = "AuthRoles";
const TOKEN_EXPIRATION_KEY: string = "AuthExpiration";
const TOKEN_ISSUED_AT_KEY: string = "AuthIssuedAt";


@Injectable({
  providedIn: 'root'
})
export class TokenStorageService {

  private roles: string[] = [];
  constructor() { }

  public getBearerToken():string{
    return 'Bearer ' + window.localStorage.getItem(TOKEN_KEY)
  }

  public empty():void{
    window.localStorage.removeItem(TOKEN_EXPIRATION_KEY);
    window.localStorage.removeItem(TOKEN_ISSUED_AT_KEY);
    window.localStorage.removeItem(TOKEN_KEY);
    window.localStorage.removeItem(USER_NAME_KEY);
    window.localStorage.removeItem(USER_ID_KEY);
    window.localStorage.removeItem(FIRST_NAME_KEY);
    window.localStorage.removeItem(LAST_NAME_KEY);
    window.localStorage.removeItem(MAIL_ADDRESS_KEY);
    window.localStorage.removeItem(SEX_KEY);
    window.localStorage.removeItem(ROLES_KEY);
  }

  public signOut(){
    window.localStorage.removeItem(TOKEN_KEY);
  }

  public saveSex(sex : string):void {
    window.localStorage.removeItem(SEX_KEY);
    window.localStorage.setItem(SEX_KEY, sex);
  }

  public getSex(): string {
    return window.localStorage.getItem(SEX_KEY);
  }

  public saveMailAddress(mail : string): void {
    window.localStorage.removeItem(MAIL_ADDRESS_KEY);
    window.localStorage.setItem(MAIL_ADDRESS_KEY, mail);
  }

  public getMailAddress() :string{
    return window.localStorage.getItem(MAIL_ADDRESS_KEY);
  }

  public saveLastName(lastName : string):void{
    window.localStorage.removeItem(LAST_NAME_KEY);
    window.localStorage.setItem(LAST_NAME_KEY, lastName);
  }

  public getLastName():string{
    return window.localStorage.getItem(LAST_NAME_KEY);
  }

  public saveToken(token : string): void{
    window.localStorage.removeItem(TOKEN_KEY);
    window.localStorage.setItem(TOKEN_KEY, token);
  }

  public getToken ():string{
    return localStorage.getItem(TOKEN_KEY);
  }

  public saveUserName(username: string):void{
    window.localStorage.removeItem(USER_NAME_KEY);
    window.localStorage.setItem(USER_NAME_KEY, username);
  }

  public getUserName():string {
    return window.localStorage.getItem(USER_NAME_KEY);
  }

  public getUserId(): string{
    return window.localStorage.getItem(USER_ID_KEY);
  }

  public saveUserId(id: string):void{
    window.localStorage.removeItem(USER_ID_KEY);
    window.localStorage.setItem(USER_ID_KEY, id);
  }

  public getFirstName() :string {
    return window.localStorage.getItem(FIRST_NAME_KEY);
  }

  public saveFirstName(firstName: string):void{
    window.localStorage.removeItem(FIRST_NAME_KEY);
    window.localStorage.setItem(FIRST_NAME_KEY, firstName);
  }

  public saveRoles(roles : string[]):void{
    window.localStorage.removeItem(ROLES_KEY);
    window.localStorage.setItem(ROLES_KEY, JSON.stringify(roles));
  }

  public getRoles(): string[]{
    this.roles = [];
    if(localStorage.getItem(ROLES_KEY)){
      JSON.parse(localStorage.getItem(ROLES_KEY)).forEach(role =>{
        this.roles.push(role);
      });
    }

    return this.roles;
  }

  public saveExpiration(expiration:string):void{
    window.localStorage.removeItem(TOKEN_EXPIRATION_KEY);
    window.localStorage.setItem(TOKEN_EXPIRATION_KEY, JSON.stringify(expiration.valueOf()));
  }

  public getExpiration():string{
    return window.localStorage.getItem(TOKEN_EXPIRATION_KEY);
  }

  public saveIssuedAt(issuedAt: string):void{
    window.localStorage.removeItem(TOKEN_ISSUED_AT_KEY);
    window.localStorage.setItem(TOKEN_ISSUED_AT_KEY, issuedAt);

  }

  




  



}
