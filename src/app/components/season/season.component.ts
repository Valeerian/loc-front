import { Component, OnInit, OnDestroy } from '@angular/core';
import { GenericComponent } from '../generic/generic.component';
import { SnackbarComponent } from '../snackbar/snackbar.component';
import { AuthService } from 'src/app/services/auth-service';
import { SeasonService } from 'src/app/services/dataServices/season.service';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { IMAGES } from 'src/app/util/images';
import { SeasonModel } from 'src/app/models/dataModels/seasonModel';

@Component({
  selector: 'app-season',
  templateUrl: './season.component.html',
  styleUrls: ['./season.component.css']
})
export class SeasonComponent extends GenericComponent implements OnInit, OnDestroy {

  idLogoSrcOne : string = '';
  idLogoSrcTwo : string = '';
  nameLogoSrcOne: string ='';
  nameLogoSrcTwo: string ='';

  constructor(protected snackbar: SnackbarComponent, protected authService: AuthService, protected seasonService: SeasonService, protected formBuilder: FormBuilder) {
    super(snackbar, authService, seasonService, formBuilder); 
  }

  ngOnInit() {
    super.ngOnInit();
  }

  ngOnDestroy(){
    super.ngOnDestroy();
  }


  createOrderLogos():void{
    this.idLogoSrcOne = IMAGES.upArrowFirstD;
    this.idLogoSrcTwo = IMAGES.upArrowSecondD;
    this.nameLogoSrcOne = IMAGES.upArrowFirstD;
    this.nameLogoSrcTwo = IMAGES.upArrowSecondD;
  }

  initForm(){
    this.addModelForm = this.formBuilder.group({
      'season': new FormControl('', [Validators.required, Validators.pattern("^([A-Z]([a-zéè]{2,10}( (20|19)[0-9]{2})?))$")]),
    });
  }

  onSubmitAddDataForm(){
    let newSeason = new SeasonModel();
    newSeason.season = this.addModelForm.value['season'];
    this.seasonService.localAddData(newSeason).subscribe(
      ()=>{
        this.snackbar.openSnackBar("Saison ajoutée", "OK", "success-snackbar");
        this.reloadDatas();
        this.initForm();
      }, error=>{
        this.snackbar.openSnackBar('Une erreur est survenue lors de l\'ajout de la saison' + error.error, 'OK', 'error-snackbar');
      }
    );
  }


  onOrderById():void{
    if(this.idLogoSrcOne === IMAGES.upArrowFirstD){
      this.idLogoSrcOne = IMAGES.downArrowFirstD;
      this.idLogoSrcTwo = IMAGES.downArrowSecondD;
      this.seasonService.changeOrder("DESC", "seasonId");
    }else{
      this.idLogoSrcOne = IMAGES.upArrowFirstD;
      this.idLogoSrcTwo = IMAGES.upArrowSecondD;
      this.seasonService.changeOrder("ASC", "seasonId");
    }
    this.reloadDatas();
  }

  onOrderByName():void{
    if(this.nameLogoSrcOne === IMAGES.upArrowFirstD){
      this.nameLogoSrcOne = IMAGES.downArrowFirstD;
      this.nameLogoSrcTwo = IMAGES.downArrowSecondD;
      this.seasonService.changeOrder("DESC", "name");
    }else{
      this.nameLogoSrcOne = IMAGES.upArrowFirstD;
      this.nameLogoSrcTwo = IMAGES.upArrowSecondD;
      this.seasonService.changeOrder("ASC", "name");
    }
    this.reloadDatas();
  }

  createDataFormGroup(seasonModel: SeasonModel): FormGroup{
    return this.formBuilder.group({
      'seasonId': new FormControl(seasonModel.seasonId, [Validators.required, Validators.min(1)]),
      'season': new FormControl(seasonModel.season, [Validators.required, Validators.pattern("^([A-Z]([a-zéè]{2,10}( (20|19)[0-9]{2})?))$")])
    });
  }

  onSubmitEditData(index:number){
    let seasonModelToUpdate = new SeasonModel();
    seasonModelToUpdate.seasonId = this.formArray.controls[index].value['seasonId'];
    seasonModelToUpdate.season = this.formArray.controls[index].value['season'];
    this.seasonService.localEditData(seasonModelToUpdate).subscribe(
      ()=>{
        this.snackbar.openSnackBar('Saison modifiée!', 'OK', 'success-snackbar');
        this.reloadDatas();
      }, error=>{
        this.snackbar.openSnackBar('Une erreur est survenue lors de la modification de la saison: ' +error.error, 'OK', 'error-snackbar');
      }
    );
  }

}
