import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { QueryModel } from 'src/app/models/queryModel';
import { BrandListModel } from 'src/app/models/dataListModels/brandListModel';
import { FormArray, FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { SnackbarComponent } from '../snackbar/snackbar.component';
import { AuthService } from 'src/app/services/auth-service';
import { BrandService } from 'src/app/services/dataServices/brand.service';
import { BrandModel } from 'src/app/models/dataModels/brandModel';
import { IMAGES } from 'src/app/util/images';
import { GenericComponent } from '../generic/generic.component';

@Component({
  selector: 'app-brands',
  templateUrl: './brands.component.html',
  styleUrls: ['./brands.component.css']
})
export class BrandsComponent extends GenericComponent implements OnInit, OnDestroy{

  idLogoSrcOne : string = '';
  idLogoSrcTwo : string = '';
  nameLogoSrcOne: string ='';
  nameLogoSrcTwo: string ='';

  constructor(protected snackbar: SnackbarComponent, protected authService: AuthService, protected brandService: BrandService, protected formBuilder: FormBuilder) {
    super(snackbar, authService, brandService, formBuilder); 
  }

  ngOnInit() {
    super.ngOnInit();
  }

  ngOnDestroy(){
    super.ngOnDestroy();
  }


  createOrderLogos():void{
    this.idLogoSrcOne = IMAGES.upArrowFirstD;
    this.idLogoSrcTwo = IMAGES.upArrowSecondD;
    this.nameLogoSrcOne = IMAGES.upArrowFirstD;
    this.nameLogoSrcTwo = IMAGES.upArrowSecondD;
  }

  initForm(){
    this.addModelForm = this.formBuilder.group({
      'name': new FormControl('', [Validators.required, Validators.pattern("^[a-zA-Z0-9#?:.ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØŒŠþÙÚÛÜÝŸàáâãäåæçèéêëìíîïðñòóôõöøœšÞùúûüýÿß¢ß¥£™©®ª×÷±²³¼½¾µ¿¶·¸º°¯§…¤¦≠¬ˆ¨ '-_/]{1,50}$")]),
    });
  }

  onSubmitAddDataForm(){
    let newBrand = new BrandModel();
    newBrand.name = this.addModelForm.value['name'];
    this.brandService.localAddData(newBrand).subscribe(
      ()=>{
        this.snackbar.openSnackBar("Marque ajoutée", "OK", "success-snackbar");
        this.reloadDatas();
        this.initForm();
      }, error=>{
        this.snackbar.openSnackBar('Une erreur est survenue lors de l\'ajout de la marque' + error.error, 'OK', 'error-snackbar');
      }
    );
  }


  onOrderById():void{
    if(this.idLogoSrcOne === IMAGES.upArrowFirstD){
      this.idLogoSrcOne = IMAGES.downArrowFirstD;
      this.idLogoSrcTwo = IMAGES.downArrowSecondD;
      this.brandService.changeOrder("DESC", "brandId");
    }else{
      this.idLogoSrcOne = IMAGES.upArrowFirstD;
      this.idLogoSrcTwo = IMAGES.upArrowSecondD;
      this.brandService.changeOrder("ASC", "brandId");
    }
    this.reloadDatas();
  }

  onOrderByName():void{
    if(this.nameLogoSrcOne === IMAGES.upArrowFirstD){
      this.nameLogoSrcOne = IMAGES.downArrowFirstD;
      this.nameLogoSrcTwo = IMAGES.downArrowSecondD;
      this.brandService.changeOrder("DESC", "name");
    }else{
      this.nameLogoSrcOne = IMAGES.upArrowFirstD;
      this.nameLogoSrcTwo = IMAGES.upArrowSecondD;
      this.brandService.changeOrder("ASC", "name");
    }
    this.reloadDatas();
  }

  createDataFormGroup(brandModel: BrandModel): FormGroup{
    return this.formBuilder.group({
      'brandId': new FormControl(brandModel.brandId, [Validators.required, Validators.min(1)]),
      'name': new FormControl(brandModel.name, [Validators.required, Validators.pattern("^[a-zA-Z0-9#?:.ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØŒŠþÙÚÛÜÝŸàáâãäåæçèéêëìíîïðñòóôõöøœšÞùúûüýÿß¢ß¥£™©®ª×÷±²³¼½¾µ¿¶·¸º°¯§…¤¦≠¬ˆ¨ '-_/]{1,50}$")])
    });
  }

  onSubmitEditData(index:number){
    let brandModelToUpdate = new BrandModel();
    brandModelToUpdate.brandId = this.formArray.controls[index].value['brandId'];
    brandModelToUpdate.name = this.formArray.controls[index].value['name'];
    this.brandService.localEditData(brandModelToUpdate).subscribe(
      ()=>{
        this.snackbar.openSnackBar('Marque modifiée!', 'OK', 'success-snackbar');
        this.reloadDatas();
      }, error=>{
        this.snackbar.openSnackBar('Une erreur est survenue lors de la modification de la marque: ' +error.error, 'OK', 'error-snackbar');
      }
    );
  }
}
