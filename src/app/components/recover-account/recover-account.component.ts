import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { SnackbarComponent } from '../snackbar/snackbar.component';
import { AuthService } from 'src/app/services/auth-service';
import { Router } from '@angular/router';
import { UserModel } from 'src/app/models/dataModels/userModel';
import { MatTooltipModule } from'@angular/material/tooltip';

@Component({
  selector: 'app-recover-account',
  templateUrl: './recover-account.component.html',
  styleUrls: ['./recover-account.component.css']
})
export class RecoverAccountComponent implements OnInit, OnDestroy {
  private userModel: UserModel;
  isLoggedIn: boolean;
  isLoggedInSubscription: Subscription;

  recoverForm: FormGroup;

  constructor(private snackbar: SnackbarComponent, private authService: AuthService, private router: Router, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.isLoggedIn = this.authService.getAuthStatus();
    this.isLoggedInSubscription = this.authService.isLoggedInSubject.subscribe(
      (isLogged:boolean)=>{
        this.isLoggedIn = isLogged;
      }
    );
    this.initForm();
  }

  initForm(){
    this.recoverForm = this.formBuilder.group({
      'mail': new FormControl('', [Validators.email, Validators.required])
    });
  }

  onSubmitForm(){
    this.userModel = new UserModel();
    this.userModel.mail=this.recoverForm.value['mail']
    this.authService.attemptLocalPasswordRecoveryProcedure(this.userModel).subscribe(
      () => {
        this.router.navigate(['/recoverAccount2']);
        this.snackbar.openSnackBar('Mail envoyé!', 'OK', 'success-snackbar');
      }, error =>{
        this.snackbar.openSnackBar('Erreur : ' + error.error, 'OK', "error-snackbar");
      }
    )
  }

  ngOnDestroy(){
    this.isLoggedInSubscription.unsubscribe();
  }

  

}
