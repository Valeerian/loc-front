import { Component, OnInit, OnDestroy } from '@angular/core';
import { GenericComponent } from '../generic/generic.component';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { SnackbarComponent } from '../snackbar/snackbar.component';
import { AuthService } from 'src/app/services/auth-service';
import { MaterialService } from 'src/app/services/dataServices/material.service';
import { IMAGES } from 'src/app/util/images';
import { MaterialModel } from 'src/app/models/dataModels/materialModel';

@Component({
  selector: 'app-materials',
  templateUrl: './materials.component.html',
  styleUrls: ['./materials.component.css']
})
export class MaterialsComponent extends GenericComponent implements OnInit, OnDestroy {

  idLogoSrcOne : string = '';
  idLogoSrcTwo : string = '';
  nameLogoSrcOne: string ='';
  nameLogoSrcTwo: string ='';

  constructor(protected snackBar: SnackbarComponent, protected autheService: AuthService, protected materialService: MaterialService, protected formBuilder: FormBuilder) {
    super(snackBar, autheService, materialService, formBuilder);
   }

  ngOnInit() {
    super.ngOnInit();
  }

  ngOnDestroy(){
    super.ngOnDestroy();
  }

  protected createOrderLogos(): void {
    this.idLogoSrcOne = IMAGES.upArrowFirstD;
    this.idLogoSrcTwo = IMAGES.upArrowSecondD;
    this.nameLogoSrcOne = IMAGES.upArrowFirstD;
    this.nameLogoSrcTwo = IMAGES.upArrowSecondD;
  }
 
  protected initForm(): void {
    this.addModelForm = this.formBuilder.group({
      'name': new FormControl('',[Validators.required, Validators.pattern("^[A-Za-z0-9%éè -]{2,50}$")])
    })
  }

  protected createDataFormGroup(materialModel: MaterialModel): FormGroup {
    return this.formBuilder.group({
      'materialId': new FormControl(materialModel.materialId, [Validators.required, Validators.min(1)]),
      'name': new FormControl(materialModel.name, [Validators.required, Validators.pattern("^[A-Za-z0-9%éè -]{2,50}$")])
    });
  }
  protected onSubmitEditData(index: number): void {
    let materialModelToUpdate = new MaterialModel();
    materialModelToUpdate.materialId = this.formArray.controls[index].value['materialId'];
    materialModelToUpdate.name = this.formArray.controls[index].value['name'];
    this.materialService.localEditData(materialModelToUpdate).subscribe(
      ()=>{
        this.snackbar.openSnackBar('Matière modifiée!', 'OK', 'success-snackbar');
        this.reloadDatas();
      }, error=>{
        this.snackbar.openSnackBar('Une erreur est survenue lors de la modification de la matière: ' +error.error, 'OK', 'error-snackbar');
      }
    );
  }
  
  
  protected onSubmitAddDataForm(): void {
    let newMaterial = new MaterialModel();
    newMaterial.name = this.addModelForm.value['name'];
    this.materialService.localAddData(newMaterial).subscribe(
      ()=>{
        this.snackbar.openSnackBar("Matière ajoutée", "OK", "success-snackbar");
        this.reloadDatas();
        this.initForm();
      }, error=>{
        this.snackbar.openSnackBar('Une erreur est survenue lors de l\'ajout de la matière' + error.error, 'OK', 'error-snackbar');
      }
    );
  }

  onOrderById():void{
    if(this.idLogoSrcOne === IMAGES.upArrowFirstD){
      this.idLogoSrcOne = IMAGES.downArrowFirstD;
      this.idLogoSrcTwo = IMAGES.downArrowSecondD;
      this.materialService.changeOrder("DESC", "materialId");
    }else{
      this.idLogoSrcOne = IMAGES.upArrowFirstD;
      this.idLogoSrcTwo = IMAGES.upArrowSecondD;
      this.materialService.changeOrder("ASC", "materialId");
    }
    this.reloadDatas();
  }

  onOrderByName():void{
    if(this.nameLogoSrcOne === IMAGES.upArrowFirstD){
      this.nameLogoSrcOne = IMAGES.downArrowFirstD;
      this.nameLogoSrcTwo = IMAGES.downArrowSecondD;
      this.materialService.changeOrder("DESC", "name");
    }else{
      this.nameLogoSrcOne = IMAGES.upArrowFirstD;
      this.nameLogoSrcTwo = IMAGES.upArrowSecondD;
      this.materialService.changeOrder("ASC", "name");
    }
    this.reloadDatas();
  }
  

}
