import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecoverAccountP2Component } from './recover-account-p2.component';

describe('RecoverAccountP2Component', () => {
  let component: RecoverAccountP2Component;
  let fixture: ComponentFixture<RecoverAccountP2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecoverAccountP2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecoverAccountP2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
