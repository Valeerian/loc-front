import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserModel } from 'src/app/models/dataModels/userModel';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { SnackbarComponent } from '../snackbar/snackbar.component';
import { AuthService } from 'src/app/services/auth-service';
import { TokenStorageService } from 'src/app/services/token-storage.service';
import { Router } from '@angular/router';
import * as jwt_decode from 'jwt-decode';

@Component({
  selector: 'app-recover-account-p2',
  templateUrl: './recover-account-p2.component.html',
  styleUrls: ['./recover-account-p2.component.css']
})
export class RecoverAccountP2Component implements OnInit,  OnDestroy{

  private userModel: UserModel;
  private isLoggedIn: boolean;
  private isLoggedInSubscription: Subscription;

  recoverForm: FormGroup;
  constructor(private snackbar: SnackbarComponent, private authService: AuthService, private token: TokenStorageService, private router: Router, private formbuilder: FormBuilder) { }

  ngOnInit() {
    this.isLoggedIn = this.authService.getAuthStatus();
    this.isLoggedInSubscription = this.authService.isLoggedInSubject.subscribe(
      (isLogged: boolean)=>{
        this.isLoggedIn = isLogged;
      }
    );
    this.initForm();
  }

  initForm(){
    this.recoverForm = this.formbuilder.group({
      'mail': new FormControl('', [Validators.required, Validators.email]),
      'newPassword': new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(50)]),
      'newPasswordConfirmation': new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(50)]),
      'recoveryString': new FormControl('', [Validators.required, Validators.minLength(64), Validators.maxLength(128)])
    })
  }

  ngOnDestroy(){
    this.isLoggedInSubscription.unsubscribe();
  }

  onSubmitForm(){
    this.userModel = new UserModel();
    this.userModel.mail = this.recoverForm.value['mail']
    this.userModel.newPassword = this.recoverForm.value['newPassword'];
    this.userModel.newPasswordConfirmation = this.recoverForm.value['newPasswordConfirmation'];
    this.userModel.passwordRecoveryString = this.recoverForm.value['recoveryString'];
    this.authService.attemptLocalPasswordRecoveryProcedurePhaseTwo(this.userModel).subscribe(
      answer => {
        this.token.saveFirstName(answer.firstName);
        this.token.saveLastName(answer.lastName);
        this.token.saveSex(this.setExplicitSex(answer.sex));
        this.token.saveToken(answer.jwtToken);
        this.token.saveUserName(answer.userName);
        this.token.saveMailAddress(answer.mail);
        this.token.saveRoles(answer.roles);
        var decodedToken = jwt_decode(answer.jwtToken);
        this.token.saveExpiration(decodedToken['exp']);
        this.token.saveIssuedAt(decodedToken['iat']);
        this.authService.login();
        this.authService.setRoles(answer.roles);
        this.router.navigate(['/home']);
        this.snackbar.openSnackBar('Changement de mot de passe effectué, heureux de vous revoir, ' + answer.userName + '!', 'OK', 'success-snackbar');
      }, (error) =>{
        this.snackbar.openSnackBar('Le changement de mot de passe a échoué :' + error.error, 'OK', 'error-snackbar');
      }
    )
  }

  setExplicitSex(sex?: Boolean):string{
    if(sex === null){
      return "Non communiqué";
    }
    if(sex=== false){
      return "Homme";
    }
    else {return "Femme";}
  }


}
