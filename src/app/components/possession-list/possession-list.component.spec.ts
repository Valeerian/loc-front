import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PossessionListComponent } from './possession-list.component';

describe('PossessionListComponent', () => {
  let component: PossessionListComponent;
  let fixture: ComponentFixture<PossessionListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PossessionListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PossessionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
