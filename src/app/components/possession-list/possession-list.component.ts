import { Component, OnInit, OnDestroy } from '@angular/core';
import { GenericComponent } from '../generic/generic.component';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { OrderListModel } from 'src/app/models/dataListModels/orderListModel';
import { SnackbarComponent } from '../snackbar/snackbar.component';
import { AuthService } from 'src/app/services/auth-service';
import { PossessionService } from 'src/app/services/dataServices/possession-service.service';
import { PossessionStatusService } from 'src/app/services/dataServices/possession-status.service';
import { FriendshipService } from 'src/app/services/dataServices/friendship.service';
import { PossessionStatusModel } from 'src/app/models/dataModels/possessionStatusModel';
import { PossessionStatusListModel } from 'src/app/models/dataListModels/possessionStatusListModel';
import { FriendshipListModel } from 'src/app/models/dataListModels/friendshipListModel';
import { error } from 'protractor';
import { TokenStorageService } from 'src/app/services/token-storage.service';
import { QueryModel } from 'src/app/models/queryModel';
import { ClotheService } from 'src/app/services/dataServices/clothe.service';
import { SeasonService } from 'src/app/services/dataServices/season.service';
import { ClotheTypeService } from 'src/app/services/dataServices/clothe-type.service';
import { SeasonSelectProviderService } from 'src/app/services/dataServices/subServices/season-select-provider-service.service';
import { ClotheTypeSelectProviderService } from 'src/app/services/dataServices/subServices/clothe-type-select-provider.service';
import { SexSelectProviderService } from 'src/app/services/dataServices/subServices/sex-select-provider.service';
import { ColorSelectProviderService } from 'src/app/services/dataServices/subServices/color-select-provider.service';
import { BrandSelectProviderService } from 'src/app/services/dataServices/subServices/brand-select-provider.service';
import { SizeSelectProviderService } from 'src/app/services/dataServices/subServices/size-select-provider.service';
import { StyleSelectProviderService } from 'src/app/services/dataServices/subServices/style-select-provider.service';
import { ThemeSelectProviderService } from 'src/app/services/dataServices/subServices/theme-select-provider.service';
import { UserListModel } from 'src/app/models/dataListModels/userListModel';
import { UserModel } from 'src/app/models/dataModels/userModel';
import { ClotheListModel } from 'src/app/models/dataListModels/clotheListModel';
import { ClotheTypeListModel } from 'src/app/models/dataListModels/clotheTypeListModel';
import { ColorListModel } from 'src/app/models/dataListModels/colorListModel';
import { SeasonListModel } from 'src/app/models/dataListModels/seasonListModel';
import { SexListModel } from 'src/app/models/dataListModels/sexListModel';
import { BrandListModel } from 'src/app/models/dataListModels/brandListModel';
import { SizeListModel } from 'src/app/models/dataListModels/sizeListModel';
import { StyleListModel } from 'src/app/models/dataListModels/styleListModel';
import { ThemeListModel } from 'src/app/models/dataListModels/themeListModel';
import { PossessionOrders } from 'src/app/util/possessionOrders';
import { OrderModel } from 'src/app/models/dataModels/orderModel';
import { ColorService } from 'src/app/services/dataServices/color-service';
import { SexService } from 'src/app/services/dataServices/sex.service';
import { BrandService } from 'src/app/services/dataServices/brand.service';
import { StyleService } from 'src/app/services/dataServices/style.service';
import { ThemeService } from 'src/app/services/dataServices/theme.service';
import { SizesService } from 'src/app/services/dataServices/sizes.service';
import { ClotheModel } from 'src/app/models/dataModels/clotheModel';
import { ClotheTypeModel } from 'src/app/models/dataModels/clotheTypeModel';
import { ColorModel } from 'src/app/models/dataModels/colorModel';
import { SeasonModel } from 'src/app/models/dataModels/seasonModel';
import { SexModel } from 'src/app/models/dataModels/sexModel';
import { BrandModel } from 'src/app/models/dataModels/brandModel';
import { SizeModel } from 'src/app/models/dataModels/sizeModel';
import { StyleModel } from 'src/app/models/dataModels/styleModel';
import { ThemeModel } from 'src/app/models/dataModels/themeModel';
import { PossessionModel } from 'src/app/models/dataModels/possessionModel';

@Component({
  selector: 'app-possession-list',
  templateUrl: './possession-list.component.html',
  styleUrls: ['./possession-list.component.css']
})
export class PossessionListComponent extends GenericComponent implements OnInit, OnDestroy {

//Booléens utilisés pour indiquer aux selects de se remettre à zéro
  resetFlagClothe: boolean = false;
  resetFlagClotheType: boolean = false;
  resetFlagColor: boolean = false;
  resetFlagPossessionStatus:boolean = false;
  resetFlagOwner:boolean = false;
  resetFlagSeason: boolean = false;
  resetFlagSex: boolean = false;
  resetFlagBrand : boolean = false;
  resetFlagSize : boolean  = false;
  resetFlagStyle: boolean = false;
  resetFlagTheme: boolean = false;
//Tableaux contenant les critères de recherche sous forme d'id (number)
  clotheCriteriaArray: number[] = [];
  clotheTypeCriteriaArray: number[] = [];
  colorCriteriaArray: number[] = [];
  possessionStatusCriteriaArray: number[] = [];
  ownerCriteriaArray: number[] = [];
  seasonCriteriaArray: number[] = [];
  sexCriteriaArray: number[] = [];
  brandCriteriaArray: number[] = [];
  sizeCriteriaArray: number[] = [];
  styleCriteriaArray: number[] = [];
  themeCriteriaArray: number[] = [];

  //Permet de stocker le type de tri que l'on souhaite
  orders: OrderListModel = new OrderListModel();
  sizeCategories: string[] = [];
  clotheTypeCategories: string[]=[];


//ListModels contenant les données à envoyer aux selects pour qu'ils remplissent leurs options
  ownerList: UserListModel = new UserListModel();
  clotheList: ClotheListModel = new ClotheListModel();
  clotheTypeList: ClotheTypeListModel = new ClotheTypeListModel();
  colorList: ColorListModel = new ColorListModel();
  possessionStatusList: PossessionStatusListModel = new PossessionStatusListModel();
  seasonList: SeasonListModel = new SeasonListModel();
  sexList: SexListModel = new SexListModel();
  brandList: BrandListModel = new BrandListModel();
  sizeList: SizeListModel = new SizeListModel();
  styleList: StyleListModel = new StyleListModel();
  themeList: ThemeListModel = new ThemeListModel();
//Permet de stocker tous les possessionStatuses et non pas juste ceux qui sont en vigueur sur les différentes possessions de l'utilisateur
  everyPossessionStatuses: PossessionStatusListModel = new PossessionStatusListModel();
//Permet de stocker tous les amis de l'utilisateur actuel et non pas juste ceux qui ont prêté des vêtements à l'utilisateur connecté
  everyFriends : UserListModel = new UserListModel();


  constructor(protected snackbar: SnackbarComponent, private token: TokenStorageService, protected authService: AuthService, protected formBuilder: FormBuilder, protected dataService: PossessionService,
    private possessionStatusService: PossessionStatusService, private friendshipService: FriendshipService,
    private clotheService: ClotheService, private seasonService: SeasonService, private clotheTypeService: ClotheTypeService,
    private sexService: SexService, private colorService: ColorService, private brandService: BrandService, private sizeService: SizesService,
    private styleService: StyleService, private themeService: ThemeService) {
    super(snackbar, authService, dataService, formBuilder);
   }

  ngOnInit() {
    super.ngOnInit();
    this.loadDatasForSpecialSelects();
    this.loadDatasToFillSelects();
  }

  /**
   * Fonction appelée lors de la soumission de critères de recherche par l'utilisateur, transforme les criteriaArrays en critères compréhensibles par le back-end et appelle reloadDatas()
   */
  submitFilter(){
    this.dataService.removeCriterias();
    if(this.ownerCriteriaArray.length > 0 ){
      for(let id of this.ownerCriteriaArray ){
        this.dataService.addCriteria("owner", ":", ""+id);
      }
    }
    if(this.clotheCriteriaArray.length > 0 ){
      for(let id of this.clotheCriteriaArray ){
        this.dataService.addCriteria("clotheId", ":", ""+id);
      }
    }
    if(this.clotheTypeCriteriaArray.length > 0 ){
      for(let id of this.clotheTypeCriteriaArray ){
        this.dataService.addCriteria("clotheTypeId", ":", ""+id);
      }
    }
    if(this.colorCriteriaArray.length > 0 ){
      for(let id of this.colorCriteriaArray ){
        this.dataService.addCriteria("colorId", ":", ""+id);
      }
    }
    if(this.possessionStatusCriteriaArray.length > 0 ){
      for(let id of this.possessionStatusCriteriaArray ){
        this.dataService.addCriteria("possessionStatusId", ":", ""+id);
      }
    }
    if(this.seasonCriteriaArray.length > 0 ){
      for(let id of this.seasonCriteriaArray ){
        this.dataService.addCriteria("seasonId", ":", ""+id);
      }
    }
    if(this.sexCriteriaArray.length > 0 ){
      for(let id of this.sexCriteriaArray ){
        this.dataService.addCriteria("sexId", ":", ""+id);
      }
    }
    if(this.brandCriteriaArray.length > 0 ){
      for(let id of this.brandCriteriaArray ){
        this.dataService.addCriteria("brandId", ":", ""+id);
      }
    }
    if(this.sizeCriteriaArray.length > 0 ){
      for(let id of this.sizeCriteriaArray ){
        this.dataService.addCriteria("sizeId", ":", ""+id);
      }
    }
    if(this.styleCriteriaArray.length > 0 ){
      for(let id of this.styleCriteriaArray ){
        this.dataService.addCriteria("styleId", ":", ""+id);
      }
    }
    if(this.themeCriteriaArray.length > 0 ){
      for(let id of this.themeCriteriaArray ){
        this.dataService.addCriteria("themeId", ":", ""+id);
      }
    }
    this.reloadDatas();

  }

  /**
   * Fonction qui n'est appelée que par ngOnInti => donc au chargement du component
   * Permet de récupérer l'intégralité des amis de l'utilisateur courant
   * permet de récupérer l'intégralité des statuts de possessions
   * Met ces deux informations dans des tableaux dont le but est de stocker ces informations
   */
  loadDatasForSpecialSelects(){

    this.possessionStatusService.localGetDatas().subscribe(
      (list: PossessionStatusListModel)=>{
        this.everyPossessionStatuses = list;
      },error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la récupération des statuts auprès du serveur: "+ error.error, "OK", "error-snackbar");
      }
    );
    this.friendshipService.localGetMyFriends().subscribe(
      (list: UserListModel)=>{
        this.everyFriends = list;
      },error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la récupération de vos amis: " + error.error, "OK", "error-snackbar");
      }
    );
    
  }

  /**
   * Permet de supprimer les critères du QueryModel, et de remettre l'ordre par défaut.
   * Appelle la fonction reloadDatas afin de récupérer la liste de possessions correspondante du serveur
   */
  resetFilter(){
    this.dataService.removeCriterias();
    this.resetFlagBrand = this.switchBoolean(this.resetFlagBrand);
    this.resetFlagClothe = this.switchBoolean(this.resetFlagClothe);
    this.resetFlagClotheType = this.switchBoolean(this.resetFlagClotheType);
    this.resetFlagColor = this.switchBoolean(this.resetFlagColor);
    this.resetFlagPossessionStatus = this.switchBoolean(this.resetFlagPossessionStatus);
    this.resetFlagOwner = this.switchBoolean(this.resetFlagOwner);
    this.resetFlagSeason = this.switchBoolean(this.resetFlagSeason);
    this.resetFlagSex = this.switchBoolean(this.resetFlagSex);
    this.resetFlagSize = this.switchBoolean(this.resetFlagSize);
    this.resetFlagStyle = this.switchBoolean(this.resetFlagStyle);
    this.resetFlagTheme = this.switchBoolean(this.resetFlagTheme);
    this.dataService.changeOrder(PossessionOrders.idAsc.orderType, PossessionOrders.idAsc.orderBy);
    this.reloadDatas();
  }

  switchBoolean(boo : boolean): boolean{
    if(boo == true){
      return false;
    }
    else{
      return true;
    }
  }

  onChangeOrder(orderModel: OrderModel){
    if(orderModel){
      this.dataService.changeOrder(orderModel.orderType, orderModel.orderBy);
    }else{
      this.dataService.changeOrder(PossessionOrders.idAsc.orderType,PossessionOrders.idAsc.orderBy);
    }
  }




  /**
   *Fonction permettant de récupérer depuis le serveur les données nécessaires pour combler les selects
   */
  loadDatasToFillSelects():void{
    this.orders.list=[];
    this.orders.list.push(PossessionOrders.idAsc);
    this.orders.list.push(PossessionOrders.idDesc);
    this.orders.list.push(PossessionOrders.ownerAsc);
    this.orders.list.push(PossessionOrders.ownerDesc);
    this.orders.list.push(PossessionOrders.sizeAsc);
    this.orders.list.push(PossessionOrders.sizeDesc);
    this.orders.list.push(PossessionOrders.clotheAsc);
    this.orders.list.push(PossessionOrders.clotheDesc);
    this.orders.list.push(PossessionOrders.userAsc);
    this.orders.list.push(PossessionOrders.userDesc);
    this.clotheService.localGetClotheForPossession().subscribe(
      (list: ClotheListModel)=>{
        this.clotheList = list;
      },error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la récupération des vêtements: " +error.error ,"OK","error-snackbar");
      }
    );
    this.clotheTypeService.localGetClotheTypeForPossession().subscribe(
      (list: ClotheTypeListModel)=>{
        this.clotheTypeList = list;
        for(let ct of list.list){
          let alreadyFoundFlag : boolean = false;
          for(let cat of this.clotheTypeCategories){
            if(ct.category === cat){
              alreadyFoundFlag = true;
              break;
            }
          }
          if(!alreadyFoundFlag){
            this.clotheTypeCategories.push(ct.category);
          }
        }
      },error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la récupération des types: " +error.error ,"OK","error-snackbar");
      }
    );
    this.colorService.localGetColorForPossession().subscribe(
      (list: ColorListModel)=>{
        this.colorList = list;
      },error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la récupération des couleurs: " +error.error ,"OK","error-snackbar");
      }
    );
    this.friendshipService.localGetOwners().subscribe(
      (list: UserListModel)=>{
        this.ownerList = list;
      },error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la récupération des propriétaires : " +error.error ,"OK","error-snackbar");
      }
    );
    this.possessionStatusService.localGetPossessionStatusForPossession().subscribe(
      (list: PossessionStatusListModel)=>{
        this.possessionStatusList = list;
      },error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la récupération des statuts: " +error.error ,"OK","error-snackbar");
      }
    );
    this.seasonService.localGetSeasonForPossession().subscribe(
      (list: SeasonListModel)=>{
        this.seasonList = list;
      },error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la récupération des saisons: " +error.error ,"OK","error-snackbar");
      }
    );
    this.sexService.localGetSexForPossession().subscribe(
      (list: SexListModel)=>{
        this.sexList = list;
      },error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la récupération des sexes :" +error.error ,"OK","error-snackbar");
      }
    );
    this.brandService.localGetBrandForPossession().subscribe(
      (list: BrandListModel)=>{
        this.brandList = list;
      },error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la récupération des marques: " +error.error ,"OK","error-snackbar");
      }
    );
    this.sizeService.localGetSizeForPossession().subscribe(
      (list: SizeListModel)=>{
        this.sizeList = list;
        for(let size of list.list){
          let alreadyFoundFlag: boolean = false;
          for(let cat of this.sizeCategories){
            if(size.category === cat){
              alreadyFoundFlag = true;
              break;
            }
          }
          if(!alreadyFoundFlag){
            this.sizeCategories.push(size.category);
          }
        }
      },error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la récupération des tailles: " +error.error ,"OK","error-snackbar");
      }
    );
    this.styleService.localGetStyleForPossession().subscribe(
      (list: StyleListModel)=>{
        this.styleList = list;
      },error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la récupération des styles: " +error.error ,"OK","error-snackbar");
      }
    );
    this.themeService.localGetThemeForPossession().subscribe(
      (list: ThemeListModel)=>{
        this.themeList = list;
      },error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la récupération des thèmes: " +error.error ,"OK","error-snackbar");
      }
    );

  }

  ngOnDestroy(){
    super.ngOnInit();
  }

  protected createDataFormGroup(model: any): FormGroup {
    return this.formBuilder.group({

    });
  }
  protected onSubmitEditData(index: number): void {
    //throw new Error("Method not implemented.");
  }
  protected createOrderLogos(): void {
    //throw new Error("Method not implemented.");
  }
  protected initForm(): void {
    //throw new Error("Method not implemented.");
  }
  protected onSubmitAddDataForm(): void {
    //throw new Error("Method not implemented.");
  }

  onChangeOwnerFilter(userModels : UserModel[]){
    this.ownerCriteriaArray = [];
    if(userModels && userModels.length > 0){
      for(let user of userModels){
        this.ownerCriteriaArray.push(user.userId);
      }
    }
  }

  onChangeClotheFilter(clotheModels : ClotheModel[]){
    this.clotheCriteriaArray = [];
    if(clotheModels && clotheModels.length > 0){
      for(let clothe of clotheModels){
        this.clotheCriteriaArray.push(clothe.clotheId);
      }
    }
  }

  onChangeClotheTypeFilter(clotheTypeModels : ClotheTypeModel[]){
    this.clotheTypeCriteriaArray = [];
    if(clotheTypeModels && clotheTypeModels.length > 0){
      for(let clotheType of clotheTypeModels){
        this.clotheTypeCriteriaArray.push(clotheType.clotheTypeId);
      }
    }
  }

  onChangeColorFilter(colorModels : ColorModel[]){
    this.colorCriteriaArray = [];
    if(colorModels && colorModels.length > 0){
      for(let color of colorModels){
        this.colorCriteriaArray.push(color.colorId);
      }
    }
  }

  onChangePossessionStatusFilter(possessionStatusModels : PossessionStatusModel[]){
    this.possessionStatusCriteriaArray = [];
    if(possessionStatusModels && possessionStatusModels.length > 0){
      for(let possessionStatus of possessionStatusModels){
        this.possessionStatusCriteriaArray.push(possessionStatus.possessionStatusId);
      }
    }
  }

  onChangeSeasonFilter(seasonModels : SeasonModel[]){
    this.seasonCriteriaArray = [];
    if(seasonModels && seasonModels.length > 0){
      for(let season of seasonModels){
        this.seasonCriteriaArray.push(season.seasonId);
      }
    }
  }

  onChangeSexFilter(sexModels : SexModel[]){
    this.sexCriteriaArray = [];
    if(sexModels && sexModels.length > 0){
      for(let sex of sexModels){
        this.sexCriteriaArray.push(sex.sexId);
      }
    }
  }

  onChangeBrandFilter(brandModels : BrandModel[]){
    this.brandCriteriaArray = [];
    if(brandModels && brandModels.length > 0){
      for(let brand of brandModels){
        this.brandCriteriaArray.push(brand.brandId);
      }
    }
  }

  onChangeSizeFilter(sizeModels : SizeModel[]){
    this.sizeCriteriaArray = [];
    if(sizeModels && sizeModels.length > 0){
      for(let size of sizeModels){
        this.sizeCriteriaArray.push(size.id);
      }
    }
  }

  onChangeStyleFilter(styleModels: StyleModel[]){
    this.styleCriteriaArray = [];
    if(styleModels && styleModels.length > 0){
      for(let style of styleModels){
        this.styleCriteriaArray.push(style.styleId);
      }
    }
  }

  onChangeThemeFilter(themeModels: ThemeModel[]){
    this.themeCriteriaArray = [];
    if(themeModels && themeModels.length > 0){
      for(let theme of themeModels){
        this.themeCriteriaArray.push(theme.themeId);
      }
    }
  }

  onUpdatePossession(possessionModel: PossessionModel){
    this.dataService.localEditData(possessionModel).subscribe(
      ()=>{
        this.snackbar.openSnackBar("Possession mise à jour!", "OK", "success-snackbar");
        this.reloadDatas();
      },error=>{
        this.snackbar.openSnackBar("Une erreur est survenue durant la modification de la possession: " +error.error, "OK", "error-snackbar");
      }
    );
  }

  onDeletePossession(possessionModel: PossessionModel){
    this.dataService.localDeleteData(possessionModel).subscribe(
      ()=>{
        this.snackbar.openSnackBar("Possession supprimée!", "OK", "success-snackbar");
        this.reloadDatas();
      },error=>{
        this.snackbar.openSnackBar("Une erreur est survenue durant la suppression de la possession: " +error.error, "OK", "error-snackbar");
      }
    );
  }

  onSharePossession(possessionModel: PossessionModel){
    this.dataService.localSharePossession(possessionModel).subscribe(
      ()=>{
        this.snackbar.openSnackBar("Votre possession a été confiée à votre ami!", "OK", "success-snackbar");
        this.reloadDatas();
      },error=>{
        this.snackbar.openSnackBar("Une erreur est survenue durant le prêt de la  possession: " +error.error, "OK", "error-snackbar");
      }
    );
  }

  onGiveBackPossession(possessionModel: PossessionModel){
    this.dataService.localGiveBackPossession(possessionModel).subscribe(
      ()=>{
        this.snackbar.openSnackBar("Cette possession a été rendue à son propriétaire d'origine", "OK", "success-snackbar");
        this.reloadDatas();
      },error=>{
        this.snackbar.openSnackBar("Une erreur est survenue durant processus de retour au propriétaire " +error.error, "OK", "error-snackbar");
      }
    );
  }

  

}
