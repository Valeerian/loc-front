import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManeStatusComponent } from './mane-status.component';

describe('ManeStatusComponent', () => {
  let component: ManeStatusComponent;
  let fixture: ComponentFixture<ManeStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManeStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManeStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
