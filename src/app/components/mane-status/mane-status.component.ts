import { Component, OnInit, OnDestroy } from '@angular/core';
import { GenericComponent } from '../generic/generic.component';
import { SnackbarComponent } from '../snackbar/snackbar.component';
import { AuthService } from 'src/app/services/auth-service';
import { ManeStatusService } from 'src/app/services/dataServices/mane-status.service';
import { FormBuilder, Validators, FormControl, FormGroup } from '@angular/forms';
import { IMAGES } from 'src/app/util/images';
import { ManeStatusModel } from 'src/app/models/dataModels/maneStatusModel';

@Component({
  selector: 'app-mane-status',
  templateUrl: './mane-status.component.html',
  styleUrls: ['./mane-status.component.css']
})
export class ManeStatusComponent extends GenericComponent implements OnInit, OnDestroy {

  idLogoSrcOne : string = '';
  idLogoSrcTwo : string = '';
  nameLogoSrcOne: string ='';
  nameLogoSrcTwo: string ='';

  constructor(protected snackbar: SnackbarComponent, protected authService: AuthService, protected maneStatusService: ManeStatusService, protected formBuilder: FormBuilder) {
    super(snackbar, authService, maneStatusService, formBuilder); 
  }

  ngOnInit() {
    super.ngOnInit();
  }

  ngOnDestroy(){
    super.ngOnDestroy();
  }


  createOrderLogos():void{
    this.idLogoSrcOne = IMAGES.upArrowFirstD;
    this.idLogoSrcTwo = IMAGES.upArrowSecondD;
    this.nameLogoSrcOne = IMAGES.upArrowFirstD;
    this.nameLogoSrcTwo = IMAGES.upArrowSecondD;
  }

  initForm(){
    this.addModelForm = this.formBuilder.group({
      'status': new FormControl('', [Validators.required, Validators.pattern("^[A-ZÇÈÉÀ]([a-zéèàç ]{2,39})$")]),
    });
  }

  onSubmitAddDataForm(){
    let newManeStatus = new ManeStatusModel();
    newManeStatus.status = this.addModelForm.value['status'];
    this.maneStatusService.localAddData(newManeStatus).subscribe(
      ()=>{
        this.snackbar.openSnackBar("Statut ajouté", "OK", "success-snackbar");
        this.reloadDatas();
        this.initForm();
      }, error=>{
        this.snackbar.openSnackBar('Une erreur est survenue lors de l\'ajout du statut' + error.error, 'OK', 'error-snackbar');
      }
    );
  }


  onOrderById():void{
    if(this.idLogoSrcOne === IMAGES.upArrowFirstD){
      this.idLogoSrcOne = IMAGES.downArrowFirstD;
      this.idLogoSrcTwo = IMAGES.downArrowSecondD;
      this.maneStatusService.changeOrder("DESC", "maneStatusId");
    }else{
      this.idLogoSrcOne = IMAGES.upArrowFirstD;
      this.idLogoSrcTwo = IMAGES.upArrowSecondD;
      this.maneStatusService.changeOrder("ASC", "maneStatusId");
    }
    this.reloadDatas();
  }

  onOrderByName():void{
    if(this.nameLogoSrcOne === IMAGES.upArrowFirstD){
      this.nameLogoSrcOne = IMAGES.downArrowFirstD;
      this.nameLogoSrcTwo = IMAGES.downArrowSecondD;
      this.maneStatusService.changeOrder("DESC", "name");
    }else{
      this.nameLogoSrcOne = IMAGES.upArrowFirstD;
      this.nameLogoSrcTwo = IMAGES.upArrowSecondD;
      this.maneStatusService.changeOrder("ASC", "name");
    }
    this.reloadDatas();
  }

  createDataFormGroup(maneStatusModel: ManeStatusModel): FormGroup{
    return this.formBuilder.group({
      'maneStatusId': new FormControl(maneStatusModel.maneStatusId, [Validators.required, Validators.min(1)]),
      'status': new FormControl(maneStatusModel.status, [Validators.required, Validators.pattern("^[A-ZÇÈÉÀ]([a-zéèàç ]{2,39})$")])
    });
  }

  onSubmitEditData(index:number){
    let maneStatusModelToUpdate = new ManeStatusModel();
    maneStatusModelToUpdate.maneStatusId = this.formArray.controls[index].value['maneStatusId'];
    maneStatusModelToUpdate.status = this.formArray.controls[index].value['status'];
    this.maneStatusService.localEditData(maneStatusModelToUpdate).subscribe(
      ()=>{
        this.snackbar.openSnackBar('Statut modifié!', 'OK', 'success-snackbar');
        this.reloadDatas();
      }, error=>{
        this.snackbar.openSnackBar('Une erreur est survenue lors de la modification du statut: ' +error.error, 'OK', 'error-snackbar');
      }
    );
  }

}
