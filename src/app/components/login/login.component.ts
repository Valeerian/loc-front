import { Component, OnInit, OnDestroy } from '@angular/core';
import { JwtRequest } from 'src/app/models/loginModel';
import { SnackbarComponent } from '../snackbar/snackbar.component';
import { AuthService } from 'src/app/services/auth-service';
import { TokenStorageService } from 'src/app/services/token-storage.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import * as jwt_decode from 'jwt-decode';
import { HttpErrorResponse } from '@angular/common/http';
import * as moment from 'moment';  
  import { from, Subscription } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

  isLoggedIn:boolean;
  isLoggedInSubscription: Subscription;


  private login: JwtRequest;

  userName: string;
  password: string;

  loginForm: FormGroup;


  constructor(private snackbar: SnackbarComponent, private authService: AuthService, private token: TokenStorageService, private router: Router, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.isLoggedIn = this.authService.getAuthStatus();
    this.isLoggedInSubscription = this.authService.isLoggedInSubject.subscribe(
      (isLogged:boolean)=>{
        this.isLoggedIn = isLogged;
      }
    );
      this.initForm();  
  }

  initForm(){
    this.loginForm = this.formBuilder.group({
      userName: ['', Validators.required],
      password: ['',Validators.required]
    });
  }

  onLogin(){
    this.login = new JwtRequest(this.loginForm.value['userName'],this.loginForm.value['password']);
    this.authService.attemptLocalAuth(this.login).subscribe(
      data => {
        this.token.saveFirstName(data.firstName);
        this.token.saveLastName(data.lastName);
        this.token.saveSex(this.setExplicitSex(data.sex));
        this.token.saveToken(data.jwtToken);
        this.token.saveUserName(data.userName);
        this.token.saveMailAddress(data.mail);
        this.token.saveRoles(data.roles);
        var decodedToken = jwt_decode(data.jwtToken);
        this.token.saveExpiration(decodedToken['exp']);
        this.token.saveIssuedAt(decodedToken['iat']);
        this.authService.login();
        this.authService.setRoles(data.roles);
        this.router.navigate(['/home']);
        this.snackbar.openSnackBar('Authentification réussie, bienvenue, ' + data.userName + '.', 'OK', 'success-snackbar');        
      },
      error => {
        this.snackbar.openSnackBar('Authentification échouée: ' + error.error, 'OK', "error-snackbar");
      }
    );
  }

  ngOnDestroy(){
    this.isLoggedInSubscription.unsubscribe();
  }


  setExplicitSex(sex? : Boolean): string{
    if(sex === null){
      return "Non communiqué"
    }
    if(sex === false){
      return "Homme";
    }
    else{
      return "Femme";
    }
  }

  



}
