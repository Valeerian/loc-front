import { Component, OnInit, OnDestroy } from '@angular/core';
import { GenericComponent } from '../generic/generic.component';
import { SnackbarComponent } from '../snackbar/snackbar.component';
import { AuthService } from 'src/app/services/auth-service';
import { ThemeService } from 'src/app/services/dataServices/theme.service';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { IMAGES } from 'src/app/util/images';
import { ThemeModel } from 'src/app/models/dataModels/themeModel';

@Component({
  selector: 'app-theme',
  templateUrl: './theme.component.html',
  styleUrls: ['./theme.component.css']
})
export class ThemeComponent extends GenericComponent implements OnInit, OnDestroy {
  idLogoSrcOne : string = '';
  idLogoSrcTwo : string = '';
  nameLogoSrcOne: string ='';
  nameLogoSrcTwo: string ='';

  constructor(protected snackbar: SnackbarComponent, protected authService: AuthService, protected themeService: ThemeService, protected formBuilder: FormBuilder) {
    super(snackbar, authService, themeService, formBuilder); 
  }

  ngOnInit() {
    super.ngOnInit();
  }

  ngOnDestroy(){
    super.ngOnDestroy();
  }


  createOrderLogos():void{
    this.idLogoSrcOne = IMAGES.upArrowFirstD;
    this.idLogoSrcTwo = IMAGES.upArrowSecondD;
    this.nameLogoSrcOne = IMAGES.upArrowFirstD;
    this.nameLogoSrcTwo = IMAGES.upArrowSecondD;
  }

  initForm(){
    this.addModelForm = this.formBuilder.group({
      'theme': new FormControl('', [Validators.required, Validators.pattern("^[A-Z0-9ÈÉ]([A-Za-z0-9ÀÁÂÄÃÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕ&ÖØŒŠþÙÚÛÜÝŸàáâãäåæçèéêëìíîïðñòóôõöøœšÞùúûüýÿß¢ß¥£™©®ª×÷±²³¼½¾µ¿¶·¸º°¯§…¤¦≠¬ˆ¨ '-_/]{0,39})$")]),
    });
  }

  onSubmitAddDataForm(){
    let newTheme = new ThemeModel();
    newTheme.theme = this.addModelForm.value['theme'];
    this.themeService.localAddData(newTheme).subscribe(
      ()=>{
        this.snackbar.openSnackBar("Thème ajouté", "OK", "success-snackbar");
        this.reloadDatas();
        this.initForm();
      }, error=>{
        this.snackbar.openSnackBar('Une erreur est survenue lors de l\'ajout du thème' + error.error, 'OK', 'error-snackbar');
      }
    );
  }


  onOrderById():void{
    if(this.idLogoSrcOne === IMAGES.upArrowFirstD){
      this.idLogoSrcOne = IMAGES.downArrowFirstD;
      this.idLogoSrcTwo = IMAGES.downArrowSecondD;
      this.themeService.changeOrder("DESC", "themeId");
    }else{
      this.idLogoSrcOne = IMAGES.upArrowFirstD;
      this.idLogoSrcTwo = IMAGES.upArrowSecondD;
      this.themeService.changeOrder("ASC", "themeId");
    }
    this.reloadDatas();
  }

  onOrderByName():void{
    if(this.nameLogoSrcOne === IMAGES.upArrowFirstD){
      this.nameLogoSrcOne = IMAGES.downArrowFirstD;
      this.nameLogoSrcTwo = IMAGES.downArrowSecondD;
      this.themeService.changeOrder("DESC", "name");
    }else{
      this.nameLogoSrcOne = IMAGES.upArrowFirstD;
      this.nameLogoSrcTwo = IMAGES.upArrowSecondD;
      this.themeService.changeOrder("ASC", "name");
    }
    this.reloadDatas();
  }

  createDataFormGroup(themeModel: ThemeModel): FormGroup{
    return this.formBuilder.group({
      'themeId': new FormControl(themeModel.themeId, [Validators.required, Validators.min(1)]),
      'theme': new FormControl(themeModel.theme, [Validators.required, Validators.pattern("^[A-Z0-9ÈÉ]([A-Za-z0-9ÀÁÂÄÃÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØŒ&ŠþÙÚÛÜÝŸàáâãäåæçèéêëìíîïðñòóôõöøœšÞùúûüýÿß¢ß¥£™©®ª×÷±²³¼½¾µ¿¶·¸º°¯§…¤¦≠¬ˆ¨ '-_/]{0,39})$")])
    });
  }

  onSubmitEditData(index:number){
    let themeModelToUpdate = new ThemeModel();
    themeModelToUpdate.themeId = this.formArray.controls[index].value['themeId'];
    themeModelToUpdate.theme = this.formArray.controls[index].value['theme'];
    this.themeService.localEditData(themeModelToUpdate).subscribe(
      ()=>{
        this.snackbar.openSnackBar('Thème modifié!', 'OK', 'success-snackbar');
        this.reloadDatas();
      }, error=>{
        this.snackbar.openSnackBar('Une erreur est survenue lors de la modification du thème: ' +error.error, 'OK', 'error-snackbar');
      }
    );
  }

}
