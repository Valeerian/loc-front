import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LaundryGroupListComponent } from './laundry-group-list.component';

describe('LaundryGroupListComponent', () => {
  let component: LaundryGroupListComponent;
  let fixture: ComponentFixture<LaundryGroupListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LaundryGroupListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaundryGroupListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
