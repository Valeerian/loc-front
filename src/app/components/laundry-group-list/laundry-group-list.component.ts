import { Component, OnInit, OnDestroy } from '@angular/core';
import { GenericComponent } from '../generic/generic.component';
import { FormGroup, FormBuilder } from '@angular/forms';
import { SnackbarComponent } from '../snackbar/snackbar.component';
import { AuthService } from 'src/app/services/auth-service';
import { LaundryGroupService } from 'src/app/services/dataServices/laundry-group.service';
import { LaundryGroupModel } from 'src/app/models/dataModels/laundryGroupModel';
import { MimicListModel } from 'src/app/models/dataListModels/mimicListModel';
import { OrderListModel } from 'src/app/models/dataListModels/orderListModel';
import { LaundryGroupOrders } from 'src/app/util/laundryGroupOrders';
import { OrderModel } from 'src/app/models/dataModels/orderModel';
import { MimicModel } from 'src/app/models/dataModels/mimicModel';
import { LaundryManeModel } from 'src/app/models/dataModels/laundryManeModel';
import { UserListModel } from 'src/app/models/dataListModels/userListModel';
import { FriendshipService } from 'src/app/services/dataServices/friendship.service';
import { ManeStatusListModel } from 'src/app/models/dataListModels/maneStatusListModel';
import { ManeStatusService } from 'src/app/services/dataServices/mane-status.service';

@Component({
  selector: 'app-laundry-group-list',
  templateUrl: './laundry-group-list.component.html',
  styleUrls: ['./laundry-group-list.component.css']
})
export class LaundryGroupListComponent extends GenericComponent implements OnInit, OnDestroy {
  
  resetFlagOwner: boolean = false;
  resetFlagOrder: boolean = false;

  ownershipCriteria: string = "";

  maneStatusList: ManeStatusListModel = new ManeStatusListModel();

  addLaundryGroup: LaundryGroupModel = new LaundryGroupModel();
  ownerMimicList: MimicListModel = new MimicListModel();
  orders: OrderListModel = new OrderListModel();
  friendsList: UserListModel = new UserListModel();



  constructor(protected snackbar: SnackbarComponent, protected authService: AuthService, protected dataService: LaundryGroupService, protected formBuilder: FormBuilder,private friendshipService: FriendshipService, private maneStatusService: ManeStatusService) {
    super(snackbar, authService, dataService, formBuilder);
   }

  ngOnInit() {
    super.ngOnInit();
    this.loadDatasToFillSelects();
    this.addLaundryGroup.laundryGroupId=0;
  }

  submitFilter(){
    this.dataService.removeCriterias();
    if(this.ownershipCriteria != ""){
      this.dataService.addCriteria("creator", ":", this.ownershipCriteria);
    }
    this.reloadDatas();
  }
  resetFilter(){
    this.resetFlagOwner = this.switchBoolean(this.resetFlagOwner);
    this.resetFlagOrder = this.switchBoolean(this.resetFlagOrder);
    this.dataService.changeOrder(LaundryGroupOrders.idAsc.orderType,LaundryGroupOrders.idAsc.orderBy);
    this.reloadDatas();
  }

  switchBoolean(boo : boolean): boolean{
    if(boo == true){
      return false;
    }
    else{
      return true;
    }
  }

  onChangeOrder(orderModel: OrderModel){
    if(orderModel){
      this.dataService.changeOrder(orderModel.orderType, orderModel.orderBy);
    }else{
      this.dataService.changeOrder(LaundryGroupOrders.idAsc.orderType,LaundryGroupOrders.idAsc.orderBy);
    }
  }


  loadDatasToFillSelects():void{
    this.orders.list = [];
    this.orders.list.push(LaundryGroupOrders.idAsc);
    this.orders.list.push(LaundryGroupOrders.idDesc);
    this.orders.list.push(LaundryGroupOrders.nameAsc);
    this.orders.list.push(LaundryGroupOrders.nameDesc);
    this.orders.list.push(LaundryGroupOrders.creatorAsc);
    this.orders.list.push(LaundryGroupOrders.creatorDesc);
    let meAsACreatorOnly: MimicModel= new MimicModel("Groupes dont je suis le créateur",0,"mine");
    let meAsAMemberOnly: MimicModel = new MimicModel("Groupes dont je ne suis qu'un membre",1, "notMine");
    this.ownerMimicList.list.push(meAsACreatorOnly, meAsAMemberOnly);
    this.ownerMimicList.numberOfItems=2;
    this.dataService.localGetPotentialGroupMembers().subscribe(
      (list: UserListModel)=>{
        this.friendsList = list;
      },error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la récupération de vos amis: " +error.error ,"OK","error-snackbar")
      }
    );
    this.maneStatusService.localGetForSelect().subscribe(
      (list: ManeStatusListModel)=>{
        this.maneStatusList = list;
      },error=>{
        this.snackbar.openSnackBar("une erreur est survenue lors de la récupération des statuts de mannes: " + error.error, "OK", "error-snackbar");
      }
    )
  }

  ngOnDestroy(){
    super.ngOnDestroy();
  }

  onChangeOwnerFilter(mimicModel: MimicModel){
    this.ownershipCriteria = "";
    if(mimicModel){
      if(mimicModel.value==="mine"){
        this.ownershipCriteria = "mine";
      }
      if(mimicModel.value==="notMine"){
        this.ownershipCriteria="notMine";
      }
    }
    
  }

  onCreateLaundryGroup(laundryGroup: LaundryGroupModel){
    this.dataService.localAddData(laundryGroup).subscribe(
      ()=>{
        this.snackbar.openSnackBar("Groupe créé!", "OK", "success-snackbar");
        this.addLaundryGroup.laundryGroupName="";
        this.addLaundryGroup.laundryGroupMembers = [];
        this.reloadDatas();
      },error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la création du groupe: " + error.error, "OK", "error-snackbar");
      }
    );
  }

  onQuitLaundryGroup(laundryGroup: LaundryGroupModel){
    this.dataService.localQuit(laundryGroup).subscribe(
      ()=>{
        this.snackbar.openSnackBar("Vous avez quitté le groupe!", "OK", "success-snackbar");
        this.reloadDatas();
      },error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de votre retrait du groupe: " + error.error, "OK", "error-snackbar");
      }
    )
  }

  onDeleteLaundryGroup(laundryGroup: LaundryGroupModel){
    this.dataService.localDeleteData(laundryGroup).subscribe(
      ()=>{
        this.snackbar.openSnackBar("Groupe Supprimé", "OK", "success-snackbar");
        this.reloadDatas();
      },error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la suppression du groupe", "OK", "error-snackbar");
      }
    );
  }

  onUpdateLaundryGroup(laundryGroup: LaundryGroupModel){
    this.dataService.localEditData(laundryGroup).subscribe(
      ()=>{
        this.snackbar.openSnackBar("Groupe Mis à jour", "OK", "success-snackbar");
        this.reloadDatas();
      },error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la mise à jour du groupe", "OK", "error-snackbar");
      }
    );
  }


  
  
  protected createDataFormGroup(model: any): FormGroup {
    return this.formBuilder.group({});
  }
  protected onSubmitEditData(index: number): void {
    
  }
  protected createOrderLogos(): void {
    
  }
  protected initForm(): void {
    
  }
  protected onSubmitAddDataForm(): void {
    
  }

  onTest(LaundryGroupModel : LaundryManeModel){
    console.log(LaundryGroupModel);
  }

}
