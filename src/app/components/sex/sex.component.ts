import { Component, OnInit, OnDestroy } from '@angular/core';
import { GenericComponent } from '../generic/generic.component';
import { SnackbarComponent } from '../snackbar/snackbar.component';
import { IMAGES } from 'src/app/util/images';
import { AuthService } from 'src/app/services/auth-service';
import { SexService } from 'src/app/services/dataServices/sex.service';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { SexModel } from 'src/app/models/dataModels/sexModel';

@Component({
  selector: 'app-sex',
  templateUrl: './sex.component.html',
  styleUrls: ['./sex.component.css']
})
export class SexComponent extends GenericComponent implements OnInit, OnDestroy {

  idLogoSrcOne : string = '';
  idLogoSrcTwo : string = '';
  nameLogoSrcOne: string ='';
  nameLogoSrcTwo: string ='';

  constructor(protected snackbar: SnackbarComponent, protected authService: AuthService, protected sexService: SexService, protected formBuilder: FormBuilder) {
    super(snackbar, authService, sexService, formBuilder); 
  }

  ngOnInit() {
    super.ngOnInit();
  }

  ngOnDestroy(){
    super.ngOnDestroy();
  }


  createOrderLogos():void{
    this.idLogoSrcOne = IMAGES.upArrowFirstD;
    this.idLogoSrcTwo = IMAGES.upArrowSecondD;
    this.nameLogoSrcOne = IMAGES.upArrowFirstD;
    this.nameLogoSrcTwo = IMAGES.upArrowSecondD;
  }

  initForm(){
    this.addModelForm = this.formBuilder.group({
      'sex': new FormControl('', [Validators.required, Validators.pattern("^[A-ZÈÉ]([a-zéèê-]{1,29})$")]),
    });
  }

  onSubmitAddDataForm(){
    let newSex = new SexModel();
    newSex.sex = this.addModelForm.value['sex'];
    this.sexService.localAddData(newSex).subscribe(
      ()=>{
        this.snackbar.openSnackBar("Sexe ajouté", "OK", "success-snackbar");
        this.reloadDatas();
        this.initForm();
      }, error=>{
        this.snackbar.openSnackBar('Une erreur est survenue lors de l\'ajout du sexe' + error.error, 'OK', 'error-snackbar');
      }
    );
  }


  onOrderById():void{
    if(this.idLogoSrcOne === IMAGES.upArrowFirstD){
      this.idLogoSrcOne = IMAGES.downArrowFirstD;
      this.idLogoSrcTwo = IMAGES.downArrowSecondD;
      this.sexService.changeOrder("DESC", "sexId");
    }else{
      this.idLogoSrcOne = IMAGES.upArrowFirstD;
      this.idLogoSrcTwo = IMAGES.upArrowSecondD;
      this.sexService.changeOrder("ASC", "sexId");
    }
    this.reloadDatas();
  }

  onOrderByName():void{
    if(this.nameLogoSrcOne === IMAGES.upArrowFirstD){
      this.nameLogoSrcOne = IMAGES.downArrowFirstD;
      this.nameLogoSrcTwo = IMAGES.downArrowSecondD;
      this.sexService.changeOrder("DESC", "name");
    }else{
      this.nameLogoSrcOne = IMAGES.upArrowFirstD;
      this.nameLogoSrcTwo = IMAGES.upArrowSecondD;
      this.sexService.changeOrder("ASC", "name");
    }
    this.reloadDatas();
  }

  createDataFormGroup(sexModel: SexModel): FormGroup{
    return this.formBuilder.group({
      'sexId': new FormControl(sexModel.sexId, [Validators.required, Validators.min(1)]),
      'sex': new FormControl(sexModel.sex, [Validators.required, Validators.pattern("^[A-ZÈÉ]([a-zéèê-]{1,29})$")])
    });
  }

  onSubmitEditData(index:number){
    let brandModelToUpdate = new SexModel();
    brandModelToUpdate.sexId = this.formArray.controls[index].value['sexId'];
    brandModelToUpdate.sex = this.formArray.controls[index].value['sex'];
    this.sexService.localEditData(brandModelToUpdate).subscribe(
      ()=>{
        this.snackbar.openSnackBar('Sexe modifié!', 'OK', 'success-snackbar');
        this.reloadDatas();
      }, error=>{
        this.snackbar.openSnackBar('Une erreur est survenue lors de la modification du sexe: ' +error.error, 'OK', 'error-snackbar');
      }
    );
  }

}
