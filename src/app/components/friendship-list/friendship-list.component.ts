import { Component, OnInit, OnDestroy } from '@angular/core';
import { SnackbarComponent } from '../snackbar/snackbar.component';
import { AuthService } from 'src/app/services/auth-service';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { FriendshipService } from 'src/app/services/dataServices/friendship.service';
import { GenericComponent } from '../generic/generic.component';
import { Subscription } from 'rxjs';
import { FriendshipListModel } from 'src/app/models/dataListModels/friendshipListModel';
import { OrderModel } from 'src/app/models/dataModels/orderModel';
import { OrderListModel } from 'src/app/models/dataListModels/orderListModel';
import { FriendshipOrders } from 'src/app/util/friendshipOrders';
import { FriendshipModel } from 'src/app/models/dataModels/friendshipModel';
import { GenericListModel } from 'src/app/models/dataListModels/genericListModel';

@Component({
  selector: 'app-friendship-list',
  templateUrl: './friendship-list.component.html',
  styleUrls: ['./friendship-list.component.css']
})
export class FriendshipListComponent extends GenericComponent implements OnInit, OnDestroy {
  

  friends: FriendshipListModel = new FriendshipListModel();
  onHold: FriendshipListModel = new FriendshipListModel();
  toConfirm: FriendshipListModel = new FriendshipListModel();
  orders: OrderListModel = new OrderListModel();
  friendNameFilter = new FormControl('', [Validators.pattern(/^[a-z0-9-A-Z_-çàéèñùÇâêîôûøÇ ]{0,30}$/)]);
  resetFlagOrder: boolean = false;
  addFriendField = new FormControl('', [Validators.pattern(/^[a-z0-9-A-Z_-çàéèñùÇâêîôûøÇ ]{4,30}$/)]);
  constructor(protected snackbar: SnackbarComponent, protected authService: AuthService, protected formBuilder: FormBuilder, protected dataService: FriendshipService) {
    super(snackbar, authService, dataService, formBuilder);
    
   }

  ngOnInit() {
    this.dataList = new FriendshipListModel();
    super.ngOnInit();
    this.loadDatasForOtherListModels();
    this.orders.list.push(FriendshipOrders.idAsc);
    this.orders.list.push(FriendshipOrders.idDesc);
    this.orders.list.push(FriendshipOrders.userOneAsc);
    this.orders.list.push(FriendshipOrders.userOneDesc);
    this.orders.list.push(FriendshipOrders.userTwoAsc);
    this.orders.list.push(FriendshipOrders.userTwoDesc);
  }

  ngOnDestroy(){
    super.ngOnDestroy();
  }

  loadDatasForOtherListModels(){
    this.dataService.localGetFriendshipsOnHold().subscribe(
      (list: FriendshipListModel)=>{
        this.onHold = list;
      },error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la récupération des demandes en attente: " +error.error ,"OK","error-snackbar");
      }
    );
    this.dataService.localGetFriendshipsToConfirm().subscribe(
      (list: FriendshipListModel)=>{
        this.toConfirm = list;
      },error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la récupération des demandes à confirmer: " +error.error ,"OK","error-snackbar");
      }
    )
  }

  onChangeOrder(order: OrderModel){
    if(order){
      this.dataService.changeOrder(order.orderType, order.orderBy);
    }else{
      this.dataService.changeOrder(FriendshipOrders.idAsc.orderType, FriendshipOrders.idAsc.orderBy);
    }
  }

  submitFilter(){
    this.dataService.removeCriterias();
    if(this.friendNameFilter.value){
      this.dataService.addCriteria("userTwo", "~", this.friendNameFilter.value);
    }
    this.reloadDatas();

  }

  resetFilter(){
    this.dataService.removeCriterias();
    this.friendNameFilter.setValue('');
    this.resetFlagOrder = this.switchBoolean(this.resetFlagOrder);
    this.dataService.changeOrder(FriendshipOrders.idAsc.orderType, FriendshipOrders.idAsc.orderBy);
    this.reloadDatas();
  }

  switchBoolean(bool: boolean): boolean{
    if(bool == true){
      return false;
    }else{
      return true;
    }
  }

  onDeleteEstablishedFriendship(friendshipModel: FriendshipModel){
    this.dataService.deleteData(friendshipModel).subscribe(
      ()=>{
        this.snackbar.openSnackBar("Amitié supprimée", "OK", "success-snackbar");
        this.reloadDatas
      },error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la suppression de la relation: " + error.error, "OK", "error-snackbar");
      }
    );
  }

  onCancelFriendshipRequest(friendshipModel: FriendshipModel){
    this.dataService.localCancelFriendshipRequest(friendshipModel).subscribe(
      ()=>{
        this.snackbar.openSnackBar("Demande annulée!", "OK", "success-snackbar");
        this.loadDatasForOtherListModels();
      },error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la suppression de la demande: " + error.error, "OK", "error-snackbar");
      }
    );
  }

  onRejectFriendshipRequest(friendshipModel: FriendshipModel){
    this.dataService.localRejectFriendshipRequest(friendshipModel).subscribe(
      ()=>{
        this.snackbar.openSnackBar("Demande rejetée!", "OK", "success-snackbar");
        this.loadDatasForOtherListModels();
      },error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors du rejet de la demande: " + error.error, "OK", "error-snackbar");
      }
    );
  }

  onAcceptFriendshipRequest(friendshipModel: FriendshipModel){
    this.dataService.localAcceptFriendshipRequest(friendshipModel).subscribe(
      ()=>{
        this.snackbar.openSnackBar("Demande acceptée", "OK", "success-snackbar");
        this.loadDatasForOtherListModels();
        this.reloadDatas();
      },error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de l'acceptation de la demande': " + error.error, "OK", "error-snackbar");
      }
    );
  }

  onSendFriendshipRequest(){
    this.dataService.localSendRequest(this.addFriendField.value).subscribe(
      ()=>{
        this.snackbar.openSnackBar("Demande envoyée!", "OK", "success-snackbar");
        this.loadDatasForOtherListModels();
        this.addFriendField.setValue('');
      },error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de l'envoi de la demande: " + error.error, "OK", "error-snackbar");
      }
    )
  }


  protected createDataFormGroup(model: any): FormGroup {
    return this.formBuilder.group({});
  }
  protected onSubmitEditData(index: number): void {
    //throw new Error("Method not implemented.");
  }
  protected createOrderLogos(): void {
    //throw new Error("Method not implemented.");
  }
  protected initForm(): void {
    //throw new Error("Method not implemented.");
  }
  protected onSubmitAddDataForm(): void {
    //throw new Error("Method not implemented.");
  }

}
