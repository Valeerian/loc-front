import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClotheNoteComponent } from './clothe-note.component';

describe('ClotheNoteComponent', () => {
  let component: ClotheNoteComponent;
  let fixture: ComponentFixture<ClotheNoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClotheNoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClotheNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
