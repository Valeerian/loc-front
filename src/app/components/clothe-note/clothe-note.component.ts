import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { GenericComponent } from '../generic/generic.component';
import { SnackbarComponent } from '../snackbar/snackbar.component';
import { ClotheNoteService } from 'src/app/services/dataServices/clothe-note.service';
import { ClotheModel } from 'src/app/models/dataModels/clotheModel';
import { AuthService } from 'src/app/services/auth-service';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { QueryModel } from 'src/app/models/queryModel';
import { IMAGES } from 'src/app/util/images';
import { ClotheNoteModel } from 'src/app/models/dataModels/clotheNoteModel';
import { GenericListModel } from 'src/app/models/dataListModels/genericListModel';
import { error } from 'protractor';

@Component({
  selector: 'app-clothe-note',
  templateUrl: './clothe-note.component.html',
  styleUrls: ['./clothe-note.component.css']
})
export class ClotheNoteComponent extends GenericComponent implements OnInit, OnDestroy {

  /**Fonction appelée lorsque le component parent envoie un clotheModel à ce component
   * Doit contacter le serveur pour lui demander de récupérer tous les commentaires
   * Peut-être aussi faire en sorte que le component appelle le serveur par après pour lui demander spécifiquement le commentaire de l'utilisateur actuel sur ce vêtement?
   */
  @Input() set setClotheModel(clotheModel: ClotheModel){
    //Colonnes à afficher: date, utilisateur, note, commentaire, id?
    //colonnes avec lesquelles on devrait pouvoir trier: Id, date, utilisateur, note
    //Nom des colonnes: clothe, note, creationDate, clotheNoteId, comment, poseterId
    this.onResetAndSetValueToQM("clothe",":",""+clotheModel.clotheId);
    this.dataService.localGetMyComment(this.queryModel).subscribe(
      (clotheNote : ClotheNoteModel)=>{
        this.myComment = clotheNote;
        this.initForm();
      },error =>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la récupération de votre commentaire", "OK", "error-snackbar");
      }
    )
  }

  myComment: ClotheNoteModel = new ClotheNoteModel();

  idLogoSrcOne: string = "";
  idLogoSrcTwo: string = "";
  dateLogoSrcOne: string = "";
  dateLogoSrcTwo: string = "";
  noteLogoSrcOne: string = "";
  noteLogoSrcTwo: string = "";
  notes: number[]  = [0,1,2,3,4,5,6,7,8,9,10];

  constructor(protected snackbar: SnackbarComponent, protected authService: AuthService,  protected dataService: ClotheNoteService, protected formBuilder: FormBuilder) {
    super(snackbar, authService, dataService, formBuilder );
   }

  ngOnInit() {
   super.ngOnInit();
  }

  ngOnDestroy(){
    super.ngOnDestroy();
  }

  onClick(){
    console.log(this.myComment);
    console.log(this.queryModel);
  }

  createOrderLogos():void{
  this.idLogoSrcOne= IMAGES.upArrowFirstD;
  this.idLogoSrcTwo= IMAGES.upArrowSecondD;
  this.dateLogoSrcOne= IMAGES.upArrowFirstD;
  this.dateLogoSrcTwo= IMAGES.upArrowSecondD;
  this.noteLogoSrcOne= IMAGES.upArrowFirstD;
  this.noteLogoSrcTwo= IMAGES.upArrowSecondD;
  }

  initForm(){
    this.addModelForm = this.formBuilder.group({
      'note': new FormControl(this.myComment.note, [Validators.required, Validators.min(0), Validators.max(10)]),
      'comment': new FormControl(this.myComment.comment, [Validators.pattern(/^[A-Za-z0-9ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØŒŠþÙÚÛÜÝŸàáâãäåæçèéêëìíîïðñòóôõöøœšÞùúûü:ýÿ=ß,\\\/'\"!\{\}\[\] ().\-\;?_]{0,255}$/)])
    });
  }

  onSubmitAddDataForm(){
    this.myComment.note = this.addModelForm.value['note'];
    this.myComment.comment = this.addModelForm.value['comment'];
    this.dataService.localAddData(this.myComment).subscribe(
      ()=>{
        this.snackbar.openSnackBar("Commentaire créé ou mis à jour", "OK", "success-snackbar");
      },error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la création/modification du commentaire", "OK", "error-snackbar");
      }
    )
  }

  onOrderById():void{
    if(this.idLogoSrcOne === IMAGES.upArrowFirstD){
      this.idLogoSrcOne = IMAGES.downArrowFirstD;
      this.idLogoSrcTwo = IMAGES.downArrowSecondD;
      this.dataService.changeOrder("DESC", "clotheNoteId");
    }else{
      this.idLogoSrcOne = IMAGES.upArrowFirstD;
      this.idLogoSrcTwo = IMAGES.upArrowSecondD;
      this.dataService.changeOrder("ASC", "clotheNoteId");
    }
    this.reloadDatas();
  }

  onOrderByNote():void{
    if(this.noteLogoSrcOne === IMAGES.upArrowFirstD){
      this.noteLogoSrcOne = IMAGES.downArrowFirstD;
      this.noteLogoSrcTwo = IMAGES.downArrowSecondD;
      this.dataService.changeOrder("DESC", "note");
    }else{
      this.noteLogoSrcOne = IMAGES.upArrowFirstD;
      this.noteLogoSrcTwo = IMAGES.upArrowSecondD;
      this.dataService.changeOrder("ASC", "note");
    }
    this.reloadDatas();
  }

  onOrderByDate():void{
    if(this.dateLogoSrcOne === IMAGES.upArrowFirstD){
      this.dateLogoSrcOne = IMAGES.downArrowFirstD;
      this.dateLogoSrcTwo = IMAGES.downArrowSecondD;
      this.dataService.changeOrder("DESC", "creationDate");
    }else{
      this.dateLogoSrcOne = IMAGES.upArrowFirstD;
      this.dateLogoSrcTwo = IMAGES.upArrowSecondD;
      this.dataService.changeOrder("ASC", "creationDate");
    }
    this.reloadDatas();
  }

  //Pas sûr que celle là nous servira
  createDataFormGroup(clotheNoteModel: ClotheNoteModel){
    return this.formBuilder.group({});
  }

  //Pas sûr que celui-ci nous servira
  onSubmitEditData(){}



}
