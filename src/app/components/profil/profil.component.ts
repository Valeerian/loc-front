import { Component, OnInit, OnDestroy } from '@angular/core';
import { SnackbarComponent } from '../snackbar/snackbar.component';
import { AuthService } from 'src/app/services/auth-service';
import { TokenStorageService } from 'src/app/services/token-storage.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ProfilService } from 'src/app/services/dataServices/profil.service';
import { UserModel } from 'src/app/models/dataModels/userModel';
import * as jwt_decode from 'jwt-decode';
import { Subject, Subscription } from 'rxjs';
import { SizeListModel } from 'src/app/models/dataListModels/sizeListModel';
import { SizeModel } from 'src/app/models/dataModels/sizeModel';
import { QueryModel } from 'src/app/models/queryModel';
@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit, OnDestroy {
  private userModel: UserModel;
  private chestSizeList: SizeListModel;
  private bottomSizeList: SizeListModel;
  private footSizeList: SizeListModel;
  private queryModel : QueryModel;

  changePasswordForm: FormGroup;
  changePersonnalInfosForm: FormGroup;

  sizeUnits = [{name:'', valeur:null},{name: 'EU', valeur:'EU'},{name: 'IT', valeur:'IT'},{name: 'US', valeur:'US'},{name: 'UK', valeur:'UK'}];
  metrics = [{name:'', valeur: null}, {name: 'Centimètres', valeur: 'CM'}, {name:'Pouces', valeur: 'INC'}]; 

  constructor(private snackbar: SnackbarComponent, private authService: AuthService, private token: TokenStorageService, private router: Router, private formBuilder: FormBuilder, private profilService: ProfilService) { }

  ngOnInit() {
    this.userModel = new UserModel();
    this.chestSizeList = new SizeListModel();
    this.bottomSizeList = new SizeListModel();
    this.footSizeList = new SizeListModel();
    this.prepareQueryModel();
    

    this.profilService.localGetSizes(this.queryModel).subscribe(
      (sizeList : SizeListModel)=>{
        let emptySM= new SizeModel();
        emptySM.size=null;
        this.footSizeList.list.push(emptySM);
        this.chestSizeList.list.push(emptySM);
        this.bottomSizeList.list.push(emptySM);
        for(let size of sizeList.list){
          if(size.category ==="FOOT"){
            this.footSizeList.numberOfItems = this.footSizeList.list.push(size);
          }
          else if(size.category === "CHEST"){
            this.chestSizeList.numberOfItems = this.chestSizeList.list.push(size);
          }
          else if(size.category === "BOTTOM"){
            this.bottomSizeList.numberOfItems = this.bottomSizeList.list.push(size);
          }
          else{}
        }
        this.updateChangePersonnalInfosForm();
      }, error=>{
        this.snackbar.openSnackBar('Une erreur est survenue durant la récupération des tailles ' + error.error, 'OK', 'error-snackbar');
      }
    );

    this.initForm();
    this.profilService.localGetProfile().subscribe(
      answer => {
        this.userModel.metric = answer.metric;
        this.userModel.chestSize= answer.chestSize;
        this.userModel.footSize = answer.footSize;
        this.userModel.bottomSize = answer.bottomSize;
        this.userModel.sizeUnit = answer.sizeUnit;
        this.userModel.firstName = answer.firstName;
        this.userModel.lastName = answer.lastName;
        this.userModel.mail = answer.mail;
        this.userModel.sex = answer.sex;
        this.userModel.explicitSex = this.setExplicitSex(answer.sex);
      },error => {
        this.snackbar.openSnackBar('Une erreur est survenue durant la récupération de vos données: ' + error.error, 'OK', 'error-snackbar');
      }
    );
  }

  prepareQueryModel(){
    this.queryModel = new QueryModel();
    this.queryModel.elementsPerPage = 500;
    this.queryModel.pageNumber = 0;
    this.queryModel.addOrder("category", "ASC");
    this.queryModel.addOrder("size", "ASC");
    this.queryModel.addCriteria("category", ":", "FOOT");
    this.queryModel.addCriteria("category", ":", "CHEST");
    this.queryModel.addCriteria("category", ":", "BOTTOM");
    this.queryModel.addCriteriaCombiner(0,1,"|",0); //C0= cat=FOOT OU CHEST
    this.queryModel.addCriteriaCombiner(2,3,"|",1); //C1= C0 ou cat=BOTTOM
  }

  
  ngOnDestroy(){}

  updateChangePersonnalInfosForm(){
    
    this.changePersonnalInfosForm.controls['firstName'].setValue(this.userModel.firstName);
    this.changePersonnalInfosForm.controls['lastName'].setValue(this.userModel.lastName);
    this.changePersonnalInfosForm.controls['mail'].setValue(this.userModel.mail);
    this.changePersonnalInfosForm.controls['metric'].setValue(this.userModel.metric);
    this.changePersonnalInfosForm.controls['chestSize'].setValue(this.userModel.chestSize);
    this.changePersonnalInfosForm.controls['footSize'].setValue(this.userModel.footSize);
    this.changePersonnalInfosForm.controls['bottomSize'].setValue(this.userModel.bottomSize);
    this.changePersonnalInfosForm.controls['sizeUnit'].setValue(this.userModel.sizeUnit);
    if(!(this.changePersonnalInfosForm.value['sex'] === this.userModel.sex)){/**
      Lorsqu'on essaye d'attribuer deux fois la même valeur à un champ de type radio button, l'attribut checked disparaît du radio
      qui est actuellement sélectionné. Pour éviter que cela se produise, je vérifie que je n'essaye pas d'attribuer à mon radi group
      la valeur qu'il a déjà*/
      this.changePersonnalInfosForm.controls['sex'].setValue(this.userModel.sex);
    }   
  } 
 
  


  initForm(){
    this.changePasswordForm =this.formBuilder.group({
      'oldPassword': new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(50)]),
      'newPassword': new FormControl('',[Validators.required, Validators.minLength(6), Validators.maxLength(50)]),
      'newPasswordConfirmation': new FormControl('',[Validators.required, Validators.minLength(6), Validators.maxLength(50)]),
    });
    this.changePersonnalInfosForm = this.formBuilder.group({
      'firstName':new FormControl(this.userModel.firstName, [Validators.required, Validators.minLength(2), Validators.maxLength(30)]),
      'lastName': new FormControl(this.userModel.lastName, [Validators.required, Validators.minLength(2), Validators.maxLength(30)]),
      'mail': new FormControl(this.userModel.mail, [Validators.required, Validators.email]),  
      'metric': new FormControl(this.userModel.metric, [Validators.pattern("^(CM|INC)$")]),
      'chestSize': new FormControl(this.userModel.chestSize, [Validators.pattern("^([SML]|[0-9]?X(S|L)|X{0,4}[LS])$")]),
      'footSize': new FormControl(this.userModel.footSize, [Validators.pattern("^(EU|UK|US|IT)([0-9]{1,2}(,5)?)$")]),
      'bottomSize': new FormControl(this.userModel.bottomSize, [Validators.pattern("^(EU|UK|US|IT)W([0-9]{1,2})L([0-9]{1,2})$")]),
      'sizeUnit': new FormControl(this.userModel.sizeUnit, [Validators.pattern("^(EU|UK|IT|US)$")]),
      'sex': new FormControl(this.userModel.sex),

    });
  }


  setExplicitSex(sex? : Boolean): string{
    if(sex === null){
      return "Non communiqué"
    }
    if(sex === false){
      return "Homme";
    }
    else{
      return "Femme";
    }
  }

  onSubmitChangeInfosForm(){
    this.userModel.firstName = this.changePersonnalInfosForm.value['firstName'];
    this.userModel.lastName = this.changePersonnalInfosForm.value['lastName'];
    this.userModel.mail = this.changePersonnalInfosForm.value['mail'];
    this.userModel.metric = this.changePersonnalInfosForm.value['metric'];
    this.userModel.chestSize = this.changePersonnalInfosForm.value['chestSize'];
    this.userModel.footSize = this.changePersonnalInfosForm.value['footSize'];
    this.userModel.bottomSize = this.changePersonnalInfosForm.value['bottomSize'];
    this.userModel.sizeUnit = this.changePersonnalInfosForm.value['sizeUnit'];
    this.userModel.sex = this.changePersonnalInfosForm.value['sex'];
    this.profilService.localEditProfile(this.userModel).subscribe(
      answer =>{
        this.userModel.firstName = answer.firstName;
        this.userModel.lastName = answer.lastName;
        this.userModel.mail = answer.mail;
        this.userModel.metric = answer.metric;
        this.userModel.sizeUnit = answer.sizeUnit;
        this.userModel.sex = answer.sex;
        this.userModel.bottomSize = answer.bottomSize;
        this.userModel.chestSize = answer.chestSize;
        this.userModel.footSize = answer.footSize;
        this.userModel.explicitSex = this.setExplicitSex(answer.sex);
        this.snackbar.openSnackBar('Votre profil a bien été mis à jour!', 'OK', 'success-snackbar');
        this.updateChangePersonnalInfosForm();
      },error =>{
        this.snackbar.openSnackBar('Une erreur est survenue pendant la mise à jour de votre profil: ' +error.error, 'OK', 'error-snackbar');
      }
    );
  }

  onSubmitChangePasswordForm(){
    let errorFlag: Boolean;
    this.userModel.oldPassword = this.changePasswordForm.value['oldPassword'];
    this.userModel.newPassword = this.changePasswordForm.value['newPassword'];
    this.userModel.newPasswordConfirmation = this.changePasswordForm.value['newPasswordConfirmation'];
    if(this.userModel.newPasswordConfirmation != this.userModel.newPassword){
      this.snackbar.openSnackBar('Le mot de passe et sa confirmation ne concordent pas', 'OK', 'error-snackbar');
      errorFlag = true;
    }
    if(!errorFlag){
      this.authService.attemptLocalChangePassword(this.userModel).subscribe(
        answer => {
          this.token.saveToken(answer.jwtToken);
          let decodedToken = jwt_decode(answer.jwtToken);
          this.token.saveIssuedAt(decodedToken['iat']);
          this.token.saveExpiration(decodedToken['exp']);
          this.snackbar.openSnackBar('Mot de passe changé!', 'OK', 'success-snackbar');
        },error => {
          this.snackbar.openSnackBar('Echec de la modification du mot de passe: ' + error.error, 'OK', 'error-snackbar')
        }
      )
    }
  }

  onDeleteAccount(){
    this.authService.attemptLocalDeleteAccount().subscribe(
      () => {
        this.token.empty();
        this.authService.logout();
        this.router.navigate(['/home'])
        this.snackbar.openSnackBar('Votre compte a été supprimé.', 'OK', 'success-snackbar')
      },error =>{
        this.snackbar.openSnackBar('Votre compte n\'a pas été supprimé a cause d\'une erreur: ' + error.error, 'OK', 'error-snackbar');
      }
    )
  }

}


