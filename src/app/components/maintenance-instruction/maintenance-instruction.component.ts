import { Component, OnInit, OnDestroy } from '@angular/core';
import { IMAGES } from 'src/app/util/images';
import { GenericComponent } from '../generic/generic.component';
import { MaintenanceInstructionService } from 'src/app/services/dataServices/maintenance-instruction.service';
import { AuthService } from 'src/app/services/auth-service';
import { SnackbarComponent } from '../snackbar/snackbar.component';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { MaintenanceInstructionModel } from 'src/app/models/dataModels/maintenanceInstruction';

@Component({
  selector: 'app-maintenance-instruction',
  templateUrl: './maintenance-instruction.component.html',
  styleUrls: ['./maintenance-instruction.component.css']
})
export class MaintenanceInstructionComponent extends GenericComponent implements OnInit, OnDestroy {

  idLogoSrcOne : string = '';
  idLogoSrcTwo : string = '';
  nameLogoSrcOne: string ='';
  nameLogoSrcTwo: string ='';
  instructionLogoSrcOne: string='';
  instructionLogoSrcTwo: string = '';

  constructor(protected snackbar: SnackbarComponent, protected authService: AuthService, protected maintenanceInstructionService: MaintenanceInstructionService, protected formBuilder: FormBuilder) {
    super(snackbar, authService, maintenanceInstructionService, formBuilder); 
  }

  ngOnInit() {
    super.ngOnInit();
  }

  ngOnDestroy(){
    super.ngOnDestroy();
  }


  createOrderLogos():void{
    this.idLogoSrcOne = IMAGES.upArrowFirstD;
    this.idLogoSrcTwo = IMAGES.upArrowSecondD;
    this.nameLogoSrcOne = IMAGES.upArrowFirstD;
    this.nameLogoSrcTwo = IMAGES.upArrowSecondD;
    this.instructionLogoSrcOne = IMAGES.upArrowFirstD;
    this.instructionLogoSrcOne = IMAGES.upArrowSecondD;
  }

  initForm(){
    this.addModelForm = this.formBuilder.group({
      'name': new FormControl('', [Validators.required, Validators.pattern("^[A-Za-z0-9°ÈÉÇ éèà]{2,50}$")]),
      'instruction': new FormControl('',[Validators.required, Validators.pattern("^[A-ZÈÉÇ]([a-z0-9çéê°,èêà \-'.]{2,254})$")])
    });
  }

  onSubmitAddDataForm(){
    let newInstruction = new MaintenanceInstructionModel();
    newInstruction.name = this.addModelForm.value['name'];
    newInstruction.instruction = this.addModelForm.value['instruction'];
    this.maintenanceInstructionService.localAddData(newInstruction).subscribe(
      ()=>{
        this.snackbar.openSnackBar("Instruction ajoutée", "OK", "success-snackbar");
        this.reloadDatas();
        this.initForm();
      }, error=>{
        this.snackbar.openSnackBar('Une erreur est survenue lors de l\'ajout de l\'instruction' + error.error, 'OK', 'error-snackbar');
      }
    );
  }


  onOrderById():void{
    if(this.idLogoSrcOne === IMAGES.upArrowFirstD){
      this.idLogoSrcOne = IMAGES.downArrowFirstD;
      this.idLogoSrcTwo = IMAGES.downArrowSecondD;
      this.maintenanceInstructionService.changeOrder("DESC", "maintenanceInstructionId");
    }else{
      this.idLogoSrcOne = IMAGES.upArrowFirstD;
      this.idLogoSrcTwo = IMAGES.upArrowSecondD;
      this.maintenanceInstructionService.changeOrder("ASC", "maintenanceInstructionId");
    }
    this.reloadDatas();
  }

  onOrderByName():void{
    if(this.nameLogoSrcOne === IMAGES.upArrowFirstD){
      this.nameLogoSrcOne = IMAGES.downArrowFirstD;
      this.nameLogoSrcTwo = IMAGES.downArrowSecondD;
      this.maintenanceInstructionService.changeOrder("DESC", "name");
    }else{
      this.nameLogoSrcOne = IMAGES.upArrowFirstD;
      this.nameLogoSrcTwo = IMAGES.upArrowSecondD;
      this.maintenanceInstructionService.changeOrder("ASC", "name");
    }
    this.reloadDatas();
  }

  onOrderByInstruction():void{
    if(this.instructionLogoSrcOne === IMAGES.upArrowFirstD){
      this.instructionLogoSrcOne = IMAGES.downArrowFirstD;
      this.instructionLogoSrcTwo = IMAGES.downArrowSecondD;
      this.maintenanceInstructionService.changeOrder("DESC", "instruction");
    }else{
      this.instructionLogoSrcOne = IMAGES.upArrowFirstD;
      this.instructionLogoSrcTwo = IMAGES.upArrowSecondD;
      this.maintenanceInstructionService.changeOrder("ASC", "instruction");
    }
    this.reloadDatas();
  }

  createDataFormGroup(instructionModel: MaintenanceInstructionModel): FormGroup{
    return this.formBuilder.group({
      'id': new FormControl(instructionModel.maintenanceInstructionId, [Validators.required, Validators.min(1)]),
      'name': new FormControl(instructionModel.name, [Validators.required, Validators.pattern("^[A-Za-z0-9°ÈÉÇ éèà]{2,50}$")]),
      'instruction': new FormControl(instructionModel.instruction,[Validators.required, Validators.pattern("^[A-ZÈÉÇ]([a-z0-9çéê°èà, \-'.]{2,254})$")])
    });
  }

  onSubmitEditData(index:number){
    let maintenanceInstructionModelToUpdate = new MaintenanceInstructionModel();
    maintenanceInstructionModelToUpdate.maintenanceInstructionId = this.formArray.controls[index].value['id'];
    maintenanceInstructionModelToUpdate.name = this.formArray.controls[index].value['name'];
    maintenanceInstructionModelToUpdate.instruction = this.formArray.controls[index].value['instruction'];
    this.maintenanceInstructionService.localEditData(maintenanceInstructionModelToUpdate).subscribe(
      ()=>{
        this.snackbar.openSnackBar('Instruction modifiée!', 'OK', 'success-snackbar');
        this.reloadDatas();
      }, error=>{
        this.snackbar.openSnackBar('Une erreur est survenue lors de la modification de l\'instruction: ' +error.error, 'OK', 'error-snackbar');
      }
    );
  }

}
