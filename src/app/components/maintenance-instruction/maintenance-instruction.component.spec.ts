import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaintenanceInstructionComponent } from './maintenance-instruction.component';

describe('MaintenanceInstructionComponent', () => {
  let component: MaintenanceInstructionComponent;
  let fixture: ComponentFixture<MaintenanceInstructionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaintenanceInstructionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaintenanceInstructionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
