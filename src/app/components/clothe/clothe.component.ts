import { Component, OnInit, Input, OnDestroy, ViewChild } from '@angular/core';
import { SeasonModel } from 'src/app/models/dataModels/seasonModel';
import { SeasonSelectProviderService } from 'src/app/services/dataServices/subServices/season-select-provider-service.service';
import { SeasonListModel } from 'src/app/models/dataListModels/seasonListModel';
import { SexSelectProviderService } from 'src/app/services/dataServices/subServices/sex-select-provider.service';
import { SexListModel } from 'src/app/models/dataListModels/sexListModel';
import { SnackbarComponent } from '../snackbar/snackbar.component';
import { ClotheTypeSelectProviderService } from 'src/app/services/dataServices/subServices/clothe-type-select-provider.service';
import { BrandSelectProviderService } from 'src/app/services/dataServices/subServices/brand-select-provider.service';
import { ColorSelectProviderService } from 'src/app/services/dataServices/subServices/color-select-provider.service';
import { MaterialSelectProviderService } from 'src/app/services/dataServices/subServices/material-select-provider.service';
import { ProviderSelectProviderService } from 'src/app/services/dataServices/subServices/provider-select-provider.service';
import { SizeSelectProviderService } from 'src/app/services/dataServices/subServices/size-select-provider.service';
import { StyleSelectProviderService } from 'src/app/services/dataServices/subServices/style-select-provider.service';
import { ThemeSelectProviderService } from 'src/app/services/dataServices/subServices/theme-select-provider.service';
import { BrandListModel } from 'src/app/models/dataListModels/brandListModel';
import { ClotheTypeListModel } from 'src/app/models/dataListModels/clotheTypeListModel';
import { ColorListModel } from 'src/app/models/dataListModels/colorListModel';
import { MaterialListModel } from 'src/app/models/dataListModels/materialListModel';
import { ProviderListModel } from 'src/app/models/dataListModels/providerListModel';
import { SizeListModel } from 'src/app/models/dataListModels/sizeListModel';
import { StyleListModel } from 'src/app/models/dataListModels/styleListModel';
import { ThemeListModel } from 'src/app/models/dataListModels/themeListModel';
import { Validators, FormControl, FormBuilder, FormGroup } from '@angular/forms';
import { GenericComponent } from '../generic/generic.component';
import { AuthService } from 'src/app/services/auth-service';
import { ClotheService } from 'src/app/services/dataServices/clothe.service';
import { SexMultiDropdownComponent } from '../subComponents/sex-multi-dropdown/sex-multi-dropdown.component';
import { ClotheModel } from 'src/app/models/dataModels/clotheModel';
import { TokenStorageService } from 'src/app/services/token-storage.service';
import { Subscription } from 'rxjs';
import { MaintenanceInstructionService } from 'src/app/services/dataServices/maintenance-instruction.service';
import { MaintenanceInstructionListModel } from 'src/app/models/dataListModels/maintenanceInstructionListModel';
import { MaintenanceInstructionSelectProviderService } from 'src/app/services/dataServices/subServices/maintenance-instruction-select-provider.service';
import { ClotheListModel } from 'src/app/models/dataListModels/clotheListModel';
import { OrderListModel } from 'src/app/models/dataListModels/orderListModel';
import { OrderModel } from 'src/app/models/dataModels/orderModel';
import { ClothesOrders } from 'src/app/util/clothesOrders';
import { SizeModel } from 'src/app/models/dataModels/sizeModel';
import { MaterialModel } from 'src/app/models/dataModels/materialModel';
import { BrandModel } from 'src/app/models/dataModels/brandModel';
import { SexModel } from 'src/app/models/dataModels/sexModel';
import { ColorModel } from 'src/app/models/dataModels/colorModel';
import { ClotheTypeModel } from 'src/app/models/dataModels/clotheTypeModel';
import { ProviderModel } from 'src/app/models/dataModels/providerModel';
import { StyleModel } from 'src/app/models/dataModels/styleModel';
import { ThemeModel } from 'src/app/models/dataModels/themeModel';

@Component({
  selector: 'app-clothe',
  templateUrl: './clothe.component.html',
  styleUrls: ['./clothe.component.css']
})
export class ClotheComponent extends GenericComponent implements OnInit, OnDestroy {

  // FormCOntrol utilisé pour le champ de recherche par nom
  clotheNameFilter=new FormControl('',[Validators.pattern(/^[a-zA-z0-9 \-_\/çàâêîôû'"\.Ç]{0,50}$/)]);
  //Flags utilisés pour reset les selects utilisés pour filtrer
  resetFlagSeason:boolean = false;
  resetFlagProvider:boolean = false;
  resetFlagClotheType:boolean = false;
  resetFlagColor:boolean = false;
  resetFlagSex:boolean = false;
  resetFlagBrand:boolean = false;
  resetFlagMaterial:boolean = false;
  resetFlagSize:boolean = false;
  resetFlagStyle:boolean = false;
  resetFlagTheme:boolean = false;
  resetFlagMinimumNote:boolean = false;
  resetFlagOrder:boolean = false;

  //Tableaux permettants de stocker les critères de recherche jusqu'à ce que le filtrage soit demandé
  seasonCriteriaArray: number[] = [];
  providerCriteriaArray: number[] = [];
  clotheTypeCriteriaArray: number[] = [];
  colorCriteriaArray: number[] = [];
  sexCriteriaArray: number[] = [];
  brandCriteriaArray: number[] = [];
  materialCriteriaArray: number[] = [];
  sizeCriteriaArray: number[] = [];
  styleCriteriaArray: number[] = [];
  themeCriteriaArray: number[] = [];
  minimumNoteCriteria: number;
  orders: OrderListModel = new OrderListModel();
  protected hasAdminRole: boolean = false;

  addClotheIndex: number =-1;

  clotheNameForComments: string = "";

  showCommentSection: boolean = false;
  clotheModelForCommentSection: ClotheModel;

  /**
   * Utilisés pour stocker les données reçues du serveur et les envoyer aux components enfants;
   */
  sizeListModel: SizeListModel = new SizeListModel();
  sizeCategories: string[] = [];
  clotheTypeListModel: ClotheTypeListModel =new ClotheTypeListModel();
  clotheTypeCategories: string[] = [];
  sexListModel: SexListModel = new SexListModel();
  maintenanceInstructionListModel: MaintenanceInstructionListModel = new MaintenanceInstructionListModel();
  brandListModel: BrandListModel = new BrandListModel();
  seasonListModel: SeasonListModel = new SeasonListModel();
  colorListModel: ColorListModel = new ColorListModel();
  materialListModel: MaterialListModel=  new MaterialListModel();
  providerListModel: ProviderListModel = new ProviderListModel();
  styleListModel: StyleListModel = new StyleListModel();
  themeListModel: ThemeListModel = new ThemeListModel();
  




  constructor(protected snackbar: SnackbarComponent, protected authService: AuthService ,private seasonService: SeasonSelectProviderService, private sexService: SexSelectProviderService,
    private brandService: BrandSelectProviderService, private maintenanceInstructionService: MaintenanceInstructionSelectProviderService, private clotheTypeService: ClotheTypeSelectProviderService, private colorService: ColorSelectProviderService,
    private materialService: MaterialSelectProviderService, private providerService: ProviderSelectProviderService, private sizeService: SizeSelectProviderService,
    private styleService: StyleSelectProviderService, protected clotheService: ClotheService, private themeService: ThemeSelectProviderService, protected formBuilder: FormBuilder, private token: TokenStorageService) {
      super(snackbar, authService, clotheService, formBuilder)
    }

    /**
   * Fonction appelée lors du clic sur le bouton valider de la section filtres
   * Récupérer le contenu de clotheNameFilter
   */
  submitFilter(){
    this.clotheService.removeCriterias();
    if(this.seasonCriteriaArray.length>0){
      for(let id of this.seasonCriteriaArray){
        this.clotheService.addCriteria("seasonId", ":", ''+id);
      }
    }
    if(this.providerCriteriaArray.length>0){
      for(let id of this.providerCriteriaArray){
        this.clotheService.addCriteria("providerId", ":", ''+id);
      }
    }
    if(this.clotheTypeCriteriaArray.length>0){
      for(let id of this.clotheTypeCriteriaArray){
        this.clotheService.addCriteria("clotheTypeId", ":", ''+id);
      }
    }
    if(this.colorCriteriaArray.length>0){
      for(let id of this.colorCriteriaArray){
        this.clotheService.addCriteria("colorId", ":", ''+id);
      }
    }
    if(this.sexCriteriaArray.length>0){
      for(let id of this.sexCriteriaArray){
        this.clotheService.addCriteria("sexId", ":", ''+id);
      }
    }
    if(this.brandCriteriaArray.length>0){
      for(let id of this.brandCriteriaArray){
        this.clotheService.addCriteria("brandId", ":", ''+id);
      }
    }
    if(this.materialCriteriaArray.length>0){
      for(let id of this.materialCriteriaArray){
        this.clotheService.addCriteria("materialId", ":", ''+id);
      }
    }
    if(this.sizeCriteriaArray.length>0){
      for(let id of this.sizeCriteriaArray){
        this.clotheService.addCriteria("sizeId", ":", ''+id);
      }
    }
    if(this.styleCriteriaArray.length>0){
      for(let id of this.styleCriteriaArray){
        this.clotheService.addCriteria("styleId", ":", ''+id);
      }
    }
    if(this.themeCriteriaArray.length>0){
      for(let id of this.themeCriteriaArray){
        this.clotheService.addCriteria("themeId", ":", ''+id);
      }
    }
    if(this.minimumNoteCriteria){
      this.clotheService.addCriteria("note",">", ""+this.minimumNoteCriteria);
    }
    if(this.clotheNameFilter.value){
      this.clotheService.addCriteria("name","~", this.clotheNameFilter.value);
    }
    this.reloadDatas();
  }

  ngOnInit() {
    //Permet de savoir si l'utilisateur est administrateur
    let roles :string[] = this.token.getRoles();
    for(let role of roles){
      if(role === "administrateur"){
        this.hasAdminRole = true;
      }
    }
    //Récupère les données pour les Selects
    this.loadDatasToFillSelects();
    super.ngOnInit();
  }
  ngOnDestroy(){
    super.ngOnDestroy();
  }


  /**
   *Fonction appelée lors du clic sur le bouton réinitialiser de la section filtres 
   Vide les sélects et le champ de recherche de leur contenu
   */
  resetFilter(){
    this.clotheService.removeCriterias()
    this.clotheNameFilter.setValue('');
    this.resetFlagSeason = this.switchBoolean(this.resetFlagSeason);
    this.resetFlagProvider = this.switchBoolean(this.resetFlagProvider);
    this.resetFlagClotheType = this.switchBoolean(this.resetFlagClotheType);
    this.resetFlagColor = this.switchBoolean(this.resetFlagColor);
    this.resetFlagSex = this.switchBoolean(this.resetFlagSex);
    this.resetFlagBrand = this.switchBoolean(this.resetFlagBrand);
    this.resetFlagMaterial = this.switchBoolean(this.resetFlagMaterial);
    this.resetFlagSize = this.switchBoolean(this.resetFlagSize);
    this.resetFlagStyle = this.switchBoolean(this.resetFlagStyle);
    this.resetFlagTheme = this.switchBoolean(this.resetFlagTheme);
    this.resetFlagMinimumNote = this.switchBoolean(this.resetFlagMinimumNote);
    this.resetFlagOrder = this.switchBoolean(this.resetFlagOrder);
    this.dataService.changeOrder(ClothesOrders.idAsc.orderType, ClothesOrders.idAsc.orderBy);
    this.reloadDatas();
  }

  /**
   * Permet de transformer un boolean true en false et vice-versa
   * @param bool Le boolean a modifier
   */
  switchBoolean(bool: boolean): boolean{
    if(bool == true){
      return false;
    }else{
      return true;
    }
  }

  /**
   * Permet d'ajouter un clotheModel à la liste pour pouvoir ajouter un vêtement à la bdd
   */
  onAddClothe(){
    let clotheModel: ClotheModel = new ClotheModel();
    clotheModel.clotheId = this.addClotheIndex;
    this.addClotheIndex--;
    if(this.dataList){//Si dataList existe
      if(this.dataList.list){//Si dataList existe et que ça liste ne vaut pas undefined
        if(this.dataList.list.length < 1){//si la liste de dataliste existe mais est vide
          this.dataList.list = [];
        }
      }else{//Si dataList existe mais que sa liste est encore undefined
        this.dataList.list = [];
      }
    }
    else{
      this.dataList = new ClotheListModel();
    }
    let clothes: ClotheModel[] = this.dataList.list as ClotheModel[]
    clothes.push(clotheModel);
    this.dataList.list = clothes; 
  }

  


  onChangeOrder(orderModel: OrderModel){
    if(orderModel){//Si l'utilisateur ne souhaite pas laisser l'ordre par défaut
    this.dataService.changeOrder(orderModel.orderType, orderModel.orderBy)
    }else{//Si tri par défaut
      this.dataService.changeOrder(ClothesOrders.idAsc.orderType, ClothesOrders.idAsc.orderBy);      
    }
  }
  

  /**
   * Afin de ne pas harceler le serveur avec une requête pour récupérer les données avec lesquelles remplir les select à chaque création d'un component "fils", on
   * contacte le serveur directement depuis le parent (depuis ici donc)
   */
  loadDatasToFillSelects():void{
    this.seasonService.localGetDatas().subscribe(//Partie pour les select des saisons
      (list: SeasonListModel)=>{
        this.seasonListModel = list;
      }, error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la récupération des saisons", "OK", "error-snackbar");
      }
    );
    this.orders.list.push(ClothesOrders.idAsc);
    this.orders.list.push(ClothesOrders.idDesc);
    this.orders.list.push(ClothesOrders.providerAsc);
    this.orders.list.push(ClothesOrders.providerDesc);
    this.orders.list.push(ClothesOrders.nameAsc);
    this.orders.list.push(ClothesOrders.nameDesc);
    this.orders.list.push(ClothesOrders.sexAsc);
    this.orders.list.push(ClothesOrders.sexDesc);
    this.orders.list.push(ClothesOrders.seasonAsc);
    this.orders.list.push(ClothesOrders.seasonDesc);
    this.orders.list.push(ClothesOrders.brandAsc);
    this.orders.list.push(ClothesOrders.brandDesc);

    this.sexService.localGetDatas().subscribe(//Partie pour les select des sexes
      (list: SexListModel)=>{
        this.sexListModel = list;
      }, error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la récupération des sexes", "OK", "error-snackbar");
      }
    );
    this.maintenanceInstructionService.localGetDatas().subscribe(//Partie pour les select instructions de maintenance
      (list: MaintenanceInstructionListModel)=>{
        this.maintenanceInstructionListModel = list;
      }, error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la récupération des instructions de maintenance", "OK", "error-snackbar");
      }
    );
    this.brandService.localGetDatas().subscribe(//Partie pour les select des marquess
      (list: BrandListModel)=>{
        this.brandListModel = list;
      }, error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la récupération des marques", "OK", "error-snackbar");
      }
    );
    this.clotheTypeService.localGetDatas().subscribe(//Partie pour les select des Types de vêtements
      (list: ClotheTypeListModel)=>{
        this.clotheTypeListModel = list;
        for(let ct of list.list){
          let alreadyFoundFlag : boolean = false;
          for(let cat of this.clotheTypeCategories){
            if(ct.category === cat){
              alreadyFoundFlag = true;
              break;
            }
          }
          if(!alreadyFoundFlag){
            this.clotheTypeCategories.push(ct.category);
          }
        }
      }, error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la récupération des types de vêtements", "OK", "error-snackbar");
      }
    );
    this.colorService.localGetDatas().subscribe(//Partie pour les select des Couleurs
      (list: ColorListModel)=>{
        this.colorListModel = list;
      }, error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la récupération des couleurs", "OK", "error-snackbar");
      }
    );
    this.materialService.localGetDatas().subscribe(//Partie pour les select des matières
      (list: MaterialListModel)=>{
        this.materialListModel = list;
      }, error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la récupération des matières", "OK", "error-snackbar");
      }
    );
    this.providerService.localGetDatas().subscribe(//Partie pour les select des fournisseurs
      (list: ProviderListModel)=>{
        this.providerListModel = list;
      }, error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la récupération des fournisseurs", "OK", "error-snackbar");
      }
    );
    this.sizeService.localGetDatas().subscribe(//Partie pour les select des tailles
      (list: SizeListModel)=>{
        this.sizeListModel = list;
        for(let size of list.list){
          let alreadyFoundFlag: boolean = false;
          for(let cat of this.sizeCategories){
            if(size.category === cat){
              alreadyFoundFlag = true;
              break;
            }
          }
          if(!alreadyFoundFlag){
            this.sizeCategories.push(size.category);
          }
        }
      }, error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la récupération des tailles", "OK", "error-snackbar");
      }
    );
    this.styleService.localGetDatas().subscribe(//Partie pour les select des styles
      (list: StyleListModel)=>{
        this.styleListModel = list;
      }, error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la récupération des styles", "OK", "error-snackbar");
      }
    );
    this.themeService.localGetDatas().subscribe(//Partie pour les select des thèmes
      (list: ThemeListModel)=>{
        this.themeListModel = list;
      }, error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la récupération des thèmes", "OK", "error-snackbar");
      }
    );
  }

  /**
   * 
   * @param seasonId : L'id de la saison pour la recherche OU undefined si pas de saison choisie
   */
  onChangeSeasonFilter(seasons: SeasonModel[]){
    this.seasonCriteriaArray = []
    if(seasons && seasons.length > 0 ){
      for(let s of seasons){
        this.seasonCriteriaArray.push(s.seasonId);
      }
    }
  }

  onChangeProviderFilter(providers: ProviderModel[]){
    this.providerCriteriaArray = []
    if(providers && providers.length > 0 ){
      for(let pro of providers){
        this.providerCriteriaArray.push(pro.providerId);
      }
    }
  }

  onChangeClotheTypeFilter(clotheTypes: ClotheTypeModel[]){
    this.clotheTypeCriteriaArray = []
    if(clotheTypes && clotheTypes.length > 0 ){
      for(let ct of clotheTypes){
        this.clotheTypeCriteriaArray.push(ct.clotheTypeId);
      }
    }
  }

  onChangeColorFilter(colors: ColorModel[]){
    this.colorCriteriaArray = []
    if(colors && colors.length > 0 ){
      for(let color of colors){
        this.colorCriteriaArray.push(color.colorId);
      }
    }
  }

  onChangeSexFilter(sexs: SexModel[]){
    this.sexCriteriaArray = []
    if(sexs && sexs.length > 0 ){
      for(let sex of sexs){
        this.sexCriteriaArray.push(sex.sexId);
      }
    }
  }

  onChangeBrandFilter(brands: BrandModel[]){
    this.brandCriteriaArray = []
    if(brands && brands.length > 0 ){
      for(let brand of brands){
        this.brandCriteriaArray.push(brand.brandId);
      }
    }
  }

  onChangeMaterialFilter(materials: MaterialModel[]){
    this.materialCriteriaArray = []
    if(materials && materials.length > 0 ){
      for(let mat of materials){
        this.materialCriteriaArray.push(mat.materialId);
      }
    }
  }

  onChangeSizeFilter(sizes: SizeModel[]){
    this.sizeCriteriaArray = []
    if(sizes && sizes.length > 0 ){
      for(let size of sizes){
        this.sizeCriteriaArray.push(size.id);
      }
    }
  }

  onChangeStyleFilter(styles: StyleModel[]){
    this.styleCriteriaArray = []
    if(styles && styles.length > 0 ){
      for(let style of styles){
        this.styleCriteriaArray.push(style.styleId);
      }
    }
  }

  onChangeThemeFilter(themes: ThemeModel[]){
    this.themeCriteriaArray = []
    if(themes && themes.length > 0 ){
      for(let theme of themes){
        this.themeCriteriaArray.push(theme.themeId);
      }
    }
  }

  onChangeMinimumNoteFilter(minimalNote: number){
    if(!minimalNote){
      this.minimumNoteCriteria = undefined;
    }
    else{
      this.minimumNoteCriteria = minimalNote;
    }
  }

  
  
  protected createOrderLogos(): void {
    
  }
  

  /**
   * Fonction appelée lorsque l'utilisateur clique sur le bouton enregistrer les modifications d'un clotheListItemComponent, permet de contacter le serveur et de lui envoyer
   * le clotheModel que ce clotheListItemComponent gérait pour l'enregistrer ou le modifier
   * @param clotheModel Le clotheModel a envoyer au serveur
   */
  onUpdateClothe(clotheModel: ClotheModel){

    if(clotheModel.clotheId < 1){//Si l'utilisateur souhaite enregistrer un nouveau vêtement
    clotheModel.clotheId = 0;
    this.clotheService.localAddData(clotheModel).subscribe(
      ()=>{
        this.snackbar.openSnackBar("Vêtement ajouté", "OK", "success-snackbar");
        this.reloadDatas();
      },error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la création du vêtement: "+error.error, "OK", "error-snackbar");
      }
    );
    }
    else{ //S'il s'agit de la modification d'un vêtement déjà existant
    this.clotheService.localEditData(clotheModel).subscribe(
      ()=>{
        this.snackbar.openSnackBar("Vêtement modifié!", "OK", "success-snackbar");
        this.reloadDatas();
      },error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la modification du vêtement: " + error.error, "OK", "error-snackbar");
      }
    );
    }
  }

  /**
   * Fonction appelée lorsque l'utilisateur clique sur le bouton supprimer d'un clotheListItemComponent, permet de contacter le serveur et de lui indiquer quel vêtement doit 
   * être supprimé. Si le clotheModel en question possède un id négatif (ce qui signifie qu'il n'existe pas sur le serveur et que l'utilisateur essayait avait cliqué sur créer un vêtement),
   * ne fait que supprimer le clotheModel de la liste des clothemodel actuellement enregistrés dans la mémoire de l'application 
   * @param clotheModel 
   */
  onDeleteClothe(clotheModel: ClotheModel){
    if(clotheModel.clotheId < 1){//S'il s'agit d'un vêtement que l'utilisateur essayait de créer
    let clothes: ClotheModel[] = this.dataList.list as ClotheModel[];
    for(let clothe of clothes){
      if(clotheModel.clotheId === clothe.clotheId){//Si c'est le vêtement que l'utilisateur souhaite supprimer
        const index = clothes.indexOf(clothe, 0);
        if(index > -1){
          this.dataList.list.splice(index, 1);
          }
        }
      }
    }
    else{//S'il s'agit d'un vêtement récupéré depuis le serveur
      this.clotheService.localDeleteData(clotheModel).subscribe(
        ()=>{
          this.snackbar.openSnackBar("Vêtement supprimé!", "OK", "success-snackbar");
          this.reloadDatas();
        },error=>{
          this.snackbar.openSnackBar("Une erreur est survenue lors de la suppression du vêtement: " + error.error, "OK", "error-snackbar");
        });
      }
    }

    


    protected createDataFormGroup(clotheModel: ClotheModel): FormGroup {
      return this.formBuilder.group({
        
      })
     }
     protected onSubmitEditData(index: number): void {
       return null;
     }
    protected initForm(): void {
      this.addModelForm = this.formBuilder.group({
        
      });
    }
    protected onSubmitAddDataForm(): void {
     // console.log("Alors comme ça on veut ajouter un vêtement? coquin va")
    }    

    onGetComments(clotheModel: ClotheModel){
      this.clotheNameForComments = clotheModel.name;
      this.clotheModelForCommentSection =clotheModel;
      this.showCommentSection = true;
    }

}
