import { Component, OnInit, OnDestroy } from '@angular/core';
import { GenericComponent } from '../generic/generic.component';
import { SnackbarComponent } from '../snackbar/snackbar.component';
import { AuthService } from 'src/app/services/auth-service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { OutfitService } from 'src/app/services/dataServices/outfit.service';
import { MimicListModel } from 'src/app/models/dataListModels/mimicListModel';
import { OutfitListModel } from 'src/app/models/dataListModels/outfitListModel';
import { ClotheListModel } from 'src/app/models/dataListModels/clotheListModel';
import { OrderListModel } from 'src/app/models/dataListModels/orderListModel';
import { ClotheService } from 'src/app/services/dataServices/clothe.service';
import { OrderModel } from 'src/app/models/dataModels/orderModel';
import { OutfitOrders } from 'src/app/util/outfitOrders';
import { ClotheModel } from 'src/app/models/dataModels/clotheModel';
import { OutfitModel } from 'src/app/models/dataModels/outfitModel';
import { MimicModel } from 'src/app/models/dataModels/mimicModel';
import { PossessionListModel } from 'src/app/models/dataListModels/possessionListModel';
import { PossessionService } from 'src/app/services/dataServices/possession-service.service';

@Component({
  selector: 'app-outfit',
  templateUrl: './outfit.component.html',
  styleUrls: ['./outfit.component.css']
})
export class OutfitComponent extends GenericComponent implements OnInit, OnDestroy {

  resetFlagOutfit: boolean = false;
  resetFlagClothe: boolean = false;
  resetFlagOrder: boolean = false;
  resetFlagIsFavorite:boolean = false;
  resetFlagIsPublic:boolean = false

  clotheCriteriaArray: number[] = [];
  outfitCriteriaArray: number[] = [];
  publicCriteria: string = "";
  favoriteCriteria: string = "";

  addOutfit: OutfitModel = new OutfitModel();

  publicMimicList: MimicListModel = new MimicListModel();
  favoriteMimicList: MimicListModel = new MimicListModel();
  outfitList: OutfitListModel = new OutfitListModel();
  clotheList: ClotheListModel = new ClotheListModel();
  orders: OrderListModel = new OrderListModel();
  possessionList: PossessionListModel = new PossessionListModel();
  outfitNameForComments: string = "";
  showCommentSection: boolean = false;
  outfitModelForCommentSection: OutfitModel;


  constructor(protected snackbar: SnackbarComponent, protected authService: AuthService, protected formBuilder: FormBuilder, protected dataService: OutfitService, 
    private clotheService: ClotheService,private possessionService: PossessionService) {
    super(snackbar, authService, dataService, formBuilder);
   }

  ngOnInit() {
    super.ngOnInit();
    this.loadDatasToFillSelects();
  }

  submitFilter(){
    this.dataService.removeCriterias();
    if(this.clotheCriteriaArray.length > 0){
      for(let id of this.clotheCriteriaArray){
        this.dataService.addCriteria("clotheId",":",""+id);
      }
    }
    if(this.outfitCriteriaArray.length > 0){
      for(let id of this.outfitCriteriaArray){
        this.dataService.addCriteria("outfitId",":", ""+id);
      }
    }
    if(!(this.publicCriteria === "")){
      if(this.publicCriteria === "public"){
        this.dataService.addCriteria("isPublic",":","true");
      }
      else if(this.publicCriteria === "private"){
        this.dataService.addCriteria("isPublic",":","false");
      }
    }
    if(!(this.favoriteCriteria === "")){
      if(this.favoriteCriteria === "favorite"){
        this.dataService.addCriteria("isFavorite",":", "true");
      }
      else if(this.favoriteCriteria === "not favorite"){
        this.dataService.addCriteria("isFavorite",":","false");
      }
    }
    this.reloadDatas();

  }

  resetFilter(){
    this.resetFlagClothe = this.switchBoolean(this.resetFlagClothe);
    this.resetFlagOrder = this.switchBoolean(this.resetFlagOrder);
    this.resetFlagOutfit = this.switchBoolean(this.resetFlagOutfit);
    this.resetFlagIsFavorite = this.switchBoolean(this.resetFlagIsFavorite);
    this.resetFlagIsPublic = this.switchBoolean(this.resetFlagIsPublic);
    this.dataService.changeOrder(OutfitOrders.idAsc.orderType,OutfitOrders.idAsc.orderBy);
    this.reloadDatas();
  }

  switchBoolean(boo : boolean): boolean{
    if(boo == true){
      return false;
    }
    else{
      return true;
    }
  }

  onChangeOrder(orderModel: OrderModel){
    if(orderModel){
      this.dataService.changeOrder(orderModel.orderType, orderModel.orderBy);
    }else{
      this.dataService.changeOrder(OutfitOrders.idAsc.orderType,OutfitOrders.idAsc.orderBy);
    }
  }

  loadDatasToFillSelects():void{
    this.orders.list = [];
    this.orders.list.push(OutfitOrders.idAsc);
    this.orders.list.push(OutfitOrders.idDesc);
    this.orders.list.push(OutfitOrders.nameAsc);
    this.orders.list.push(OutfitOrders.nameDesc);
    this.orders.list.push(OutfitOrders.creationDateAsc);
    this.orders.list.push(OutfitOrders.creationDateDesc);
    this.orders.list.push(OutfitOrders.dueDateAsc);
    this.orders.list.push(OutfitOrders.dueDateDesc);
    this.orders.list.push(OutfitOrders.isFavoriteAsc);
    this.orders.list.push(OutfitOrders.isFavoriteDesc);
    this.orders.list.push(OutfitOrders.isPublicAsc);
    this.orders.list.push(OutfitOrders.isPublicDesc);
    this.orders.list.push(OutfitOrders.userAsc);
    this.orders.list.push(OutfitOrders.userDesc);
    let favoriteTrue: MimicModel = new MimicModel("Favoris",0, "favorite");
    let favoriteFalse: MimicModel = new MimicModel("Non favoris",1, "not favorite");
    this.favoriteMimicList.list.push( favoriteTrue, favoriteFalse);
    this.favoriteMimicList.numberOfItems= 2;
    let publicTrue: MimicModel = new MimicModel("Publique", 3, "public");
    let publicfalse: MimicModel = new MimicModel("Privée", 4, "private");
    this.publicMimicList.list.push( publicTrue, publicfalse );
    this.publicMimicList.numberOfItems=2;
    this.dataService.localGetForSelect().subscribe(
      (list: OutfitListModel)=>{
        this.outfitList = list;
      },error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la récupération de vos tenues: " +error.error ,"OK","error-snackbar");
      }
    );
    this.clotheService.localGetClotheForPossession().subscribe(
      (list: ClotheListModel)=>{
        this.clotheList = list;
      },error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la récupération des vêtements: " +error.error ,"OK","error-snackbar");
      }
    );
    this.possessionService.localGetMyPossessions().subscribe(
      (list: PossessionListModel)=>{
        this.possessionList = list;
      },error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la récupération de vos possessions: " +error.error ,"OK","error-snackbar");
      }
    )
  }

  ngOnDestroy(){
    super.ngOnDestroy();
  }



  protected createDataFormGroup(model: any): FormGroup {
    return this.formBuilder.group({

    });
  }

  onChangeClotheFilter(clotheModels: ClotheModel[]){
    this.clotheCriteriaArray = [];
    if(clotheModels && clotheModels.length > 0){
      for(let clothe of clotheModels){
        this.clotheCriteriaArray.push(clothe.clotheId);
      }
    }
  }

  onChangeOutfitFilter(outfitModels: OutfitModel[]){
    this.outfitCriteriaArray = [];
    if(outfitModels && outfitModels.length > 0){
      for(let outfit of outfitModels){
        this.clotheCriteriaArray.push(outfit.outfitId);
      }
    }
  }

  onChangePublicFilter(mimicModel: MimicModel){
    this.publicCriteria = "";
    if(mimicModel){
      if(mimicModel.value === "public"){
        this.publicCriteria = "public";
      }
      if(mimicModel.value === "private"){
        this.publicCriteria = "private";
      }
    }
  }

  onChangeFavoriteFilter(mimicModel: MimicModel){
    this.favoriteCriteria = "";
    if(mimicModel){
      if(mimicModel.value ==="favorite"){
        this.favoriteCriteria = "favorite";
      }
      if(mimicModel.value === "not favorite"){
        this.favoriteCriteria = "not favorite";
      }
    }
    
  }




  protected onSubmitEditData(index: number): void {
    //throw new Error("Method not implemented.");
  }
  protected createOrderLogos(): void {
    //throw new Error("Method not implemented.");
  }
  protected initForm(): void {
    //throw new Error("Method not implemented.");
  }
  protected onSubmitAddDataForm(): void {
    //throw new Error("Method not implemented.");
  }

  onDeleteOutfit(outfitModel: OutfitModel){
    this.dataService.localDeleteData(outfitModel).subscribe(
      ()=>{
        this.snackbar.openSnackBar("Tenue supprimée!" ,"OK","success-snackbar");
        this.reloadDatas();
      },error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de suppression de la tenue: " +error.error ,"OK","error-snackbar");
      }
    );
  }

  onUpdateOutfit(outfitModel: OutfitModel){
    this.dataService.localEditData(outfitModel).subscribe(
      ()=>{
        this.snackbar.openSnackBar("Tenue mise à jour!" ,"OK","success-snackbar");
        this.reloadDatas();
      },error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de modification de la tenue: " +error.error ,"OK","error-snackbar");
      }
    );
  }
  onShowComments(outfitModel: OutfitModel){
    this.outfitNameForComments = outfitModel.name;
    this.outfitModelForCommentSection =outfitModel;
    this.showCommentSection = true;
  }

  onAddOutfit(outfitModel: OutfitModel){
    this.dataService.localAddData(outfitModel).subscribe(
      ()=>{
        this.snackbar.openSnackBar("Tenue ajoutée!" ,"OK","success-snackbar");
        this.reloadDatas();
      },error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de l'ajout de la tenue: " +error.error ,"OK","error-snackbar");
      }
    );
  }

  


}
