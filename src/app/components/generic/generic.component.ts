import { Component, OnInit } from '@angular/core';
import { SnackbarComponent } from '../snackbar/snackbar.component';
import { AuthService } from 'src/app/services/auth-service';
import { GenericDataProviderService } from 'src/app/services/dataServices/generic-data-provider.service';
import { FormBuilder, FormArray, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { GenericListModel } from 'src/app/models/dataListModels/genericListModel';
import { QueryModel } from 'src/app/models/queryModel';

@Component({
  selector: 'app-generic',
  templateUrl: './generic.component.html',
  styleUrls: ['./generic.component.css']
})
export abstract class GenericComponent implements OnInit {
  protected isLogged: boolean = false;
  protected isLoggedInSubscription: Subscription;
  protected dataSubscription: Subscription;
  protected dataList: GenericListModel;
  protected queryModel: QueryModel = new QueryModel();
  protected queryModelSubscription: Subscription;
  protected formArray: FormArray;
  protected addModelForm: FormGroup;
  protected elementsPerPage: number[] = [10,20,25,50];
  protected pages : number[] = [];

  constructor(protected snackbar: SnackbarComponent, protected authService: AuthService, protected dataService: GenericDataProviderService, protected formBuilder: FormBuilder ) { }

  ngOnInit() {
    this.isLogged = this.authService.getAuthStatus();

    this.queryModelSubscription = this.dataService.queryModelSubject.subscribe(
      (queryModel: QueryModel)=>{
        this.queryModel = queryModel;
      }
    );
    this.dataSubscription = this.dataService.genericListSubject.subscribe(
      (genericList : GenericListModel)=>{
        this.dataList = genericList;
        this.updateListView();
      }
    );

    this.isLoggedInSubscription = this.authService.isLoggedInSubject.subscribe(
      (isLogged: boolean)=>{
        this.isLogged = isLogged;
      }
    );
    this.createQueryModel();
    
    this.reloadDatas();
    this.updateListView();
    this.initForm();
    this.createOrderLogos();
  }

  /**
   * Permet de demander à l'implémentation adéquate de GenericDataProviderService de récupérer les données qui nous intéressent auprès du serveur
   * à partir des informations contenues dans le QueryModel;
   */
  protected reloadDatas():void{
    this.dataService.localGetDatas(this.queryModel).subscribe(
      (dataList: GenericListModel)=>{
        this.dataService.updateList(dataList);
      },error=>{
        this.snackbar.openSnackBar("Une erreur est survenue durant la récupération des données: " + error.error, "OK", "error-snackbar");
      }
    );
  }

  /**
   * Fonction dont le but est de créer un formArray à partir des données contenues dans la dataList de notre component 
   */
  protected updateListView():void{
    this.formArray = this.formBuilder.array([]);
    if(this.dataList !== undefined){
      for(let data of this.dataList.list){
        this.formArray.push(this.createDataFormGroup(data));
      }
      this.createPagePicker();
    }
    
  }

  /**
   * Fonction permettant de créer les select pour passer d'une page de données à l'autre
   */
  protected createPagePicker():void{
    this.pages = [];
    if(this.dataList.numberOfPages > 1){
      for(let i : number  = 0; i < this.dataList.numberOfPages; i++){
        this.pages.push(i + 1);
      }
    }
  }

  /**
   * Fonction permettant à l'utilisateur de demander une page où l'autre de données au serveur
   * @param newPageNumber Le numéro de page auquel l'utilisateur souhaite accéder
   */
  protected onChangePageNumber(newPageNumber: number):void{
    this.dataService.changePageNumber(newPageNumber);
    this.reloadDatas();
  }

  /**
   * Permet au client d'indiquer qu'il souhaite récupérer les modèles de la page indiquée
   * @param newValue : le numéro de la page (POV Client) de laquelle le client souhaite récupérer les données
   */
  protected onChangeElementsPerPage(newValue: number):void{
    this.dataService.changeElementsPerPage(newValue);
    this.dataService.changePageNumber(1);
    this.reloadDatas();
  }

  protected onResetAndSetValueToQM(key: string, operator: string, value: string):void{
    this.dataService.removeCriterias();
    this.dataService.addCriteria(key, operator, value);
    this.reloadDatas();
  }

  /**
   * Permet de demander au serveur de supprimer un modèle
   * @param index : L'index, dans la liste des modèles, du modèle à modifier
   */
  protected  onDeleteData(index: number): void{
    this.dataService.localDeleteData(this.dataList.list[index]).subscribe(
      ()=>{
        this.snackbar.openSnackBar("L'élément a bien été supprimé!", "OK", "success-snackbar");
        this.reloadDatas();
      }, error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la suppression de l'élément: " + error.error, "OK", "error-snackbar");
      }
    );
  };

  /**
   * Permet de définir le nbre d'éléments/page et le numéro de page 
   * @param elementsPerPage : le nombre de modèles à afficher/liste
   * @param pageNumber : le numéro de page (POV Client) des données qui nous intéressent
   */
  protected createQueryModel(elementsPerPage?: number, pageNumber?: number):void{
    if(!pageNumber){
      this.dataService.changePageNumber(1);
    }else{
      this.dataService.changePageNumber(pageNumber);
    }
    if(!elementsPerPage){
      this.dataService.changeElementsPerPage(10);
    }else{
      this.dataService.changeElementsPerPage(elementsPerPage);
    }
  }

  protected ngOnDestroy(){
    this.dataSubscription.unsubscribe();
    this.isLoggedInSubscription.unsubscribe();
    this.queryModelSubscription.unsubscribe();
  }
  
  /**
   * Doit être implémentée, permet de créer un formulaire d'ajout pour le modèle concerné
   * @param model : Le modèle de données à partir duquel créer le formulaire
   */
  protected abstract createDataFormGroup(model: any):FormGroup;

  /**
   * Doit être implémentée, permet de modifier les propriétés d'un modèle et de signaler ces changements au Serveur pour qu'il les applique
   * @param index : L'index, dans la liste des modèles, du modèle à modifier
   */
  protected abstract onSubmitEditData(index: number):void;
  
  /**
   * Fonction qui va attribuer les adresses aux svg 
   */
  protected abstract createOrderLogos():void;

  /**
   * Fonction permettant de créer un formulaire d'ajout de modèle
   */
  protected abstract initForm():void;

  /**
   * Fonction appelée lorsque l'utilisateur souhaite soumettre le formulaire d'ajout d'un modèle
   */
  protected abstract onSubmitAddDataForm():void ;
}
