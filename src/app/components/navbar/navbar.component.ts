import { Component, OnInit, OnDestroy } from '@angular/core';
import { TokenStorageService } from 'src/app/services/token-storage.service';
import { SnackbarComponent } from '../snackbar/snackbar.component';
import { AuthService } from 'src/app/services/auth-service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit, OnDestroy {
  
  private isLoggedIn: boolean = false
  private roles: string[] = [];
  isLoggedInSubscription: Subscription;
  rolesSubscription: Subscription;

  constructor(private token: TokenStorageService, private snackbar: SnackbarComponent, private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.isLoggedInSubscription = this.authService.isLoggedInSubject.subscribe(
      ( logStatus:boolean) => {
        this.isLoggedIn = logStatus;
      } 
    );
    this.rolesSubscription = this.authService.rolesSubject.subscribe(
      (roles: string[])=>{
        this.roles = roles;
      }
    )
    if(this.token.getToken()){
      this.authService.login();
    }
    if(this.token.getRoles()){
      this.authService.setRoles(this.token.getRoles())
    }
  }

  logout(){
    this.token.empty();
    this.authService.logout();
    this.authService.removeRoles();
    this.router.navigate(['/home']);
    this.snackbar.openSnackBar('Vous vous êtes déconnecté, au revoir! ', 'OK', 'success-snackbar')
  }

  

  reloadPage(){
    window.location.reload();
  }

  ngOnDestroy(){
    this.isLoggedInSubscription.unsubscribe();
    this.rolesSubscription.unsubscribe();
  }

}
