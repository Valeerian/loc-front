import { Component, OnInit, OnDestroy } from '@angular/core';
import { GenericComponent } from '../generic/generic.component';
import { SnackbarComponent } from '../snackbar/snackbar.component';
import { AuthService } from 'src/app/services/auth-service';
import { StyleService } from 'src/app/services/dataServices/style.service';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { IMAGES } from 'src/app/util/images';
import { StyleModel } from 'src/app/models/dataModels/styleModel';

@Component({
  selector: 'app-style',
  templateUrl: './style.component.html',
  styleUrls: ['./style.component.css']
})
export class StyleComponent extends GenericComponent implements OnInit, OnDestroy {

  idLogoSrcOne : string = '';
  idLogoSrcTwo : string = '';
  nameLogoSrcOne: string ='';
  nameLogoSrcTwo: string ='';

  constructor(protected snackbar: SnackbarComponent, protected authService: AuthService, protected styleService: StyleService, protected formBuilder: FormBuilder) {
    super(snackbar, authService, styleService, formBuilder); 
  }

  ngOnInit() {
    super.ngOnInit();
  }

  ngOnDestroy(){
    super.ngOnDestroy();
  }


  createOrderLogos():void{
    this.idLogoSrcOne = IMAGES.upArrowFirstD;
    this.idLogoSrcTwo = IMAGES.upArrowSecondD;
    this.nameLogoSrcOne = IMAGES.upArrowFirstD;
    this.nameLogoSrcTwo = IMAGES.upArrowSecondD;
  }

  initForm(){
    this.addModelForm = this.formBuilder.group({
      'style': new FormControl('', [Validators.required, Validators.pattern("^[A-ZÈÉ]([a-zéè '-]{2,39})$")]),
    });
  }

  onSubmitAddDataForm(){
    let newStyle = new StyleModel();
    newStyle.style = this.addModelForm.value['style'];
    this.styleService.localAddData(newStyle).subscribe(
      ()=>{
        this.snackbar.openSnackBar("Style ajouté", "OK", "success-snackbar");
        this.reloadDatas();
        this.initForm();
      }, error=>{
        this.snackbar.openSnackBar('Une erreur est survenue lors de l\'ajout du style' + error.error, 'OK', 'error-snackbar');
      }
    );
  }


  onOrderById():void{
    if(this.idLogoSrcOne === IMAGES.upArrowFirstD){
      this.idLogoSrcOne = IMAGES.downArrowFirstD;
      this.idLogoSrcTwo = IMAGES.downArrowSecondD;
      this.styleService.changeOrder("DESC", "styleId");
    }else{
      this.idLogoSrcOne = IMAGES.upArrowFirstD;
      this.idLogoSrcTwo = IMAGES.upArrowSecondD;
      this.styleService.changeOrder("ASC", "styleId");
    }
    this.reloadDatas();
  }

  onOrderByName():void{
    if(this.nameLogoSrcOne === IMAGES.upArrowFirstD){
      this.nameLogoSrcOne = IMAGES.downArrowFirstD;
      this.nameLogoSrcTwo = IMAGES.downArrowSecondD;
      this.styleService.changeOrder("DESC", "name");
    }else{
      this.nameLogoSrcOne = IMAGES.upArrowFirstD;
      this.nameLogoSrcTwo = IMAGES.upArrowSecondD;
      this.styleService.changeOrder("ASC", "name");
    }
    this.reloadDatas();
  }

  createDataFormGroup(styleModel: StyleModel): FormGroup{
    return this.formBuilder.group({
      'styleId': new FormControl(styleModel.styleId, [Validators.required, Validators.min(1)]),
      'style': new FormControl(styleModel.style, [Validators.required, Validators.pattern("^[A-ZÈÉ]([a-zéè '-]{2,39})$")])
    });
  }

  onSubmitEditData(index:number){
    let styleModelToUpdate = new StyleModel();
    styleModelToUpdate.styleId = this.formArray.controls[index].value['styleId'];
    styleModelToUpdate.style = this.formArray.controls[index].value['style'];
    this.styleService.localEditData(styleModelToUpdate).subscribe(
      ()=>{
        this.snackbar.openSnackBar('Style modifié!', 'OK', 'success-snackbar');
        this.reloadDatas();
      }, error=>{
        this.snackbar.openSnackBar('Une erreur est survenue lors de la modification du style: ' +error.error, 'OK', 'error-snackbar');
      }
    );
  }

}
