import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LaundryManeListItemComponent } from './laundry-mane-list-item.component';

describe('LaundryManeListItemComponent', () => {
  let component: LaundryManeListItemComponent;
  let fixture: ComponentFixture<LaundryManeListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LaundryManeListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaundryManeListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
