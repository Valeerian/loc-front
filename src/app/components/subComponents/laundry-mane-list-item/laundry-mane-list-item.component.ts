import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { LaundryGroupModel } from 'src/app/models/dataModels/laundryGroupModel';
import { LaundryManeModel } from 'src/app/models/dataModels/laundryManeModel';
import { PossessionService } from 'src/app/services/dataServices/possession-service.service';
import { FormControl } from '@angular/forms';
import { ManeStatusListModel } from 'src/app/models/dataListModels/maneStatusListModel';
import { PossessionModel } from 'src/app/models/dataModels/possessionModel';
import { UserModel } from 'src/app/models/dataModels/userModel';
import { PossessionListModel } from 'src/app/models/dataListModels/possessionListModel';
import { MaintenanceInstructionModel } from 'src/app/models/dataModels/maintenanceInstruction';
import { LaundryManeService } from 'src/app/services/dataServices/laundry-mane.service';
import { ManeStatusModel } from 'src/app/models/dataModels/maneStatusModel';

@Component({
  selector: 'app-laundry-mane-list-item',
  templateUrl: './laundry-mane-list-item.component.html',
  styleUrls: ['./laundry-mane-list-item.component.css']
})
export class LaundryManeListItemComponent implements OnInit {

  precautions: MaintenanceInstructionModel[] = [];
  startDate: FormControl =  new FormControl('', );
  laundryGroupModel: LaundryGroupModel = new LaundryGroupModel();
  @Input() set setLaundryGroup(laundryGroupModel: LaundryGroupModel){
    this.laundryGroupModel=laundryGroupModel;
  }
  laundryManeModel: LaundryManeModel = new LaundryManeModel();
  @Input() set setLaundryMane(laundryManeModel: LaundryManeModel){
    this.laundryManeModel = laundryManeModel;
    if(laundryManeModel.startDate){
      this.startDate.setValue(laundryManeModel.startDate.toString().slice(0,16));
    }//Partie précautions
    if(laundryManeModel.possessionModels){
      this.updatePrecautions(laundryManeModel.possessionModels);
    }
  }

  updatePrecautions(possessions: PossessionModel[]){
    this.precautions = [];
      for (let possession of possessions){
        if(possession.clotheModel.maintenanceInstructionModels){
          for(let warning of possession.clotheModel.maintenanceInstructionModels){
            let alreadyWarned : boolean = false;
            for(let prc of this.precautions){
  
              if(prc.maintenanceInstructionId == warning.maintenanceInstructionId){
                alreadyWarned = true;
                break;
              }
            }
            if(!alreadyWarned){
              this.precautions.push(warning);
            }
          }
        }
      }
  }
  maneStatusList: ManeStatusListModel= new ManeStatusListModel();
  @Input() set setmaneStatusOptions(maneStatusListModel: ManeStatusListModel){
    this.maneStatusList = maneStatusListModel;
  }


  onValidateChanges(){
    if(this.startDate.value == ''){
      this.laundryManeModel.startDate = null;
    }else{
      this.laundryManeModel.startDate = new Date(this.startDate.value);
    }

    
  }

  deleteLaundryMane(){
    //this.laundryManeService.deleteLaundryMane(this.laundryManeModel);
  }

  /**
   * 
   * @param possessionModels : Une liste de possessions issues d'un même utilisateur
   */
  onChangePossessions(object :{propOne: PossessionModel[], propTwo: UserModel}){
    if(this.laundryManeModel.possessionModels){//Si à ce stade la manne contient déjà des vêtements
      let newPossessionList: PossessionModel[] = []
      for(let possession of this.laundryManeModel.possessionModels){
        if(possession.user != object.propTwo.userName){//Si la possession qu'on évalue n'appartient pas à cet utilisateur (donc à ce select)
          newPossessionList.push(possession);
        }
      }
      for(let possession of object.propOne){
        newPossessionList.push(possession);
      }
      this.laundryManeModel.possessionModels=  newPossessionList;
    }else{//Si la manne ne contient pas de vêtements
      this.laundryManeModel.possessionModels = object.propOne;
    }

    this.updatePrecautions(this.laundryManeModel.possessionModels);
  }

  constructor(private possessionService: PossessionService, private laundryManeService: LaundryManeService) { }

  ngOnInit() {
  }

  onChangeManeStatus(maneStatus: ManeStatusModel){
    this.laundryManeModel.maneStatusModel = maneStatus;
  }

}
