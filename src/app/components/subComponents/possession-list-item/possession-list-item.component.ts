import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { UserListModel } from 'src/app/models/dataListModels/userListModel';
import { UserModel } from 'src/app/models/dataModels/userModel';
import { PossessionStatusListModel } from 'src/app/models/dataListModels/possessionStatusListModel';
import { PossessionModel } from 'src/app/models/dataModels/possessionModel';
import { SizeListModel } from 'src/app/models/dataListModels/sizeListModel';
import { SizeModel } from 'src/app/models/dataModels/sizeModel';
import { PossessionStatusModel } from 'src/app/models/dataModels/possessionStatusModel';

@Component({
  selector: 'app-possession-list-item',
  templateUrl: './possession-list-item.component.html',
  styleUrls: ['./possession-list-item.component.css']
})
export class PossessionListItemComponent implements OnInit {

  intendsToShare: boolean  = false;
  newUser: string  = "";
  isOwner: boolean = false;
  
  friends: UserListModel = new UserListModel();
  @Input() set setFriendOptions(userListModel: UserListModel){
    this.friends = userListModel;
  }
  everyPossessionStatuses: PossessionStatusListModel = new PossessionStatusListModel();
  @Input() set setPossessionStatusOptions(possessionStatusList: PossessionStatusListModel){
    this.everyPossessionStatuses = possessionStatusList;
  }
  possessionModel: PossessionModel = new PossessionModel();
  availableSizes: SizeListModel = new SizeListModel();
  sizeCategories: string[] = []
  @Input() set setPossessionModel(possessionModel: PossessionModel){
    if(possessionModel.owner === possessionModel.user){
      this.isOwner = true;
    }
    if(possessionModel.clotheModel){
      this.possessionModel= possessionModel;
      this.checkStatuses();
      let sLM = new SizeListModel();
      sLM.list = possessionModel.clotheModel.sizeModels;
      if(possessionModel.clotheModel.sizeModels){
        sLM.numberOfItems = possessionModel.clotheModel.sizeModels.length;
      }
      sLM.pageNumber=0;
      sLM.numberOfPages=1;
      this.availableSizes = sLM;
      if(this.isIterable(possessionModel.clotheModel.sizeModels)){
        for(let size of possessionModel.clotheModel.sizeModels){
          let flag : boolean = false;
          for(let cat of this.sizeCategories){
            if(cat === size.category){
              flag = true; 
              break;
            }
          }
          if(!flag){
            this.sizeCategories.push(size.category);
          }
        }
      }
      
    }
    
  }
  isIterable(obj: any){
    if(obj == null){
      return false;
    }
    return typeof obj[Symbol.iterator] ==='function';
  }

  @Output() updatePossession = new EventEmitter<PossessionModel>();
  @Output() deletePossession = new EventEmitter<PossessionModel>();
  @Output() sharePossession = new EventEmitter<PossessionModel>();
  @Output() giveBack = new EventEmitter<PossessionModel>();
  hasReadyStatus: boolean = false;

  checkStatuses(){
    let flag =false;
    for(let stat of this.possessionModel.statuses){
      if(stat.possessionStatus == "Prêt à porter"){
        flag = true;
        break
      }
    }
    if(flag){
      this.hasReadyStatus = true;
    }else{
      this.hasReadyStatus = false;
    }
  }
  constructor() { }

  ngOnInit() {
  }

  /**Fonction appelée lorsque l'utilisateur clique sur un utilisateur du select des amis qui permet de prêter la possession */
  onChangeWearer(userModel :UserModel){
    this.newUser = userModel.userName;
    this.intendsToShare = true;
  }

  onChangeSize(size: SizeModel){
    this.possessionModel.sizeModel = size;
  }

  /**
   * Fonction appelée pour prêter un vêtement à un ami
   */
  onshareToFriend(){
    this.possessionModel.user = this.newUser;
    this.sharePossession.emit(this.possessionModel);
  }

  /**
   * Fonction appelée pour rendre un vêtement qui a été prêté à l'utilisateur
   */
  onGiveBack(){
    this.giveBack.emit(this.possessionModel);
  }

  onDeletePossession(){
    this.deletePossession.emit(this.possessionModel);
  }

  onUpdatePossession(){
    this.updatePossession.emit(this.possessionModel);
  }

  onChangeStatuses(statuses: PossessionStatusModel[]){
    this.possessionModel.statuses = statuses;
  }

}
