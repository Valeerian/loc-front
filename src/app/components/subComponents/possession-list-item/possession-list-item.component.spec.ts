import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PossessionListItemComponent } from './possession-list-item.component';

describe('PossessionListItemComponent', () => {
  let component: PossessionListItemComponent;
  let fixture: ComponentFixture<PossessionListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PossessionListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PossessionListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
