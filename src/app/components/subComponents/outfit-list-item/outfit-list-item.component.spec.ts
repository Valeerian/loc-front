import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutfitListItemComponent } from './outfit-list-item.component';

describe('OutfitListItemComponent', () => {
  let component: OutfitListItemComponent;
  let fixture: ComponentFixture<OutfitListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutfitListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutfitListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
