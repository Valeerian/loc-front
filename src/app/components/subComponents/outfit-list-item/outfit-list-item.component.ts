import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { OutfitModel } from 'src/app/models/dataModels/outfitModel';
import { PossessionListModel } from 'src/app/models/dataListModels/possessionListModel';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { MimicModel } from 'src/app/models/dataModels/mimicModel';
import { MimicListModel } from 'src/app/models/dataListModels/mimicListModel';
import { PossessionModel } from 'src/app/models/dataModels/possessionModel';
import { MatSelectChange } from '@angular/material';


@Component({
  selector: 'app-outfit-list-item',
  templateUrl: './outfit-list-item.component.html',
  styleUrls: ['./outfit-list-item.component.css']
})
export class OutfitListItemComponent implements OnInit {

  isReady: boolean = false;
  trueOrFalse: boolean[] = [];
  isNew: boolean = false;


  outfitModel: OutfitModel = new OutfitModel();
  @Input() set setOutfit(outfitModel: OutfitModel){
    this.outfitModel = outfitModel;
     if(! outfitModel.outfitId || outfitModel.outfitId < 1 ){
       this.isNew = true;
     }
  }
  
  @Input() idName: string = "";

  possessionListModel: PossessionListModel = new PossessionListModel();
  @Input() set setPossessionOptions(PossessionListModel: PossessionListModel){
    this.possessionListModel = PossessionListModel;   
  }
  @Output() onDeleteOutfit = new EventEmitter<OutfitModel>();
  @Output() onShowComments = new EventEmitter<OutfitModel>();
  @Output() updateOutfit = new EventEmitter<OutfitModel>()
  @Output() addOutfit = new EventEmitter<OutfitModel>();


  editOutfitForm: FormGroup;
  constructor(private formBuilder: FormBuilder) { 
  }

  ngOnInit() {
    this.initForm();
    this.trueOrFalse.push(true);
    this.trueOrFalse.push(false);
  }

  initForm(){
    if(this.outfitModel.dueDate == null){
      this.editOutfitForm = this.formBuilder.group({
        'name':new FormControl(this.outfitModel.name, [Validators.required,Validators.pattern(/^[A-Za-z0-9ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØŒŠþÙÚÛÜÝŸàáâãäåæçèéêëìíîïðñòóôõöøœšÞùúûü:ýÿ=ß,\\\/'\"!\{\}\[\] ().\-\;?_]{1,60}$/)]),
        'description': new FormControl(this.outfitModel.description, [Validators.pattern(/^[A-Za-z0-9ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØŒŠþÙÚÛÜÝŸàáâãäåæçèéêëìíîïðñòóôõöøœšÞùúûü:ýÿ=ß,\\\/'\"!\{\}\[\] ().\-\;?_]{1,100}$/)]),
        'publik':new FormControl(this.outfitModel.publik,[Validators.required]),
        'favorite':new FormControl(this.outfitModel.favorite,[Validators.required]),
        'dueDate': new FormControl()
      });  
    }else{
      this.editOutfitForm = this.formBuilder.group({
        'name':new FormControl(this.outfitModel.name, [Validators.required,Validators.pattern(/^[A-Za-z0-9ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØŒŠþÙÚÛÜÝŸàáâãäåæçèéêëìíîïðñòóôõöøœšÞùúûü:ýÿ=ß,\\\/'\"!\{\}\[\] ().\-\;?_]{1,60}$/)]),
        'description': new FormControl(this.outfitModel.description, [Validators.pattern(/^[A-Za-z0-9ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØŒŠþÙÚÛÜÝŸàáâãäåæçèéêëìíîïðñòóôõöøœšÞùúûü:ýÿ=ß,\\\/'\"!\{\}\[\] ().\-\;?_]{1,100}$/)]),
        'publik':new FormControl(this.outfitModel.publik,[Validators.required]),
        'favorite':new FormControl(this.outfitModel.favorite,[Validators.required]),
        'dueDate': new FormControl(new Date(this.outfitModel.dueDate))
      });
    }

    
  }

  

  

  onChangePublic(val: MatSelectChange){
    this.outfitModel.publik = val.value;
  }

  onChangeFavorite(val: MatSelectChange){
    this.outfitModel.favorite = val.value;
  }

  onSubmitEditOutfit(){
    this.outfitModel.name = this.editOutfitForm.controls['name'].value;
    this.outfitModel.description = this.editOutfitForm.controls['description'].value;
    this.outfitModel.publik = this.editOutfitForm.controls['publik'].value;
    this.outfitModel.favorite = this.editOutfitForm.controls["favorite"].value;
    this.outfitModel.dueDate = this.editOutfitForm.controls["dueDate"].value;
    this.updateOutfit.emit(this.outfitModel);
  }
  onSubmitDeleteOutfit() {
    this.onDeleteOutfit.emit(this.outfitModel);
  }

  onRemovePossessionFromOutfit(possession: PossessionModel){
    let copy : PossessionModel[] = [];
    copy = this.outfitModel.possessionModels;
    //this.outfitModel.possessionModels;
    const index = this.outfitModel.possessionModels.indexOf(possession);
    if(index > -1){
      copy.splice(index,1);

      this.outfitModel.possessionModels= copy; 
    }
    this.outfitModel.possessionModels =[];
    for(let poss of copy){
      this.outfitModel.possessionModels.push(poss);
    }
  }

  onSubmitShowComments(){
    this.onShowComments.emit(this.outfitModel);
  }

  onChangePossessionsInOutfit(possessions: PossessionModel[]){
    this.outfitModel.possessionModels = possessions;
  }

  onSubmitAddOutfit(){
    this.outfitModel.name = this.editOutfitForm.controls['name'].value;
    this.outfitModel.description = this.editOutfitForm.controls['description'].value;
    this.outfitModel.publik = this.editOutfitForm.controls['publik'].value;
    this.outfitModel.favorite = this.editOutfitForm.controls["favorite"].value;
    this.outfitModel.dueDate = this.editOutfitForm.controls["dueDate"].value;
    this.addOutfit.emit(this.outfitModel);
  }

  onResetModel(){
    this.outfitModel = new OutfitModel();
    this.initForm();
  }

}


