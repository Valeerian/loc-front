import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaterialMultiDropdownComponent } from './material-multi-dropdown.component';

describe('MaterialMultiDropdownComponent', () => {
  let component: MaterialMultiDropdownComponent;
  let fixture: ComponentFixture<MaterialMultiDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaterialMultiDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaterialMultiDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
