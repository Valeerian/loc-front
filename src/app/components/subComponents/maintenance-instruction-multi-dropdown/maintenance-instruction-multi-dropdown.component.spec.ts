import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaintenanceInstructionMultiDropdownComponent } from './maintenance-instruction-multi-dropdown.component';

describe('MaintenanceInstructionMultiDropdownComponent', () => {
  let component: MaintenanceInstructionMultiDropdownComponent;
  let fixture: ComponentFixture<MaintenanceInstructionMultiDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaintenanceInstructionMultiDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaintenanceInstructionMultiDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
