import { Component, OnInit, OnDestroy, Output, EventEmitter, Input } from '@angular/core';
import { GenericDropdownComponent } from '../generic-dropdown/generic-dropdown.component';
import { MaintenanceInstructionModel } from 'src/app/models/dataModels/maintenanceInstruction';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MaintenanceInstructionSelectProviderService } from 'src/app/services/dataServices/subServices/maintenance-instruction-select-provider.service';
import { MatSelectChange } from '@angular/material';

@Component({
  selector: 'app-maintenance-instruction-multi-dropdown',
  templateUrl: './maintenance-instruction-multi-dropdown.component.html',
  styleUrls: ['./maintenance-instruction-multi-dropdown.component.css']
})
export class MaintenanceInstructionMultiDropdownComponent extends GenericDropdownComponent implements OnInit, OnDestroy {


  @Input() set setValues(instructions: MaintenanceInstructionModel[]){
      this.form.controls.selectForm.setValue(instructions);
  }
  constructor(protected formBuilder: FormBuilder) {
    super(formBuilder);
   }

  ngOnInit() {
  }

  ngOnDestroy(){
  }

  compareObjects(object1: MaintenanceInstructionModel, object2: MaintenanceInstructionModel){
    return object1.maintenanceInstructionId && object2 && object1.maintenanceInstructionId == object2.maintenanceInstructionId;
  }

}
