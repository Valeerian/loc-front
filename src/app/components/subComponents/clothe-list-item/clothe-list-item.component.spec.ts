import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClotheListItemComponent } from './clothe-list-item.component';

describe('ClotheListItemComponent', () => {
  let component: ClotheListItemComponent;
  let fixture: ComponentFixture<ClotheListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClotheListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClotheListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
