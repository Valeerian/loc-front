import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ClotheModel } from 'src/app/models/dataModels/clotheModel';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatSelectChange } from '@angular/material';
import { MaterialService } from 'src/app/services/dataServices/material.service';
import { ClotheTypeModel } from 'src/app/models/dataModels/clotheTypeModel';
import { PictureModel } from 'src/app/models/dataModels/pictureModel';
import { MaintenanceInstructionModel } from 'src/app/models/dataModels/maintenanceInstruction';
import { ColorModel } from 'src/app/models/dataModels/colorModel';
import { MaterialModel } from 'src/app/models/dataModels/materialModel';
import { SizeModel } from 'src/app/models/dataModels/sizeModel';
import { ThemeModel } from 'src/app/models/dataModels/themeModel';
import { StyleModel } from 'src/app/models/dataModels/styleModel';
import { SnackbarComponent } from '../../snackbar/snackbar.component';
import { PossessionModel } from 'src/app/models/dataModels/possessionModel';
import { PossessionService } from 'src/app/services/dataServices/possession-service.service';
import { SizeListModel } from 'src/app/models/dataListModels/sizeListModel';
import { ClotheTypeListModel } from 'src/app/models/dataListModels/clotheTypeListModel';
import { SexListModel } from 'src/app/models/dataListModels/sexListModel';
import { MaintenanceInstructionListModel } from 'src/app/models/dataListModels/maintenanceInstructionListModel';
import { BrandListModel } from 'src/app/models/dataListModels/brandListModel';
import { SeasonListModel } from 'src/app/models/dataListModels/seasonListModel';
import { ColorListModel } from 'src/app/models/dataListModels/colorListModel';
import { MaterialListModel } from 'src/app/models/dataListModels/materialListModel';
import { ProviderListModel } from 'src/app/models/dataListModels/providerListModel';
import { StyleListModel } from 'src/app/models/dataListModels/styleListModel';
import { ThemeListModel } from 'src/app/models/dataListModels/themeListModel';
import { SeasonModel } from 'src/app/models/dataModels/seasonModel';
import { ProviderModel } from 'src/app/models/dataModels/providerModel';
import { SexModel } from 'src/app/models/dataModels/sexModel';
import { BrandModel } from 'src/app/models/dataModels/brandModel';




@Component({
  selector: 'app-clothe-list-item',
  templateUrl: './clothe-list-item.component.html',
  styleUrls: ['./clothe-list-item.component.css']
})
export class ClotheListItemComponent implements OnInit {

  addPictureIndex: number = -1;
  numberOfPossessionFrames: boolean = false;

  sizeListModel: SizeListModel = new SizeListModel();
  sizeCategories: string[] = [];
  clotheTypeListModel: ClotheTypeListModel = new ClotheTypeListModel();
  clotheTypeCategories: string[] = [];
  sexListModel: SexListModel = new SexListModel();
  maintenanceInstructionListModel: MaintenanceInstructionListModel = new MaintenanceInstructionListModel();
  brandListModel: BrandListModel = new BrandListModel;
  seasonListModel: SeasonListModel = new SeasonListModel;
  colorListModel: ColorListModel = new ColorListModel();
  materialListModel: MaterialListModel = new MaterialListModel();
  providerListModel: ProviderListModel = new ProviderListModel();
  styleListModel: StyleListModel = new StyleListModel();
  themeListModel: ThemeListModel = new ThemeListModel();
  clotheModel: ClotheModel = new ClotheModel();
  isNewCloth: boolean =false; 
  isNotCompleteEnough: boolean = false;

  @Input() set setClotheModel (clotheModel : ClotheModel){
    this.clotheModel = clotheModel;
    if(clotheModel.clotheId < 1){
      this.isNewCloth = true;
    }
    this.evaluatePostability();
  };
  @Input() hasAdminRole : boolean = false;
  @Input() set setSizeOptions(sizeListModel: SizeListModel){
    this.sizeListModel = sizeListModel;
  }
  @Input() set setSizeCategories(categories: string[]){
    this.sizeCategories = categories;
  }
  @Input() set setClotheTypeOptions(clotheTypeListModel: ClotheTypeListModel){
    this.clotheTypeListModel = clotheTypeListModel;
  }
  @Input() set setClotheTypeCategories(categories: string[]){
    this.clotheTypeCategories = categories;
  }
  @Input() set setSexOptions(sexListModel: SexListModel){
    this.sexListModel = sexListModel;
  }
  @Input() set setMaintenanceInstructionOptions(maint: MaintenanceInstructionListModel){
    this.maintenanceInstructionListModel = maint;
  }
  @Input() set setBrandOptions(brandListModel: BrandListModel){
    this.brandListModel = brandListModel;
  }
  @Input() set setSeasonOptions(seasonModel: SeasonListModel){
    this.seasonListModel = seasonModel;
  }
  @Input() set setColorOptions(colorModel: ColorListModel){
    this.colorListModel = colorModel;
  }
  @Input() set setMaterialOptions(materialListModel: MaterialListModel){
    this.materialListModel = materialListModel;
  }
  @Input() set setProviderOptions(providerListModel: ProviderListModel){
    this.providerListModel = providerListModel
  }
  @Input() set setStyleOptions(styleListModel: StyleListModel){
    this.styleListModel = styleListModel;
  }
  
  @Input() set setThemeOptions(themeListModel: ThemeListModel){
    this.themeListModel = themeListModel;
  }





  @Output() updateClothe = new EventEmitter<ClotheModel>();
  @Output() deleteClothe = new EventEmitter<ClotheModel>();
  @Output() getComments = new EventEmitter<ClotheModel>();
  editClotheForm: FormGroup;
  addToPossessionsFlag: boolean = false;

  constructor(private formBuilder: FormBuilder, private snackbar: SnackbarComponent, private possessionService: PossessionService) { 
    this.clotheModel = new ClotheModel();
  }

  ngOnInit() {
    this.initForm();
  }

  onAddToPossessions(){
    this.addToPossessionsFlag = true;

    if(this.numberOfPossessionFrames){
      this.numberOfPossessionFrames = false;
    }else{
      this.numberOfPossessionFrames= true;
    }
    
  }

  initForm(){
    this.editClotheForm = this.formBuilder.group({
      'clotheId': new FormControl(this.clotheModel.clotheId, [Validators.required, Validators.min(-100)]),
      'name': new FormControl(this.clotheModel.name, [Validators.required, Validators.pattern(/^[a-zA-Z0-9çéèÇàß+' -_]{2,60}$/)]),
      'url' : new FormControl(this.clotheModel.url, [Validators.required, Validators.pattern(/^((http(s?):)([\/|.|\w%|-])*\.(?:html|asp|aspx|asp)$)/gm)]),
    })
  }

  

  /**
   * Permet de créer un nouveau PictureModel, ce qui entraîne la création d'un nouveau PictureDisplayer
   * De plus, diminue de 1 la valeur de addPictureIndex, ce qui permet d'identifier de manière unique les pictureModels ajoutés
   */
  onAddPicture(){
    let pm = new PictureModel();
    pm.pictureId = this.addPictureIndex;
    this.addPictureIndex--;
    if(this.clotheModel.pictureModels){
      if(this.clotheModel.pictureModels.length < 1){
        this.clotheModel.pictureModels = [];
      }
    }else{
      this.clotheModel.pictureModels = [];
    }
    
    this.clotheModel.pictureModels.push(pm);
  }

  /**
   * Fonction appelée lorsque l'utilisateur appuie sur le bouton appliquer d'un pictureDisplayer
   * @param pictureToUpdate PictureModel modifié
   */
  onUpdatePicture(pictureToUpdate: PictureModel){
    for(let pic of this.clotheModel.pictureModels){
      if(pic.pictureId === pictureToUpdate.pictureId){
        pic.name = pictureToUpdate.name;
        pic.url = pictureToUpdate.url;
      }
      break;
    }
  }

  /**
   * Supprime un PictureModel de la liste des PictureModels du ClotheModel
   */
  onDeletePicture(pictureToDelete: PictureModel){
    if(pictureToDelete){
      for(let pic of this.clotheModel.pictureModels){
        if(pic.pictureId === pictureToDelete.pictureId){
          const index = this.clotheModel.pictureModels.indexOf(pic, 0);
          if(index > -1){
            this.clotheModel.pictureModels.splice(index, 1);
          }
        }
      }
    }
  }

  /**
   * Fonction appelée lorsque l'administrateur valide la modification d'un vêtement
   */
  onSubmitEditClothe(){
    this.clotheModel.name = this.editClotheForm.controls['name'].value;
    this.clotheModel.url = this.editClotheForm.controls['url'].value;
    for(let picture of this.clotheModel.pictureModels){
      if(picture.pictureId < 1){
        picture.pictureId = 0;
      }
    }
      this.updateClothe.emit(this.clotheModel);
  }

  evaluatePostability():void{
    let requiredModelMissing: boolean = false
    if(!this.clotheModel.providerModel || !this.clotheModel.providerModel.provider){//Si pas de providerModel
      requiredModelMissing = true;
    }else{//Si providerModel mais id non valide
      if(this.clotheModel.providerModel.providerId<1){
        requiredModelMissing = true
      }
    }
    if(!this.clotheModel.seasonModel  || !this.clotheModel.seasonModel.seasonId){//Si pas de seasonModel
      requiredModelMissing = true;
    }else{
      if(this.clotheModel.seasonModel.seasonId < 1){//Si seasonModel mais id invalide
        requiredModelMissing = true
      }
    }
    if(!this.clotheModel.sexModel || !this.clotheModel.sexModel.sexId){//Si pas de sexModel
      requiredModelMissing = true;
    }else{
      if(this.clotheModel.sexModel.sexId < 1){
        requiredModelMissing = true;
      }
    }
    if(!this.clotheModel.brandModel || !this.clotheModel.brandModel){//Si pas de brandModel
      requiredModelMissing = true;
    }else{
      if(this.clotheModel.brandModel.brandId <1){
        requiredModelMissing = true;
      }
    }
    if(requiredModelMissing){
      this.isNotCompleteEnough = true;
    }
    else{
      this.isNotCompleteEnough = false;
    }
  }

  onChangeMaintenanceInstructions(val: MaintenanceInstructionModel[]){
    this.clotheModel.maintenanceInstructionModels = val;
  }
  
  onChangeColor(val: ColorModel[]){
    this.clotheModel.colorModels = val;
  }

  onChangeStyle(val: StyleModel[]){
    this.clotheModel.styleModels = val;
  }

  onChangeTheme(val: ThemeModel[]){
  this.clotheModel.themeModels =  val;
  }

  onChangeSize(val: SizeModel[]){
    this.clotheModel.sizeModels = val;
  }

  onChangeMaterial(val: MaterialModel[]){
    this.clotheModel.materialModels = val;
  }

  onChangeSeason(val: SeasonModel){
     this.clotheModel.seasonModel =val;
     this.evaluatePostability();
  }

  onChangeProvider(val: ProviderModel){
      this.clotheModel.providerModel = val;
      this.evaluatePostability();
  }

  onChangeSex(val: SexModel){
      this.clotheModel.sexModel =val;
      this.evaluatePostability();
  }

  onChangeClotheTypes(val: ClotheTypeModel[]){
    this.clotheModel.clotheTypeModels = val;
  }

  onChangeBrand(val: BrandModel){
      this.clotheModel.brandModel =val;
      this.evaluatePostability();
  }

  /**Supprimer un vêtement */
  onDeleteClothe(){
    this.deleteClothe.emit(this.clotheModel);
  }

  onCancelAddToPossession(value: boolean){
    this.addToPossessionsFlag = value;
  }

  /**
   * Récupère la liste des PossessionModels
   * Y ajoute le ClotheModel contenu par ce component
   * Va devoir contacter le serveur pour qu'il les ajoute héhé
   * @param possessionModels La liste de PossessionModels reçus par l'utilisateur via addToPossessionFrameComponent
   */
  onUserSubmitAddPossessions(possessionModels: PossessionModel[]){
    for(let  possession of possessionModels){
      possession.clotheModel = this.clotheModel;
    }
    this.possessionService.localAddData(possessionModels).subscribe(
      ()=>{
        this.snackbar.openSnackBar("Possessions Ajoutées!", "OK", "success-snackbar");
        this.onCancelAddToPossession(false);
      },error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de l'ajout des possessions: " + error.error, "OK", "error-snackbar");
      }
    )
  }

  onGoToComments(){
    this.getComments.emit(this.clotheModel);
  }
}
