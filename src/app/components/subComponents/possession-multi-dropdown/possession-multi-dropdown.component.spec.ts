import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PossessionMultiDropdownComponent } from './possession-multi-dropdown.component';

describe('PossessionMultiDropdownComponent', () => {
  let component: PossessionMultiDropdownComponent;
  let fixture: ComponentFixture<PossessionMultiDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PossessionMultiDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PossessionMultiDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
