import { Component, OnInit, Input } from '@angular/core';
import { GenericDropdownComponent } from '../generic-dropdown/generic-dropdown.component';
import { PossessionModel } from 'src/app/models/dataModels/possessionModel';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-possession-multi-dropdown',
  templateUrl: './possession-multi-dropdown.component.html',
  styleUrls: ['./possession-multi-dropdown.component.css']
})
export class PossessionMultiDropdownComponent extends GenericDropdownComponent implements OnInit {

  @Input() set setValues(possessionModels: PossessionModel[]){
    if(possessionModels){
      this.form.controls.selectForm.setValue(possessionModels);
    }
  }

  constructor(protected formBuilder: FormBuilder) {
    super(formBuilder);
   }

  ngOnInit() {
  }

  compareObjects(object1: PossessionModel, object2: PossessionModel){
    return object1.possessionId && object2 && object1.possessionId == object2.possessionId;
  }


}
