import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PictureModel } from 'src/app/models/dataModels/pictureModel';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';


@Component({
  selector: 'app-picture-displayer',
  templateUrl: './picture-displayer.component.html',
  styleUrls: ['./picture-displayer.component.css']
})
export class PictureDisplayerComponent implements OnInit {

  @Input() hasAdminRole:boolean = false;
  @Input() set setPictureModel(pictureModel: PictureModel){
    this.pictureModel = pictureModel;
    this.form = this.formBuilder.group({
      'name': new FormControl(this.pictureModel.name, [Validators.required, Validators.pattern("^[a-zA-z0-9'-_ ]{2,50}$")]),
      'url': new FormControl(this.pictureModel.url, [Validators.required, Validators.pattern(/^https?:\/\/(?:[a-z0-9\-]+\.)+[a-z]{2,6}(?:\/[^\/#?]+)+\.(?:jpg|gif|png)(\?[a-zA-Z0-9\/\-\=&]*)*$/)])
    });
  }
  @Output() delPicture =  new EventEmitter<PictureModel>();
  @Output() upPicture = new EventEmitter<PictureModel>();
  pictureModel: PictureModel = new PictureModel();
  form: FormGroup;

  constructor(private formBuilder: FormBuilder) {
    this.form = formBuilder.group({
      'name': new FormControl(this.pictureModel.name, [Validators.required, Validators.pattern("^[a-zA-z0-9'-_ ]{2,50}$")]),
      'url': new FormControl(this.pictureModel.url, [Validators.required, Validators.pattern(/^https?:\/\/(?:[a-z0-9\-]+\.)+[a-z]{2,6}(?:\/[^\/#?]+)+\.(?:jpg|gif|png)(\?[a-zA-Z0-9\/\-\=&]*)*$/)])
    });
   }

  ngOnInit() {
  }

  openLink(){
    window.open(this.pictureModel.url, "_blank");
  }

  /**
   * Permet d'envoyer au composant parent le pictureModel à supprimer
   */
  deletePicture(){
    this.delPicture.emit(this.pictureModel);
  }

  /**
   * Permet d'envoyer un pictureModel mis à jour au composant parent
   */
  updatePicture(){
    this.pictureModel.name = this.form.controls['name'].value;
    this.pictureModel.url = this.form.controls['url'].value;
    this.upPicture.emit(this.pictureModel);
  }
}
