import { Component, OnInit, OnDestroy, Output, EventEmitter, Input } from '@angular/core';
import { GenericDropdownComponent } from '../generic-dropdown/generic-dropdown.component';
import { ColorSelectProviderService } from 'src/app/services/dataServices/subServices/color-select-provider.service';
import { MatSelectChange } from '@angular/material';
import { ColorModel } from 'src/app/models/dataModels/colorModel';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-color-dropdown',
  templateUrl: './color-dropdown.component.html',
  styleUrls: ['./color-dropdown.component.css']
})
export class ColorDropdownComponent extends GenericDropdownComponent implements OnInit, OnDestroy {
 @Input() set setValues(colorModel: ColorModel){
   if(colorModel){
     this.form.controls.selectForm.setValue(colorModel);
   }
 }
  constructor(protected formBuilder: FormBuilder) {
    super(formBuilder);
   }

  ngOnInit() {
  }
  ngOnDestroy(){
  }

  compareObjects(object1: ColorModel, object2: ColorModel){
    return object1.colorId && object2 && object1.colorId == object2.colorId;
  }

}
