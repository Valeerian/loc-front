import { Component, OnInit, OnDestroy, Output, EventEmitter, Input } from '@angular/core';
import { GenericDropdownComponent } from '../generic-dropdown/generic-dropdown.component';
import { ClotheTypeSelectProviderService } from 'src/app/services/dataServices/subServices/clothe-type-select-provider.service';
import { MatSelectChange } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ClotheTypeModel } from 'src/app/models/dataModels/clotheTypeModel';

@Component({
  selector: 'app-clothe-type-multi-dropdown',
  templateUrl: './clothe-type-multi-dropdown.component.html',
  styleUrls: ['./clothe-type-multi-dropdown.component.css']
})
export class ClotheTypeMultiDropdownComponent extends GenericDropdownComponent implements OnInit, OnDestroy {

  @Input() set setValues(clotheTypes: ClotheTypeModel[]){
      this.form.controls.selectForm.setValue(clotheTypes);
  }
  constructor( formBuilder: FormBuilder) { 
    super(formBuilder);
  }

  ngOnInit() {
     }

  ngOnDestroy(){
   }

   compareObjects(object1: ClotheTypeModel, object2: ClotheTypeModel){
    return object1.clotheTypeId && object2 && object1.clotheTypeId == object2.clotheTypeId;
  }

}
