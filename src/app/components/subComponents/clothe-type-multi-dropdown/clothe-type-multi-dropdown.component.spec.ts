import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClotheTypeMultiDropdownComponent } from './clothe-type-multi-dropdown.component';

describe('ClotheTypeMultiDropdownComponent', () => {
  let component: ClotheTypeMultiDropdownComponent;
  let fixture: ComponentFixture<ClotheTypeMultiDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClotheTypeMultiDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClotheTypeMultiDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
