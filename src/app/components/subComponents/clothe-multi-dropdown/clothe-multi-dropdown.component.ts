import { Component, OnInit, Input } from '@angular/core';
import { GenericDropdownComponent } from '../generic-dropdown/generic-dropdown.component';
import { ClotheModel } from 'src/app/models/dataModels/clotheModel';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-clothe-multi-dropdown',
  templateUrl: './clothe-multi-dropdown.component.html',
  styleUrls: ['./clothe-multi-dropdown.component.css']
})
export class ClotheMultiDropdownComponent extends GenericDropdownComponent implements OnInit {

  @Input() set setValues(clotheModels: ClotheModel[]){
    if(clotheModels){
      this.form.controls.selectForm.setValue(clotheModels);
    }
  }

  constructor(protected formBuilder: FormBuilder) {
    super(formBuilder);
   }

   compareObjects(object1: ClotheModel, object2: ClotheModel){
    return object1.clotheId && object2 && object1.clotheId == object2.clotheId;
  }


}
