import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClotheMultiDropdownComponent } from './clothe-multi-dropdown.component';

describe('ClotheMultiDropdownComponent', () => {
  let component: ClotheMultiDropdownComponent;
  let fixture: ComponentFixture<ClotheMultiDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClotheMultiDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClotheMultiDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
