import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MimicDropdownComponent } from './mimic-dropdown.component';

describe('MimicDropdownComponent', () => {
  let component: MimicDropdownComponent;
  let fixture: ComponentFixture<MimicDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MimicDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MimicDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
