import { Component, OnInit, Input } from '@angular/core';
import { MimicModel } from 'src/app/models/dataModels/mimicModel';
import { GenericDropdownComponent } from '../generic-dropdown/generic-dropdown.component';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-mimic-dropdown',
  templateUrl: './mimic-dropdown.component.html',
  styleUrls: ['./mimic-dropdown.component.css']
})
export class MimicDropdownComponent extends GenericDropdownComponent implements OnInit {

  @Input() set setValues(mimicModel: MimicModel[]){
    if(mimicModel){
      this.form.controls.selectForm.setValue(mimicModel);
    }
  }

  constructor(protected formbuilder: FormBuilder) {
    super(formbuilder);
   }

  ngOnInit() {
  }

  compareObjects(object1: MimicModel, object2: MimicModel){
    return object1.mimicId && object2 && object1.mimicId == object2.mimicId;
  }

}
