import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MinimumRatingDropdownComponent } from './minimum-rating-dropdown.component';

describe('MinimumRatingDropdownComponent', () => {
  let component: MinimumRatingDropdownComponent;
  let fixture: ComponentFixture<MinimumRatingDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MinimumRatingDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MinimumRatingDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
