import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { MatSelectChange } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-minimum-rating-dropdown',
  templateUrl: './minimum-rating-dropdown.component.html',
  styleUrls: ['./minimum-rating-dropdown.component.css']
})
export class MinimumRatingDropdownComponent implements OnInit {

  notes: number[] = [0,1,2,3,4,5,6,7,8,9,10];
  @Input() set resetFlag(resetFlag: boolean){
    this.reset();
  }
  @Input() placeHolder: string = "placeHolder par défaut";
  @Input() isFilter: boolean = true;
  form: FormGroup;

  @Output() selectStateChanged = new EventEmitter<number>();

  constructor(protected formBuilder: FormBuilder) {
    this.form = formBuilder.group({
      selectForm: [null]
    });
  }

  ngOnInit() {
  }

  onSelect(val: MatSelectChange):void{
    this.selectStateChanged.emit(val.value);
  }

  reset(){
    this.form.reset();
    this.selectStateChanged.emit(this.form.value['selectForm']);
  }

}
