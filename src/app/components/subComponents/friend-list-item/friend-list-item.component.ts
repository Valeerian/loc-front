import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FriendshipModel } from 'src/app/models/dataModels/friendshipModel';
import { SnackbarComponent } from '../../snackbar/snackbar.component';
import { PossessionService } from 'src/app/services/dataServices/possession-service.service';
import { PossessionListModel } from 'src/app/models/dataListModels/possessionListModel';
import { error } from 'protractor';
import { LaundryGroupService } from 'src/app/services/dataServices/laundry-group.service';
import { LaundryGroupListModel } from 'src/app/models/dataListModels/LaundryGroupListModel';

@Component({
  selector: 'app-friend-list-item',
  templateUrl: './friend-list-item.component.html',
  styleUrls: ['./friend-list-item.component.css']
})
export class FriendListItemComponent implements OnInit {
  commonLaundryGroup: LaundryGroupListModel = new LaundryGroupListModel();
  sharedWithThisFriend: PossessionListModel = new PossessionListModel();
  sharedFromThisFriend: PossessionListModel = new PossessionListModel();
  friendshipModel: FriendshipModel = new FriendshipModel();
  @Input() set setFriend(friendshipModel: FriendshipModel){
    console.log("????");
    console.log(friendshipModel);
    this.friendshipModel = friendshipModel;
    if(friendshipModel.friendshipId){
      if(friendshipModel.friendshipId > 0){
        this.possessionService.localGetPossessionsSharedWithThisFriend(this.friendshipModel).subscribe(
          (possessionList: PossessionListModel)=>{
            this.sharedWithThisFriend = possessionList;
          },error=>{
            this.snackbar.openSnackBar("Une erreur est survenue lors de la récupération des vêtements prêtés à l'utilisateur " + friendshipModel.userTwo.userName + ": " + error.error, "OK", "error-snackbar");
          }
        );
        this.possessionService.localGetPossessionsSharedFromThisFriend(this.friendshipModel).subscribe(
          (possessionList: PossessionListModel)=>{
            this.sharedFromThisFriend = possessionList;
          },error=>{
            this.snackbar.openSnackBar("Une erreur est survenue lors de la récupération des vêtements que l'utilisateur " + friendshipModel.userTwo.userName + " vous a prêtés: " + error.error, "OK", "error-snackbar");
          }
        );
        this.laundryGroupService.localGetCommonLaundryGroups(this.friendshipModel.userTwo).subscribe(
          (laundryGroupList: LaundryGroupListModel)=>{
            this.commonLaundryGroup = laundryGroupList;
          },error=>{
            this.snackbar.openSnackBar("Une erreur est survenue lors de la récupération des groupes lessives que vous avez en commun avec " + friendshipModel.userTwo.userName + ": " + error.error, "OK", "error-snackbar");
          }
        );

      }
    }
  }

  @Output() onDeleteFriend = new EventEmitter<FriendshipModel>();
  @Output() onShowWishList = new EventEmitter<FriendshipModel>();
  @Output() onShowPossessions = new EventEmitter<FriendshipModel>();
  @Output() onShowOutfit = new EventEmitter<FriendshipModel>();

  constructor(private snackbar: SnackbarComponent, private possessionService: PossessionService, private laundryGroupService: LaundryGroupService) { }

  ngOnInit() {
  }

  onGoToWishList(){
    this.onShowWishList.emit(this.friendshipModel);
  }

  onGoToPossessions(){
    this.onShowPossessions.emit(this.friendshipModel);
  }

  onGoToOutfits(){
    this.onShowOutfit.emit(this.friendshipModel);
  }
  
  deleteFriend(){
    this.onDeleteFriend.emit(this.friendshipModel);
  }

}
