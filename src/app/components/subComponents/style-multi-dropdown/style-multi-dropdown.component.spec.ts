import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StyleMultiDropdownComponent } from './style-multi-dropdown.component';

describe('StyleMultiDropdownComponent', () => {
  let component: StyleMultiDropdownComponent;
  let fixture: ComponentFixture<StyleMultiDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StyleMultiDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StyleMultiDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
