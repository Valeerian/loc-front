import { Component, OnInit, OnDestroy, Output, EventEmitter, Input } from '@angular/core';
import { GenericDropdownComponent } from '../generic-dropdown/generic-dropdown.component';
import { StyleSelectProviderService } from 'src/app/services/dataServices/subServices/style-select-provider.service';
import { MatSelectChange } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';
import { StyleModel } from 'src/app/models/dataModels/styleModel';

@Component({
  selector: 'app-style-multi-dropdown',
  templateUrl: './style-multi-dropdown.component.html',
  styleUrls: ['./style-multi-dropdown.component.css']
})
export class StyleMultiDropdownComponent extends GenericDropdownComponent implements OnInit, OnDestroy {


  @Input() set setValues(styleModels: StyleModel[]){
    if(styleModels){
      this.form.controls.selectForm.setValue(styleModels);
    }
  }

  constructor(protected formBuilder: FormBuilder) { 
    super(formBuilder);
  }

  ngOnInit() {
  }

  ngOnDestroy(){
  }

  compareObjects(object1: StyleModel, object2: StyleModel){
    return object1.styleId && object2 && object1.styleId == object2.styleId;
  }
}
