import { Component, OnInit, Input } from '@angular/core';
import { GenericDropdownComponent } from '../generic-dropdown/generic-dropdown.component';
import { FormBuilder } from '@angular/forms';
import { OutfitModel } from 'src/app/models/dataModels/outfitModel';

@Component({
  selector: 'app-outfit-multi-dropdown',
  templateUrl: './outfit-multi-dropdown.component.html',
  styleUrls: ['./outfit-multi-dropdown.component.css']
})
export class OutfitMultiDropdownComponent extends GenericDropdownComponent implements OnInit {

  @Input() set setValues(outfitModels: OutfitModel[]){
    if(outfitModels){
      this.form.controls.selectForm.setValue(outfitModels);
    }
  }

  constructor(protected formBuilder: FormBuilder) {
    super(formBuilder);
   }

  ngOnInit() {
  }

  compareObjects(object1: OutfitModel, object2: OutfitModel){
    return object1.outfitId && object2 && object1.outfitId == object2.outfitId;
  }

}
