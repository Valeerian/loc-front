import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutfitMultiDropdownComponent } from './outfit-multi-dropdown.component';

describe('OutfitMultiDropdownComponent', () => {
  let component: OutfitMultiDropdownComponent;
  let fixture: ComponentFixture<OutfitMultiDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutfitMultiDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutfitMultiDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
