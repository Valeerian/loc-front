import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LaundryGroupListItemComponent } from './laundry-group-list-item.component';

describe('LaundryGroupListItemComponent', () => {
  let component: LaundryGroupListItemComponent;
  let fixture: ComponentFixture<LaundryGroupListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LaundryGroupListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaundryGroupListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
