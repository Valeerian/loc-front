import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { LaundryGroupModel } from 'src/app/models/dataModels/laundryGroupModel';
import { UserListModel } from 'src/app/models/dataListModels/userListModel';
import { TokenStorageService } from 'src/app/services/token-storage.service';
import { OutfitModel } from 'src/app/models/dataModels/outfitModel';
import { FormControl, Validators, FormBuilder, FormGroup } from '@angular/forms';
import { UserModel } from 'src/app/models/dataModels/userModel';
import { LaundryManeModel } from 'src/app/models/dataModels/laundryManeModel';
import { ManeStatusListModel } from 'src/app/models/dataListModels/maneStatusListModel';

@Component({
  selector: 'app-laundry-group-list-item',
  templateUrl: './laundry-group-list-item.component.html',
  styleUrls: ['./laundry-group-list-item.component.css']
})
export class LaundryGroupListItemComponent implements OnInit {

  constructor(private token: TokenStorageService,private formBuilder: FormBuilder) {
    
   }

  
  @Output() addLaundryGroup = new EventEmitter<LaundryGroupModel>();
  @Output() onDeleteLaundryGroup = new EventEmitter<LaundryGroupModel>();
  @Output() onUpdateLaundryGroup = new EventEmitter<LaundryGroupModel>();
  @Output() onQuitLaundryGroup = new EventEmitter<LaundryGroupModel>();
  addManeIndex: number = -1; 
  @Input() idName: string = "";
  userListModel: UserListModel = new UserListModel()
  @Input() set setGroupMembersOptions(potentialMembers: UserListModel){
    this.userListModel = potentialMembers;
  }
  isNew: boolean = false;
  groupMembers: UserListModel = new UserListModel();
  groupMembersWithoutCurrentUser: UserListModel = new UserListModel(); 
  isGroupOwner: boolean = false;
  laundryGroupModel: LaundryGroupModel = new LaundryGroupModel();
  form: FormGroup;

  maneStatusList: ManeStatusListModel= new ManeStatusListModel();
  @Input() set setmaneStatusOptions(maneStatusListModel: ManeStatusListModel){
    this.maneStatusList = maneStatusListModel;
  }

  @Input() set setLaundryGroup(lg : LaundryGroupModel){
    this.laundryGroupModel = lg; 
    this.form = this.formBuilder.group({
      groupName: new FormControl(this.laundryGroupModel.laundryGroupName, [Validators.required,Validators.pattern(/^[A-Za-z0-9ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØŒŠþÙÚÛÜÝŸàáâãäåæçèéêëìíîïðñòóôõöøœšÞùúûü:ýÿ=ß,\\\/'\"!\{\}\[\] ().\-\;?_]{1,50}$/)])
    });
    if(!lg || lg.laundryGroupId < 1){
      this.isNew = true;
      this.isGroupOwner = true;
    }
    if(lg){
      if(lg.laundryGroupOwner){
        if(this.token.getUserName() == lg.laundryGroupOwner.userName){
          this.isGroupOwner = true;
        }
        else{
          this.form.disable();
        }
        
      }
      if(lg.laundryGroupMembers){
        if(lg.laundryGroupMembers.length > 0){//S'il y a au moins un membre dans le groupe
          this.groupMembers.list = lg.laundryGroupMembers;
          this.groupMembers.numberOfItems = lg.laundryGroupMembers.length;
          let temp: UserModel[] = [];
          for(let user of lg.laundryGroupMembers){
            temp.push(user);
          }
          let ind = -1;
          for(let user of temp){
            if(user.userName === this.token.getUserName()){
              ind = temp.indexOf(user);
            }
          }
          if(ind > -1){
            temp.splice(ind, 1);
            this.groupMembersWithoutCurrentUser.list = temp;
            this.groupMembersWithoutCurrentUser.numberOfItems = temp.length;
          }
        }
      }
    }
  }

  

  onChangeGroupOwner(userModel: UserModel){
    this.laundryGroupModel.laundryGroupOwner = userModel;
  }



  onChangeGroupMembers(userModels: UserModel[]){
    this.laundryGroupModel.laundryGroupMembers = userModels;
    this.groupMembers.list = userModels;
    this.groupMembers.numberOfItems = userModels.length;
    let temp: UserModel[] = [];
    for(let user of userModels){
      temp.push(user);
    }
    let ind = -1;
    for(let user of temp){
      if(user.userName === this.token.getUserName()){
        ind = temp.indexOf(user);
        }
      }
    if(ind > 1){
      temp.splice(ind, 1);
      this.groupMembersWithoutCurrentUser.list = temp;
      this.groupMembersWithoutCurrentUser.numberOfItems = temp.length;
    }

  }

  onSubmitEdit(){
    this.laundryGroupModel.laundryGroupName = this.form.controls['groupName'].value;
    this.onUpdateLaundryGroup.emit(this.laundryGroupModel);
  }

  onDelete(){
    this.onDeleteLaundryGroup.emit(this.laundryGroupModel);
  }

  onQuit(){
    this.onQuitLaundryGroup.emit(this.laundryGroupModel);
  }

  onCreate(){
    this.laundryGroupModel.laundryGroupName = this.form.controls['groupName'].value;
    for(let user of this.userListModel.list){
      if(user.userName == this.token.getUserName()){
        this.laundryGroupModel.laundryGroupOwner = user;
      }
    }
    this.addLaundryGroup.emit(this.laundryGroupModel);
  }

  onCreateLaundryMane(){
    let newBasket : LaundryManeModel = new LaundryManeModel();
    newBasket.laundryManeId = this.addManeIndex;
    this.addManeIndex--;
    if(this.laundryGroupModel){//SI le groupe reçu ne vaut pas null/undefined
      if(this.laundryGroupModel.laundryManes){//si le groupe reçu possède quelque chose dans laundryManes
        if(this.laundryGroupModel.laundryManes.length < 1){//Si ce quelque chose est vide
          this.laundryGroupModel.laundryManes = [];
          this.laundryGroupModel.laundryManes.push(newBasket);
        }
        else{//Si ce quelque chose contient déjà des mannes
          this.laundryGroupModel.laundryManes.push(newBasket);
        }
      }
      else{//Si laundryManes est null/undef
        this.laundryGroupModel.laundryManes = [];
        this.laundryGroupModel.laundryManes.push(newBasket);
      }
    }else{//Si laundryGroupModel est null/undefined
      this.laundryGroupModel = new LaundryGroupModel();
      this.laundryGroupModel.laundryManes = [];
      this.laundryGroupModel.laundryManes.push(newBasket);
    }
  }

  onDeleteLaundryMane(laundryManeModel: LaundryManeModel){
    for(let basket of this.laundryGroupModel.laundryManes){
      if(basket.laundryManeId === laundryManeModel.laundryManeId){
        const index =this.laundryGroupModel.laundryManes.indexOf(basket);
        if(index > -1){
          this.laundryGroupModel.laundryManes.splice(index, 1);
        }
      }
    }
  }



  ngOnInit() {
    
  }

}
