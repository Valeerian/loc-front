import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SizeMultiDropdownComponent } from './size-multi-dropdown.component';

describe('SizeMultiDropdownComponent', () => {
  let component: SizeMultiDropdownComponent;
  let fixture: ComponentFixture<SizeMultiDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SizeMultiDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SizeMultiDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
