import { Component, OnInit, OnDestroy, Output, EventEmitter, Input } from '@angular/core';
import { GenericDropdownComponent } from '../generic-dropdown/generic-dropdown.component';
import { SizeSelectProviderService } from 'src/app/services/dataServices/subServices/size-select-provider.service';
import { MatSelectChange } from '@angular/material';
import { SizeModel } from 'src/app/models/dataModels/sizeModel';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-size-multi-dropdown',
  templateUrl: './size-multi-dropdown.component.html',
  styleUrls: ['./size-multi-dropdown.component.css']
})
export class SizeMultiDropdownComponent extends GenericDropdownComponent implements OnInit, OnDestroy {

  @Input() set setValues(sizeModels: SizeModel[]){
    this.form.controls.selectForm.setValue(sizeModels);
  }

  constructor( formBuilder: FormBuilder) { 
    super(formBuilder);
    
  }

  ngOnInit() {
  }
  compareObjects(object1: SizeModel, object2: SizeModel){
    return object1.id && object2 && object1.id == object2.id;
  }

  ngOnDestroy(){
  }

}
