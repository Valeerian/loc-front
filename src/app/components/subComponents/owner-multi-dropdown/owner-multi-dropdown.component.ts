import { Component, OnInit, OnDestroy, Output, EventEmitter, Input } from '@angular/core';
import { GenericDropdownComponent } from '../generic-dropdown/generic-dropdown.component';
import { UserModel } from 'src/app/models/dataModels/userModel';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UserListModel } from 'src/app/models/dataListModels/userListModel';
import { SizeSelectProviderService } from 'src/app/services/dataServices/subServices/size-select-provider.service';
import { MatSelectChange } from '@angular/material';

@Component({
  selector: 'app-owner-multi-dropdown',
  templateUrl: './owner-multi-dropdown.component.html',
  styleUrls: ['./owner-multi-dropdown.component.css']
})
export class OwnerMultiDropdownComponent extends GenericDropdownComponent implements OnInit, OnDestroy {

  @Input() set setValues(userModels: UserModel[]){
    if(userModels){
      this.form.controls.selectForm.setValue(userModels);
    }
  }

  constructor(protected formBuilder: FormBuilder) {
    super(formBuilder);
   }

  ngOnInit() {
  }

  compareObjects(object1: UserModel, object2: UserModel){
    return object1.userId && object2 && object1.userId == object2.userId;
  }

  ngOnDestroy(){}


}