import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OwnerMultiDropdownComponent } from './owner-multi-dropdown.component';

describe('OwnerMultiDropdownComponent', () => {
  let component: OwnerMultiDropdownComponent;
  let fixture: ComponentFixture<OwnerMultiDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OwnerMultiDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OwnerMultiDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
