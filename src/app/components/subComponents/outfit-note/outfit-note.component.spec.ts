import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutfitNoteComponent } from './outfit-note.component';

describe('OutfitNoteComponent', () => {
  let component: OutfitNoteComponent;
  let fixture: ComponentFixture<OutfitNoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutfitNoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutfitNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
