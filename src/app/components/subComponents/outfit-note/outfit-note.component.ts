import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { GenericComponent } from '../../generic/generic.component';
import { OutfitModel } from 'src/app/models/dataModels/outfitModel';
import { OutfitNoteModel } from 'src/app/models/dataModels/outfitNoteModel';
import { SnackbarComponent } from '../../snackbar/snackbar.component';
import { AuthService } from 'src/app/services/auth-service';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { OutfitNoteService } from 'src/app/services/dataServices/outfit-note.service';
import { IMAGES } from 'src/app/util/images';

@Component({
  selector: 'app-outfit-note',
  templateUrl: './outfit-note.component.html',
  styleUrls: ['./outfit-note.component.css']
})
export class OutfitNoteComponent extends GenericComponent implements OnInit, OnDestroy {

  @Input() set setOutfitModel(outfitModel: OutfitModel){
    this.onResetAndSetValueToQM("outfit",":",""+outfitModel.outfitId);
    this.dataService.localGetMyComment(this.queryModel).subscribe(
      (outfitNote : OutfitNoteModel)=>{
        this.myComment = outfitNote;
        this.initForm();
      },error =>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la récupération de votre commentaire", "OK", "error-snackbar");
      }
    );
  }

  myComment: OutfitNoteModel = new OutfitNoteModel();
  idLogoSrcOne: string = "";
  idLogoSrcTwo: string = "";
  dateLogoSrcOne: string = "";
  dateLogoSrcTwo: string = "";
  noteLogoSrcOne: string = "";
  noteLogoSrcTwo: string = "";
  notes: number[]  = [0,1,2,3,4,5,6,7,8,9,10];

  constructor(protected snackbar: SnackbarComponent, protected authService: AuthService,  protected dataService: OutfitNoteService, protected formBuilder: FormBuilder) {
    super(snackbar, authService, dataService, formBuilder );
   }

  ngOnInit() {
    super.ngOnInit();
  }

  ngOnDestroy(){
    super.ngOnDestroy();
  }

  createOrderLogos():void{
    this.idLogoSrcOne= IMAGES.upArrowFirstD;
    this.idLogoSrcTwo= IMAGES.upArrowSecondD;
    this.dateLogoSrcOne= IMAGES.upArrowFirstD;
    this.dateLogoSrcTwo= IMAGES.upArrowSecondD;
    this.noteLogoSrcOne= IMAGES.upArrowFirstD;
    this.noteLogoSrcTwo= IMAGES.upArrowSecondD;
    }

    initForm(){
      this.addModelForm = this.formBuilder.group({
        'note': new FormControl(this.myComment.note, [Validators.required, Validators.min(0), Validators.max(10)]),
        'comment': new FormControl(this.myComment.comment, [Validators.pattern(/^[A-Za-z0-9ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØŒŠþÙÚÛÜÝŸàáâãäåæçèéêëìíîïðñòóôõöøœšÞùúûü:ýÿ=ß,\\\/'\"!\{\}\[\] ().\-\;?_]{0,255}$/)])
      });
    }

    onSubmitAddDataForm(){
      this.myComment.note = this.addModelForm.value['note'];
      this.myComment.comment = this.addModelForm.value['comment'];
      this.dataService.localAddData(this.myComment).subscribe(
        ()=>{
          this.snackbar.openSnackBar("Commentaire créé ou mis à jour", "OK", "success-snackbar");
        },error=>{
          this.snackbar.openSnackBar("Une erreur est survenue lors de la création/modification du commentaire", "OK", "error-snackbar");
        }
      )
    }

    onOrderById():void{
      if(this.idLogoSrcOne === IMAGES.upArrowFirstD){
        this.idLogoSrcOne = IMAGES.downArrowFirstD;
        this.idLogoSrcTwo = IMAGES.downArrowSecondD;
        this.dataService.changeOrder("DESC", "clotheNoteId");
      }else{
        this.idLogoSrcOne = IMAGES.upArrowFirstD;
        this.idLogoSrcTwo = IMAGES.upArrowSecondD;
        this.dataService.changeOrder("ASC", "clotheNoteId");
      }
      this.reloadDatas();
    }
  
    onOrderByNote():void{
      if(this.noteLogoSrcOne === IMAGES.upArrowFirstD){
        this.noteLogoSrcOne = IMAGES.downArrowFirstD;
        this.noteLogoSrcTwo = IMAGES.downArrowSecondD;
        this.dataService.changeOrder("DESC", "note");
      }else{
        this.noteLogoSrcOne = IMAGES.upArrowFirstD;
        this.noteLogoSrcTwo = IMAGES.upArrowSecondD;
        this.dataService.changeOrder("ASC", "note");
      }
      this.reloadDatas();
    }
  
    onOrderByDate():void{
      if(this.dateLogoSrcOne === IMAGES.upArrowFirstD){
        this.dateLogoSrcOne = IMAGES.downArrowFirstD;
        this.dateLogoSrcTwo = IMAGES.downArrowSecondD;
        this.dataService.changeOrder("DESC", "creationDate");
      }else{
        this.dateLogoSrcOne = IMAGES.upArrowFirstD;
        this.dateLogoSrcTwo = IMAGES.upArrowSecondD;
        this.dataService.changeOrder("ASC", "creationDate");
      }
      this.reloadDatas();
    }

    createDataFormGroup(outfitNoteModel: OutfitNoteModel){
      return this.formBuilder.group({});
    }

    onSubmitEditData(){}

}
