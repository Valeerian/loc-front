import { Component, OnInit, OnDestroy, Output, EventEmitter, Input } from '@angular/core';
import { GenericDropdownComponent } from '../generic-dropdown/generic-dropdown.component';
import { SizeSelectProviderService } from 'src/app/services/dataServices/subServices/size-select-provider.service';
import { MatSelectChange } from '@angular/material';
import { SizeModel } from 'src/app/models/dataModels/sizeModel';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-size-dropdown',
  templateUrl: './size-dropdown.component.html',
  styleUrls: ['./size-dropdown.component.css']
})
export class SizeDropdownComponent extends GenericDropdownComponent implements OnInit, OnDestroy {

  @Input() set setValues(sizeModel: SizeModel){
    if(sizeModel){
      this.form.controls.selectForm.setValue(sizeModel);
    }
  }

  constructor(protected formBuilder: FormBuilder) {
    super(formBuilder);
   }

  ngOnInit() {
  }
  ngOnDestroy(){
  }

  compareObjects(object1: SizeModel, object2: SizeModel){
    return object1.id && object2 && object1.id == object2.id;
  }


}
