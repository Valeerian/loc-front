import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManeGroupMemberRowComponent } from './mane-group-member-row.component';

describe('ManeGroupMemberRowComponent', () => {
  let component: ManeGroupMemberRowComponent;
  let fixture: ComponentFixture<ManeGroupMemberRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManeGroupMemberRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManeGroupMemberRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
