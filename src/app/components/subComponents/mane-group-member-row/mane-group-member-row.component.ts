import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { UserModel } from 'src/app/models/dataModels/userModel';
import { PossessionService } from 'src/app/services/dataServices/possession-service.service';
import { PossessionListModel } from 'src/app/models/dataListModels/possessionListModel';
import { SnackbarComponent } from '../../snackbar/snackbar.component';
import { PossessionModel } from 'src/app/models/dataModels/possessionModel';

@Component({
  selector: 'app-mane-group-member-row',
  templateUrl: './mane-group-member-row.component.html',
  styleUrls: ['./mane-group-member-row.component.css']
})
export class ManeGroupMemberRowComponent implements OnInit {

  

  

  
  possessionList: PossessionListModel = new PossessionListModel();
  user: UserModel = new UserModel();
  @Input() set setUser(UserModel: UserModel){
    this.user = UserModel;
    if(UserModel){
      if(UserModel.userId){
        this.possessionService.localGetHisPossessions(UserModel).subscribe(
          (list: PossessionListModel)=>{
            this.possessionList = list;
          },error=>{
            this.snackbar.openSnackBar("Une erreur est survenue lors de la récupération des vêtements de l'utilisateur " + UserModel.userName , "OK", "error-snackbar" );
          }
        )
      }
    }
  }
  possessionsThatBelongsToThisUser: PossessionModel[] = [];
  @Input() set setPossessions(possessionModels: PossessionModel[]){
    if(possessionModels){
      for(let possession of possessionModels){
        if(possession.user == this.user.userName){
          this.possessionsThatBelongsToThisUser.push(possession);
        }
      }  
    }
  }
  @Output() changePossessions = new EventEmitter<{propOne: PossessionModel[], propTwo: UserModel}>();

  constructor(private possessionService : PossessionService, private snackbar: SnackbarComponent) { }

  onChangePossessions(possessionModels: PossessionModel[]){

    this.changePossessions.emit({propOne: possessionModels, propTwo: this.user });
  }

  ngOnInit() {
  }

}
