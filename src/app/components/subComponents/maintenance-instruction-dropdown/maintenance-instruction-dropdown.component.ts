import { Component, OnInit, OnDestroy, Output, Input } from '@angular/core';
import { GenericDropdownComponent } from '../generic-dropdown/generic-dropdown.component';
import { EventEmitter } from '@angular/core';
import { MaintenanceInstructionModel } from 'src/app/models/dataModels/maintenanceInstruction';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatSelectChange } from '@angular/material';
import { MaintenanceInstructionSelectProviderService } from 'src/app/services/dataServices/subServices/maintenance-instruction-select-provider.service';

@Component({
  selector: 'app-maintenance-instruction-dropdown',
  templateUrl: './maintenance-instruction-dropdown.component.html',
  styleUrls: ['./maintenance-instruction-dropdown.component.css']
})
export class MaintenanceInstructionDropdownComponent extends GenericDropdownComponent implements OnInit, OnDestroy {

  @Input() set setValues(maintenanceInstructionModel: MaintenanceInstructionModel){
    if(maintenanceInstructionModel){
      this.form.controls.selectForm.setValue(maintenanceInstructionModel);
    }
  }
  
  constructor(protected formBuilder: FormBuilder) {
    super(formBuilder);
   }

  ngOnInit() {
  }
  ngOnDestroy(){
  }
  
  compareObjects(object1: MaintenanceInstructionModel, object2: MaintenanceInstructionModel){
    return object1.maintenanceInstructionId && object2 && object1.maintenanceInstructionId == object2.maintenanceInstructionId;
  }


}
