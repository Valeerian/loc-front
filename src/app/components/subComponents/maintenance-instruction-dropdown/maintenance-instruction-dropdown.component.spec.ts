import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaintenanceInstructionDropdownComponent } from './maintenance-instruction-dropdown.component';

describe('MaintenanceInstructionDropdownComponent', () => {
  let component: MaintenanceInstructionDropdownComponent;
  let fixture: ComponentFixture<MaintenanceInstructionDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaintenanceInstructionDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaintenanceInstructionDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
