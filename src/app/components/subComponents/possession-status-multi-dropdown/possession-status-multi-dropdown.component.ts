import { Component, OnInit, Input } from '@angular/core';
import { GenericDropdownComponent } from '../generic-dropdown/generic-dropdown.component';
import { PossessionStatusModel } from 'src/app/models/dataModels/possessionStatusModel';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-possession-status-multi-dropdown',
  templateUrl: './possession-status-multi-dropdown.component.html',
  styleUrls: ['./possession-status-multi-dropdown.component.css']
})
export class PossessionStatusMultiDropdownComponent extends GenericDropdownComponent implements OnInit {

  @Input() set setValues(possessionStatusModels: PossessionStatusModel[]){
    if(possessionStatusModels){
      this.form.controls.selectForm.setValue(possessionStatusModels);
    }
  }

  constructor(protected formBuilder: FormBuilder) {
    super(formBuilder);
   }

  ngOnInit() {
  }

  compareObjects(object1: PossessionStatusModel, object2: PossessionStatusModel){
    return object1.possessionStatusId && object2 && object1.possessionStatusId == object2.possessionStatusId;
  }
  

}
