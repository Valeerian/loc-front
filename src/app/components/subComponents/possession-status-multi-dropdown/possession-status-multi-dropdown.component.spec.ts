import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PossessionStatusMultiDropdownComponent } from './possession-status-multi-dropdown.component';

describe('PossessionStatusMultiDropdownComponent', () => {
  let component: PossessionStatusMultiDropdownComponent;
  let fixture: ComponentFixture<PossessionStatusMultiDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PossessionStatusMultiDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PossessionStatusMultiDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
