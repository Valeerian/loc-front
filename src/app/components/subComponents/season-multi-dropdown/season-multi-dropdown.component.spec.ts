import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeasonMultiDropdownComponent } from './season-multi-dropdown.component';

describe('SeasonMultiDropdownComponent', () => {
  let component: SeasonMultiDropdownComponent;
  let fixture: ComponentFixture<SeasonMultiDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeasonMultiDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeasonMultiDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
