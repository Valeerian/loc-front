import { Component, OnInit, OnDestroy, Output, EventEmitter, Input } from '@angular/core';
import { GenericDropdownComponent } from '../generic-dropdown/generic-dropdown.component';
import { SeasonSelectProviderService } from 'src/app/services/dataServices/subServices/season-select-provider-service.service';
import { MatSelectChange } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';
import { SeasonModel } from 'src/app/models/dataModels/seasonModel';


@Component({
  selector: 'app-season-multi-dropdown',
  templateUrl: './season-multi-dropdown.component.html',
  styleUrls: ['./season-multi-dropdown.component.css']
})
export class SeasonMultiDropdownComponent extends GenericDropdownComponent implements OnInit, OnDestroy {

  @Input() set setValues(seasonModels: SeasonModel[]){
    if(seasonModels){
      this.form.controls.selectForm.setValue(seasonModels);
    }
  }
  
  constructor(protected formBuilder: FormBuilder) { 
    super(formBuilder);
  }

  ngOnInit() {
  }

  ngOnDestroy(){
  }

  compareObjects(object1: SeasonModel, object2: SeasonModel){
    return object1.seasonId && object2 && object1.seasonId == object2.seasonId;
  }

}
