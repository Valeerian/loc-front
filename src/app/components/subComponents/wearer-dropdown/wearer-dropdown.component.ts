import { Component, OnInit, Input } from '@angular/core';
import { GenericDropdownComponent } from '../generic-dropdown/generic-dropdown.component';
import { UserModel } from 'src/app/models/dataModels/userModel';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-wearer-dropdown',
  templateUrl: './wearer-dropdown.component.html',
  styleUrls: ['./wearer-dropdown.component.css']
})
export class WearerDropdownComponent extends GenericDropdownComponent implements OnInit {

  @Input() set setValues(userModel: UserModel){
    if(userModel){
      this.form.controls.selectForm.setValue(userModel);
    }
  }

  constructor(protected formBuilder: FormBuilder) {
    super(formBuilder);
   }

  ngOnInit() {
  }

  compareObjects(object1: UserModel, object2: UserModel){
    return object1.userId && object2 && object1.userId == object2.userId;
  }

}
