import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WearerDropdownComponent } from './wearer-dropdown.component';

describe('WearerDropdownComponent', () => {
  let component: WearerDropdownComponent;
  let fixture: ComponentFixture<WearerDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WearerDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WearerDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
