import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManeStatusDropdownComponent } from './mane-status-dropdown.component';

describe('ManeStatusDropdownComponent', () => {
  let component: ManeStatusDropdownComponent;
  let fixture: ComponentFixture<ManeStatusDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManeStatusDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManeStatusDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
