import { Component, OnInit, Input } from '@angular/core';
import { ManeStatusModel } from 'src/app/models/dataModels/maneStatusModel';
import { GenericDropdownComponent } from '../generic-dropdown/generic-dropdown.component';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-mane-status-dropdown',
  templateUrl: './mane-status-dropdown.component.html',
  styleUrls: ['./mane-status-dropdown.component.css']
})
export class ManeStatusDropdownComponent extends GenericDropdownComponent implements OnInit {

  @Input() set setValue(maneStatusModel: ManeStatusModel){
    if(maneStatusModel){
      this.form.controls.selectForm.setValue(maneStatusModel);
    }
  }

  constructor(formBuilder: FormBuilder) {
    super(formBuilder);
   }

  ngOnInit() {
  }

  compareObjects(object1: ManeStatusModel, object2: ManeStatusModel){
    return object1.maneStatusId && object2 && object1.maneStatusId == object2.maneStatusId;
  }
}
