import { Component, OnInit, OnDestroy, Output, EventEmitter, Input } from '@angular/core';
import { SeasonSelectProviderService } from 'src/app/services/dataServices/subServices/season-select-provider-service.service';
import { GenericDropdownComponent } from '../generic-dropdown/generic-dropdown.component';
import { MatSelectChange } from '@angular/material';
import { SeasonModel } from 'src/app/models/dataModels/seasonModel';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-season-dropdown',
  templateUrl: './season-dropdown.component.html',
  styleUrls: ['./season-dropdown.component.css']
})
export class SeasonDropdownComponent extends GenericDropdownComponent implements OnInit, OnDestroy {

  @Input() set setValues(seasonModel: SeasonModel){
    if(seasonModel){
      if(seasonModel.seasonId){
        this.form.controls.selectForm.setValue(seasonModel);
      }
    }
  }
  constructor(protected formBuilder: FormBuilder) {
    super(formBuilder);
  }

  ngOnInit() {
   
  }
  ngOnDestroy(){
   
  }

  compareObjects(object1: SeasonModel, object2: SeasonModel){
    return object1.seasonId && object2 && object1.seasonId == object2.seasonId;
  }

}
