import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs';
import { GenericListModel } from 'src/app/models/dataListModels/genericListModel';
import { GenericSelectProviderService } from 'src/app/services/dataServices/subServices/generic-select-provider-service.service';
import { GenericDataProviderService } from 'src/app/services/dataServices/generic-data-provider.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatSelectChange } from '@angular/material';



@Component({
  selector: 'app-generic-dropdown',
  templateUrl: './generic-dropdown.component.html',
  styleUrls: ['./generic-dropdown.component.css']
})
export abstract class GenericDropdownComponent implements OnInit, OnDestroy {
  protected dataList: GenericListModel;
  protected form: FormGroup;
  protected categories: string[] = [];

  constructor(formBuilder: FormBuilder) {
    this.form = formBuilder.group({
      selectForm:[null]
    })
   }

  ngOnInit() {
  }

  @Output() selectStateChanged = new EventEmitter<any>();
  @Input() placeHolder: string = "PlaceHolder Par Défaut";
  @Input() isFilter: boolean = true;
  @Input() set resetFlag(resetFlag: boolean){
    this.reset();
  }

  @Input() mustBeSet: boolean = false;

  @Input() set setOptions(genericListModel: GenericListModel){
    this.dataList = genericListModel;
  }

  @Input() set setCategories(categories: string[]){
    this.categories = categories;
  }


  reset(){
    this.form.reset();
    this.selectStateChanged.emit(this.form.value['selectForm']);
  }

  onSelect(val: MatSelectChange){
    this.selectStateChanged.emit(val.value);
  }

  ngOnDestroy(){
  }

  

}
