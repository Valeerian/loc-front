import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClotheTypeDropdownComponent } from './clothe-type-dropdown.component';

describe('ClotheTypeDropdownComponent', () => {
  let component: ClotheTypeDropdownComponent;
  let fixture: ComponentFixture<ClotheTypeDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClotheTypeDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClotheTypeDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
