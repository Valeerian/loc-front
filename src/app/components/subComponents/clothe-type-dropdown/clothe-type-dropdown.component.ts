import { Component, OnInit, OnDestroy, Output, EventEmitter, Input } from '@angular/core';
import { GenericDropdownComponent } from '../generic-dropdown/generic-dropdown.component';
import { ClotheTypeSelectProviderService } from 'src/app/services/dataServices/subServices/clothe-type-select-provider.service';
import { MatSelectChange } from '@angular/material';
import { ClotheTypeModel } from 'src/app/models/dataModels/clotheTypeModel';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-clothe-type-dropdown',
  templateUrl: './clothe-type-dropdown.component.html',
  styleUrls: ['./clothe-type-dropdown.component.css']
})
export class ClotheTypeDropdownComponent extends GenericDropdownComponent implements OnInit, OnDestroy {

  @Input() set setValues(clotheTypeModel: ClotheTypeModel){
    if(clotheTypeModel){
      this.form.controls.selectForm.setValue(clotheTypeModel);
    }
  }

  constructor(protected formBuilder: FormBuilder) {
    super(formBuilder);
   }

  ngOnInit() {
  }
  ngOnDestroy(){
  }

  compareObjects(object1: ClotheTypeModel, object2: ClotheTypeModel){
    return object1.clotheTypeId && object2 && object1.clotheTypeId == object2.clotheTypeId;
  }

}
