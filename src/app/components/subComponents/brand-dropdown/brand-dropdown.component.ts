import { Component, OnInit, OnDestroy, EventEmitter, Output, Input } from '@angular/core';
import { GenericDropdownComponent } from '../generic-dropdown/generic-dropdown.component';
import { BrandSelectProviderService } from 'src/app/services/dataServices/subServices/brand-select-provider.service';
import { MatSelectChange } from '@angular/material';
import { BrandModel } from 'src/app/models/dataModels/brandModel';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-brand-dropdown',
  templateUrl: './brand-dropdown.component.html',
  styleUrls: ['./brand-dropdown.component.css']
})
export class BrandDropdownComponent extends GenericDropdownComponent implements OnInit, OnDestroy {


  @Input() set setValues(brandModel: BrandModel){
    if(brandModel){
      if(brandModel.brandId){
        this.form.controls.selectForm.setValue(brandModel);
      }
    }
  }
  constructor(formBuilder: FormBuilder) {
    super(formBuilder);
   }

  ngOnInit() {
  }
  ngOnDestroy(){
  }
  
  compareObjects(object1: BrandModel, object2: BrandModel){
    return object1.brandId && object2 && object1.brandId == object2.brandId;
  }


}
