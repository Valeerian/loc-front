import { Component, OnInit, OnDestroy, Output, EventEmitter, Input } from '@angular/core';
import { GenericDropdownComponent } from '../generic-dropdown/generic-dropdown.component';
import { BrandSelectProviderService } from 'src/app/services/dataServices/subServices/brand-select-provider.service';
import { MatSelectChange } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';
import { BrandModel } from 'src/app/models/dataModels/brandModel';
import { BrandListModel } from 'src/app/models/dataListModels/brandListModel';

@Component({
  selector: 'app-brand-multi-dropdown',
  templateUrl: './brand-multi-dropdown.component.html',
  styleUrls: ['./brand-multi-dropdown.component.css']
})
export class BrandMultiDropdownComponent extends GenericDropdownComponent implements OnInit, OnDestroy {


  @Input() set setValues(brandModels: BrandModel[]){
    if(brandModels){
      this.form.controls.selectForm.setValue(brandModels); 
    }
  }

  constructor(protected brandService: BrandSelectProviderService, formBuilder: FormBuilder) { 
    super(formBuilder);
  }


  ngOnInit() { }

  ngOnDestroy(){}

  compareObjects(object1: BrandModel, object2: BrandModel){
    return object1.brandId && object2 && object1.brandId == object2.brandId;
  }


}
