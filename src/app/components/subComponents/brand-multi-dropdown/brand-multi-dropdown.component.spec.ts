import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrandMultiDropdownComponent } from './brand-multi-dropdown.component';

describe('BrandMultiDropdownComponent', () => {
  let component: BrandMultiDropdownComponent;
  let fixture: ComponentFixture<BrandMultiDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrandMultiDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrandMultiDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
