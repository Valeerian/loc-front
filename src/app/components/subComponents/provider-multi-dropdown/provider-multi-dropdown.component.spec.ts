import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderMultiDropdownComponent } from './provider-multi-dropdown.component';

describe('ProviderMultiDropdownComponent', () => {
  let component: ProviderMultiDropdownComponent;
  let fixture: ComponentFixture<ProviderMultiDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderMultiDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderMultiDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
