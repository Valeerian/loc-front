import { Component, OnInit, OnDestroy, Output, EventEmitter, Input } from '@angular/core';
import { GenericDropdownComponent } from '../generic-dropdown/generic-dropdown.component';
import { ProviderSelectProviderService } from 'src/app/services/dataServices/subServices/provider-select-provider.service';
import { MatSelectChange } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ProviderModel } from 'src/app/models/dataModels/providerModel';

@Component({
  selector: 'app-provider-multi-dropdown',
  templateUrl: './provider-multi-dropdown.component.html',
  styleUrls: ['./provider-multi-dropdown.component.css']
})
export class ProviderMultiDropdownComponent extends GenericDropdownComponent implements OnInit, OnDestroy {

  @Input() set setValues(providerModels: ProviderModel[]){
    if(providerModels){ 
      this.form.controls.selectForm.setValue(providerModels);
    }
  }

  constructor(protected formBuilder: FormBuilder) { 
    super(formBuilder);
  }

  ngOnInit() {
  }

  ngOnDestroy(){
  }

  compareObjects(object1: ProviderModel, object2: ProviderModel){
    return object1.providerId && object2 && object1.providerId == object2.providerId;
  }

}
