import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultOutfitComponent } from './consult-outfit.component';

describe('ConsultOutfitComponent', () => {
  let component: ConsultOutfitComponent;
  let fixture: ComponentFixture<ConsultOutfitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultOutfitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultOutfitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
