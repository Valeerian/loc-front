import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { OutfitService } from 'src/app/services/dataServices/outfit.service';
import { FriendshipService } from 'src/app/services/dataServices/friendship.service';
import { SnackbarComponent } from '../../snackbar/snackbar.component';
import { OutfitListModel } from 'src/app/models/dataListModels/outfitListModel';
import { FriendshipListModel } from 'src/app/models/dataListModels/friendshipListModel';
import { QueryModel } from 'src/app/models/queryModel';
import { GenericComponent } from '../../generic/generic.component';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth-service';
import { OutfitOrders } from 'src/app/util/outfitOrders';
import { OrderModel } from 'src/app/models/dataModels/orderModel';
import { OrderListModel } from 'src/app/models/dataListModels/orderListModel';
import { MimicListModel } from 'src/app/models/dataListModels/mimicListModel';
import { MimicModel } from 'src/app/models/dataModels/mimicModel';
import { UserListModel } from 'src/app/models/dataListModels/userListModel';
import { ClotheService } from 'src/app/services/dataServices/clothe.service';
import { ClotheListModel } from 'src/app/models/dataListModels/clotheListModel';
import { ClotheModel } from 'src/app/models/dataModels/clotheModel';
import { UserModel } from 'src/app/models/dataModels/userModel';
import { OutfitModel } from 'src/app/models/dataModels/outfitModel';
import { SpecialOutfitService } from 'src/app/services/dataServices/special-outfit.service';

@Component({
  selector: 'app-consult-outfit',
  templateUrl: './consult-outfit.component.html',
  styleUrls: ['./consult-outfit.component.css']
})
export class ConsultOutfitComponent extends GenericComponent implements OnInit, OnDestroy {
 

  private outfitList: OutfitListModel = new OutfitListModel();
  private friendList: UserListModel = new UserListModel();
  private clotheList: ClotheListModel = new ClotheListModel();
  orders: OrderListModel = new OrderListModel();

  resetFlagFriend: boolean = false;
  resetFlagOrder: boolean = false;
  resetFlagclothe: boolean = false;
  resetFlagMinimumNote: boolean = false;
  

  friendCriteriaArray: number[] = [];
  clotheCriteriaArray: number[] = [];
  publicCriteria: boolean = false;
  minimumNoteCriteria: number;
  publik: MimicModel = new MimicModel("De mes amis", 4, "private");
  
  outfitNameFilter=new FormControl('',[Validators.pattern(/^[A-Za-z0-9ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØŒŠþÙÚÛÜÝŸàáâãäåæçèéêëìíîïðñòóôõöøœšÞùúûü:ýÿ=ß,\\\/'\"!\{\}\[\] ().\-\;?_]{0,60}$/)]);
  userNameFilter=new FormControl('',[Validators.pattern(/[a-z0-9-A-Z_-çàéèñùÇâêîôûøÇ ]{0,30}$/)]);
  publicMimicList: MimicListModel = new MimicListModel();

  @Output() showComments = new  EventEmitter<OutfitModel>()


  constructor(protected outfitService: SpecialOutfitService, protected authService: AuthService, protected formBuilder: FormBuilder, private friendService: FriendshipService, protected snackbar: SnackbarComponent, private clotheService: ClotheService) {
    super(snackbar, authService, outfitService, formBuilder);
   }

  ngOnInit() {
    super.ngOnInit();
    this.loadDatasToFillSelects();
  }

  submitPublicFilter(){ //Nom tenue; nom utilisateur, note minimale, clotheId
    this.dataService.removeCriterias();

    if(this.clotheCriteriaArray.length > 0){
      for(let id of this.clotheCriteriaArray){
        this.dataService.addCriteria("clotheId",":", ""+id);
      }
    }
    if(this.minimumNoteCriteria > -1){
      this.dataService.addCriteria("creationDate",":", ""+this.minimumNoteCriteria);
    }
    if(this.outfitNameFilter.value){
      this.dataService.addCriteria('name', "~", this.outfitNameFilter.value);
    }
    if(this.userNameFilter.value){
      this.dataService.addCriteria('creator', "~", this.outfitNameFilter.value);
    }
    this.dataService.addCriteria("publik",":", ""+this.publicCriteria);
    this.reloadDatas();
  }

  submitPrivateFilter(){ // Ami, nom tenue, 
    this.dataService.removeCriterias();
    if(this.friendCriteriaArray.length>0){
      for(let friend of this.friendCriteriaArray){
        this.dataService.addCriteria("outfitId", ":", ""+friend);
      }
    }
    if(this.outfitNameFilter.value){
      this.dataService.addCriteria('name', "~", this.outfitNameFilter.value);
    }
    this.dataService.addCriteria("isPublik",":", ""+this.publicCriteria);
    this.reloadDatas();
  }
  resetFilter(){
    this.dataService.removeCriterias();
    this.dataService.addCriteria('isPublik',":", ""+this.publicCriteria);
    this.outfitNameFilter.setValue('');
    this.userNameFilter.setValue('');
    this.resetFlagFriend= this.switchBoolean(this.resetFlagFriend);
    this.resetFlagOrder = this.switchBoolean(this.resetFlagOrder);
    this.resetFlagclothe = this.switchBoolean(this.resetFlagclothe);
    this.resetFlagMinimumNote = this.switchBoolean(this.resetFlagMinimumNote);
    this.dataService.changeOrder(OutfitOrders.idAsc.orderType,OutfitOrders.idAsc.orderBy);
    this.reloadDatas();
  }
  switchBoolean(boo : boolean): boolean{
    if(boo == true){
      return false;
    }
    else{
      return true;
    }
  }

  onChangeOrder(orderModel: OrderModel){
    if(orderModel){
      this.dataService.changeOrder(orderModel.orderType, orderModel.orderBy);
    }else{
      this.dataService.changeOrder(OutfitOrders.idAsc.orderType,OutfitOrders.idAsc.orderBy);
    }
  }

  loadDatasToFillSelects():void{
    this.orders.list = [];
    this.orders.list.push(OutfitOrders.idAsc);
    this.orders.list.push(OutfitOrders.idDesc);
    this.orders.list.push(OutfitOrders.nameAsc);
    this.orders.list.push(OutfitOrders.nameDesc);
    this.orders.list.push(OutfitOrders.creationDateAsc);
    this.orders.list.push(OutfitOrders.creationDateDesc);
    this.orders.list.push(OutfitOrders.dueDateAsc);
    this.orders.list.push(OutfitOrders.dueDateDesc);
    this.orders.list.push(OutfitOrders.isFavoriteAsc);
    this.orders.list.push(OutfitOrders.isFavoriteDesc);
    this.orders.list.push(OutfitOrders.isPublicAsc);
    this.orders.list.push(OutfitOrders.isPublicDesc);
    this.orders.list.push(OutfitOrders.userAsc);
    this.orders.list.push(OutfitOrders.userDesc);
    let publicTrue: MimicModel = new MimicModel("Publiques", 3, "public");
    let publicfalse: MimicModel = new MimicModel("De mes amis", 4, "private");
    this.publicMimicList.list.push( publicTrue, publicfalse );
    this.publicMimicList.numberOfItems=2;
    this.friendService.localGetMyFriends().subscribe(
      (list: UserListModel)=>{
        this.friendList = list;
      },error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la récupération de vos amis: " +error.error ,"OK","error-snackbar")
      }
    );
    this.clotheService.localGetClotheForPossession().subscribe(
      (list: ClotheListModel)=>{
        this.clotheList = list;
      },error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la récupération de vos vêtements: " +error.error ,"OK","error-snackbar")
      }
    );
  }

  ngOnDestroy(){
    super.ngOnDestroy();
  }

  onChangeClotheFilter(clotheModels: ClotheModel[]){
    this.clotheCriteriaArray = [];
    if(clotheModels && clotheModels.length>0){
      for(let clothe of clotheModels){
        this.clotheCriteriaArray.push(clothe.clotheId);
      }
    }
  }

  onChangeFriendFilter(userModels: UserModel[]){
    this.friendCriteriaArray = [];
    if(userModels && userModels.length>0){
      for(let user of userModels){
        this.friendCriteriaArray.push(user.userId);
      }
    }
  }

  onChangePublicFilter(mimicModel: MimicModel){
    if(mimicModel){
      if(mimicModel.value === "public"){
        this.publicCriteria = true;
        this.publik= mimicModel;
      }
      if(mimicModel.value === "private"){
        this.publicCriteria = false;
        this.publik= mimicModel;
      }
    }
  }

  onChangeMinimumNoteFilter(minimalNote: number){
    if(!minimalNote){
      this.minimumNoteCriteria = undefined;
    }
    else{
      this.minimumNoteCriteria = minimalNote;
    }
  }

  onShowComments(outfitModel: OutfitModel){
    this.showComments.emit(outfitModel);
  }

  protected createDataFormGroup(model: any): FormGroup {
    return this.formBuilder.group({

    });
  }
  protected onSubmitEditData(index: number): void {
   // throw new Error("Method not implemented.");
  }
  protected createOrderLogos(): void {
   // throw new Error("Method not implemented.");
  }
  protected initForm(): void {
   // throw new Error("Method not implemented.");
  }
  protected onSubmitAddDataForm(): void {
   // throw new Error("Method not implemented.");
  }

}
