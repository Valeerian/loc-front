import { Component, OnInit, OnDestroy, Output, EventEmitter, Input } from '@angular/core';
import { GenericDropdownComponent } from '../generic-dropdown/generic-dropdown.component';
import { SexSelectProviderService } from 'src/app/services/dataServices/subServices/sex-select-provider.service';
import { MatSelectChange } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs';
import { SexModel } from 'src/app/models/dataModels/sexModel';

@Component({
  selector: 'app-sex-multi-dropdown',
  templateUrl: './sex-multi-dropdown.component.html',
  styleUrls: ['./sex-multi-dropdown.component.css']
})
export class SexMultiDropdownComponent extends GenericDropdownComponent implements OnInit, OnDestroy {


  @Input() set setValues(sexModels: SexModel[]){
    if(sexModels){
      this.form.controls.selectForm.setValue(sexModels);
    }
  }

  constructor(protected formBuilder: FormBuilder) { 
    super(formBuilder);
  }

  ngOnInit() {
  }

  ngOnDestroy(){
  }


  compareObjects(object1: SexModel, object2: SexModel){
    return object1.sexId && object2 && object1.sexId == object2.sexId;
  }

}
