import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SexMultiDropdownComponent } from './sex-multi-dropdown.component';

describe('SexMultiDropdownComponent', () => {
  let component: SexMultiDropdownComponent;
  let fixture: ComponentFixture<SexMultiDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SexMultiDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SexMultiDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
