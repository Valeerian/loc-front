import { Component, OnInit, OnDestroy, Output, EventEmitter, Input } from '@angular/core';
import { GenericDropdownComponent } from '../generic-dropdown/generic-dropdown.component';
import { ThemeSelectProviderService } from 'src/app/services/dataServices/subServices/theme-select-provider.service';
import { MatSelectChange } from '@angular/material';
import { ThemeModel } from 'src/app/models/dataModels/themeModel';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-theme-dropdown',
  templateUrl: './theme-dropdown.component.html',
  styleUrls: ['./theme-dropdown.component.css']
})
export class ThemeDropdownComponent extends GenericDropdownComponent implements OnInit, OnDestroy {

  @Input() set setValues(themeModel: ThemeModel){
    if(themeModel){
      this.form.controls.selectForm.setValue(themeModel);
    }
  }
  constructor(protected formBuilder: FormBuilder) {
    super(formBuilder);
   }

  ngOnInit() {
  }
  ngOnDestroy(){
  }

  compareObjects(object1: ThemeModel, object2: ThemeModel){
    return object1.themeId && object2 && object1.themeId == object2.themeId;
  }

}
