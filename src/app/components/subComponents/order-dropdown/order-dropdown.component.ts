import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { OrderListModel } from 'src/app/models/dataListModels/orderListModel';
import { OrderModel } from 'src/app/models/dataModels/orderModel';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatSelectChange } from '@angular/material';

@Component({
  selector: 'app-order-dropdown',
  templateUrl: './order-dropdown.component.html',
  styleUrls: ['./order-dropdown.component.css']
})
export class OrderDropdownComponent implements OnInit {

  @Input() orders: OrderListModel;
  @Input() placeHolder: string = "PlaceHolder par défaut"; 
  @Output() orderChanged = new EventEmitter<OrderModel>();
  @Input() set resetFlag(resetFlag: boolean){
    this.reset();
  }
  form: FormGroup;
  constructor(private formBuilder: FormBuilder) {
    this.form = formBuilder.group({
      selectForm: [null]
    });
   }

  ngOnInit() {
  }

  onSelect(val: MatSelectChange){
    this.orderChanged.emit(val.value)
  }

  reset(){
    this.form.reset();
  }

}
