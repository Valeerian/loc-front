import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderDropdownComponent } from './order-dropdown.component';

describe('OrderDropdownComponent', () => {
  let component: OrderDropdownComponent;
  let fixture: ComponentFixture<OrderDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
