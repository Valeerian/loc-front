import { Component, OnInit, OnDestroy, Output, EventEmitter, Input } from '@angular/core';
import { GenericDropdownComponent } from '../generic-dropdown/generic-dropdown.component';
import { ColorSelectProviderService } from 'src/app/services/dataServices/subServices/color-select-provider.service';
import { MatSelectChange } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ColorModel } from 'src/app/models/dataModels/colorModel';

@Component({
  selector: 'app-color-multi-dropdown',
  templateUrl: './color-multi-dropdown.component.html',
  styleUrls: ['./color-multi-dropdown.component.css']
})
export class ColorMultiDropdownComponent extends GenericDropdownComponent implements OnInit, OnDestroy {


  @Input() set setValues(colordModels: ColorModel[]){
    if(colordModels){
      this.form.controls.selectForm.setValue(colordModels);
    }
  }

  constructor(protected formBuilder: FormBuilder) { 
    super(formBuilder);
  }

  ngOnInit() {
    
  }

  ngOnDestroy(){
    
  }

  compareObjects(object1: ColorModel, object2: ColorModel){
    return object1.colorId && object2 && object1.colorId == object2.colorId;
  }

}
