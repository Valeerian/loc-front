import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColorMultiDropdownComponent } from './color-multi-dropdown.component';

describe('ColorMultiDropdownComponent', () => {
  let component: ColorMultiDropdownComponent;
  let fixture: ComponentFixture<ColorMultiDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColorMultiDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColorMultiDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
