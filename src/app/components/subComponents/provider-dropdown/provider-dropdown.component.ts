import { Component, OnInit, OnDestroy, Output, EventEmitter, Input } from '@angular/core';
import { GenericDropdownComponent } from '../generic-dropdown/generic-dropdown.component';
import { ProviderSelectProviderService } from 'src/app/services/dataServices/subServices/provider-select-provider.service';
import { MatSelectChange } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ProviderModel } from 'src/app/models/dataModels/providerModel';

@Component({
  selector: 'app-provider-dropdown',
  templateUrl: './provider-dropdown.component.html',
  styleUrls: ['./provider-dropdown.component.css']
})
export class ProviderDropdownComponent extends GenericDropdownComponent implements OnInit, OnDestroy {

  @Input() set setValues(providerModel:ProviderModel){
    if(providerModel){
      if(providerModel.provider){
        this.form.controls.selectForm.setValue(providerModel);
      }
    }
   }

  constructor(protected formBuilder: FormBuilder) {
    super(formBuilder);
  }
   

  ngOnInit() {
    
  }
  ngOnDestroy(){
    
  }

  compareObjects(object1: ProviderModel, object2: ProviderModel){
    return object1.providerId && object2 && object1.providerId == object2.providerId;
  }
  
}
