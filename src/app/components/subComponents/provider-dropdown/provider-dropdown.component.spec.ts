import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProviderDropdownComponent } from './provider-dropdown.component';

describe('ProviderDropdownComponent', () => {
  let component: ProviderDropdownComponent;
  let fixture: ComponentFixture<ProviderDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProviderDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProviderDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
