import { Component, OnInit, OnDestroy, Output, EventEmitter, Input } from '@angular/core';
import { GenericDropdownComponent } from '../generic-dropdown/generic-dropdown.component';
import { SexSelectProviderService } from 'src/app/services/dataServices/subServices/sex-select-provider.service';
import { MatSelectChange } from '@angular/material';
import { SexModel } from 'src/app/models/dataModels/sexModel';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-sex-dropdown',
  templateUrl: './sex-dropdown.component.html',
  styleUrls: ['./sex-dropdown.component.css']
})
export class SexDropdownComponent extends GenericDropdownComponent implements OnInit, OnDestroy {


  @Input() set setValues(sexModel: SexModel){
    if(sexModel){
      if(sexModel.sexId){
        this.form.controls.selectForm.setValue(sexModel);
      }
    }
  }

  constructor(protected formbuilder: FormBuilder) {
    super(formbuilder);
   }

  ngOnInit() {
  }
  ngOnDestroy(){
  }

  compareObjects(object1: SexModel, object2: SexModel){
    return object1.sexId && object2 && object1.sexId == object2.sexId;
  }

}
