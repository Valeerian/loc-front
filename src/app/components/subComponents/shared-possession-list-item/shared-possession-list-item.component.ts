import { Component, OnInit, Input } from '@angular/core';
import { PossessionModel } from 'src/app/models/dataModels/possessionModel';

@Component({
  selector: 'app-shared-possession-list-item',
  templateUrl: './shared-possession-list-item.component.html',
  styleUrls: ['./shared-possession-list-item.component.css']
})
export class SharedPossessionListItemComponent implements OnInit {

  possessionModel: PossessionModel = new PossessionModel();
  @Input() set setPossessionModel(possessionModel: PossessionModel){
    this.possessionModel = possessionModel;
  }

  constructor() { }

  ngOnInit() {
  }

}
