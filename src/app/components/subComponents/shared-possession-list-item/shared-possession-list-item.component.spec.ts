import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharedPossessionListItemComponent } from './shared-possession-list-item.component';

describe('SharedPossessionListItemComponent', () => {
  let component: SharedPossessionListItemComponent;
  let fixture: ComponentFixture<SharedPossessionListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SharedPossessionListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharedPossessionListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
