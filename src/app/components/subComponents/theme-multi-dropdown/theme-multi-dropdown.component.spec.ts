import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThemeMultiDropdownComponent } from './theme-multi-dropdown.component';

describe('ThemeMultiDropdownComponent', () => {
  let component: ThemeMultiDropdownComponent;
  let fixture: ComponentFixture<ThemeMultiDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThemeMultiDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThemeMultiDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
