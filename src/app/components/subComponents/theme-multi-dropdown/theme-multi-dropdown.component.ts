import { Component, OnInit, OnDestroy, Output, EventEmitter, Input } from '@angular/core';
import { GenericDropdownComponent } from '../generic-dropdown/generic-dropdown.component';
import { ThemeSelectProviderService } from 'src/app/services/dataServices/subServices/theme-select-provider.service';
import { MatSelectChange } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ThemeModel } from 'src/app/models/dataModels/themeModel';

@Component({
  selector: 'app-theme-multi-dropdown',
  templateUrl: './theme-multi-dropdown.component.html',
  styleUrls: ['./theme-multi-dropdown.component.css']
})
export class ThemeMultiDropdownComponent extends GenericDropdownComponent implements OnInit, OnDestroy {

  @Input() set setValues(themeModels: ThemeModel[]){
    if(themeModels){
      this.form.controls.selectForm.setValue(themeModels);
    }
  }

  constructor(protected formBuilder:FormBuilder) { 
    super(formBuilder);
  }

  ngOnInit() {
  }

  ngOnDestroy(){
  }

  compareObjects(object1: ThemeModel, object2: ThemeModel){
    return object1.themeId && object2 && object1.themeId == object2.themeId;
  }

}
