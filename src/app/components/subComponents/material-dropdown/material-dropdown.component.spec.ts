import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaterialDropdownComponent } from './material-dropdown.component';

describe('MaterialDropdownComponent', () => {
  let component: MaterialDropdownComponent;
  let fixture: ComponentFixture<MaterialDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaterialDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaterialDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
