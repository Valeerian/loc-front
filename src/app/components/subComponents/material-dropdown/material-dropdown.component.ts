import { Component, OnInit, OnDestroy, Output, EventEmitter, Input } from '@angular/core';
import { GenericDropdownComponent } from '../generic-dropdown/generic-dropdown.component';
import { MaterialSelectProviderService } from 'src/app/services/dataServices/subServices/material-select-provider.service';
import { MatSelectChange } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MaterialModel } from 'src/app/models/dataModels/materialModel';

@Component({
  selector: 'app-material-dropdown',
  templateUrl: './material-dropdown.component.html',
  styleUrls: ['./material-dropdown.component.css']
})
export class MaterialDropdownComponent extends GenericDropdownComponent implements OnInit, OnDestroy {

  @Input() set setValues(materialModel: MaterialModel){
    if(materialModel){
      this.form.controls.selectForm.setValue(materialModel);
    }
  }

  constructor(protected formBuilder: FormBuilder) {
    super(formBuilder);
   }

  ngOnInit() {
  }
  ngOnDestroy(){
  }

  compareObjects(object1: MaterialModel, object2: MaterialModel){
    return object1.materialId && object2 && object1.materialId == object2.materialId;
  }

}
