import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddToPossessionFrameComponent } from './add-to-possession-frame.component';

describe('AddToPossessionFrameComponent', () => {
  let component: AddToPossessionFrameComponent;
  let fixture: ComponentFixture<AddToPossessionFrameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddToPossessionFrameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddToPossessionFrameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
