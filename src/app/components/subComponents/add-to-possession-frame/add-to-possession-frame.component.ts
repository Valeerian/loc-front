import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SizeModel } from 'src/app/models/dataModels/sizeModel';
import { FormGroup, FormBuilder, FormControl, Validators, FormArray } from '@angular/forms';
import { MatSelectChange } from '@angular/material';
import { PossessionModel } from 'src/app/models/dataModels/possessionModel';

@Component({
  selector: 'app-add-to-possession-frame',
  templateUrl: './add-to-possession-frame.component.html',
  styleUrls: ['./add-to-possession-frame.component.css']
})
export class AddToPossessionFrameComponent implements OnInit {

  numbers;
  numberOfFrameToDisplay: number = 0;
  quantities: number[]= [1,2,3,4,5,6,7,8,9,10];
  availableSizes: SizeModel[] = [];
  @Input() set numberOfFrames(flag: boolean){
    this.numberOfFrameToDisplay++;
    this.numbers = Array(this.numberOfFrameToDisplay).fill(0).map((x,i)=>i);// Transforme le nbre d'éléments que je veux afficher en un tableau, ce qui le rend itérable pour le ngFor
    if(!this.formArray){//Si le component vient d'être initialisé, formArray sera undefined et cela risque de poser problème 
      this.formArray = this.formBuilder.array([]);
    }
    this.formArray.push(this.createFormGroup())
    
  }
  @Output() userClickedOnCancel = new EventEmitter<boolean>();
  @Output() submitPossessions = new EventEmitter<PossessionModel[]>();

  @Input() set setAvailableSizes(sizeModels: SizeModel[]){
    this.formArray.reset();
    this.availableSizes = sizeModels;
  }
  form: FormGroup;
  formArray: FormArray;
  constructor(private formBuilder: FormBuilder) { 

  }

  createFormGroup(){
    return this.formBuilder.group({
      'quantity': new FormControl(1, [Validators.required, Validators.min(1)]),
      'size': new FormControl('', [Validators.required, Validators.min(1)])
    })
  }

  onDeleteRow(i: number){
    this.formArray.removeAt(i);
  }

  ngOnInit() {
  }

  /**
   * Fonction appelée lorsque l'utilisateur soumet une liste de possessions à ajouter, faut renvoyer ça au parent
   */
  onSubmitAddPossessions(){
    let possessionModels : PossessionModel[] = [];
    for( let i = 0; i < this.formArray.controls.length; i++){
      
      for(let j = 0; j < this.formArray.controls[i].value['quantity']; j++){
        let possMod : PossessionModel = new PossessionModel;
        possMod.sizeModel =this.formArray.controls[i].value['size'];
        possMod.possessionId = 0;
        possessionModels.push(possMod)
      }
    }
    if(possessionModels.length > 0){
      this.submitPossessions.emit(possessionModels);
    }
  }

  compareObjects(object1: SizeModel, object2: SizeModel){
    return object1.id && object2 && object1.id == object2.id;
  }

  onCancelAddPossessions(){
    this.userClickedOnCancel.emit(false);

   // console.log("Cancel, nbre de controles: " +this.formArray.controls.length  );
   // for(let i = this.formArray.controls.length ; i >= 0; i--){
   //   console.log("boucle, nbre de controles: " +this.formArray.controls.length + "i vaut " + i  );
   //   this.formArray.removeAt(i);
    //}
  }

}
