import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { SnackbarComponent } from '../snackbar/snackbar.component';
import { AuthService } from 'src/app/services/auth-service';
import { TokenStorageService } from 'src/app/services/token-storage.service';
import { Router } from '@angular/router';
import { Subscription, from } from 'rxjs';
import { SignupModel } from 'src/app/models/SignupModel';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { MatTooltipModule } from'@angular/material/tooltip';
import * as jwt_decode from 'jwt-decode';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit, OnDestroy {
  
  private signupModel: SignupModel;
  isLoggedIn: boolean;
  isLoggedInSubscription: Subscription;

  signupForm: FormGroup;

  constructor(private snackbar: SnackbarComponent, private authService: AuthService, private token: TokenStorageService, private router: Router, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.isLoggedIn = this.authService.getAuthStatus();
    this.signupModel = new SignupModel('','','','','','','',false);
    this.isLoggedInSubscription = this.authService.isLoggedInSubject.subscribe(
      (isLogged: boolean)=>{
        this.isLoggedIn = isLogged;
      }
    );
    this.initForm();
  }

  initForm(){
    this.signupForm = this.formBuilder.group({
      'userName': new FormControl(this.signupModel.userName, [Validators.required, Validators.minLength(4), Validators.maxLength(30)]),
      'mail': new FormControl(this.signupModel.mail, [Validators.required, Validators.email]),
      'mailConfirmation' :new FormControl(this.signupModel.mailConfirmation, [Validators.required, Validators.email]),
      'firstName':new FormControl(this.signupModel.firstName, [Validators.required, Validators.minLength(2), Validators.maxLength(30)]),
      'lastName':new FormControl(this.signupModel.lastName, [Validators.required, Validators.minLength(2), Validators.maxLength(30)]), 
      'password':new FormControl(this.signupModel.password, [Validators.required, Validators.minLength(6), Validators.maxLength(50)]),
      'passwordConfirmation':new FormControl(this.signupModel.passwordConfirmation, [Validators.required, Validators.minLength(6), Validators.maxLength(50)]),
      'agreedDataPolicy': new FormControl(this.signupModel.agreedDataPolicy, [Validators.requiredTrue])
    });
  }

  onSubmitForm(){
    this.signupModel.userName = this.signupForm.value['userName']
    this.signupModel.mail =  this.signupForm.value['mail']
    this.signupModel.mailConfirmation = this.signupForm.value['mailConfirmation']
    this.signupModel.firstName = this.signupForm.value['firstName']
    this.signupModel.lastName = this.signupForm.value['lastName']
    this.signupModel.password = this.signupForm.value['password']
    this.signupModel.passwordConfirmation = this.signupForm.value['passwordConfirmation']
    this.signupModel.agreedDataPolicy= this.signupForm.value['agreedDataPolicy'];  
    this.authService.attemptLocalSignup(this.signupModel).subscribe(
      answer => {
        this.token.saveFirstName(answer.firstName);
        this.token.saveLastName(answer.lastName);
        this.token.saveSex(this.setExplicitSex(answer.sex));
        this.token.saveToken(answer.jwtToken);
        this.token.saveUserName(answer.userName);
        this.token.saveMailAddress(answer.mail);
        this.token.saveRoles(answer.roles);
        var decodedToken = jwt_decode(answer.jwtToken);
        this.token.saveExpiration(decodedToken['exp']);
        this.token.saveIssuedAt(decodedToken['iat']);
        this.authService.login();
        this.authService.setRoles(answer.roles);
        this.router.navigate(['/home']);
        this.snackbar.openSnackBar('Inscription réussie, bienvenue, ' + answer.userName + '.', 'OK', 'success-snackbar');
      }, error =>{
        this.snackbar.openSnackBar('Inscription échouée: ' + error.error, 'OK', 'error-snackbar')
      }
    )
  }

  ngOnDestroy(){
    this.isLoggedInSubscription.unsubscribe();
  }

  setExplicitSex(sex? : Boolean): string{
    if(sex === null){
      return "Non communiqué"
    }
    if(sex === false){
      return "Homme";
    }
    else{
      return "Femme";
    }
  }
}
