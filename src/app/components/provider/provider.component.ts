import { Component, OnInit, OnDestroy } from '@angular/core';
import { GenericComponent } from '../generic/generic.component';
import { SnackbarComponent } from '../snackbar/snackbar.component';
import { AuthService } from 'src/app/services/auth-service';
import { ProviderService } from 'src/app/services/dataServices/provider.service';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { IMAGES } from 'src/app/util/images';
import { ProviderModel } from 'src/app/models/dataModels/providerModel';

@Component({
  selector: 'app-provider',
  templateUrl: './provider.component.html',
  styleUrls: ['./provider.component.css']
})
export class ProviderComponent extends GenericComponent implements OnInit, OnDestroy {


  idLogoSrcOne = '';
  idLogoSrcTwo = '';
  providerLogoSrcOne = '';
  providerLogoSrcTwo = '';
  websiteLogoSrcOne = '';
  websiteLogoSrcTwo = '';

  constructor(protected snackbar: SnackbarComponent, protected authService: AuthService, protected providerService: ProviderService, formBuilder: FormBuilder) {
    super(snackbar, authService, providerService, formBuilder);
   }

  ngOnInit() {
    super.ngOnInit();
  }

  ngOnDestroy(){
    super.ngOnDestroy();
  }

  createOrderLogos():void{
    this.idLogoSrcOne = IMAGES.upArrowFirstD;
    this.idLogoSrcTwo = IMAGES.upArrowSecondD
    this.providerLogoSrcOne = IMAGES.upArrowFirstD;
    this.providerLogoSrcTwo = IMAGES.upArrowSecondD;
    this.websiteLogoSrcOne = IMAGES.upArrowFirstD;
    this.websiteLogoSrcTwo = IMAGES.upArrowSecondD;
  }

  initForm():void{
    this.addModelForm = this.formBuilder.group({
      'provider': new FormControl('', [Validators.required, Validators.pattern("^[A-Z0-9ÈÉ][A-Za-z0-9 -'&éèêâôû]{2,30}$")]),
      'website': new FormControl('', [Validators.pattern("^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$")])
    });
  }

  onSubmitAddDataForm():void {
    let newProvider = new ProviderModel();
    newProvider.provider = this.addModelForm.value['provider'];
    newProvider.website = this.addModelForm.value['website'];
    this.providerService.localAddData(newProvider).subscribe(
      ()=>{
        this.snackbar.openSnackBar('Fournisseur ajouté', 'OK', 'success-snackbar');
        this.reloadDatas();
        this.initForm();
      }, error=>{
        this.snackbar.openSnackBar('Une erreur est survenue lors de l\'ajout du fournisseur' + error.error, 'OK', 'error-snackbar');
      }
    );
  }

  onOrderById():void{
    if(this.idLogoSrcOne === IMAGES.upArrowFirstD){
      this.idLogoSrcOne = IMAGES.downArrowFirstD;
      this.idLogoSrcTwo = IMAGES.downArrowSecondD;
      this.providerService.changeOrder("DESC", "providerId");
    }else{
      this.idLogoSrcOne = IMAGES.upArrowFirstD;
      this.idLogoSrcTwo = IMAGES.upArrowSecondD;
      this.providerService.changeOrder("ASC", "providerId");
    }
    this.reloadDatas();
  }

  onOrderByProvider():void{
    if(this.providerLogoSrcOne === IMAGES.upArrowFirstD){
      this.providerLogoSrcOne = IMAGES.downArrowFirstD;
      this.providerLogoSrcTwo = IMAGES.downArrowSecondD;
      this.providerService.changeOrder("DESC", "name");
    }else{
      this.providerLogoSrcOne = IMAGES.upArrowFirstD;
      this.providerLogoSrcTwo = IMAGES.upArrowSecondD;
      this.providerService.changeOrder("ASC", "name");
    }
    this.reloadDatas();
  }

  onOrderByWebsite():void{
    if(this.websiteLogoSrcOne === IMAGES.upArrowFirstD){
      this.websiteLogoSrcOne = IMAGES.downArrowFirstD;
      this.websiteLogoSrcTwo = IMAGES.downArrowSecondD;
      this.providerService.changeOrder("DESC", "webSite");
    }else{
      this.websiteLogoSrcOne = IMAGES.upArrowFirstD;
      this.websiteLogoSrcTwo = IMAGES.upArrowSecondD;
      this.providerService.changeOrder("ASC", "webSite");
    }
    this.reloadDatas();
  }

  createDataFormGroup(providerModel: ProviderModel): FormGroup{
    return this.formBuilder.group({
      'id': new FormControl(providerModel.providerId, [Validators.required, Validators.min(1)]),
      'provider': new FormControl(providerModel.provider, [Validators.required, Validators.pattern("^[A-Z0-9ÈÉ][A-Za-z0-9 -'&éèêâôû]{2,30}$")]),
      'website': new FormControl(providerModel.website, [Validators.pattern("^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$")])
    });
  }

  onSubmitEditData(index: number){
    let providerModelToUpdate = new ProviderModel();
    providerModelToUpdate.providerId = this.formArray.controls[index].value['id'];
    providerModelToUpdate.provider = this.formArray.controls[index].value['provider'];
    providerModelToUpdate.website = this.formArray.controls[index].value['website'];
    this.providerService.localEditData(providerModelToUpdate).subscribe(
      ()=>{
        this.snackbar.openSnackBar('Fournisseur modifié!', 'OK', 'success-snackbar');
        this.reloadDatas();
      }, error=>{
        this.snackbar.openSnackBar('Une erreur est survenue lors de la modification du fournisseur: ' +error.error, 'OK', 'error-snackbar');
      }
    );
  }


}
