import { Component, OnInit, OnDestroy } from '@angular/core';
import { GenericComponent } from '../generic/generic.component';
import { SnackbarComponent } from '../snackbar/snackbar.component';
import { AuthService } from 'src/app/services/auth-service';
import { PictureService } from 'src/app/services/dataServices/picture.service';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { IMAGES } from 'src/app/util/images';
import { PictureModel } from 'src/app/models/dataModels/pictureModel';

@Component({
  selector: 'app-picture',
  templateUrl: './picture.component.html',
  styleUrls: ['./picture.component.css']
})
export class PictureComponent extends GenericComponent implements OnInit, OnDestroy {

  idLogoSrcOne : string = '';
  idLogoSrcTwo : string = '';
  nameLogoSrcOne: string ='';
  nameLogoSrcTwo: string ='';
  urlLogoSrcOne: string = '';
  urlLogoSrcTwo: string = '';

  constructor(protected snackbar: SnackbarComponent, protected authService: AuthService, protected pictureService: PictureService, protected formBuilder: FormBuilder) {
    super(snackbar, authService, pictureService, formBuilder); 
  }

  ngOnInit() {
    super.ngOnInit();
  }

  ngOnDestroy(){
    super.ngOnDestroy();
  }


  createOrderLogos():void{
    this.idLogoSrcOne = IMAGES.upArrowFirstD;
    this.idLogoSrcTwo = IMAGES.upArrowSecondD;
    this.nameLogoSrcOne = IMAGES.upArrowFirstD;
    this.nameLogoSrcTwo = IMAGES.upArrowSecondD;
    this.urlLogoSrcOne = IMAGES.upArrowFirstD;
    this.urlLogoSrcTwo = IMAGES.upArrowSecondD;
  }

  initForm(){
    this.addModelForm = this.formBuilder.group({
      'name': new FormControl('', [Validators.required, Validators.pattern("^[a-zA-z0-9'-_ÀÁÂÄÃÅÆÇÈÉÊËÌÍÎÏ&ÐÑÒÓÔÕÖØŒŠþÙÚÛÜÝŸàáâãäåæçèéêëìíîïðñòóôõöøœšÞùúûüýÿß¢ß¥£™©®ª×÷±²³¼½¾µ¿¶·¸º°¯§…¤¦≠¬ˆ¨/ ]{2,50}$")]),
      'url': new FormControl('', [Validators.required, Validators.pattern(/^https?:\/\/(?:[a-z0-9\-]+\.)+[a-z]{2,6}(?:\/[^\/#?]+)+\.(?:jpg|gif|png)(\?[a-zA-Z0-9\/\-\=&]*)*$/)])
    });
  }

  onSubmitAddDataForm(){
    let newPicture = new PictureModel();
    newPicture.name = this.addModelForm.value['name'];
    newPicture.url = this.addModelForm.value['url'];
    this.pictureService.localAddData(newPicture).subscribe(
      ()=>{
        this.snackbar.openSnackBar("Photo ajoutée", "OK", "success-snackbar");
        this.reloadDatas();
        this.initForm();
      }, error=>{
        this.snackbar.openSnackBar('Une erreur est survenue lors de l\'ajout de la photo' + error.error, 'OK', 'error-snackbar');
      }
    );
  }


  onOrderById():void{
    if(this.idLogoSrcOne === IMAGES.upArrowFirstD){
      this.idLogoSrcOne = IMAGES.downArrowFirstD;
      this.idLogoSrcTwo = IMAGES.downArrowSecondD;
      this.pictureService.changeOrder("DESC", "pictureId");
    }else{
      this.idLogoSrcOne = IMAGES.upArrowFirstD;
      this.idLogoSrcTwo = IMAGES.upArrowSecondD;
      this.pictureService.changeOrder("ASC", "pictureId");
    }
    this.reloadDatas();
  }

  onOrderByName():void{
    if(this.nameLogoSrcOne === IMAGES.upArrowFirstD){
      this.nameLogoSrcOne = IMAGES.downArrowFirstD;
      this.nameLogoSrcTwo = IMAGES.downArrowSecondD;
      this.pictureService.changeOrder("DESC", "name");
    }else{
      this.nameLogoSrcOne = IMAGES.upArrowFirstD;
      this.nameLogoSrcTwo = IMAGES.upArrowSecondD;
      this.pictureService.changeOrder("ASC", "name");
    }
    this.reloadDatas();
  }

  onOrderByUrl():void{
    if(this.urlLogoSrcOne === IMAGES.upArrowFirstD){
      this.urlLogoSrcOne = IMAGES.downArrowFirstD;
      this.urlLogoSrcTwo = IMAGES.downArrowSecondD;
      this.pictureService.changeOrder("DESC", "url");
    }else{
      this.urlLogoSrcOne = IMAGES.upArrowFirstD;
      this.urlLogoSrcTwo = IMAGES.upArrowSecondD;
      this.pictureService.changeOrder("ASC", "url");
    }
    this.reloadDatas();
  }

  createDataFormGroup(pictureModel: PictureModel): FormGroup{
    return this.formBuilder.group({
      'pictureId': new FormControl(pictureModel.pictureId, [Validators.required, Validators.min(1)]),
      'name': new FormControl(pictureModel.name, [Validators.required, Validators.pattern("^[a-zA-z0-9'-_ ]{2,50}$")]),
      'url': new FormControl(pictureModel.url, [Validators.required, Validators.pattern(/^https?:\/\/(?:[a-z0-9\-]+\.)+[a-z]{2,6}(?:\/[^\/#?]+)+\.(?:jpg|gif|png)(\?[a-zA-Z0-9\/\-\=&]*)*$/)])
    });
  }

  goToLink(url:string){
    window.open(url, "_blank");
  }

  onSubmitEditData(index:number){
    let pictureModelToUpdate = new PictureModel();
    pictureModelToUpdate.pictureId = this.formArray.controls[index].value['pictureId'];
    pictureModelToUpdate.name = this.formArray.controls[index].value['name'];
    pictureModelToUpdate.url = this.formArray.controls[index].value['url'];
    this.pictureService.localEditData(pictureModelToUpdate).subscribe(
      ()=>{
        this.snackbar.openSnackBar('Photo modifiée!', 'OK', 'success-snackbar');
        this.reloadDatas();
      }, error=>{
        this.snackbar.openSnackBar('Une erreur est survenue lors de la modification de la photo: ' +error.error, 'OK', 'error-snackbar');
      }
    );
  }

}
