import { Component, OnInit, OnDestroy } from '@angular/core';
import { SnackbarComponent } from '../snackbar/snackbar.component';
import * as $ from 'jquery';
import { TokenStorageService } from 'src/app/services/token-storage.service';
import { async } from '@angular/core/testing';
import { ColorListModel } from 'src/app/models/dataListModels/colorListModel';
import { Subscription, of } from 'rxjs';
import { ColorService } from 'src/app/services/dataServices/color-service';
import { QueryModel } from 'src/app/models/queryModel';
import { FormArray, FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ColorModel } from 'src/app/models/dataModels/colorModel';
import { AuthService } from 'src/app/services/auth-service';
import { IMAGES } from 'src/app/util/images';
import { GenericComponent } from '../generic/generic.component';

@Component({
  selector: 'app-colors',
  templateUrl: './colors.component.html',
  styleUrls: ['./colors.component.css']
})
export class ColorsComponent  extends GenericComponent  implements OnInit, OnDestroy{

  idLogoSrcOne = '';
  idLogoSrcTwo = '';
  colorLogoSrcOne = '';
  colorLogoSrcTwo = '';

  constructor(protected snackbar: SnackbarComponent, protected authService: AuthService, protected colorService: ColorService, protected formBuilder: FormBuilder ) {
    super(snackbar, authService, colorService, formBuilder);
   }

  ngOnInit() {
  super.ngOnInit();
  }

  /**
   * Fonction permettant utilisée dans l'attribution d'images aux éléments permettant le tri par colonne
   */
  createOrderLogos():void{
    this.idLogoSrcOne = IMAGES.upArrowFirstD;
    this.idLogoSrcTwo = IMAGES.upArrowSecondD
    this.colorLogoSrcOne = IMAGES.upArrowFirstD;
    this.colorLogoSrcTwo = IMAGES.upArrowSecondD;
  }
  /**
   * Fonction permettant d'attribuer ses propriétés au formulaire d'addition de couleur
   */
  initForm():void{
    this.addModelForm = this.formBuilder.group({
      'color': new FormControl('', [Validators.required, Validators.pattern("^[A-ZÂÊÎÔÛÈÀÌÒÙÉÇ]([a-zA-ZéÂÊÎÔÛÈÀÌÒÙÉèàâêîôûùç'\\- ]){2,39}$")]),
    });
  }
  /**
   * Fonction gérant la soumission du formulaire d'ajout de couleur
   */
  onSubmitAddDataForm():void {
    let newColor = new ColorModel();
    newColor.color = this.addModelForm.value['color'];
    this.colorService.localAddData(newColor).subscribe(
      ()=>{
        this.snackbar.openSnackBar('Couleur ajoutée', 'OK', 'success-snackbar');
        this.reloadDatas();
        this.initForm();
      }, error=>{
        this.snackbar.openSnackBar('Une erreur est survenue lors de l\'ajout de la couleur' + error.error, 'OK', 'error-snackbar');
      }
    );
  }
  /**
   * Fonction permettant de gérer le tri par ID
   */
  onOrderById():void{
    if(this.idLogoSrcOne === IMAGES.upArrowFirstD){
      this.idLogoSrcOne = IMAGES.downArrowFirstD;
      this.idLogoSrcTwo = IMAGES.downArrowSecondD;
      this.colorService.changeOrder("DESC", "colorId");
    }else{
      this.idLogoSrcOne = IMAGES.upArrowFirstD;
      this.idLogoSrcTwo = IMAGES.upArrowSecondD;
      this.colorService.changeOrder("ASC", "colorId");
    }
    this.reloadDatas();
  }
  /**
   * Fonction permettant de gérer le tri par nom de couleur
   */
  onOrderByColor():void{
    if(this.colorLogoSrcOne === IMAGES.upArrowFirstD){
      this.colorLogoSrcOne = IMAGES.downArrowFirstD;
      this.colorLogoSrcTwo = IMAGES.downArrowSecondD;
      this.colorService.changeOrder("DESC", "name");
    }else{
      this.colorLogoSrcOne = IMAGES.upArrowFirstD;
      this.colorLogoSrcTwo = IMAGES.upArrowSecondD;
      this.colorService.changeOrder("ASC", "name");
    }
    this.reloadDatas();
  }
  /**
   * Fonction créant un FormGroup pré-rempli à partir des informations d'un ColorModel
   * @param colorModel : Le colorModel dont les données doivent être affichées
   */
  createDataFormGroup(colorModel: ColorModel): FormGroup{
    return this.formBuilder.group({
      'id': new FormControl(colorModel.colorId, [Validators.required, Validators.min(1)]),
      'color': new FormControl(colorModel.color, [Validators.required, Validators.pattern("^[A-ZÂÊÎÔÛÈÀÌÒÙÉÇ]([a-zA-ZéÂÊÎÔÛÈÀÌÒÙÉèàâêîôûùç'\\- ]){2,39}$")])
    });
  }
  /**
   * Fonction permettant de gérer la soumission d'un formulaire de modification de couleurs
   * @param index : l'index, dans le FormArray, de cette couleur
   */
  onSubmitEditData(index: number){
    let colorModelToUpdate = new ColorModel();
    colorModelToUpdate.colorId = this.formArray.controls[index].value['id'];
    colorModelToUpdate.color = this.formArray.controls[index].value['color'];
    this.colorService.localEditData(colorModelToUpdate).subscribe(
      ()=>{
        this.snackbar.openSnackBar('Couleur modifiée!', 'OK', 'success-snackbar');
        this.reloadDatas();
      }, error=>{
        this.snackbar.openSnackBar('Une erreur est survenue lors de la modification de la couleur: ' +error.error, 'OK', 'error-snackbar');
      }
    );
  }

  ngOnDestroy(){
    super.ngOnDestroy();
  }

}
