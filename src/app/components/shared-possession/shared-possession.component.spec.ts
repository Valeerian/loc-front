import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharedPossessionComponent } from './shared-possession.component';

describe('SharedPossessionComponent', () => {
  let component: SharedPossessionComponent;
  let fixture: ComponentFixture<SharedPossessionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SharedPossessionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharedPossessionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
