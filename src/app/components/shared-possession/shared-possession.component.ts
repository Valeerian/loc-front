import { Component, OnInit } from '@angular/core';
import { GenericComponent } from '../generic/generic.component';
import { OrderListModel } from 'src/app/models/dataListModels/orderListModel';
import { UserListModel } from 'src/app/models/dataListModels/userListModel';
import { ClotheListModel } from 'src/app/models/dataListModels/clotheListModel';
import { ClotheTypeListModel } from 'src/app/models/dataListModels/clotheTypeListModel';
import { BrandListModel } from 'src/app/models/dataListModels/brandListModel';
import { SnackbarComponent } from '../snackbar/snackbar.component';
import { AuthService } from 'src/app/services/auth-service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { PossessionService } from 'src/app/services/dataServices/possession-service.service';
import { ClotheTypeService } from 'src/app/services/dataServices/clothe-type.service';
import { ClotheService } from 'src/app/services/dataServices/clothe.service';
import { FriendshipService } from 'src/app/services/dataServices/friendship.service';
import { BrandService } from 'src/app/services/dataServices/brand.service';
import { PossessionOrders } from 'src/app/util/possessionOrders';
import { OrderModel } from 'src/app/models/dataModels/orderModel';
import { UserModel } from 'src/app/models/dataModels/userModel';
import { ClotheModel } from 'src/app/models/dataModels/clotheModel';
import { ClotheTypeModel } from 'src/app/models/dataModels/clotheTypeModel';
import { BrandModel } from 'src/app/models/dataModels/brandModel';
import { SharedPossessionService } from 'src/app/services/dataServices/shared-possession.service';

@Component({
  selector: 'app-shared-possession',
  templateUrl: './shared-possession.component.html',
  styleUrls: ['./shared-possession.component.css']
})
export class SharedPossessionComponent extends GenericComponent implements OnInit {

  orders: OrderListModel = new OrderListModel();
  clotheTypeCategories: string[]= [];
  wearerList: UserListModel = new UserListModel();
  clotheList: ClotheListModel = new ClotheListModel();
  clotheTypeList: ClotheTypeListModel = new ClotheTypeListModel();
  brandList: BrandListModel = new BrandListModel();

  clotheTypeCriteriaArray: number[] = [];
  wearerCriteriaArray: number[] = [];
  clotheCriteriaArray: number[] = [];
  brandCriteriaArray: number[] = [];

  resetFlagClotheType: boolean = false;
  resetFlagClothe: boolean = false;
  resetFlagWearer: boolean = false;
  resetFlagBrand: boolean = false;

  constructor(protected snackbar: SnackbarComponent, protected authService: AuthService, protected formBuilder: FormBuilder, protected dataService: SharedPossessionService,
    private clotheTypeService: ClotheTypeService, private clotheService: ClotheService, private friendshipService: FriendshipService, private brandService: BrandService) {
      super(snackbar, authService, dataService, formBuilder);
     }

  ngOnInit() {
    super.ngOnInit();
    this.loadDatasToFillSelects();
    super.ngOnInit();
  }

  submitFilter(){
    this.dataService.removeCriterias();
    if(this.wearerCriteriaArray.length > 0){
      for(let id of this.wearerCriteriaArray){
        this.dataService.addCriteria("user",":",""+id);
      }
    }
    if(this.clotheCriteriaArray.length > 0){
      for(let id of this.clotheCriteriaArray){
        this.dataService.addCriteria("clotheId",":",""+id);
      }
    }
    if(this.clotheTypeCriteriaArray.length > 0 ){
      for(let id of this.clotheTypeCriteriaArray ){
        this.dataService.addCriteria("clotheTypeId", ":", ""+id);
      }
    }
    if(this.brandCriteriaArray.length > 0 ){
      for(let id of this.brandCriteriaArray ){
        this.dataService.addCriteria("brandId", ":", ""+id);
      }
    }
    this.reloadDatas();
  }

  resetFilter(){
    this.dataService.removeCriterias();
    this.resetFlagBrand = this.switchBoolean(this.resetFlagBrand);
    this.resetFlagClothe = this.switchBoolean(this.resetFlagClothe);
    this.resetFlagClotheType = this.switchBoolean(this.resetFlagClotheType);
    this.resetFlagWearer = this.switchBoolean(this.resetFlagWearer);
    this.dataService.changeOrder(PossessionOrders.idAsc.orderType, PossessionOrders.idAsc.orderBy);
    this.reloadDatas();
  }

  switchBoolean(boo : boolean): boolean{
    if(boo == true){
      return false;
    }
    else{
      return true;
    }
  }

  onChangeOrder(orderModel: OrderModel){
    if(orderModel){
      this.dataService.changeOrder(orderModel.orderType, orderModel.orderBy);
    }else{
      this.dataService.changeOrder(PossessionOrders.idAsc.orderType,PossessionOrders.idAsc.orderBy);
    }
  }

  loadDatasToFillSelects():void{
    this.orders.list.push(PossessionOrders.idAsc);
    this.orders.list.push(PossessionOrders.idDesc);
    this.orders.list.push(PossessionOrders.ownerAsc);
    this.orders.list.push(PossessionOrders.ownerDesc);
    this.orders.list.push(PossessionOrders.sizeAsc);
    this.orders.list.push(PossessionOrders.sizeDesc);
    this.orders.list.push(PossessionOrders.clotheAsc);
    this.orders.list.push(PossessionOrders.clotheDesc);
    this.orders.list.push(PossessionOrders.userAsc);
    this.orders.list.push(PossessionOrders.userDesc);
    this.clotheService.localGetClotheForSharedPossession().subscribe(
      (list: ClotheListModel)=>{
        this.clotheList = list;
      },error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la récupération des vêtements: " +error.error ,"OK","error-snackbar");
      }
    );
    this.clotheTypeService.localGetClotheTypeForSharedPossession().subscribe(
      (list: ClotheTypeListModel)=>{
        this.clotheTypeList = list;
        for(let ct of list.list){
          let alreadyFoundFlag : boolean = false;
          for(let cat of this.clotheTypeCategories){
            if(ct.category === cat){
              alreadyFoundFlag = true;
              break;
            }
          }
          if(!alreadyFoundFlag){
            this.clotheTypeCategories.push(ct.category);
          }
        }
      },error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la récupération des types: " +error.error ,"OK","error-snackbar");
      }
    );
    this.brandService.localGetBrandForSharedPossession().subscribe(
      (list: BrandListModel)=>{
        this.brandList = list;
      },error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la récupération des marques: " +error.error ,"OK","error-snackbar");
      }
    );
    this.friendshipService.localGetWearers().subscribe(
      (list: UserListModel)=>{
        this.wearerList= list;
      },error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la récupération des propriétaires : " +error.error ,"OK","error-snackbar");
      }
    );
  }

  protected createDataFormGroup(model: any): FormGroup{
    return this.formBuilder.group({});
  }

  protected onSubmitEditData(index: number): void {
    //throw new Error("Method not implemented.");
  }
  protected createOrderLogos(): void {
    //throw new Error("Method not implemented.");
  }
  protected initForm(): void {
    //throw new Error("Method not implemented.");
  }
  protected onSubmitAddDataForm(): void {
    //throw new Error("Method not implemented.");
  }

  onChangeWearerFilter(userModels : UserModel[]){
    this.wearerCriteriaArray = [];
    if(userModels && userModels.length > 0){
      for(let user of userModels){
        this.wearerCriteriaArray.push(user.userId);
      }
    }
  }

  onChangeClotheFilter(clotheModels : ClotheModel[]){
    this.clotheCriteriaArray = [];
    if(clotheModels && clotheModels.length > 0){
      for(let clothe of clotheModels){
        this.clotheCriteriaArray.push(clothe.clotheId);
      }
    }
  }

  onChangeClotheTypeFilter(clotheTypeModels : ClotheTypeModel[]){
    this.clotheTypeCriteriaArray = [];
    if(clotheTypeModels && clotheTypeModels.length > 0){
      for(let clotheType of clotheTypeModels){
        this.clotheTypeCriteriaArray.push(clotheType.clotheTypeId);
      }
    }
  }

  onChangeBrandFilter(brandModels : BrandModel[]){
    this.brandCriteriaArray = [];
    if(brandModels && brandModels.length > 0){
      for(let brand of brandModels){
        this.brandCriteriaArray.push(brand.brandId);
      }
    }
  }

}
