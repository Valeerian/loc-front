import { Component, OnInit, OnDestroy } from '@angular/core';
import { QueryModel } from 'src/app/models/queryModel';
import { ColorListModel } from 'src/app/models/dataListModels/colorListModel';
import { SnackbarComponent } from '../snackbar/snackbar.component';
import { SizesService } from 'src/app/services/dataServices/sizes.service';
import { FormBuilder, FormGroup, FormArray, Validators, FormControl } from '@angular/forms';
import { error } from 'protractor';
import { SizeListModel } from 'src/app/models/dataListModels/sizeListModel';
import { SizeModel } from 'src/app/models/dataModels/sizeModel';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth-service';
import { GenericComponent } from '../generic/generic.component';
import { IMAGES } from 'src/app/util/images';

@Component({
  selector: 'app-sizes',
  templateUrl: './sizes.component.html',
  styleUrls: ['./sizes.component.css']
})
export class  SizesComponent extends GenericComponent implements OnInit, OnDestroy {

  idLogoSrcOne = '';
  idLogoSrcTwo = '';
  categoryLogoSrcOne='';
  categoryLogoSrcTwo='';
  sizeLogoSrcOne='';
  sizeLogoSrcTwo='';
  constructor(protected snackbar: SnackbarComponent, protected authService: AuthService, protected sizeService: SizesService, protected formBuilder: FormBuilder) {
    super(snackbar, authService, sizeService, formBuilder);
   }

  ngOnInit() {
    super.ngOnInit();
  }

  ngOnDestroy(){
    super.ngOnDestroy();
  }
  
  /**
   * Sert à créer des logos pour les noms de colonnes de la liste
   */
  createOrderLogos():void{
    this.idLogoSrcOne= IMAGES.upArrowFirstD;
    this.idLogoSrcTwo= IMAGES.upArrowSecondD;
    this.categoryLogoSrcOne = IMAGES.upArrowFirstD;
    this.categoryLogoSrcTwo = IMAGES.upArrowSecondD;
    this.sizeLogoSrcOne = IMAGES.upArrowFirstD;
    this.sizeLogoSrcTwo = IMAGES.upArrowSecondD;
  }

  /**
   * Crée le formulaire pour l'ajout d'une taille
   */
  initForm(){
    this.addModelForm = this.formBuilder.group({
      'category': new FormControl('',[Validators.required, Validators.pattern("[A-Z]{1,10}")]),
      'size': new FormControl('',[Validators.required, Validators.pattern("(^([SML]|[0-9]?X(S|L)|X{0,4}[LS])$)|(^(EU|UK|US|IT)([0-9]{1,2}(,5)?)$)|(^(EU|UK|US)W([0-9]{1,2})L([0-9]{1,2})$)")]),
    });
  }

  /**
   * Appelé lorsque l'utilisateur soumet une taille qu'il souhaite ajouter à la base de données
   */
  onSubmitAddDataForm(){
    let newSize = new SizeModel();
    newSize.category = this.addModelForm.value['category'];
    newSize.size = this.addModelForm.value['size'];
    this.sizeService.localAddData(newSize).subscribe(
      ()=>{
        this.snackbar.openSnackBar('Taille ajoutée!', "OK", "success-snackbar");
        this.reloadDatas();
        this.initForm();
      },error =>{
        this.snackbar.openSnackBar('Une erreur est survenue pendant l\'ajout de la taille: '+error.error, 'OK', 'error-snackbar');
      }
    )
  }
  
  
  /**
   * Fonction appelée lorsque l'utilisateur clique sur le logo de la flèche de la colonne ID
   * Permet de changer l'ordre de tri
   */
  onOrderById(){
    if(this.idLogoSrcOne === IMAGES.upArrowFirstD){// Si tri par id descendant demandé
      this.idLogoSrcOne= IMAGES.downArrowFirstD;
      this.idLogoSrcTwo = IMAGES.downArrowSecondD;
      this.sizeService.changeOrder("DESC", "sizeId");
    }else{//Si tri par id ascendant demandé
      this.idLogoSrcOne = IMAGES.upArrowFirstD;
      this.idLogoSrcTwo = IMAGES.upArrowSecondD;
      this.sizeService.changeOrder("ASC", "sizeId");
    }
    this.reloadDatas();
  }

  onOrderByCategory(){
    if(this.categoryLogoSrcOne === IMAGES.upArrowFirstD){
      this.categoryLogoSrcOne= IMAGES.downArrowFirstD;
      this.categoryLogoSrcTwo = IMAGES.downArrowSecondD;
      this.sizeService.changeOrder("DESC", "category");
    }else{
      this.categoryLogoSrcOne = IMAGES.upArrowFirstD;
      this.categoryLogoSrcTwo = IMAGES.upArrowSecondD;
      this.sizeService.changeOrder("ASC", "category");
    }
    this.reloadDatas();
  }

  onOrderBySize(){
    if(this.sizeLogoSrcOne === IMAGES.upArrowFirstD){
      this.sizeLogoSrcOne= IMAGES.downArrowFirstD;
      this.sizeLogoSrcTwo = IMAGES.downArrowSecondD;
      this.sizeService.changeOrder("DESC", "size");
    }else{
      this.sizeLogoSrcOne = IMAGES.upArrowFirstD;
      this.sizeLogoSrcTwo = IMAGES.upArrowSecondD;
      this.sizeService.changeOrder("ASC", "size");
    }
    this.reloadDatas();
  }

  createDataFormGroup(sizeModel: SizeModel): FormGroup{
    return this.formBuilder.group({
    'id': new FormControl(sizeModel.id,[Validators.required, Validators.min(1)]),
    'category': new FormControl(sizeModel.category, [Validators.required, Validators.pattern("[A-Z]{1,10}")]),
    'size': new FormControl(sizeModel.size, [Validators.required, Validators.pattern("(^([SML]|[0-9]?X(S|L)|X{0,4}[LS])$)|(^(EU|UK|US|IT)([0-9]{1,2}(,5)?)$)|(^(EU|UK|US)W([0-9]{1,2})L([0-9]{1,2})$)")]),
    });
  }

  onSubmitEditData(index: number){
    let smToUpdate = new SizeModel;
    smToUpdate.id = this.formArray.controls[index].value['id'];
    smToUpdate.category = this.formArray.controls[index].value['category'];
    smToUpdate.size = this.formArray.controls[index].value['size'];
    this.sizeService.localEditData(smToUpdate).subscribe(
      () =>{
        this.snackbar.openSnackBar("Taille modifiée!", "OK", "success-snackbar");
        this.reloadDatas();
      }, error=>{
        this.snackbar.openSnackBar("Une erreur est survenue lors de la mise à jour de la taille: " +error.error, "OK", "error-snackbar");
      }
    );
  }


}
