import { Component, OnInit, OnDestroy } from '@angular/core';
import { GenericComponent } from '../generic/generic.component';
import { SnackbarComponent } from '../snackbar/snackbar.component';
import { AuthService } from 'src/app/services/auth-service';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ClotheTypeService } from 'src/app/services/dataServices/clothe-type.service';
import { IMAGES } from 'src/app/util/images';
import { ClotheTypeModel } from 'src/app/models/dataModels/clotheTypeModel';

@Component({
  selector: 'app-clothe-type',
  templateUrl: './clothe-type.component.html',
  styleUrls: ['./clothe-type.component.css']
})
export class ClotheTypeComponent extends GenericComponent implements OnInit, OnDestroy {
  
  idLogoSrcOne = '';
  idLogoSrcTwo = '';
  clotheTypeLogoSrcOne = '';
  clotheTypeLogoSrcTwo = '';
  categoryLogoSrcOne = '';
  categoryLogoSrcTwo = '';

  constructor(protected snackbar: SnackbarComponent, protected authService: AuthService, protected clotheTypeService: ClotheTypeService, protected formBuilder: FormBuilder ) {
    super(snackbar, authService, clotheTypeService, formBuilder);
   }

  ngOnInit() {
    super.ngOnInit();
  }

  ngOnDestroy(){
    super.ngOnDestroy();
  }

  protected createDataFormGroup(clotheTypeModel: ClotheTypeModel): FormGroup {
    return this.formBuilder.group({
      'id': new FormControl(clotheTypeModel.clotheTypeId, [Validators.required, Validators.min(1)]),
      'clotheType' : new FormControl(clotheTypeModel.clotheType, [Validators.required, Validators.pattern("^([A-ZÈÉÇ]([a-zçéè \\-']{1,15})){1,5}$")]),
      'category': new FormControl(clotheTypeModel.category, [Validators.required, Validators.pattern("^[A-ZÇÈÉ]{2,20}$")])
    });
  }
  protected onSubmitEditData(index: number): void {
    let clotheTypeModelToUpdate = new ClotheTypeModel();
    clotheTypeModelToUpdate.category = this.formArray.controls[index].value['category'];
    clotheTypeModelToUpdate.clotheTypeId = this.formArray.controls[index].value['id'];
    clotheTypeModelToUpdate.clotheType = this.formArray.controls[index].value['clotheType'];
    this.clotheTypeService.localEditData(clotheTypeModelToUpdate).subscribe(
      ()=>{
        this.snackbar.openSnackBar('Type de vêtement modifiée!', 'OK', 'success-snackbar');
        this.reloadDatas();
      },error=>{
        this.snackbar.openSnackBar('Une erreur est survenue lors de la modification du type de vêtement: ' +error.error, 'OK', 'error-snackbar');
      }
    );
  }
  protected createOrderLogos(): void {
    this.idLogoSrcOne = IMAGES.upArrowFirstD;
    this.idLogoSrcTwo = IMAGES.upArrowSecondD;
    this.clotheTypeLogoSrcOne = IMAGES.upArrowFirstD;
    this.clotheTypeLogoSrcTwo = IMAGES.upArrowSecondD;
    this.categoryLogoSrcOne = IMAGES.upArrowFirstD;
    this.categoryLogoSrcTwo = IMAGES.upArrowSecondD;
  }
  protected initForm(): void {
    this.addModelForm = this.formBuilder.group({
      'category': new FormControl('', [Validators.required, Validators.pattern("^[A-ZÇÈÉ]{2,20}$")]),
      'clotheType': new FormControl('', [Validators.required, Validators.pattern("^([A-ZÈÉÇ]([a-zçéè \\-']{1,15})){1,5}$")])
    })
  }
  protected onSubmitAddDataForm(): void {
    let newClotheType = new ClotheTypeModel();
    newClotheType.category = this.addModelForm.value['category'];
    newClotheType.clotheType = this.addModelForm.value['clotheType'];
    this.clotheTypeService.localAddData(newClotheType).subscribe(
      ()=>{
        this.snackbar.openSnackBar("Type de vêtement ajouté", "OK", "success-snackbar");
        this.reloadDatas();
        this.initForm();
      }, error=>{
        this.snackbar.openSnackBar('Une erreur est survenue lors de l\'ajout du type de vêtement' + error.error, 'OK', 'error-snackbar');
      }
    );
  }

  onOrderById():void{
    if(this.idLogoSrcOne === IMAGES.upArrowFirstD){
      this.idLogoSrcOne = IMAGES.downArrowFirstD;
      this.idLogoSrcTwo = IMAGES.downArrowSecondD;
      this.clotheTypeService.changeOrder("DESC", "clotheTypeId");
    }else{
      this.idLogoSrcOne = IMAGES.upArrowFirstD;
      this.idLogoSrcTwo = IMAGES.upArrowSecondD;
      this.clotheTypeService.changeOrder("ASC", "clotheTypeId");
    }
    this.reloadDatas();
  }

  onOrderByCategory():void{
    if(this.categoryLogoSrcOne === IMAGES.upArrowFirstD){
      this.categoryLogoSrcOne = IMAGES.downArrowFirstD;
      this.categoryLogoSrcTwo = IMAGES.downArrowSecondD;
      this.clotheTypeService.changeOrder("DESC", "category");
    }else{
      this.categoryLogoSrcOne = IMAGES.upArrowFirstD;
      this.categoryLogoSrcTwo = IMAGES.upArrowSecondD;
      this.clotheTypeService.changeOrder("ASC", "category");
    }
    this.reloadDatas();
  }

  onOrderByClotheType():void{
    if(this.clotheTypeLogoSrcOne === IMAGES.upArrowFirstD){
      this.clotheTypeLogoSrcOne = IMAGES.downArrowFirstD;
      this.clotheTypeLogoSrcTwo = IMAGES.downArrowSecondD;
      this.clotheTypeService.changeOrder("DESC", "name");
    }else{
      this.clotheTypeLogoSrcOne = IMAGES.upArrowFirstD;
      this.clotheTypeLogoSrcTwo = IMAGES.upArrowSecondD;
      this.clotheTypeService.changeOrder("ASC", "name");
    }
    this.reloadDatas();
  }

}
