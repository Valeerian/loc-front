import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClotheTypeComponent } from './clothe-type.component';

describe('ClotheTypeComponent', () => {
  let component: ClotheTypeComponent;
  let fixture: ComponentFixture<ClotheTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClotheTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClotheTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
