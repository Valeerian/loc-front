import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FourZeroFourComponent } from './components/four-zero-four/four-zero-four.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { ColorsComponent } from './components/colors/colors.component';
import { AuthGuardService } from './services/auth-guard.service';
import { SignupComponent } from './components/signup/signup.component';
import { DataPolicyComponent } from './components/data-policy/data-policy.component';
import { RecoverAccountComponent } from './components/recover-account/recover-account.component';
import { RecoverAccountP2Component } from './components/recover-account-p2/recover-account-p2.component';
import { ProfilComponent } from './components/profil/profil.component';
import { SizesComponent } from './components/sizes/sizes.component';
import { BrandsComponent } from './components/brands/brands.component';
import { ClotheTypeComponent } from './components/clothe-type/clothe-type.component';
import { MaterialsComponent } from './components/materials/materials.component';
import { ProviderComponent } from './components/provider/provider.component';
import { SeasonComponent } from './components/season/season.component';
import { ThemeComponent } from './components/theme/theme.component';
import { StyleComponent } from './components/style/style.component';
import { SexComponent } from './components/sex/sex.component';
import { ManeStatusComponent } from './components/mane-status/mane-status.component';
import { MaintenanceInstructionComponent } from './components/maintenance-instruction/maintenance-instruction.component';
import { PictureComponent } from './components/picture/picture.component';
import { ClotheComponent } from './components/clothe/clothe.component';
import { PossessionListComponent } from './components/possession-list/possession-list.component';
import { SharedPossessionComponent } from './components/shared-possession/shared-possession.component';
import { FriendshipListComponent } from './components/friendship-list/friendship-list.component';
import { OutfitComponent } from './components/outfit/outfit.component';
import { LaundryGroupListComponent } from './components/laundry-group-list/laundry-group-list.component';


const routes: Routes = [
  
  {path:'home', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'signup', component: SignupComponent},
  {path: 'colors', canActivate:[AuthGuardService],  component: ColorsComponent},
  {path: 'sizes', canActivate:[AuthGuardService], component: SizesComponent},
  {path: 'brands', canActivate: [AuthGuardService], component: BrandsComponent},
  {path: 'clotheTypes', canActivate: [AuthGuardService], component: ClotheTypeComponent},
  {path: 'materials', canActivate: [AuthGuardService], component: MaterialsComponent},
  {path: 'seasons', canActivate: [AuthGuardService], component: SeasonComponent},
  {path: 'styles', canActivate: [AuthGuardService], component: StyleComponent},
  {path: 'providers', canActivate: [AuthGuardService], component: ProviderComponent},
  {path: 'themes', canActivate: [AuthGuardService], component: ThemeComponent},
  {path: 'maneStatuses', canActivate: [AuthGuardService], component: ManeStatusComponent},
  {path: 'pictures', canActivate: [AuthGuardService], component: PictureComponent},
  {path: 'maintenanceInstructions', canActivate: [AuthGuardService], component: MaintenanceInstructionComponent},
  {path: 'clothes', canActivate: [AuthGuardService], component: ClotheComponent},
  {path: 'sexes', canActivate: [AuthGuardService], component: SexComponent},
  {path: 'possessionList', canActivate: [AuthGuardService], component: PossessionListComponent},
  {path: 'friends', canActivate: [AuthGuardService], component: FriendshipListComponent},
  {path: 'outfits', canActivate: [AuthGuardService], component: OutfitComponent},
  {path: 'sharedPossessionList', canActivate:[AuthGuardService], component: SharedPossessionComponent},
  {path: 'laundryGroups', canActivate:[AuthGuardService], component: LaundryGroupListComponent},
  {path: 'dataPolicy', component: DataPolicyComponent},
  {path: 'recoverAccount', component: RecoverAccountComponent},
  {path: 'recoverAccount2', component: RecoverAccountP2Component},
  {path: 'profile', canActivate:[AuthGuardService], component: ProfilComponent},
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'not-found', component: FourZeroFourComponent},
  {path:'**', redirectTo:'not-found'},
  

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
