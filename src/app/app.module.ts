import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MatSnackBarModule, MatCheckboxModule, MatTooltipModule, MatInputModule, MatSelect, MatLabel, MatOption, MatFormField, MatSelectModule, MatOptionModule, MatFormFieldModule, MatButtonModule, MatDividerModule, MatExpansionModule, MatChipsModule, MatTableModule, MatRadioModule, MatDatepickerModule, MatNativeDateModule } from '@angular/material';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LoginComponent } from './components/login/login.component';
import { FourZeroFourComponent } from './components/four-zero-four/four-zero-four.component';
import { HomeComponent } from './components/home/home.component';
import { SnackbarComponent } from './components/snackbar/snackbar.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthService } from './services/auth-service';
import { TokenStorageService } from './services/token-storage.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { InterceptorService } from './services/interceptor.service';
import { ColorsComponent } from './components/colors/colors.component';
import { AuthInterceptor } from './services/auth-interceptor';
import { SignupComponent } from './components/signup/signup.component';
import { DataPolicyComponent } from './components/data-policy/data-policy.component';
import { RecoverAccountComponent } from './components/recover-account/recover-account.component';
import { RecoverAccountP2Component } from './components/recover-account-p2/recover-account-p2.component';
import { ProfilComponent } from './components/profil/profil.component';
import { SizesComponent } from './components/sizes/sizes.component';
import { BrandsComponent } from './components/brands/brands.component';
import { GenericComponent } from './components/generic/generic.component';
import { ClotheTypeComponent } from './components/clothe-type/clothe-type.component';
import { MaterialsComponent } from './components/materials/materials.component';
import { ProviderComponent } from './components/provider/provider.component';
import { SeasonComponent } from './components/season/season.component';
import { ThemeComponent } from './components/theme/theme.component';
import { StyleComponent } from './components/style/style.component';
import { SexComponent } from './components/sex/sex.component';
import { ManeStatusComponent } from './components/mane-status/mane-status.component';
import { MaintenanceInstructionComponent } from './components/maintenance-instruction/maintenance-instruction.component';
import { PictureComponent } from './components/picture/picture.component';
import { SeasonDropdownComponent } from './components/subComponents/season-dropdown/season-dropdown.component';
import { ClotheComponent } from './components/clothe/clothe.component';
import { SexDropdownComponent } from './components/subComponents/sex-dropdown/sex-dropdown.component';
import { SeasonMultiDropdownComponent } from './components/subComponents/season-multi-dropdown/season-multi-dropdown.component';
import { SexMultiDropdownComponent } from './components/subComponents/sex-multi-dropdown/sex-multi-dropdown.component';
import { BrandMultiDropdownComponent } from './components/subComponents/brand-multi-dropdown/brand-multi-dropdown.component';
import { BrandDropdownComponent } from './components/subComponents/brand-dropdown/brand-dropdown.component';
import { ClotheTypeMultiDropdownComponent } from './components/subComponents/clothe-type-multi-dropdown/clothe-type-multi-dropdown.component';
import { ClotheTypeDropdownComponent } from './components/subComponents/clothe-type-dropdown/clothe-type-dropdown.component';
import { ColorDropdownComponent } from './components/subComponents/color-dropdown/color-dropdown.component';
import { ColorMultiDropdownComponent } from './components/subComponents/color-multi-dropdown/color-multi-dropdown.component';
import { MaterialDropdownComponent } from './components/subComponents/material-dropdown/material-dropdown.component';
import { MaterialMultiDropdownComponent } from './components/subComponents/material-multi-dropdown/material-multi-dropdown.component';
import { ProviderDropdownComponent } from './components/subComponents/provider-dropdown/provider-dropdown.component';
import { ProviderMultiDropdownComponent } from './components/subComponents/provider-multi-dropdown/provider-multi-dropdown.component';
import { SizeDropdownComponent } from './components/subComponents/size-dropdown/size-dropdown.component';
import { SizeMultiDropdownComponent } from './components/subComponents/size-multi-dropdown/size-multi-dropdown.component';
import { StyleDropdownComponent } from './components/subComponents/style-dropdown/style-dropdown.component';
import { StyleMultiDropdownComponent } from './components/subComponents/style-multi-dropdown/style-multi-dropdown.component';
import { ThemeDropdownComponent } from './components/subComponents/theme-dropdown/theme-dropdown.component';
import { ThemeMultiDropdownComponent } from './components/subComponents/theme-multi-dropdown/theme-multi-dropdown.component';
import { MinimumRatingDropdownComponent } from './components/subComponents/minimum-rating-dropdown/minimum-rating-dropdown.component';
import { ClotheListItemComponent } from './components/subComponents/clothe-list-item/clothe-list-item.component';
import { MaintenanceInstructionDropdownComponent } from './components/subComponents/maintenance-instruction-dropdown/maintenance-instruction-dropdown.component';
import { MaintenanceInstructionMultiDropdownComponent } from './components/subComponents/maintenance-instruction-multi-dropdown/maintenance-instruction-multi-dropdown.component';
import { PictureDisplayerComponent } from './components/subComponents/picture-displayer/picture-displayer.component';
import { OrderDropdownComponent } from './components/subComponents/order-dropdown/order-dropdown.component';
import { AddToPossessionFrameComponent } from './components/subComponents/add-to-possession-frame/add-to-possession-frame.component';
import { ClotheNoteComponent } from './components/clothe-note/clothe-note.component';
import { PossessionListComponent } from './components/possession-list/possession-list.component';
import { OwnerMultiDropdownComponent } from './components/subComponents/owner-multi-dropdown/owner-multi-dropdown.component';
import { ClotheMultiDropdownComponent } from './components/subComponents/clothe-multi-dropdown/clothe-multi-dropdown.component';
import { PossessionStatusMultiDropdownComponent } from './components/subComponents/possession-status-multi-dropdown/possession-status-multi-dropdown.component';
import { PossessionListItemComponent } from './components/subComponents/possession-list-item/possession-list-item.component';
import { WearerDropdownComponent } from './components/subComponents/wearer-dropdown/wearer-dropdown.component';
import { SharedPossessionComponent } from './components/shared-possession/shared-possession.component';
import { SharedPossessionListItemComponent } from './components/subComponents/shared-possession-list-item/shared-possession-list-item.component';
import { FriendshipListComponent } from './components/friendship-list/friendship-list.component';
import { FriendListItemComponent } from './components/subComponents/friend-list-item/friend-list-item.component';
import { OutfitComponent } from './components/outfit/outfit.component';
import { OutfitMultiDropdownComponent } from './components/subComponents/outfit-multi-dropdown/outfit-multi-dropdown.component';
import { MimicDropdownComponent } from './components/subComponents/mimic-dropdown/mimic-dropdown.component';
import { OutfitListItemComponent } from './components/subComponents/outfit-list-item/outfit-list-item.component';
import { PossessionMultiDropdownComponent } from './components/subComponents/possession-multi-dropdown/possession-multi-dropdown.component';
import { PublikPipePipe } from './util/publik-pipe.pipe';
import { FavoritePipe } from './util/favorite.pipe';
import { ConsultOutfitComponent } from './components/subComponents/consult-outfit/consult-outfit.component';
import { OutfitNoteComponent } from './components/subComponents/outfit-note/outfit-note.component';
import { LaundryGroupListComponent } from './components/laundry-group-list/laundry-group-list.component';
import { LaundryGroupListItemComponent } from './components/subComponents/laundry-group-list-item/laundry-group-list-item.component';
import { LaundryManeListItemComponent } from './components/subComponents/laundry-mane-list-item/laundry-mane-list-item.component';
import { ManeStatusDropdownComponent } from './components/subComponents/mane-status-dropdown/mane-status-dropdown.component';
import { ManeGroupMemberRowComponent } from './components/subComponents/mane-group-member-row/mane-group-member-row.component';
@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    FourZeroFourComponent,
    HomeComponent,
    ColorsComponent,
    SignupComponent,
    DataPolicyComponent,
    RecoverAccountComponent,
    RecoverAccountP2Component,
    ProfilComponent,
    SizesComponent,
    BrandsComponent,
    ClotheTypeComponent,
    MaterialsComponent,
    ProviderComponent,
    SeasonComponent,
    ThemeComponent,
    StyleComponent,
    SexComponent,
    ManeStatusComponent,
    MaintenanceInstructionComponent,
    PictureComponent,
    SeasonDropdownComponent,
    ClotheComponent,
    SexDropdownComponent,
    SeasonMultiDropdownComponent,
    SexMultiDropdownComponent,
    BrandMultiDropdownComponent,
    BrandDropdownComponent,
    ClotheTypeMultiDropdownComponent,
    ClotheTypeDropdownComponent,
    ColorDropdownComponent,
    ColorMultiDropdownComponent,
    MaterialDropdownComponent,
    MaterialMultiDropdownComponent,
    ProviderDropdownComponent,
    ProviderMultiDropdownComponent,
    SizeDropdownComponent,
    SizeMultiDropdownComponent,
    StyleDropdownComponent,
    StyleMultiDropdownComponent,
    ThemeDropdownComponent,
    ThemeMultiDropdownComponent,
    MinimumRatingDropdownComponent,
    ClotheListItemComponent,
    MaintenanceInstructionDropdownComponent,
    MaintenanceInstructionMultiDropdownComponent,
    PictureDisplayerComponent,
    OrderDropdownComponent,
    AddToPossessionFrameComponent,
    ClotheNoteComponent,
    PossessionListComponent,
    OwnerMultiDropdownComponent,
    ClotheMultiDropdownComponent,
    PossessionStatusMultiDropdownComponent,
    PossessionListItemComponent,
    WearerDropdownComponent,
    SharedPossessionComponent,
    SharedPossessionListItemComponent,
    FriendshipListComponent,
    FriendListItemComponent,
    OutfitComponent,
    OutfitMultiDropdownComponent,
    MimicDropdownComponent,
    OutfitListItemComponent,
    PossessionMultiDropdownComponent,
    PublikPipePipe,
    FavoritePipe,
    ConsultOutfitComponent,
    OutfitNoteComponent,
    LaundryGroupListComponent,
    LaundryGroupListItemComponent,
    LaundryManeListItemComponent,
    ManeStatusDropdownComponent,
    ManeGroupMemberRowComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatSnackBarModule,
    MatInputModule,
    MatSelectModule, 
    MatOptionModule,
    MatFormFieldModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatCheckboxModule,
    MatTooltipModule,
    MatButtonModule,
    MatDividerModule,
    MatExpansionModule,
    MatChipsModule,
    MatRadioModule,
    MatTableModule,
    MatDatepickerModule,
    MatNativeDateModule
  ],
  providers: [
    SnackbarComponent,
    AuthService,
    TokenStorageService,
    InterceptorService,
    {provide: HTTP_INTERCEPTORS, useClass:AuthInterceptor, multi: true},
    
  
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
