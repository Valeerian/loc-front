import { CriteriaCombiner } from './criteriaCombiner';

export class QueryModel{
    keys: string[];
    operators: string[];
    values: any[];
    pageNumber: number;
    elementsPerPage: number;
    orderBys: string[];
    orderTypes: string[];
    criteriaCombiners: CriteriaCombiner[];

    constructor(){
        this.criteriaCombiners = [];
        this.orderTypes = [];
        this.orderBys = [];
        this.values = [];
        this.operators = [];
        this.keys = [];
    }

    addCriteria(key: string, operator: string, value:string){
        this.keys.push(key);
        this.operators.push(operator);
        this.values.push(value);
    }

    addOrder(orderBy: string, orderType:string){
        this.orderBys.push(orderBy);
        this.orderTypes.push(orderType);
    }

    addCriteriaCombiner(indexOne: number, indexTwo: number, operator: string, executionOrder: number){
       let cc = new CriteriaCombiner;
       cc.indexOne = indexOne;
       cc.indexTwo = indexTwo;
       cc.executionOrder = executionOrder;
       cc.operator = operator;
       this.criteriaCombiners.push(cc);
    }

    setNewOrder(orderBy: string, orderType:string){
        this.orderBys = [];
        this.orderTypes = [];
        this.orderTypes.push(orderType);
        this.orderBys.push(orderBy);
    }
}