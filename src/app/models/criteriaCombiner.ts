export class CriteriaCombiner{
    indexOne:number;
    indexTwo:number;
    operator:string;
    executionOrder:number;
}