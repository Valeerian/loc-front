import { RoleModel } from './RoleModel';

export class JwtResponse{
    jwtToken: string;
    userName: string;
    mail: string;
    sex: Boolean;
    firstName: string;
    lastName: string;
    roles: string[];
}