import { GenericListModel } from './genericListModel';
import { PictureModel } from '../dataModels/pictureModel';

export class PictureListModel extends GenericListModel{
    list: PictureModel[];
    constructor(){
        super();
        this.list = [];
    }
}