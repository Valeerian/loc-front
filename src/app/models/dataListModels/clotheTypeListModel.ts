import { ClotheTypeModel } from '../dataModels/clotheTypeModel';
import { GenericListModel } from './genericListModel';

export class ClotheTypeListModel extends GenericListModel{
    list: ClotheTypeModel[];

    constructor(){
        super();
        this.list=[];
    }
}