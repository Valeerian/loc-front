import { GenericListModel } from './genericListModel';
import { ClotheNoteModel } from '../dataModels/clotheNoteModel';

export class ClotheNoteListModel extends GenericListModel{
    list: ClotheNoteModel[];
    constructor(){
        super();
        this.list = [];
    }
}