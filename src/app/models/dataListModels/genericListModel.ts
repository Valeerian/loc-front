import { BrandModel } from '../dataModels/brandModel';
import { SizeModel } from '../dataModels/sizeModel';
import { ColorModel } from '../dataModels/colorModel';
import { ClotheTypeModel } from '../dataModels/clotheTypeModel';
import { MaterialModel } from '../dataModels/materialModel';
import { ProviderModel } from '../dataModels/providerModel';
import { SeasonModel } from '../dataModels/seasonModel';
import { ThemeModel } from '../dataModels/themeModel';
import { StyleModel } from '../dataModels/styleModel';
import { SexModel } from '../dataModels/sexModel';
import { ManeStatusModel } from '../dataModels/maneStatusModel';
import { MaintenanceInstructionModel } from '../dataModels/maintenanceInstruction';
import { PictureModel } from '../dataModels/pictureModel';
import { ClotheModel } from '../dataModels/clotheModel';
import { PossessionModel } from '../dataModels/possessionModel';
import { ClotheNoteModel } from '../dataModels/clotheNoteModel';
import { PossessionStatusModel } from '../dataModels/possessionStatusModel';
import { FriendshipModel } from '../dataModels/friendshipModel';
import { UserModel } from '../dataModels/userModel';
import { LaundryManeModel } from '../dataModels/laundryManeModel';
import { LaundryGroupModel } from '../dataModels/laundryGroupModel';
import { OutfitModel } from '../dataModels/outfitModel';
import { MimicModel } from '../dataModels/mimicModel';
import { OutfitNoteModel } from '../dataModels/outfitNoteModel';

export abstract class GenericListModel{
    numberOfItems: number;
    numberOfPages: number;
    pageNumber: number;
    list: BrandModel[] | OutfitNoteModel[] | FriendshipModel[] | MimicModel[] |OutfitModel[] | LaundryManeModel[] | LaundryGroupModel[] | UserModel[] |ClotheNoteModel[] |SizeModel[] | ColorModel[] | ClotheTypeModel[] | PossessionStatusModel[] | MaterialModel[] | ProviderModel[] | SeasonModel[] | ThemeModel[] | StyleModel[] | SexModel[] | ManeStatusModel[] | MaintenanceInstructionModel[] | PictureModel[] | ClotheModel[] | MaintenanceInstructionModel[] | PossessionModel[];
    
    constructor(){}
}