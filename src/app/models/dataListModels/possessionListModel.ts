import { GenericListModel } from './genericListModel';
import { PossessionModel } from '../dataModels/possessionModel';

export class PossessionListModel extends GenericListModel{
    list: PossessionModel[];
    constructor(){
        super();
        this.list = [];
    }
}