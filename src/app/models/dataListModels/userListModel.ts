import { GenericListModel } from './genericListModel';
import { UserModel } from '../dataModels/userModel';

export class UserListModel extends GenericListModel{
    list: UserModel[];
    constructor(){
        super();
        this.list = [];
    }
}