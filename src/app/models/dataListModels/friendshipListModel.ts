import { GenericListModel } from './genericListModel';
import { FriendshipModel } from '../dataModels/friendshipModel';

export class FriendshipListModel extends GenericListModel{
    list: FriendshipModel[];

    constructor(){
        super();
        this.list = [];
    }
}