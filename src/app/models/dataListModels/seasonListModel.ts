import { GenericListModel } from './genericListModel';
import { SeasonModel } from '../dataModels/seasonModel';

export class SeasonListModel extends GenericListModel{
    list: SeasonModel[];
    constructor(){
        super();
        this.list = [];
    }
}