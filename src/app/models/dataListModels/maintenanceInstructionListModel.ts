import { GenericListModel } from './genericListModel';
import { MaintenanceInstructionModel } from '../dataModels/maintenanceInstruction';

export class MaintenanceInstructionListModel extends GenericListModel{
    list: MaintenanceInstructionModel[];
    constructor(){
        super();
        this.list = [];
    }
}