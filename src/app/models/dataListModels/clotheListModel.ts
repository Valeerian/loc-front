import { GenericListModel } from './genericListModel';
import { ClotheModel } from '../dataModels/clotheModel';

export class ClotheListModel extends GenericListModel{
    list: ClotheModel[];
    constructor(){
        super();
        this.list=[];
    }
}