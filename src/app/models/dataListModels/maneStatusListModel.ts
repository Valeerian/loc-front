import { GenericListModel } from './genericListModel';
import { ManeStatusModel } from '../dataModels/maneStatusModel';

export class ManeStatusListModel extends GenericListModel{
    list: ManeStatusModel[];
    constructor(){
        super();
        this.list = [];
    }
}