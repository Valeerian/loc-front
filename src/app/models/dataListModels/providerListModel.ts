import { GenericListModel } from './genericListModel';
import { ProviderModel } from '../dataModels/providerModel';

export class ProviderListModel extends GenericListModel{
    list: ProviderModel[];

    constructor(){
        super();
        this.list = [];
    }
}