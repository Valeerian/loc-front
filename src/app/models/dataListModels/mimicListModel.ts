import { GenericListModel } from './genericListModel';
import { MimicModel } from '../dataModels/mimicModel';

export class MimicListModel extends GenericListModel{
    list: MimicModel[];
    constructor(){
        super();
        this.list = [];
    }
}