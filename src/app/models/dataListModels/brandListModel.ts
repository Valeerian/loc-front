import { GenericListModel } from './genericListModel';
import { BrandModel } from '../dataModels/brandModel';

export class BrandListModel extends GenericListModel{
    
    list: BrandModel[];
    constructor(){
        super();
        this.list = [];
    } 
}