import { OrderModel } from '../dataModels/orderModel';

export class OrderListModel{
    list: OrderModel[];

    constructor(){
        this.list = [];
    }
}