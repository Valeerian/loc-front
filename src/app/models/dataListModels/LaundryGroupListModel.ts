import { GenericListModel } from './genericListModel';
import { LaundryGroupModel } from '../dataModels/laundryGroupModel';

export class LaundryGroupListModel extends GenericListModel{
    list : LaundryGroupModel[];
    constructor(){
        super();
        this.list = [];
    }
}