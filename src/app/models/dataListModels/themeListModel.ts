import { GenericListModel } from './genericListModel';
import { ThemeModel } from '../dataModels/themeModel';

export class ThemeListModel extends GenericListModel{
    list: ThemeModel[];
    constructor(){
        super();
        this.list = [];
    }
}