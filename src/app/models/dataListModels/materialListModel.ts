import { GenericListModel } from './genericListModel';
import { MaterialModel } from '../dataModels/materialModel';

export class MaterialListModel extends GenericListModel{
    list: MaterialModel[];
    constructor(){
        super();
        this.list = [];
    }
}