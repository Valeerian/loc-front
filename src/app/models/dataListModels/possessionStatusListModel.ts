import { GenericListModel } from './genericListModel';
import { PossessionStatusModel } from '../dataModels/possessionStatusModel';

export class PossessionStatusListModel extends GenericListModel{
    list: PossessionStatusModel[];

    constructor(){
        super();
        this.list = [];
    }
}