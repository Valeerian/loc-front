import { GenericListModel } from './genericListModel';
import { StyleModel } from '../dataModels/styleModel';

export class StyleListModel extends GenericListModel{
    list: StyleModel[];
    constructor(){
        super();
        this.list = [];
    }
}