import { SizeModel } from '../dataModels/sizeModel';
import { GenericListModel } from './genericListModel';

export class SizeListModel extends GenericListModel{
    list : SizeModel[];

    constructor(){
        super();
        this.list=[];
    }
    
}