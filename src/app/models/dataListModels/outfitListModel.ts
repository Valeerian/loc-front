import { GenericListModel } from './genericListModel';
import { OutfitModel } from '../dataModels/outfitModel';

export class OutfitListModel extends GenericListModel{
    list: OutfitModel[];
    constructor(){
        super();
        this.list = [];
    }
}