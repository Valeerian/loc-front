import { ColorModel } from '../dataModels/colorModel';
import { GenericListModel } from './genericListModel';

export class ColorListModel extends GenericListModel{

    list : ColorModel[];

    constructor(){
        super();
        this.list = [];
    }
}