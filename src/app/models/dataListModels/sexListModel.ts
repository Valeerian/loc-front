import { GenericListModel } from './genericListModel';
import { SexModel } from '../dataModels/sexModel';

export class SexListModel extends GenericListModel{
    list: SexModel[];
    constructor(){
        super();
        this.list = [];
    }
}