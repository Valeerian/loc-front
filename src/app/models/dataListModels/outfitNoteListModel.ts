import { GenericListModel } from './genericListModel';
import { OutfitNoteModel } from '../dataModels/outfitNoteModel';

export class OutfitNoteListModel extends GenericListModel{
    list: OutfitNoteModel[];
    constructor(){
        super();
        this.list = [];
    }
}