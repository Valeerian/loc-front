import { GenericListModel } from './genericListModel';
import { LaundryManeModel } from '../dataModels/laundryManeModel';

export class LaundryManeListModel extends GenericListModel{
    list: LaundryManeModel[];
    constructor(){
        super();
        this.list = [];
    }
}