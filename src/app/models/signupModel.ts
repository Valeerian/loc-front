export class SignupModel{
    userName: string;
    password: string;
    mail: string;
    firstName: string;
    lastName: string;
    passwordConfirmation: string;
    mailConfirmation: string;
    agreedDataPolicy: boolean;

    constructor(username:string, password: string, email: string, firstName: string, lastName: string, passwordConfirmation: string, mailConfirmation: string, agreedDataPolicy: boolean){
        this.userName = username;
        this.password = password;
        this.mail = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.passwordConfirmation = passwordConfirmation;
        this.mailConfirmation = mailConfirmation;
        this.agreedDataPolicy = agreedDataPolicy;
    }

}