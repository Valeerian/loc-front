import { OutfitModel } from './outfitModel';

export class OutfitNoteModel{
    outfitNoteId: number;
    poste: string;
    comment: string;
    note: number;
    creationDate: Date;
    outfitModel: OutfitModel;
}