export class PictureModel{
    pictureId: number;
    name: string;
    url: string;
}