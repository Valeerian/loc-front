import { ClotheModel } from './clotheModel';

export class ClotheNoteModel{
    clotheNoteId: number;
    poster: string;
    comment: string;
    creationDate: Date;
    note: number;
    clotheModel: ClotheModel;
}