export class ClotheTypeModel {
    clotheTypeId: number;
    clotheType: string;
    category: string;
}