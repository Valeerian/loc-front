export class MimicModel{
    name: string;
    mimicId: number;
    value: string; 

    constructor(name: string, id: number, value: string){
        this.name=name;
        this.mimicId=id;
        this.value=value;
    }
}