import { UserModel } from './userModel';

export class FriendshipModel{
    friendshipId: number;
    userOne: UserModel;
    userTwo: UserModel;
    friendshipStatus: boolean;
}