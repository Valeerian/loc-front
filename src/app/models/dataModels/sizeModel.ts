export class SizeModel{
    id: number;
    category: string;
    size: string;
}