import { ProviderModel } from './providerModel';
import { SexModel } from './sexModel';
import { BrandModel } from './brandModel';
import { SeasonModel } from './seasonModel';
import { ColorModel } from './colorModel';
import { MaintenanceInstructionListModel } from '../dataListModels/maintenanceInstructionListModel';
import { MaterialModel } from './materialModel';
import { PictureModel } from './pictureModel';
import { SizeModel } from './sizeModel';
import { StyleModel } from './styleModel';
import { ThemeModel } from './themeModel';
import { ClotheTypeModel } from './clotheTypeModel';
import { MaintenanceInstructionModel } from './maintenanceInstruction';

export class ClotheModel{
    clotheId: number;
    providerModel: ProviderModel;
    url: string;
    name: string;
    sexModel: SexModel;
    brandModel: BrandModel;
    seasonModel: SeasonModel;
    colorModels: ColorModel[];
    maintenanceInstructionModels: MaintenanceInstructionModel[];
    materialModels: MaterialModel[];
    pictureModels: PictureModel[];
    sizeModels: SizeModel[];
    styleModels: StyleModel[];
    themeModels: ThemeModel[];
    clotheTypeModels: ClotheTypeModel[];

    constructor(){
        this.colorModels = [];
        this.maintenanceInstructionModels = [];
        this.materialModels = [];
        this.pictureModels = [];
        this.sizeModels = [];
        this.styleModels = [];
        this.themeModels = [];
        this.clotheTypeModels = [];
        this.providerModel = new ProviderModel();
        this.sexModel = new SexModel();
        this.seasonModel = new SeasonModel();
        this.brandModel = new BrandModel();
    }
}