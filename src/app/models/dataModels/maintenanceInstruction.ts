export class MaintenanceInstructionModel{
    maintenanceInstructionId: number;
    name: string;
    instruction: string;
}