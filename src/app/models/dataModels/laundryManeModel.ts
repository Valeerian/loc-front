import { PossessionModel } from './possessionModel';
import { ManeStatusModel } from './maneStatusModel';

export class LaundryManeModel{
    laundryManeId: number;
    description: string;
    name: string;
    startDate: Date;
    possessionModels: PossessionModel[];
    maneStatusModel: ManeStatusModel;

    constructor(){
        this.possessionModels = [];
    }
}