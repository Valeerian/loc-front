import { UserModel } from './userModel';
import { LaundryManeModel } from './laundryManeModel';

export class LaundryGroupModel{
    laundryGroupId: number;
    laundryGroupName: string;
    laundryGroupOwner: UserModel;
    laundryGroupMembers: UserModel[];
    laundryManes: LaundryManeModel[];
    constructor(){
        this.laundryGroupMembers = [];
        this.laundryManes = [];
    }
}