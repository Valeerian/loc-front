import { UserModel } from './userModel';
import { PossessionModel } from './possessionModel';

export class OutfitModel{
    outfitId: number;
    outfitCreator: UserModel;
    possessionModels: PossessionModel[];
    dueDate: Date;
    creationDate: Date;
    name: string;
    favorite: boolean;
    description: string;
    publik: boolean;
}