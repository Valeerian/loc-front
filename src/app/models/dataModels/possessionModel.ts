import { ClotheModel } from './clotheModel';
import { SizeModel } from './sizeModel';
import { PossessionStatusModel } from './possessionStatusModel';

export class PossessionModel{
    possessionId: number;
    clotheModel: ClotheModel;
    owner: string;
    user: string;
    sizeModel: SizeModel;
    statuses: PossessionStatusModel[];

    constructor(){
        this.clotheModel = new ClotheModel();
        this.sizeModel = new SizeModel();
        this.statuses = [];
    }
}