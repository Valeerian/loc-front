export class UserModel{
     userName: string;
     mail: string; 
     firstName: string;
     lastName: string;
     mailConfirmation:string;
     password: string;
     passwordConfirmation: string;
     sex: Boolean;
     agreedDataPolicy: boolean;
     oldPassword: string;
     newPassword: string;
     newPasswordConfirmation: string;
     passwordRecoveryString: string;
     chestSize: number;
     bottomSize: string;
     footSize: string;
     sizeUnit: string;
     metric: string;
     explicitSex: string;
     userId: number;

     constructor(){}
     
}