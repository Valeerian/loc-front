import { OrderModel } from '../models/dataModels/orderModel';

export const ClothesOrders: {[key:string]:OrderModel} = {

    idAsc:{displayName:"identifiant (asc)", orderBy:"clotheId", orderType:"ASC"},
    idDesc:{displayName:"identifiant (desc)", orderBy:"clotheId", orderType:"DESC"},
    providerAsc:{displayName:"fournisseur (asc)", orderBy:"provider", orderType:"ASC"},
    providerDesc:{displayName:"fournisseur (desc)", orderBy:"provider", orderType:"DESC"},
    nameAsc:{displayName:"nom (asc)", orderBy:"name", orderType:"ASC"},
    nameDesc:{displayName:"nom (desc)", orderBy:"name", orderType:"DESC"},
    sexAsc:{displayName:"sexe (asc)", orderBy:"sex", orderType:"ASC"},
    sexDesc:{displayName:"sexe (desc)", orderBy:"sex", orderType:"DESC"},
    seasonAsc:{displayName:"saison (asc)", orderBy:"season", orderType:"ASC"},
    seasonDesc:{displayName:"saison (desc)", orderBy:"season", orderType:"DESC"},
    brandAsc:{displayName:"marque (asc)", orderBy:"brand", orderType:"ASC"},
    brandDesc:{displayName:"marque (desc)", orderBy:"brand", orderType:"DESC"},

}
