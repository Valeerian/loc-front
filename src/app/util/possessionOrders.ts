import { OrderModel } from '../models/dataModels/orderModel';

export const PossessionOrders: {[key:string]:OrderModel} = {

    idAsc:{displayName:"identifiant (asc)", orderBy:"possessionId", orderType:"ASC"},
    idDesc:{displayName:"identifiant (desc)", orderBy:"possessionId", orderType:"DESC"},
    ownerAsc:{displayName:"propriétaire (asc)", orderBy:"owner", orderType:"ASC"},
    ownerDesc:{displayName:"propriétaire (desc)", orderBy:"owner", orderType:"DESC"},
    sizeAsc:{displayName:"taille (asc)", orderBy:"size", orderType:"ASC"},
    sizeDesc:{displayName:"taille (desc)", orderBy:"size", orderType:"DESC"},
    clotheAsc:{displayName:"vêtement (asc)", orderBy:"clothe", orderType:"ASC"},
    clotheDesc:{displayName:"vêtement (desc)", orderBy:"clothe", orderType:"DESC"},
    userAsc:{displayName:"utilisateur (asc)", orderBy:"user", orderType:"ASC"},
    userDesc:{displayName:"utilisateur (desc)", orderBy:"user", orderType:"DESC"},

}
