import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'publikPipe'
})
export class PublikPipePipe implements PipeTransform {

  transform(value): string {
    return value ? "Publique" : "Privée";
  }

}
