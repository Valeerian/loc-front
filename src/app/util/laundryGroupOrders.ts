import { OrderModel } from '../models/dataModels/orderModel';

export const LaundryGroupOrders: {[key:string]:OrderModel} = {

    idAsc:{displayName:"identifiant (asc)", orderBy:"laundryGroupId", orderType:"ASC"},
    idDesc:{displayName:"identifiant (desc)", orderBy:"laundryGroupId", orderType:"DESC"},
    nameAsc:{displayName:"nom (asc)", orderBy:"groupName", orderType:"ASC"},
    nameDesc:{displayName:"nom (desc)", orderBy:"groupName", orderType:"DESC"},
    creatorAsc:{displayName:"créateur (asc)", orderBy:"creator", orderType:"ASC"},
    creatorDesc:{displayName:"créateur (desc)", orderBy:"creator", orderType:"DESC"},
    
}