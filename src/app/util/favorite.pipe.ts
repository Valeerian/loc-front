import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'favorite'
})
export class FavoritePipe implements PipeTransform {

  transform(value): string {
    return value ? "Favorite" : "Non favorite";
  }

}
