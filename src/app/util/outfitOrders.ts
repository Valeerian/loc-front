import { OrderModel } from '../models/dataModels/orderModel';

export const OutfitOrders: {[key:string]:OrderModel} = {

    idAsc:{displayName:"identifiant (asc)", orderBy:"outfitId", orderType:"ASC"},
    idDesc:{displayName:"identifiant (desc)", orderBy:"outfitId", orderType:"DESC"},
    nameAsc:{displayName:"nom (asc)", orderBy:"name", orderType:"ASC"},
    nameDesc:{displayName:"nom (desc)", orderBy:"name", orderType:"DESC"},
    creationDateAsc:{displayName:"date de création (asc)", orderBy:"creationDate", orderType:"ASC"},
    creationDateDesc:{displayName:"date de création (desc)", orderBy:"creationDate", orderType:"DESC"},
    dueDateAsc:{displayName:"date prévue (asc)", orderBy:"dueDate", orderType:"ASC"},
    dueDateDesc:{displayName:"date prévue (desc)", orderBy:"dueDate", orderType:"DESC"},
    isFavoriteAsc:{displayName:"favori (asc)", orderBy:"isFavorite", orderType:"ASC"},
    isFavoriteDesc:{displayName:"favori (desc)", orderBy:"isFavorite", orderType:"DESC"},
    isPublicAsc:{displayName:"public (asc)", orderBy:"isPublic", orderType:"ASC"},
    isPublicDesc:{displayName:"public (desc)", orderBy:"isPublic", orderType:"DESC"},
    userAsc:{displayName:"créateur (asc)", orderBy:"creator", orderType:"ASC"},
    userDesc:{displayName:"créateur (desc)", orderBy:"creator", orderType:"DESC"},
    
}