import { OrderModel } from '../models/dataModels/orderModel';

export const FriendshipOrders: {[key:string]:OrderModel} = {

    idAsc:{displayName:"identifiant (asc)", orderBy:"friendshipId", orderType:"ASC"},
    idDesc:{displayName:"identifiant (desc)", orderBy:"friendshipId", orderType:"DESC"},
    userOneAsc:{displayName:"Demandeur (asc)", orderBy:"userOne", orderType:"ASC"},
    userOneDesc:{displayName:"Demandeur (desc)", orderBy:"userOne", orderType:"DESC"},
    userTwoAsc:{displayName:"Receveur (asc)", orderBy:"userTwo", orderType:"ASC"},
    userTwoDesc:{displayName:"Receveur (desc)", orderBy:"userTwo", orderType:"DESC"},
}